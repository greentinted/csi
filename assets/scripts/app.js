(function ($) {
	'use strict';

    window.app = {
        name: 'skinmatch',
        version: '1.1.3',
        // for chart colors
        color: {
            'primary':      '#0cc2aa',
            'accent':       '#a88add',
            'warn':         '#fcc100',
            'info':         '#6887ff',
            'success':      '#6cc788',
            'warning':      '#f77a99',
            'danger':       '#f44455',
            'white':        '#ffffff',
            'light':        '#f1f2f3',
            'dark':         '#2e3e4e',
            'black':        '#2a2b3c'
        },
        setting: {
            theme: {
        		primary: 'primary',
    			accent: 'accent',
    			warn: 'warn'
            },
            color: {
                primary:      '#0cc2aa',
                accent:       '#a88add',
                warn:         '#fcc100'
        	},
            folded: false,
            boxed: false,
            container: false,
            themeID: 1,
            bg: '',
            navi:{

            },
        }
    };

    var setting = 'jqStorage-'+app.name+'-Setting_v3',
        storage = $.localStorage;

    if( storage.isEmpty(setting) ){
        storage.set(setting, app.setting);
    }else{
        app.setting = storage.get(setting);
    }
    var v = window.location.search.substring(1).split('&');
    for (var i = 0; i < v.length; i++)
    {
        var n = v[i].split('=');
        app.setting[n[0]] = (n[1] == "true" || n[1]== "false") ? (n[1] == "true") : n[1];
        storage.set(setting, app.setting);
    }
    // console.log(storage);


    // init
    function setTheme(){

        $('body').removeClass($('body').attr('ui-class')).addClass(app.setting.bg).attr('ui-class', app.setting.bg);
        app.setting.folded ? $('#aside').addClass('folded') : $('#aside').removeClass('folded');
        app.setting.boxed ? $('body').addClass('container') : $('body').removeClass('container');

        $('.switcher input[value="'+app.setting.themeID+'"]').prop('checked', true);
        $('.switcher input[value="'+app.setting.bg+'"]').prop('checked', true);

        $('[data-target="folded"] input').prop('checked', app.setting.folded);
        $('[data-target="boxed"] input').prop('checked', app.setting.boxed);
    }

    // click to switch
    $(document).on('click.setting', '.switcher input', function(e){
        var $this = $(this), $target;
        $target = $this.parent().attr('data-target') ? $this.parent().attr('data-target') : $this.parent().parent().attr('data-target');
        app.setting[$target] = $this.is(':checkbox') ? $this.prop('checked') : $(this).val();
        ($(this).attr('name')=='color') && (app.setting.theme = eval('[' +  $(this).parent().attr('data-value') +']')[0]) && setColor();
        storage.set(setting, app.setting);
        setTheme(app.setting);
    });

    function setColor(){
        app.setting.color = {
            primary: getColor( app.setting.theme.primary ),
            accent: getColor( app.setting.theme.accent ),
            warn: getColor( app.setting.theme.warn )
        };
    };

    function getColor(name){
        return app.color[ name ] ? app.color[ name ] : palette.find(name);
    };

    function init(){
        $('[ui-jp]').uiJp();
        $('body').uiInclude();
    }

    $(document).on('pjaxStart', function() {
        $('#aside').modal('hide');
        $('body').removeClass('modal-open').find('.modal-backdrop').remove();
        $('.navbar-toggleable-sm').collapse('hide');
    });

    init();
    setTheme();


    $(document).on('click', '.click_reload', function(event) {
        event.preventDefault();
        // console.log($(this).val());
        var _href = $(this).data('href');
        location.href = _href+$(this).val();
    });


    $(document).on('click', '.modal_open', function(event) {
        event.preventDefault();
        var target = $(this).data("href");

        //debug_var(target);
        if(!target){
            alert_show('target 없음!');
        }else{
            // var modal = $(this).data("modal");
            var modal = '#m-a-f';
            loading();

            $(modal+" .modal-content").load(target, function(response, status, xhr) {
                // console.log(response, status, xhr);
                loading_hide();
                if ( status == "error" ) {
                    alert_show(formatErrorMessage(xhr,response));
                }else{
                    $(modal).modal({keyboard:true,show:true});
                    setTimeout(function() {
                        $('[ui-jp]').uiJp();
                    }, 300);
                }
            });
        }
    });

    $(document).on('click', '.modal_open_sm', function(event) {
        event.preventDefault();
        var target = $(this).data("href");
        if(!target){
            alert_show('target 없음!');
        }else{
            // var modal = $(this).data("modal");
            var modal = '#m-a-f_sm';
            loading();


            $(modal+" .modal-content").load(target, function(response, status, xhr) {
                $('.modal-backdrop:last').css({'z-index':'1070'});
                // console.log(response, status, xhr);
                loading_hide();
                if ( status == "error" ) {
                    alert_show(formatErrorMessage(xhr,response));
                }else{
                    $(modal).modal({keyboard:true,show:true,backdrop:'static'});
                    setTimeout(function() {
                        $('[ui-jp]').uiJp();
                    }, 300);
                }
            });
        }
    });

    $("#m-a-f_sm").on("hidden.bs.modal", function () {
        // put your default event here
        $('.modal-backdrop:last').css({'z-index':'1040'});
        $('#m-a-f').css({'overflow-x':'hidden','overflow-y':'auto'});

    });


    $(document).on('click', '.ajax_send', function(event) {
        event.preventDefault();

        var confirm_msg = $(this).data('confirm');


        var ajax_send = function(_this){
            // alert(_this.data("href"));

            ajax_req_block(_this.data("href"),'POST',_this.data("params"),function(xhr){
                if(xhr.msg){
                    alert_show(xhr.msg,function(){
                        if(xhr.is_reload) location.reload();
                        else if(xhr.url) location.href=xhr.url;
                    });
                }else{
                    if(xhr.is_reload) location.reload();
                    else if(xhr.url) location.href=xhr.url;
                }
            });
        };
        var _this = $(this);

        if(confirm_msg){
            confirm_show(confirm_msg,function(){
                ajax_send(_this);
            });

        }else{
            ajax_send(_this);
        }
    });

    $(document).on('click', '.ajax_send_prompt', function(event) {
        event.preventDefault();

        var confirm_msg = $(this).data('confirm');
        var prompt_title = $(this).data('prompt_title');
        var prompt_value = $(this).data('prompt_value');
        var prompt_name = $(this).data('prompt_name');

        var arg = prompt(prompt_title,prompt_value);

        if (arg) {

            var ajax_send = function(_this){
                var params = _this.data("params");
                // params.push('prompt_name',arg);
                params[prompt_name] = arg;

                // ajax_req_block(_this.data("href")+'&'+prompt_name+'='+arg,'POST',{},function(xhr){
                ajax_req_block(_this.data("href"),'POST',params,function(xhr){
                    if(xhr.msg){
                        alert_show(xhr.msg,function(){
                            if(xhr.is_reload) location.reload();
                        });
                    }else{
                        if(xhr.is_reload) location.reload();
                    }
                });
            };
            if(confirm_msg){
                if(confirm(confirm_msg)){
                    ajax_send($(this));
                }
            }else{
                ajax_send($(this));
            }
        }
    });
    $(document).on('click', '.ajax_form', function(event) {
        event.preventDefault();

        var _target = $(this).data('target');
        var _this = $('#'+_target);

        ajax_req_block(_this.data("href"),'POST',_this.serialize(),function(xhr){
            if(xhr.msg){
                alert_show(xhr.msg,function(){
                    if(xhr.is_reload) location.reload();
                });
            }else{
                if(xhr.is_reload) location.reload();
            }
        });
    });

    $(document).on('submit', '#modal_form', function(event) {
        event.preventDefault();
        ajax_req_block($(this).attr("href"),'POST',$(this).serialize(),function(xhr){
            if(xhr.msg){
                alert_show(xhr.msg,function(){
                    if(xhr.is_reload) location.reload();
                });
            }else{
                if(xhr.is_reload) location.reload();
            }
        });
    });

    $(document).on('submit', '#modal_form_file', function(event) {
        if (event.isDefaultPrevented()) {
            // handle the invalid form...
        } else {
            event.preventDefault();
            ajax_request_file($(this).attr("href"),new FormData(this),function(xhr){
                if(xhr.msg){
                    alert_show(xhr.msg,function(){
                        if(xhr.is_reload) location.reload();
                        else if(xhr.url) location.href=xhr.url;
                    });
                }else{
                    if(xhr.is_reload) location.reload();
                    else if(xhr.url) location.href=xhr.url;
                }
            });
        }
    });
    $(document).on('click', '.ajax_send_callback', function(event) {
        event.preventDefault();

        var confirm_msg = $(this).data('confirm');
        var callback = $(this).data('callback');


        var ajax_send = function(_this){
            // alert(_this.data("href"));

            ajax_req_block(_this.data("href"),'POST',_this.data("params"),function(xhr){
                if(xhr.msg){
                    alert_show(xhr.msg,function(){
                        if(xhr.is_reload) {
                            location.reload();
                        }else if(callback){
                            eval(callback);
                        }
                    });
                }else{
                    if(xhr.is_reload) {
                        location.reload();
                    }else if(callback){
                        eval(callback);
                    }
                }
            });
        };
        var _this = $(this);

        if(confirm_msg){
            confirm_show(confirm_msg,function(){
                ajax_send(_this);
            });

        }else{
            ajax_send(_this);
        }
    });

})(jQuery);

function lan_chg(){
    var lan = $("#lan_pack").val();

    ajax_req_block('/prod/lan','POST',{'lan':lan},function(xhr){
        if(xhr.stats){
            location.reload();
        }
    });
}

function alert_show(msg,callback){
    $.alert({
        title: '',
        content:msg,
        buttons: {
            'OK': {
                action: function(){
                    if(typeof callback === 'function') {
                        callback();
                    }
                },
                keys: ['enter','esc']
            }
        }
    });
}

function confirm_show(msg,success_callback,cancel_callback) {
    $.confirm({
        title: '',
        content: msg,
        buttons: {
            'OK':{
                action: function(){
                    if(typeof success_callback === 'function') {
                        success_callback();
                    }
                },
                keys: ['enter']
            },
            'CANCEL': {
                action: function(){
                    if(typeof cancel_callback === 'function') {
                        cancel_callback();
                    }
                },
                keys: ['esc']
            },
        }
    });
}

// function prompt_show(msg,success_callback,cancel_callback) {

//     $.confirm({
//         title: 'Prompt!',
//         content: '' +
//         '<form action="" class="formName">' +
//         '<div class="form-group">' +
//         '<label>Enter something here</label>' +
//         '<input type="text" placeholder="Your name" class="name form-control" required />' +
//         '</div>' +
//         '</form>',
//         buttons: {
//             formSubmit: {
//                 text: 'Submit',
//                 btnClass: 'btn-blue',
//                 action: function () {
//                     var name = this.$content.find('.name').val();
//                     if(!name){
//                         $.alert('provide a valid name');
//                         return false;
//                     }
//                     $.alert('Your name is ' + name);
//                 }
//             },
//             cancel: function () {
//                 //close
//             },
//         },
//         onContentReady: function () {
//             // bind to events
//             var jc = this;
//             this.$content.find('form').on('submit', function (e) {
//                 // if the user submits the form by pressing enter in the field.
//                 e.preventDefault();
//                 jc.$$formSubmit.trigger('click'); // reference the button and click it
//             });
//         }
//     });

// }



function loading () {
    var html = '<div id="blockUI_spinner" data-spinner=\'{"radius": 100}\'></div>';
    $.blockUI({ message: html, overlayCSS: { backgroundColor: '#fff' }, css: { border: '0px', backgroundColor: ''} ,baseZ: 9999});
    $('#blockUI_spinner').spinner();
}

function loading_hide () {
    $.unblockUI();
}


function formatErrorMessage(jqXHR, exception) {
    if (jqXHR.status === 0) {
        return ('Not connected.\nPlease verify your network connection.');
    } else if (jqXHR.status == 404) {
        return ('The requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        return ('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        return ('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        return ('Time out error.');
    } else if (exception === 'abort') {
        return ('Ajax request aborted.');
    } else {
        return ('Uncaught Error.\n' + jqXHR.responseText);
    }
}

function ajax_req(_url,_type,_data,callback) {
    ajax_request(false,_url,_type,_data,callback);
}
function ajax_req_block(_url,_type,_data,callback) {
    ajax_request(true,_url,_type,_data,callback);
}
function ajax_request(is_block,_url,_type,_data,callback) {
    if(is_block) loading();
    $.ajax({url: _url,type:_type ,dataType: 'json', data:_data
    })
    .done(function(xhr) {
        if(xhr.status) {
            if(typeof callback === 'function') {
                callback(xhr);
            }
        }else {
            alert_show(xhr.msg);
        }
    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        alert_show(formatErrorMessage(jqXHR,textStatus));
    })
    .always(function() {
        if(is_block) loading_hide();
    });
}

function ajax_request_file(_url,_data,callback) {
    loading();
    $.ajax({
        url: _url,
        type: 'POST',
        dataType: 'json',
        data: _data,//new FormData(this),
        processData: false,
        contentType: false,
        error: function(xhr, status, error) {
            // var err = eval("(" + xhr.responseText + ")");
            console.log(xhr);
        },
    })
    .done(function(xhr) {
        if(xhr.status) {
            if(typeof callback === 'function') {
                callback(xhr);
            }
        }else {
            alert_show(xhr.msg);
        }

    })
    .fail(function(jqXHR, textStatus, errorThrown) {
        alert_show(formatErrorMessage(jqXHR,textStatus));
    })
    .always(function() {
        loading_hide();
    });
}

function parseURL(url) {
    var parser = document.createElement('a'),
        searchObject = {},
        queries, split, i;
    // Let the browser do the work
    parser.href = url;
    // Convert query string to object
    queries = parser.search.replace(/^\?/, '').split('&');
    for( i = 0; i < queries.length; i++ ) {
        split = queries[i].split('=');
        searchObject[split[0]] = split[1];
    }
    return {
        protocol: parser.protocol,
        host: parser.host,
        hostname: parser.hostname,
        port: parser.port,
        pathname: parser.pathname,
        search: parser.search,
        searchObject: searchObject,
        hash: parser.hash
    };
}
