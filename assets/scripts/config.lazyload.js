// lazyload config
var MODULE_CONFIG = {
    easyPieChart:   [ '/public/admin/libs/jquery/jquery.easy-pie-chart/dist/jquery.easypiechart.fill.js' ],
    sparkline:      [ '/public/admin/libs/jquery/jquery.sparkline/dist/jquery.sparkline.retina.js' ],
    plot:           [ '/public/admin/libs/jquery/flot/jquery.flot.js',
                      '/public/admin/libs/jquery/flot/jquery.flot.resize.js',
                      '/public/admin/libs/jquery/flot/jquery.flot.pie.js',
                      '/public/admin/libs/jquery/flot/jquery.flot.time.js',
                      '/public/admin/libs/jquery/flot.tooltip/js/jquery.flot.tooltip.min.js',
                      '/public/admin/libs/jquery/flot-spline/js/jquery.flot.spline.min.js',
                      '/public/admin/libs/jquery/flot.orderbars/js/jquery.flot.orderBars.js'],
    vectorMap:      [ '/public/admin/libs/jquery/bower-jvectormap/jquery-jvectormap-1.2.2.min.js',
                      '/public/admin/libs/jquery/bower-jvectormap/jquery-jvectormap.css',
                      '/public/admin/libs/jquery/bower-jvectormap/jquery-jvectormap-world-mill-en.js',
                      '/public/admin/libs/jquery/bower-jvectormap/jquery-jvectormap-us-aea-en.js' ],
    dataTable:      [
                      '/public/admin/libs/jquery/datatables/media/js/jquery.dataTables.min.js',
                      '/public/admin/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.js',
                      '/public/admin/libs/jquery/plugins/integration/bootstrap/3/dataTables.bootstrap.css'],
    footable:       [
                      '/public/admin/libs/jquery/footable/dist/footable.all.min.js',
                      '/public/admin/libs/jquery/footable/css/footable.core.css'
                    ],
    screenfull:     [
                      '/public/admin/libs/jquery/screenfull/dist/screenfull.min.js'
                    ],
    sortable:       [
                      '/public/admin/libs/jquery/html.sortable/dist/html.sortable.min.js'
                    ],
    nestable:       [
                      '/public/admin/libs/jquery/nestable/jquery.nestable.css',
                      '/public/admin/libs/jquery/nestable/jquery.nestable.js'
                    ],
    summernote:     [
                      '/public/admin/libs/jquery/summernote/dist/summernote.css',
                      '/public/admin/libs/jquery/summernote/dist/summernote.js'
                    ],
    parsley:        [
                      '/public/admin/libs/jquery/parsleyjs/dist/parsley.css',
                      '/public/admin/libs/jquery/parsleyjs/dist/parsley.min.js'
                    ],
    select2:        [
                      '/public/admin/libs/jquery/select2/dist/css/select2.min.css',
                      '/public/admin/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
                      '/public/admin/libs/jquery/select2-bootstrap-theme/dist/select2-bootstrap.4.css',
                      '/public/admin/libs/jquery/select2/dist/js/select2.min.js'
                    ],
    datetimepicker: [
                      '/public/admin/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css',
                      '/public/admin/libs/jquery/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.dark.css',
                      '/public/admin/libs/js/moment/moment.js',
                      '/public/admin/libs/js/moment/locale/ko.js',
                      '/public/admin/libs/jquery/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js'
                    ],
    chart:          [
                      '/public/admin/libs/js/echarts/build/dist/echarts-all.js',
                      '/public/admin/libs/js/echarts/build/dist/theme.js',
                      '/public/admin/libs/js/echarts/build/dist/jquery.echarts.js'
                    ],
    bootstrapWizard:[
                      '/public/admin/libs/jquery/twitter-bootstrap-wizard/jquery.bootstrap.wizard.min.js'
                    ],
    fullCalendar:   [
                      '/public/admin/libs/jquery/moment/moment.js',
                      '/public/admin/libs/jquery/fullcalendar/dist/fullcalendar.min.js',
                      '/public/admin/libs/jquery/fullcalendar/dist/fullcalendar.css',
                      '/public/admin/libs/jquery/fullcalendar/dist/fullcalendar.theme.css',
                      '/public/admin/scripts/plugins/calendar.js'
                    ],
    dropzone:       [
                      '/public/admin/libs/js/dropzone/dist/min/dropzone.min.js',
                      '/public/admin/libs/js/dropzone/dist/min/dropzone.min.css'
                    ]
  };
