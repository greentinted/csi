var mql = window.matchMedia('screen and (max-width: 1620px)');
var mql2 = window.matchMedia('screen and (max-width: 650px)');

UI = {
	load: function(){
		$(window).resize(function(){
			UI.screenPart();
		});

		UI.screenPart();
		UI.surgeryBack();
		UI.quickActive();
		UI.gnbAction();
		UI.mainDocter();
		// UI.mainScrollActive();
		UI.branchActive();
		UI.setTabMobile();
		UI.active_tab4_wrap();
		// UI.docterTabActive();
	},

	SV : { 
		//Static Value
		// _screenFlag : 1 (2000px), 2 (1000px), 3 (mobile)
		_screenFlag :1,
		_gnbFlag : true
	},

	screenPart : function (){
		if (mql.matches && !mql2.matches){
			UI.SV._screenFlag =	2;
		}else if (mql.matches && mql2.matches){
			UI.SV._screenFlag =	3;
		}else {
			UI.SV._screenFlag =	1;
		}
	},

	gnbAction: function(){	
		var header_obj = $('#header .inner_header');
		var gnb_wrap_obj = $('.gnb_wrap');
		var gnb_obj = $('#gnb');
		var gnb_child_obj = gnb_obj.children();
		var depth1 = gnb_obj.find('.btn_depth1');
		var all_men_opener = $('#header .all_menu_opener');
		var all_men_closer = $('#header .back, #header .mobile_gnb_close');
		var fixed_flag = null;
		var re_position = null;		
		
		var gnbResize = function (){
			
		};		

		gnb_child_obj.bind('mouseover focusin',function(){
			if (UI.SV._screenFlag == 3 ){return;}
			gnb_child_obj.removeClass('on');
			$(this).addClass('on');
			
		});
		gnb_child_obj.bind('mouseleave focusout',function(){
			if (UI.SV._screenFlag == 3 ){return;}
			$(this).removeClass('on');
		});

		depth1.bind('click',function(){
			if (UI.SV._screenFlag != 3 ){return;}

			var this_parent = $(this).parent();
			if (this_parent.hasClass('on')){
				this_parent.removeClass('on');
			}else {
				gnb_child_obj.removeClass('on');
				this_parent.addClass('on');
			}
		});

		all_men_opener.bind('click',function(){
			if (UI.SV._screenFlag != 3 ){return;}

			gnb_child_obj.removeClass('on');
			$('#header').addClass('open_allmenu');
			$('#header').find('.back').css({'display':'block','opacity':0});
			$('#header').find('.back').stop().animate({'opacity':0.5});
			UI.SV._gnbFlag = false;
		});

		all_men_closer.bind('click',function(){
			if (UI.SV._screenFlag != 3 ){return;}			
			$('#header').removeClass('open_allmenu');
			$('#header').find('.back').stop().animate({'opacity':0},function (){
				$(this).css({'display':'none'});
			});
			UI.SV._gnbFlag = false;
		});

		gnbResize();

		$(window).resize(function(){
			gnbResize();
		});
	},

	docterTabActive : function (){
		var obj_tab = $('#docter_intro_tab').children();
			obj_tit = $('#docter_intro_tit > a');
			obj_conts = $('#docter_intro_conts .di_group');
			obj_hash = document.location.hash;

		obj_tab.each(function(i){
			this.num = i

			if(obj_hash == obj_tab.eq(this.num).children().attr('href')){
	            obj_conts.removeClass('on');
	            obj_conts.eq(this.num).addClass('on');

	            obj_tab.removeClass('on');
	            obj_tab.eq(this.num).addClass('on');

	            obj_tit.text( $(this).find('>a').text() );
	        }
		});

		obj_tab.bind('click',function(){
			obj_conts.removeClass('on');
			obj_conts.eq(this.num).addClass('on');

			obj_tab.removeClass('on');
			obj_tab.eq(this.num).addClass('on');

			obj_tit.text( $(this).find('>a').text() );
		});
	},

	quickActive : function (amount){
		var obj = $('#quick');
		var opener = obj.find('.opener');

		if($('#main_contents').length == 0){
			var flag = true; //서브에서 열림
			opener.addClass('open');
			obj.stop().animate({'right':0});
		}else{
			var flag = false; //메인에서 닫힘
			opener.removeClass('open');
			obj.stop().animate({'right':-104});
		}
		opener.bind('click',function(){
			if (!flag){
				opener.addClass('open');
				obj.stop().animate({'right':0});

				flag = true;
			}else {
				opener.removeClass('open');
				obj.stop().animate({'right':-104});

				flag = false;
			}
		});		
	},

	setTabMobile : function (){
		var obj = $('.sep_tab');
		var obj_tabs = obj.find('> ul > li');
		var opener = obj.find('.tit');
		var flag = true;
		opener.bind('click',function(){
			if (flag){
				obj.addClass('m_on');
				flag = false;
			}else {
				obj.removeClass('m_on');
				flag = true;
			}
		});

		obj_tabs.bind('click',function(){
			obj.removeClass('m_on');
			flag = true;
		});
	},

	active_tab4_wrap : function (){
		var obj = $('.active_tab4_wrap');
		var obj_tabs = obj.find('> ul > li');
		var opener = obj.find('.tit');
		var flag = true;
		opener.bind('click',function(){
			if (flag){
				obj.addClass('m_on');
				flag = false;
			}else {
				obj.removeClass('m_on');
				flag = true;
			}
		});

		obj_tabs.bind('click',function(){
			obj.removeClass('m_on');
			flag = true;
		});
	},

	surgeryBack : function (){
		var obj = $('#header .surgery_counter .bg_box').children();
		
		function getRandomInt(min, max) {
		  return Math.floor(Math.random() * (max - min)) + min;
		}
		obj.eq(  getRandomInt(0,obj.length) ).addClass('on');	
	},

	mainScrollActive : function (){
		if ( $('#main_contents').length <= 0 ){ return;	}
		
		var main_visual_slider = $('#main_contents .main_section1 .slider');
		main_visual_slider.cycle({
			fx : "scrollHorz",
			easing: "easeOutQuart",
			speed: 1000,
			timeout: 7000,
			swipe: true,
			autoHeight : 'container',
			pager : "#main_contents .main_section1 .slide_page",
			slides : "> a"
		});

		
		var fn_scroll = function (){
			var st = $(window).scrollTop();
			var st_top = $('.main_follow_menu').offset().top;
			
			if (st >= st_top){
				$('#main_follow_menu').addClass('mf_fixed');
			}else {
				$('#main_follow_menu').removeClass('mf_fixed');
			}
		};

		var fn_move_section = function (){
			var btns = $('#main_follow_menu').children();
			var btns2 = $('#main_contents .next_part_movement');
			var section = $('#main_contents .move_class');
			var minus = 58;

			btns.each(function(i){this.num = i - 1});
			btns2.each(function(i){this.num = i});
			btns.bind('click',function(){
				if(this.num == -1){return;}
				if (UI.SV._screenFlag == 3){ minus = 0; }else { minus = 58; }
				$('html,body').stop().animate({scrollTop: (section.eq(this.num).offset().top - minus) },700,'easeOutCubic');
			});
			btns2.bind('click',function(){
				if (UI.SV._screenFlag == 3){ minus = 0;  }else { minus = 58; }
				$('html,body').stop().animate({scrollTop: (section.eq(this.num + 1).offset().top - minus) },700,'easeOutCubic');
			});
			
		};		
		
		fn_move_section();
		fn_scroll();
		$(window).resize(function(){ fn_scroll(); });
		$(window).scroll(function(){ fn_scroll(); });
	},
	
	mainDocter : function (){
		if ( $('#main_contents').length <= 0 ){ return;	}

		var obj = $('#main_contents .main_section5');
		var doc_name = obj.find('.docter_name');
		var doc_img = obj.find('.docter_img');
		var doc_exp = obj.find('.docter_exp');
		var prev = obj.find('.prev');
		var next = obj.find('.next');
		var max_length = doc_img.length;
		var screen_flag = 0;
		var flag = 0;
		/*var doc_position = [
			[0,270,540,810,1080,1350],
			[0,163,326,489,652,815],
			[0,270,540,810,1080,1350]
		];*/
		var doc_position = [
			[0,231,462,693,924,1155,1386],
			[0,140,280,420,560,700,840],
			[0,231,462,693,924,1155,1386]
		];
		var doc_left = [407,75,0];
		var doc_opacity = [0.3,0.3,0];
		var setIn = null;
		var setTi = null;
		var autoplay_time = 5000;
		var replay_time = 3000;

		var reset = function (){
			if (mql.matches && !mql2.matches){
				screen_flag = 1;
			}else if (mql.matches && mql2.matches){
				screen_flag = 2;
			}else {
				screen_flag = 0;
			}

			if (screen_flag == 2){
				doc_img.each(function(i){
					if (flag == i){
						$(this).css({'marginLeft':-175,'opacity':1,'z-index':5});
					}else {
						$(this).css({'marginLeft':-500,'opacity': doc_opacity[screen_flag],'z-index':1});
					}
				});
			}else {
				doc_img.each(function(i){
					if (flag == i){
						$(this).css({'marginLeft':0,'left': doc_left[screen_flag],'width': 470,'opacity':1,'z-index':5});
					}else {
						$(this).css({'marginLeft':0,'left': doc_position[screen_flag][i],'width': 231,'opacity': doc_opacity[screen_flag],'z-index':1});
					}
				});
			}

			
			doc_name.each(function(i){
				if (flag == i){
					$(this).css({'opacity':1,'z-index':5});
				}else {
					$(this).css({'opacity':0,'z-index':1});
				}
			});
			doc_exp.each(function(i){
				if (flag == i){
					$(this).css({'opacity':1,'z-index':5});
				}else {
					$(this).css({'opacity':0,'z-index':1});
				}
			});
		};

		reset();

		var movement_allow = true;

		var movement = function (num){
			if (!movement_allow){return;}
			movement_allow = false;

			var doc_img_prev = doc_img.eq(flag);
			var doc_img_next = doc_img.eq(num);
			var doc_name_prev = doc_name.eq(flag);
			var doc_name_next = doc_name.eq(num);
			var doc_exp_prev = doc_exp.eq(flag);
			var doc_exp_next = doc_exp.eq(num);
			var speed = 600;

			doc_img_prev.css({'z-index':'1'});
			doc_img_next.css({'z-index':'5'});
			doc_name_prev.css({'z-index':'1'});
			doc_name_next.css({'z-index':'5'});
			doc_exp_prev.css({'z-index':'1'});
			doc_exp_next.css({'z-index':'5'});

			doc_name_prev.animate({'opacity':0},500,'easeOutCubic');
			doc_exp_prev.animate({'opacity':0},500,'easeOutCubic');
			

			doc_name_next.delay(500).animate({'opacity':1},700,'easeOutCubic');
			doc_exp_next.delay(700).animate({'opacity':1},700,'easeOutCubic',function (){
				movement_allow = true;
			});
			if (screen_flag == 2){
				doc_img_prev.stop().animate({'marginLeft':-500,'opacity':doc_opacity[screen_flag]},speed,'easeOutCubic');
				doc_img_next.stop().animate({'marginLeft':-175,'opacity':1},speed,'easeOutCubic');
			}else {
				doc_img_prev.stop().animate({'left': doc_position[screen_flag][flag],'width':270,'opacity':doc_opacity[screen_flag]},speed,'easeOutCubic');
				doc_img_next.stop().animate({'left': doc_left[screen_flag],'width':470,'opacity':1},speed,'easeOutCubic');
			}
			flag = num;
		};

		prev.bind('click',function(){
			clearInterval(setIn);
			clearTimeout(setTi);
			auto_play();

			if (flag > 0  ){
				movement(flag - 1);
			}else {
				movement(max_length -1);
			}
		});

		next.bind('click',function(){
			clearInterval(setIn);
			clearTimeout(setTi);
			auto_play();

			if (flag < max_length -1 ){
				movement(flag + 1);
			}else {
				movement(0);
			}
		});

		var auto_play = function (){
			setIn = setInterval(function(){
				if (flag < max_length -1 ){
					movement(flag + 1);
				}else {
					movement(0);
				}
			},autoplay_time);
		};

		auto_play();

		var re_play = function (){
			clearInterval(setIn);
			clearTimeout(setTi);
			setTi = setTimeout(function(){
				auto_play();
			},replay_time);
		};

		$(window).resize(function(){
			clearInterval(setIn);
			clearTimeout(setTi);
			reset();
			re_play();
		});
	},

	surgeryCounter : function (amount){
		var obj = $('#header .surgery_counter .counter_box');
		var number = obj.find('.num');
		var amount = amount.split("");
		var line_height = 37;

		if (amount.length < 4){
			alert('반드시 4자리를 입력해주세요.');
			return;
		}

		number.each(function(i){
			var num = $(this).find('strong');
			var text = $(this).find('em');
			var it_number = Number(amount[i]);
			text.text(it_number);
			num.delay(200 * i).animate({backgroundPosition: '0 -' + (it_number * line_height) + 'px'},1300);
		});
	},

	branchActive : function (){
		var branch_big_slider = $('.branch_big_img_wrap .slider');
		var branch_small_slider = $('.branch_small_img_wrap .slider');
		var big_img = $('.branch_big_img_wrap li');
		var small_img = $('.branch_small_img_wrap span a');
		var flag = 0;

		if (branch_small_slider.length <= 0){ return; }

		small_img.each(function(i){this.num = i});

		branch_big_slider.cycle({
			fx : "scrollHorz",
			easing: "easeOutQuart",
			speed: 1000,
			timeout: 99999999,
			autoHeight : 'container',
			slides : "> li"
		});

		branch_small_slider.cycle({
			fx : "scrollHorz",
			easing: "easeOutQuart",
			speed: 1000,
			timeout: 99999999,
			swipe: true,
			autoHeight : 'container',
			prev:'.branch_small_img_wrap .prev',
			next:'.branch_small_img_wrap .next',
			slides : "> span",
		});

		small_img.bind('click',function(){
			if (flag == this.num){ return; }
			small_img.eq(flag).removeClass('on');
			small_img.eq(this.num).addClass('on');

			branch_big_slider.cycle('goto', this.num);

			flag = this.num;
		});
	},
	

	layerPopup: function(_target, type){
		var this_layer	= $(_target);

		switch(type){
			case 'open':
				this_layer.fadeIn(300);
				break;
			case 'close':
				this_layer.fadeOut(300);
				break;
			default:;
		}
	},

	tabAction: function(_tab, _con){
		var tab = $(_tab).children();
		var con = $(_con).children();

		tab.each(function(i){this.num = i});

		tab.click(function(){
			$(this).siblings().removeClass('on');
			$(this).addClass('on');
			con.eq(this.num).siblings().removeClass('on');
			con.eq(this.num).addClass('on');
		});
	}
}

$(document).ready(function(){
	UI.load();
});
