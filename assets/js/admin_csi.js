$(document).ready(function() {

    function check_car_no(v){
        var pattern1 = /^\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 12저1234 
        var pattern2 = /^\d{3}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 123저1234 
        var pattern3 = /[가-힣ㄱ-ㅎㅏ-ㅣ\x20]{2}\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 서울12치1233 

        if (!pattern1.test(v)) { 
            if (!pattern2.test(v)) { 
                if (!pattern3.test(v)) { 
                    //result.innerHTML = "No"; 
                    alert('차량번호 형식이 잘못되었습니다. (12저1234 or 123저1234 or 서울12치1233) \n ===>' +v);
                    return false;
                 }else {
                    return true;
                 }
            } 
            else { 
                //result.innerHTML = "Yes"; 
                //return true;
                //alert('차량번호 형식 맞음');
                return true;
            } 
        } 
        else { 
            //result.innerHTML = "Yes"; 
            //return true;
            //alert('차량번호 형식이 맞음');
            return true;
        } 

        alert('차량번호 형식이 잘못되었습니다. 12저1234 or 서울12치1233');
        return false;

    };

    $('.checkContractForm').click(function(){      

        var v  = $('#car_no').val();

        console.log('v' , v);

        //var result = document.getElementById('result'); 

        var ret =  check_car_no(v);

        if(!ret) return false;
        var n_id = $('#n_id').val();
        var did = $('#new_contract_did').val();
        var wpid = $('#new_contractor_wpid').val();
        var car_no = v;
        var pay_date = $('#pay_date').val();
        var discount_type = $('#discount_type').val();
        var car_owner = $('#car_owner').val();
        var car_type = $('#car_type').val();
        var tel_number = $('#tel_number').val();
        var nc_amount = $('#nc_amount').val();
        var nc_type = $('#nc_type ').val();
        var nc_origin_amount = $('#nc_origin_amount').val();
        var description = $('#message').val();



        $.ajax({  
                url:"/escalate/contractor/new_save",  
                method:"post",  
                data:{n_id:n_id,
                      did:did , 
                      wpid:wpid, 
                      car_no:car_no, 
                      pay_date:pay_date, 
                      discount_type:discount_type, 
                      car_owner:car_owner, 
                      car_type:car_type, 
                      tel_number:tel_number,
                      nc_amount:nc_amount,
                      nc_origin_amount:nc_origin_amount,
                      description:description,

                },  
                success:function(data){  
                    //console.log(data.status);
                    if(data.status == true){
                        alert('저장하였습니다.');
                        if(nc_type == 'new'){
                            $("#new_contractor_form")[0].reset();      
                            window.location.href = '/escalate/contractor/new_list';
                        } else {
                            window.location.href = '/escalate/contractor/new_list';
                        }
                        
                    } else{
                        alert('저장에 실패하였습니다.');
                    }
                }  
        });  

    });


    $('.init_password').click(function(){  
        //console.log('`workplace_`get');
        var aid = $(this).attr("id"); 

        $.ajax({  
                url:"/admin/account/write_warning",  
                method:"post",  
                data:{aid:aid},  
                success:function(data){  
                    // console.log('data' , data);

                 $('#init_passwd_modal').html(data);  
                     $('#init_passwdModal').modal("show");  
                }  
        });  

    });


    $('#history_leave_query_btn').click(function(){

    $('#leave_history').submit();
            
    });


    $('#history_work_query_btn').click(function(){

        $('#work_history').submit();
            
    });

    // $('#approve_vac').daterangepicker({ 
    //         autoApply: true,
    //         showDropdowns: true,
    //         locale: {
    //              format: 'YYYY-MM-DD'
    //     },      
    // });
    $('#history_worktime_query_btn').click(function(){

        $('#worktime_history').submit();
            
    });


    $('#approve_vac_query_btn').click(function(){

        $('#approve_vac').submit();
            
    });


    // $('#inandout_query_btn').click(function(){

    //     $('#inandout_search').submit();
            
    // });
    

    $('.wtp_delete').click(function(e){
        

        // var id = $(this).find('[name=id]').val();

        var id = $("input[name*='id']").val();

        alert('정말로 삭제하시겠습니까?');

        //console.log(id);
        $.ajax({  
                url:"/admin/datahandle/wtp_delete",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    if(data.status){
                        alert('삭제 성공');
                        location.reload();
                    } else{
                        alert('삭제 실패');
                    }
                }  
        });  



    });

    $('.btn_wp_delete').click(function(e){
        

        // var id = $(this).find('[name=id]').val();

       var id = $(this).attr("id");

       if(confirm('정말로 삭제하시겠습니까?')== true) {
            $.ajax({  
                    url:"/admin/datahandle/wp_delete",  
                    method:"post",  
                    data:{id:id},  
                    success:function(data){  
                        if(data){
                            alert('삭제 성공');
                            location.href="/admin/datahandle";
                        } else{
                            alert('삭제 실패');
                            location.reload();
                        }
                    }  
            });  
       }
    });

    



    //$('#editor-one').wysiwyg();

    $('#notice-save-btn').on('click',function(){        

        //$('textarea[id=settle_amt]').val("66"); // 텍스트

        // var html = $('#editor-one').html();
        // //console.log('html ' , html);

        // $('textarea[id=notice_body]').html(html); // 텍스트


        // if(html.byteLength() > 70000){
        //     alert('이미지 파일 크기를 체크해주세요 이미지의 합은 64kb 를 초과할수 없습니다.');
        // } else {
          $('#notice_form').submit();
        // }


        


    });

    String.prototype.byteLength = function() {
        var l= 0;
         
        for(var idx=0; idx < this.length; idx++) {
            var c = escape(this.charAt(idx));
             
            if( c.length==1 ) l ++;
            else if( c.indexOf("%u")!=-1 ) l += 2;
            else if( c.indexOf("%")!=-1 ) l += c.length/3;
        }
         
        return l;
    };

    $('#bug_report-save-btn').on('click',function(){        

        //$('textarea[id=settle_amt]').val("66"); // 텍스트

        // var html = $('#editor-bug_report').html();

        // $('textarea[id=bugreport_body]').html(html); // 텍스트
        // console.log("byte:" , html.byteLength());

        // if(html.byteLength() > 70000){
        //     alert('이미지 파일 크기를 체크해주세요 이미지의 합은 64kb 를 초과할수 없습니다.');
        // } else {
          $('#bug_report_form').submit();  
        // }


        //


    });

    $('#content-save-btn').click(function(){  
        var title = $('#title').val();
        if(title == ''){
            alert('제목을 입력해 주세요');
        } else {
             alert('게시물 작성이 완료되었습니다.');
             $('#content_form').submit();  
        }



       


    });

     $('.survey_status').click(function(){  

        var id = $(this).attr("id"); 

        var this_id = '#'+id;

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        var str = id.split('_');

        var ssid = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        $.ajax({  
                url:"/admin/survey/status",  
                method:"post",  
                data:{ssid:ssid , status:status},  
                success:function(data){  
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();

                         
                    }


                }
        });  



    });



    $('.pt_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        console.log('id : ' , id);

        $.ajax({  
                url:"/admin/datahandle/pt_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#parking_type_detail').html(data);  
                     $('#ptModal').modal("show");  
                }  
        });  

    });

    // $('.parking_type_save').click(function(){  
    //     //console.log('workplace_get');
    //     var id = $(this).data("id"); 

    //     console.log('parking_type_save' , id);

    //     var form = $( this );

    //     console.log('parking_type_save' , form);

    // });

    $('.rank_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        //console.log('id : ' , id);

        $.ajax({  
                url:"/admin/datahandle/rank_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#rank_detail').html(data);  
                     $('#rankModal').modal("show");  
                }  
        });  


    });


    $('.bt_get').click(function(){  
        var btid = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/board_manage/bt_write",  
                method:"post",  
                data:{btid:btid},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#board_type_detail').html(data);  
                     $('#btModal').modal("show");  
                }  
        });       

    });

    $('.un_status').click(function(){  
        var unid = $(this).attr("id"); 
        var checked = $('input:checkbox[id="'+unid+'"]').is(":checked");
        var status = 0;
        if(checked == true){
            status = 1;
        }
        $.ajax({  
                url:"/admin/usernavigation/status",  
                method:"post",  
                data:{unid:unid , status:status},  
                success:function(data){  
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();  
                    }
                }
        });  

    });



    $('.rank_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        //console.log('rank_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var rank_id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        //console.log('rank_id : ' , rank_id);
        //console.log('status : ' , status);


        $.ajax({  
                url:"/admin/datahandle/rank_status",  
                method:"post",  
                data:{rank_id:rank_id , status:status},  
                success:function(data){  
                    console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                        // $('#'+id +"+span").html('');
                        // console.log('this_id' , this_id)
                        // if($('input:checkbox[id="'+id+'"]').is(":checked")){
                        //     $('#'+id).prop('checked', true)
                        //     $('#'+id +"+span").html('활성화');
                        // } else {
                        //     $('#'+id).prop('checked', false)
                        //     $('#'+id +"+span").html('비활성화');
                        // }
                        
                        // $('input:checkbox[id="'+id+'"]').html('니미럴 ');

                         
                    }


                }
        });  


    });

    $('.wp_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        //console.log('wp_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부


        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var wpid = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        //console.log('wpid : ' , wpid);
        //console.log('status : ' , status);


        $.ajax({  
                url:"/admin/datahandle/wp_status",  
                method:"post",  
                data:{wpid:wpid , status:status},  
                success:function(data){  
                   // console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  

    });

    $('.wt_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        //console.log('wp_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부


        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var wtid = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        //console.log('wpid : ' , wpid);
        //console.log('status : ' , status);


        $.ajax({  
                url:"/admin/datahandle/wt_status",  
                method:"post",  
                data:{wtid:wtid , status:status},  
                success:function(data){  
                   // console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  

    });

    $('.pt_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var pid = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        // console.log('pid :' , pid);
        // console.log('status :' , status);

        $.ajax({  
                url:"/admin/datahandle/pt_status",  
                method:"post",  
                data:{pid:pid , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                        alert(data.msg);
                        location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  

    });

    $('.dt_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var did = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/datahandle/dt_status",  
                method:"post",  
                data:{did:did , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });


    $('.wtp_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/datahandle/wtp_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });



    $('.wa_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/datahandle/wa_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });


    $('.ws_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/datahandle/ws_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });


    $('.nt_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/notice/status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });


    $('.ww_status').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        // console.log('pt_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/datahandle/ww_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });



    $('.ww_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/datahandle/ww_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#workwhen_detail').html(data);  
                     $('#wwModal').modal("show");  
                }  
        });  
    });


    $('.suvey_schedule').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        // console.log('id :' , id);

        $.ajax({  
                url:"/admin/survey/schedule_write",  
                method:"post",  
                data:{ssid:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#ss_detail').html(data);  
                     $('#ssModal').modal("show");  
                }  
        });  
    });


    $('.delete_answer').click(function(){  
        var id = $(this).attr("id"); 

        console.log('id :' , id);
        $.ajax({  
                url:"/admin/survey/delete_answer",  
                method:"post",  
                data:{said:id},  
                success:function(data){

                    if(data.status){
                        location.reload();

                    }  else {
                        alert(data.msg);
                    }
                    // console.log('data' , data);
                     // $('#ss_detail').html(data);  
                     // $('#ssModal').modal("show");  
                }  
        });  



    });





    $('.add_question').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        //console.log('id :' , id);


        $.ajax({  
                url:"/admin/survey/add_question",  
                method:"post",  
                data:{ssid:id},  
                success:function(data){

                    if(data.status){
                        location.reload();

                    }  else {
                        alert(data.msg);
                    }
                    // console.log('data' , data);
                     // $('#ss_detail').html(data);  
                     // $('#ssModal').modal("show");  
                }  
        });  
    });





    


    $('.add_answer').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        //console.log('id :' , id);

        $.ajax({  
                url:"/admin/survey/add_answer",  
                method:"post",  
                data:{sqid:id},  
                success:function(data){

                    if(data.status){
                        location.reload();

                    }  else {
                        alert(data.msg);
                    }
                    // console.log('data' , data);
                     // $('#ss_detail').html(data);  
                     // $('#ssModal').modal("show");  
                }  
        });  
    });


    



    $('.rank_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 

        console.log('rank_save' , id);

        var form = $( this );

        console.log('rank_save' , form);
    });

    // if (rid=!0)
    // $('.rank_delete').click(function(){  
    //     var id = $(this).data("id"); 
    //     alert('삭제하시겠습니까?');
    //     var form = $( this );
    // });

    $('.rc_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        $.ajax({  
                url:"/admin/datahandle/rc_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#recognize_color_detail').html(data);  
                     $('#rcModal').modal("show");  
                }  
        });  

    });

    $('.district_get').click(function(){  
        
        var id = $(this).attr("id"); 

        console.log('district_get id : ' , id);

        $.ajax({  
                url:"/admin/datahandle/dt_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#district_modal').html(data);  
                     $('#dtModal').modal("show");  
                }  
        });  

    });


    $('.recognize_color_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 

        console.log('workplace_save' , id);

        var form = $( this );
        console.log('workplace_save' , form);
    });




    $('.navigation_get').click(function(){  
        console.log('navigation_get');
        var id = $(this).attr("id"); 

        console.log('id' , id);

        $.ajax({  
                url:"/admin/usernavigation/write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#navigation_detail').html(data);  
                     $('#user_dataModal').modal("show");  
                }  
        });  

    });


    $('.add_operator').click(function(){  
        console.log('navigation_get');
        var id = $(this).attr("id"); 


        $.ajax({  
                url:"/admin/board/operator_write",  
                method:"post",  
                data:{bid:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#board_add_operator').html(data);  
                     $('#operator_dataModal').modal("show");  
                }  
        });  

    });



    $('.board_info_get').click(function(){  
        console.log('navigation_get');
        var id = $(this).attr("id"); 

        console.log('id' , id);

        $.ajax({  
                url:"/admin/board_manage/write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#board_info_detail').html(data);  
                     $('#user_dataModal').modal("show");  
                }  
        });  

    });






    // $('.handle_whiteip').click(function(){  
    //     console.log('`handle_whiteip get');
    //     var wiid = $(this).attr("id"); 

    //     $.ajax({  
    //             url:"/admin/datahandle/whiteip_write",  
    //             method:"post",  
    //             data:{wiid:wiid},  
    //             success:function(data){  
    //                 // console.log('data' , data);

    //                  $('#whiteip_modal_detail').html(data);  
    //                  $('#white_ip_Modal').modal("show");  
    //             }  
    //     });  


    //     //$('#init_passwdModal').modal("show");  

    // });
    //                  $('#whiteip_modal_detail').html(data);  
    //                  $('#white_ip_Modal').modal("show");  
    //             }  
    //     });  
    // });

    $('.wr_delete').click(function(){  

        //console.log('wr_delete');
        var wrid = $('input[name=id]').val();
        //console.log('wrid : ' , wrid); 
        //alert(wrid);

        if(confirm("진행하시겠습니까?")){ 
            $('#dataEditModal').modal("hide"); //닫기 

            $.ajax({  
                    url:"/home/workrecord/delete",  
                    method:"post",  
                    data:{wrid:wrid},  
                    success:function(data){  
                        console.log('data' , data);
                        if(data.status == true){
                            alert('근무 기록을 삭제하였습니다');
                            location.reload();
                        } else {
                            alert(data.msg)
                            location.reload();
                            
                        }
                    }  
            });                  
        }
    });    


    $('.wa_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/datahandle/wa_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#workassign_detail').html(data);  
                     $('#waModal').modal("show");  
                }  
        });  
    });

    $('.workassign_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 
        console.log('workassign_save' , id);
        var form = $( this );
        console.log('workassign_save' , form);

    });
    




    var chwr_data_table = $('#change_working_record_datatable').DataTable({

        lengthChange: true,
            
        searching: true,
            // 뺣젹 湲곕뒫 ④린湲
        ordering: true,
            // 뺣낫 쒖떆 ④린湲
        info: true,
            // 섏씠吏湲곕뒫 ④린湲
        responsive: true,
        paging: true , 
        "order": [[ 0, 'desc' ]],
        dom: 'Bfrtip',  
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        
    });

    // wp_data_table.on('click', '.workplace_get', function() {
    //         var id = $(this).attr("id");  
    //         location.href = "/admin/datahandle/wp_write?id="+id;
    // });

    var color_list_datatable = $('#color_list_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true

    });

    color_list_datatable.on('click', '.rc_get', function() {
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/datahandle/rc_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#recognize_color_detail').html(data);  
                     $('#rcModal').modal("show");  
                }  
        });  
        
    });


    var wtp_data_table = $('#wtp_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true,
        responsive: true,
        "order": [[ 0, 'asc' ]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]

    });


    var appvac_datatable = $('#appvac_datatable').DataTable({
        // 표시 건수기능 숨기기
        'columnDefs': [
         {
            'targets': 0,
            'checkboxes': {
               'selectRow': true
            }
         }
      ],
      'select': {
         'style': 'multi'
      },

        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        "order": [[ 4, 'desc' ]],
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]


    });


    $('#appvac_datatable tbody').on( 'click', 'tr', function () {
        $(this).toggleClass('selected');

        console.log($(this));
    } );    




    $('.approve_batch').click(function(){  

        var data = appvac_datatable.rows('.selected').data();

        var str = '';

        for(var i =0; i < data.length; i++){
            var row_column = data[i];
            
            var vrid = row_column[0];

            console.log('vrid' , vrid);



            $.ajax({  
                    url:"/admin/approve_vacation/batch_save",  
                    method:"post",  
                    data:{vrid:vrid},  
                    async: false,
                    success:function(data){  
                        console.log('data' , data);

                        if(data.status == true){
                            dd =  vrid + '일괄 처리 성공\n';
                            str += dd;
                        } else {
                            dd =  vrid + data.msg+'\n';
                            console.log('str :' , dd);
                            str += dd;
                        }
                        console.log('str :' , str);


                    }  

            });  


        }

        alert(str);
        location.reload();



        //alert( appvac_datatable.rows('.selected').data().length +' row(s) selected' );




        // appvac_datatable.each( function ( value, index ) {
        //     console.log( 'Data in index: '+index+' is: '+value );
        // } );        
        // var id = $(this).attr("id"); 

        // $.ajax({  
        //         url:"/admin/datahandle/wp_write",  
        //         method:"post",  
        //         data:{id:id},  
        //         success:function(data){  
        //             // console.log('data' , data);

        //              $('#workplace_detail').html(data);  
        //              $('#wpModal').modal("show");  
        //         }  
        // });  

    });


    


     // Handle click on "Select all" control
   $('#vac-select-all').on('click', function(){
      // Check/uncheck all checkboxes in the table
      console.log('dfsadfsda');

      var rows = appvac_datatable.rows({ 'search': 'applied' }).nodes();
      $('input[type="checkbox"]', rows).prop('checked', this.checked);
   });





    var account_data_table = $('#account_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        responsive: true,
        paging: true , 
        dom: 'Bfrtip',  
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var bugreport_datatable = $('#bugreport_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var inandout_datatable = $('#inandout_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var reconcile_datatable = $('#reconcile_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: false,

        pageLength: 100 , 
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    




    // $('.workplace_get').click(function(){  
    //     console.log('workplace_get');
    //     var id = $(this).attr("id"); 

    //     $.ajax({  
    //             url:"/admin/datahandle/wp_write",  
    //             method:"post",  
    //             data:{id:id},  
    //             success:function(data){  
    //                 // console.log('data' , data);

    //                  $('#workplace_detail').html(data);  
    //                  $('#wpModal').modal("show");  
    //             }  
    //     });  

    // });

    $(".contractor_query_btn").on("click", function(){

        var period = $('#c_q_period').val();


        //console.log('period : ', period );

        window.location.href = '/admin/contractor/new_list?n_q_period='+period;        
    });

    $(".e_query_btn").on("click", function(){

        var period = $('#e_q_period').val();


        //console.log('period : ', period );

        window.location.href = '/admin/contractor/extend_list?e_q_period='+period;        
    });


    $(".ic_query_btn").on("click", function(){

        var period = $('#ic_q_period').val();


        //console.log('period : ', period );

        window.location.href = '/admin/daily_income/list?ic_q_period='+period;        
    });


    $('.parking_type_detail').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 

        $.ajax({  
                url:"/admin/datahandle/pt_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                     $('#workplace_detail').html(data);  
                     $('#ptModal').modal("show");  
                }  
        });  

    });


    $('.workplace_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 

        console.log('workplace_save' , id);

        var form = $( this );
        console.log('workplace_save' , form);

    });


   var working_history_datatable = $('.working_history_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: false , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });

    working_history_datatable.on('click', '.btn_get_history_info', function() {
         var wrid = $(this).attr("id"); 

        $.ajax({  
                url:"/admin/account/get_history_info",  
                method:"post",  
                data:{wrid:wrid},  
                success:function(data){
                    console.log(data.working_record); 
                    $('#history_modal_crid').val(data.working_record.crid);
                    $('#history_modal_rdate').val(data.working_record.rdate);
                    $('#history_modal_id').val(data.working_record.account.id);
                    $('#history_modal_aid').val(data.working_record.aid);
                    $('#history_modal_did_title').val(data.working_record.wp_info.did_title);
                    $('#history_modal_wp_title').val(data.working_record.wp_info.title);
                    $('#history_modal_ws_title').val(data.working_record.ws_info.title);
                    $('#history_modal_wt_title').val(data.working_record.wt_info.title);
                    $('#history_modal_work_start_time').val(data.working_record.commute_record.work_start_time);
                    $('#history_modal_work_end_time').val(data.working_record.commute_record.work_end_time);
                    $('#history_modal').modal("show");  
                }  
        });  
        
    });


    function commute_record(){
        console.log('aid : ');
    }

    $('#btn_save_working_history').click(function() {
        var crid = $('#history_modal_crid').val();
        var work_start_time = $('#history_modal_work_start_time').val();
        var work_end_time = $('#history_modal_work_end_time').val();
        var aid = $('#history_modal_aid').val();
        var rdate = $('#history_modal_rdate').val();



        $.ajax({  
                url:"/admin/account/save_working_history",  
                method:"post",  
                data:{crid:crid, work_start_time:work_start_time, work_end_time:work_end_time ,aid:aid, rdate:rdate},  
                success:function(data){
                   alert("정보가 수정되었습니다.");
                   $('#history_modal').modal("hide");
                   location.reload();  
                }  
        });  

    });

    $('#btn_save_working_history_new').click(function() {
         var crid = $('#history_modal_new_crid').val();
        var work_start_time = $('#history_modal_new_work_start_time').val();
        var work_end_time = $('#history_modal_new_work_end_time').val();
        var aid = $('#history_modal_new_aid').val();
        var rdate = $('#history_modal_new_rdate').val();

        console.log('work_start_time : ' , work_start_time);
        console.log('work_end_time : ' , work_end_time);


        $.ajax({  
                url:"/admin/account/save_working_history",  
                method:"post",  
                data:{crid:crid, work_start_time:work_start_time, work_end_time:work_end_time ,aid:aid, rdate:rdate},  
                success:function(data){
                   alert("정보가 수정되었습니다.");
                   $('#history_modal').modal("hide");
                   location.reload();  
                }  
        });  
    });



    $('#prev_btn_history').click(function(){
        var viewing_month = $('#viewing_month_history').val();

        var aid = $('#workhistory_aid').val();

        // console.log('viewing_month :' , viewing_month);
        // console.log('aid :' , aid);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';



        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';


       window.location.href = '/admin/account/working_history?from='+temp_month+'&aid='+aid;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    })



    $('#prev_db_btn').click(function(){

        var db_viewing_month = $('#db_viewing_day').val();

        //console.log('viewing_month :' , viewing_month);

        var string = db_viewing_month.split('-');

        var m_day = string[0]+'/'+string[1]+'/' +string[2]+' 00:00:00';


        var day = new Date(m_day);
        day.setDate(day.getDate() - 1);

        console.log('day : ' , day)

       var month = day.getMonth();
       month += 1;
       if(month == 0){
            month = '12';
            string[0] -= 1;
       }
       if(month < 10)
           month = '0'+month;



       var day_ = day.getDate();

       if(day_ < 10)
            day_ = '0'+day_;            

       //  // console.log('date :' , date);            
       //  // console.log('month :' , month);   


       var temp_day = string[0]+'-'+month+'-'+day_;


       console.log('temp_day :' ,temp_day)

       window.location.href = '/admin/dashboard/index?from='+temp_day;

    });


    $('#next_db_btn').click(function(){
        var db_viewing_month = $('#db_viewing_day').val();

        //console.log('viewing_month :' , viewing_month);

        var string = db_viewing_month.split('-');

        var m_day = string[0]+'/'+string[1]+'/' +string[2]+' 00:00:00';


        var day = new Date(m_day);
        day.setDate(day.getDate() + 1);

        console.log('day : ' , day)

       var month = day.getMonth();
       month += 1;
       if(month == 0){
            month = '12';
            string[0] -= 1;
       }
       if(month < 10)
           month = '0'+month;

       var day_ = day.getDate();

       if(day_ < 10)
            day_ = '0'+day_;            

       //  // console.log('date :' , date);            
       //  // console.log('month :' , month);   


       var temp_day = string[0]+'-'+month+'-'+day_;


       console.log('temp_day :' ,temp_day)

       window.location.href = '/admin/dashboard/index?from='+temp_day;
    });

    $('#next_btn_history').click(function(){
        //console.log('approve_vac_query_btn');

        $('#workreport_form').submit();
        
        var viewing_month = $('#viewing_month_history').val();
        var aid = $('#workhistory_aid').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        //console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        // console.log('date :' , date);           
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href = '/admin/account/working_history?from='+temp_month+'&aid='+aid;
    });

    $('#prev_workrecord_btn').click(function(){
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';


        var aid = $('#workrecord_aid').val();

        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/home/workrecord/list?aid='+aid+'&from='+temp_month;


        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    })

    $('#next_workrecord_btn').click(function(){
        //console.log('approve_vac_query_btn');

        // $('#workreport_form').submit();
        
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';


        var aid = $('#workrecord_aid').val();

        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href = '/home/workrecord/list?aid='+aid+'&from='+temp_month;
    });





    $('#prev_btn').click(function(){
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';


        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/admin/workreport/monthly?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    })

    $('#next_btn').click(function(){
        //console.log('approve_vac_query_btn');

        $('#workreport_form').submit();
        
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href = '/admin/workreport/monthly?from='+temp_month;
    });


    function addDays(date, days) {
      var result = new Date(date);
      result.setDate(result.getDate() + days);
      return result;
    }


    $('#prev_nca_btn').click(function(){
        var viewing_month = $('#nca_viewing_month').val();


        var date = addDays(viewing_month , -1);

        var month = date.getMonth()+1;
        // console.log(month);

        if(month == 0){
              month = '12';
              //string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        var day = date.getDate();
        if(day < 10)
            day = '0'+day;



        var view_date = date.getFullYear() + '-' + month + '-' + day;


        console.log(date);
        console.log(view_date);


        window.location.href = '/admin/contractor/new_list?from='+view_date;        


        //console.log('viewing_month :' , viewing_month);

       //  var string = viewing_month.split('-');

       //  var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




       //  var date = new Date(m_date);

       //  var month = date.getMonth();
       //  if(month == 0){
       //      month = '12';
       //      string[0] -= 1;
       //  }
       //  if(month < 10)
       //      month = '0'+month;

       //  console.log('date :' , date);            
       //  console.log('month :' , month);   


       // var temp_month = string[0]+'-'+month+'-01';

       //window.location.href = '/escalate/contractor/new_list?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    });




    $('#prev_nc_btn').click(function(){
        var viewing_month = $('#nc_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/escalate/contractor/new_list?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    });


       $('#next_nc_btn').click(function(){
        //console.log('approve_vac_query_btn');

        //$('#workreport_form').submit();
        
        var viewing_month = $('#nc_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href =  '/escalate/contractor/new_list?from='+temp_month;


    });

    $('#prev_ec_btn').click(function(){
        var viewing_month = $('#ec_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/escalate/contractor/extend_list?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    });


       $('#next_ec_btn').click(function(){
        //console.log('approve_vac_query_btn');

        //$('#workreport_form').submit();
        
        var viewing_month = $('#ec_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href =  '/escalate/contractor/extend_list?from='+temp_month;


    });



      $('#prev_income_btn').click(function(){
        var viewing_month = $('#income_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/escalate/daily_income/list?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    });

    $('#next_income_btn').click(function(){
        //console.log('approve_vac_query_btn');

        //$('#workreport_form').submit();
        
        var viewing_month = $('#income_viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href = '/escalate/daily_income/list?from='+temp_month;
    });


    $('#daily_prev_btn').click(function(){
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);


        var month = date.getMonth();
        if(month == 0){
            month = '12';
            string[0] -= 1;
        }
        if(month < 10)
            month = '0'+month;

        // console.log('date :' , date);            
        // console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       window.location.href = '/admin/workreport/monthly_daily_new?from='+temp_month;



        // $("#viewing_month").val(temp_month);

//       $('#workreport_form').submit();
    });

    $('#daily_next_btn').click(function(){
        //console.log('approve_vac_query_btn');

        $('#workreport_form').submit();
        
        var viewing_month = $('#viewing_month').val();

        //console.log('viewing_month :' , viewing_month);

        var string = viewing_month.split('-');

        var m_date = string[0]+'/'+string[1]+'/01/00:00:00';




        var date = new Date(m_date);

        var month = date.getMonth()+2;
        console.log('month' , month);
        if(month > 12){
            month = 1;
            string[0]++;
        }
        if(month < 10){
            month = '0'+month;
        }

        console.log('date :' , date);           
        console.log('month :' , month);   


       var temp_month = string[0]+'-'+month+'-01';

       // $("#viewing_month").val(temp_month);
       // $('#workreport_form').submit();
        window.location.href = '/admin/workreport/monthly_daily_new?from='+temp_month;
    });



    $('.ws_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/datahandle/ws_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#workstyle_detail').html(data);  
                     $('#wsModal').modal("show");  
                }  
        });  
    });

    $('.workstyle_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 
        console.log('workstyle_save' , id);
        var form = $( this );
        console.log('workstyle_save' , form);


    });
    $('.wtp_get').click(function(){  
        console.log('workplace_get');
        var id = $(this).attr("id"); 

        // console.log('id : ' , id);

        $.ajax({  
                url:"/admin/datahandle/wtp_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);

                    $('#worktemplate_modal').html(data);  
                    $('#wtpModal').modal("show");  
                }  
        });  

    });

    $('.wt_get').click(function(){  
        //console.log('workplace_get');
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/datahandle/wt_write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                     $('#worktype_detail').html(data);  
                     $('#wtModal').modal("show");  
                }  
        });  
    });

    $('.worktype_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 
        console.log('worktype_save' , id);
        var form = $( this );
        console.log('worktype_save' , form);

    });


    $('#vacation_period').daterangepicker({ 
            autoApply: true,
            showDropdowns: true,
            locale: {
                 format: 'YYYY-MM-DD'
        },      
    });

    $('.vacation_delete').click(function(){  
        //console.log('navigation_get');
        var id = $(this).attr("id"); 

        console.log('vacation_delete' , id);

        var del = confirm("삭제 하시겠습니까?");
        if(del == false){
          return;
        }

        $.ajax({  
                url:"/home/appforvacation/delete",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    // console.log('data' , data);
                    // return;
                    if(data.status == true){
                        alert('휴가를 삭제하였습니다.')
                        location.reload();
                    } else {
                        alert(data.msg)
                        
                    }

                }
        });  

    });
    $('.work_check').click(function(){  
        var id = $(this).attr("id"); 

        console.log('work_check id ' , id);

        var str = id.split('_');

        var check_type = str[0];
        var aid = str[1];
        var crid = str[2];

        // console.log('check_type ' , check_type);
        // console.log('aid ' , aid);
        // console.log('crid ' , crid);

        // return;

        if(check_type == 'workend'){
            if(confirm("퇴근 처리하시겠습니까?") == false) {
                return;
            }
        }
        $.ajax({  
            url:"/home/workrecord/work_check",  
            method:"post",  
            data:{aid:aid , check_type:check_type , crid:crid},  
            success:function(data){  
                // console.log('data' , data);
                // return;
                if(!data.status){
                    alert(data.msg);
                    location.reload();                         
                } else{
                    if(check_type == 'workend'){
                        alert('수고하셨습니다');        
                    }
                    location.reload();                         
                }
            }
        });  
        


    });

//change_question_type

    $('.change_question_type').click(function(){  
        var id = $(this).attr("id"); 
        //console.log('whiteip_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var sqid = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        //console.log('white_id : ' , white_id);
        //console.log('checked : ' , checked);

        $.ajax({  
                url:"/admin/survey/change_input_question",  
                method:"post",  
                data:{sqid:sqid , status:status},  
                success:function(data){  
                    console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();
                        // $('#'+id +"+span").html('');
                        // console.log('this_id' , this_id)
                        // if($('input:checkbox[id="'+id+'"]').is(":checked")){
                        //     $('#'+id).prop('checked', true)
                        //     $('#'+id +"+span").html('활성화');
                        // } else {
                        //     $('#'+id).prop('checked', false)
                        //     $('#'+id +"+span").html('비활성화');
                        // }
                        
                        // $('input:checkbox[id="'+id+'"]').html('니미럴 ');

                         
                    }


                }
        });  

    });


    $('.survey_submit').click(function(e){
        //console.log('survey_submit : ' , e);

        // var length = $(".survey_li").length;
        // console.log('length :' , length);

        var list = [];

        $('#survey_ul').find('li').each(function(){
            var current = $(this);
            console.log('li : ' , current.textarea);

        });



        // for(var i = 0; i < length; i++){

        // }

    });


    $('.discount_status').click(function(){
        var id = $(this).attr("id"); 
        //console.log('whiteip_status click id:' , id);

        var this_id = '#'+id;

        console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var dis_id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }


        $.ajax({  
                url:"/admin/discount/status",  
                method:"post",  
                data:{dis_id:dis_id , status:status},  
                success:function(data){  
                    console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();
                        // $('#'+id +"+span").html('');
                        // console.log('this_id' , this_id)
                        // if($('input:checkbox[id="'+id+'"]').is(":checked")){
                        //     $('#'+id).prop('checked', true)
                        //     $('#'+id +"+span").html('활성화');
                        // } else {
                        //     $('#'+id).prop('checked', false)
                        //     $('#'+id +"+span").html('비활성화');
                        // }
                        
                        // $('input:checkbox[id="'+id+'"]').html('니미럴 ');

                         
                    }


                }
        });  


    })


    $('.whiteip_status').click(function(){  

        var id = $(this).attr("id"); 
        //console.log('whiteip_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var white_id = str[1];

        //console.log('white_id : ' , white_id);
        //console.log('checked : ' , checked);

        $.ajax({  
                url:"/admin/datahandle/whiteip_status",  
                method:"post",  
                data:{white_id:white_id , checked:checked},  
                success:function(data){  
                    console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();
                        // $('#'+id +"+span").html('');
                        // console.log('this_id' , this_id)
                        // if($('input:checkbox[id="'+id+'"]').is(":checked")){
                        //     $('#'+id).prop('checked', true)
                        //     $('#'+id +"+span").html('활성화');
                        // } else {
                        //     $('#'+id).prop('checked', false)
                        //     $('#'+id +"+span").html('비활성화');
                        // }
                        
                        // $('input:checkbox[id="'+id+'"]').html('니미럴 ');

                         
                    }


                }
        });  
    });


    $('.vc_status').click(function(){  

        var id = $(this).attr("id"); 
        //console.log('whiteip_status click id:' , id);

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;
        //console.log('checked:' , checked);        

        var str = id.split('_');

        var lars_id = str[1];

        //console.log('white_id : ' , white_id);
        //console.log('checked : ' , checked);

        var status = 0;

        if(checked == true){
            status = 1;
        }

        //console.log('status : ' , status);


        $.ajax({  
                url:"/admin/vacation_schedule/status",  
                method:"post",  
                data:{lars_id:lars_id , status:status},  
                success:function(data){  
                    console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                    } else{
                        location.reload();
                         
                    }

                }
        });  



    });



    $('.vc_get').click(function(){  
        var lars_id = $(this).attr("id");
        if(lars_id == "") {
             $('#vs_id').val("");
             $('#vs_from').val("");
             $('#vs_to').val("");
             $('#vs_deadline').val("");
             $('#vs_status').val("");
        } else {
            $.ajax({  
                url:"/admin/vacation_schedule/write",  
                method:"post",  
                data:{lars_id:lars_id},
                success:function(data){  
                    $('#vs_id').val(data.schedule_row.lars_id);
                    $('#vs_from').val(data.schedule_row.from_);
                    $('#vs_to').val(data.schedule_row.to_);
                    $('#vs_deadline').val(data.schedule_row.deadline);
                    $('#vs_status').val(data.schedule_row.status);
                }  
            });  

        }
       
        $('#vc_Modal').modal("show");  
    });



// draw js

    $('#draw_btn').click(function() {  
        $('#dataModal').modal("show");      
            setTimeout(function() { window.location.href = './draw_wp'; alert('추첨이 완료되었습니다.'); }, 5500);
    });

// draw history js
     $('.round1_button').click(function() {
        $('#history_round').val("1");
        $('#round_id_form').attr('action','/admin/draw_admin/history');
        $('#round_id_form').attr('method','post');
        $('#round_id_form').submit();       
     });
     $('.round2_button').click(function() {
        $('#history_round').val("2");
        $('#round_id_form').attr('action','/admin/draw_admin/history');
        $('#round_id_form').attr('method','post');
        $('#round_id_form').submit();       
     });    
     $('.round3_button').click(function() {
        $('#history_round').val("3");
        $('#round_id_form').attr('action','/admin/draw_admin/history');
        $('#round_id_form').attr('method','post');
        $('#round_id_form').submit();       
     });        
     $('.round4_button').click(function() {
        $('#history_round').val("4");
        $('#round_id_form').attr('action','/admin/draw_admin/history');
        $('#round_id_form').attr('method','post');
        $('#round_id_form').submit();       
     });        

// left drawlist js
    $('#draw_left').click(function(){
        var idArray = new Array();
        // alert();
        // alert();
        $("input[name=check_aid]:checked").each(function(index) { 
            var id = $(this).attr("id");
            $('#aid_list_span').append("<input type='hidden' name = 'aid" + index + "' value = "+ id +"> ");
            $('#aid_index').val(index+1);
        });
        $('#draw_left_form').submit();
    });

// draw admin list





    $('.drawlist_get').click(function(){  
        //console.log('drawlist_get');
        var id = $(this).attr("id"); 
        $.ajax({  
                url:"/admin/draw_admin/write",  
                method:"post",  
                data:{id:id},  
                success:function(data){  
                    $('#draw_index').val("");
                    $('#draw_title').val("");
                    // $('#draw_pt').val(data.ptid);
                    $('#draw_round').val(1);
                    $('#draw_status').val(1);
                    $('#draw_period').val("2019-00-00 - 2019-00-00"); // 등록기간신청
                    $('#draw_setting').val("start");
                    $('#draw_deadline_date').val("2019-00-00");
                    $('#draw_deadline_time').val("18:00:00");
                    //console.log(data);
                    $('#dataModal').modal("show");
                    $('#etc_table_initial').val("initial");
                }  
        });  
    });

    $('.drawlist_save').click(function(){  
        //console.log('workplace_get');
        var id = $(this).data("id"); 
        console.log('drawlist_save' , id);
        var form = $( this );
        console.log('drawlist_save' , form);


    });

    $('#save_draw_info').click(function(){  
        $("#draw_info_form").attr('action','/admin/draw_admin/save_draw_info');
        $("#draw_info_form").attr('method','post');
    });

    $('.drawinfo_modify').click(function(){
        var id = $(this).attr('id');
        $.ajax({  
                url:"/admin/draw_admin/modify",  
                method:"post",  
                data:{id:id},
                dataType: 'json',  
                success:function(data){  
                    $('#draw_index').val(data.dws_id);
                    $('#draw_title').val(data.title);
                    // $('#draw_pt').val(data.ptid);
                    $('#draw_round').val(data.round);
                    $('#draw_status').val(data.status);
                    var date = data.start_date.concat(" - ", data.end_date);
                    $('#draw_period').val(date); // 등록기간신청
                    $('#draw_setting').val(data.setting);
                    var deadline = data.deadline;
                    var draw_deadline_date = (deadline).substring(0,10);
                    var draw_deadline_time = (deadline).substring(11,19);
                    $('#draw_deadline_date').val(draw_deadline_date);
                    $('#draw_deadline_time').val(draw_deadline_time);

                    //console.log('data' , data);
                    $('#dataModal').modal("show");
                }  
        });  

    });


    $('#ask_end_button').click(function(){
        var id = "end";

        $.ajax({  
            url:"/admin/draw_admin/apply_setting",  
            method:"post",  
            data:{id:id},  
            success:function(data){  
                alert('주차장 신청이 마감되었습니다.');
            }  
       });  
    });


    $('#ask_button').click(function(){
        var id = "start";

        $.ajax({  
            url:"/admin/draw_admin/apply_setting",  
            method:"post",  
            data:{id:id},  
            success:function(data){  
                alert('주차장이 신청상태가 되었습니다.');
            }  
       });  
    });

    

    $('.draw_info_setting').click(function(){
        $('#draw_setting_modal').modal('show');
    });

    $('.next_draw').click(function(){
        var id = $(this).attr('id');
        
        $.ajax({
                url:"/admin/draw_admin/modify",  
                method:"post",  
                data:{id:id},
                dataType: 'json',  
                success:function(data){
                    $('.draw_round').val(data.round);
                        if(data.round == 4) {
                            alert('다음 추첨으로 이동할 수 없습니다.');
                      } else {
                          location.href = "./next_draw";
                      }

                }
        });
    });

    $('.show_history').click(function() {
        var id = $(this).attr('id');
        $('#history_dws_id').val(id);
        $('#history_dws_id_form').attr('action','/admin/draw_admin/history');
        $('#history_dws_id_form').attr('method','post');
        $('#history_dws_id_form').submit();

    });

    $('#move_draw_left').click(function() {
        window.location.href = './left_drawlist';
     });

    $('.delete_all_btn').click(function() {
        var id = $('.delete_all_btn').attr('id');
        $('#delete_dws_id').val(id);
        $('#deleteModal').modal('show');
    
    });

    $('#delete_all').click(function(){
        var id = $('#delete_dws_id').val();
        var round = $('#delete_draw_round').val();
        if(confirm("정말 초기화 하시겠습니까?") == true) {
            $.ajax({
                url:"/admin/draw_admin/delete_all",  
                method:"post",  
                data:{id:id, round:round},
                success:function(data){
                    alert(data);
                    $('#deleteModal').modal('hide');

                }
            });
        }
    });


    var admin_monthly = $('#admin_monthly').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
        processing: true,

        searching: true,
            // 정렬 기능 숨기기
        ordering: false,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var ask_parkinglot_datatable = $('#ask_parkinglot_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: false,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],

    });

    ask_parkinglot_datatable.on('click', '.ask_parkinglot', function() {
        var id = $(this).attr("id"); 
         $.ajax({  
                    url:"/askworkplace/askworkplace/ask",  
                    method:"post",  
                    data:{id:id},  
                    success:function(data){  
                         $('#ask_message').html(data);  
                         $('#dataModal').modal("show");  
                    }  
            });  
        
    });




    ask_parkinglot_datatable.on('click', '.ask_cancel', function() {
        $('#ask_btn_form').attr("action","./ask_cancel");
        $('#ask_btn_form').attr("method","post");
        $('#ask_btn_form').submit();
        
    });

    $('.ok_btn').click(function(){  
        location.reload();
    });



    var draw_datatable = $('#draw_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: false , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });    
    var history_datatable = $('#history_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: false , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var monthly_datatable = $('#report_monthly_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
        responsive: true,
        searching: true,
            // 정렬 기능 숨기기
        ordering: false,
            // 정보 표시 숨기기

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });


    var draw_admin_datatable = $('#draw_admin_datatable').DataTable({

         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: false , 
        responsive: true,
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]        

    });

    $('.move_admin_askparkinglot').click(function() {
        window.location.href = './admin_askparkinglot';
    });

    $('.dipose_btn').click(function() {
        var aid = $(this).attr("id"); 
        var wpid = $('#select_workplace'+aid).val();
        $.ajax({  
                    url:"/admin/draw_admin/dispose",  
                    method:"post",  
                    data:{aid:aid, wpid:wpid},  
                    success:function(data){  
                        alert(data);
                    }  
            });  

    });


    $('.confirm_btn').click(function() {
        var aid = $(this).attr("id"); 
        var wpid = $('#select_workplace'+aid).val();
        $.ajax({  
                    url:"/admin/draw_admin/confirm",  
                    method:"post",  
                    data:{aid:aid, wpid:wpid},  
                    success:function(data){  
                        alert(data);
                    }  
            });  

    });

    $('#draw_test_btn').click(function() {
        $('#draw_test_form').attr('action','/admin/draw_admin/draw_test');
        $('#draw_test_form').submit();


    });

    //$('textarea:not(.not_summernote)').summernote({        
    $('#summernote').summernote({

        height: 450,
        callbacks: {
                onImageUpload: function(files) {
                  // console.log(files);
                    file_no = 0;
                    sendFile(files, this);
            }
        }        
    });

    var file_no = 0;
    function sendFile(files,editor) {
        if(file_no == 0){
            loading();
        }
        if(files[file_no]){
            uploadFile(files,editor);
        }else{
            loading_hide();
        }
    }

function loading () {
    var html = '<div id="blockUI_spinner" data-spinner=\'{"radius": 100}\'></div>';
    $.blockUI({ message: html, overlayCSS: { backgroundColor: '#fff' }, css: { border: '0px', backgroundColor: ''} ,baseZ: 9999});
    $('#blockUI_spinner').spinner();
}

function loading_hide () {
    $.unblockUI();
}


function uploadFile(files,editor) {

    data = new FormData();
    data.append("upload", files[file_no]);
    var notice_no = $("#notice_no").val();
    data.append("file_no", notice_no);

    

    //'url' :  '/product/ing_update'    
    $.ajax({
        data: data,
        type: "POST",
        url: '/admin/handy/upload_file',

        cache: false,
        contentType: false,
        processData: false,
        success: function(xhr) {
            //alert(xhr.url);

            //console.log('xhr.url' , xhr.url);
            $(editor).summernote('editor.insertImage', xhr.url);
            // editor.insertImage(welEditable, xhr.url);
            file_no++;
            sendFile(files,editor);
        }
    });
}




    $('#btn_inandout_dwn').click(function(){
        //var search = 'inandout';      
        alert('btn_inandout_dwn');

        var date = $('#inandout_date').val();


        var loc = '/admin/stdb/download_inandout?date='+date;

        alert(loc);

        window.location.href = loc;    

        //alert('btn_inandout_download');
        // $.ajax({
        //     url:"/admin/stdb/download_inandout",  
        //     method:"post",  
        //     data:{date:date},
        //     success:function(data){
        //         if(data){
        //             alert(data);
        //         } else{
        //              alert('다운르도 실패 했습니다.');
        //         }
        //     }
        // });
    });


    

    $('#btn_ws_delete').click(function(){
        var wsid = $('input[name=id]').val();
        alert(wsid);
        if(confirm("정말 삭제 하시겠습니까?") == true) {
            $.ajax({
                url:"/admin/Datahandle/ws_delete",  
                method:"post",  
                data:{wsid:wsid},
                success:function(data){
                    if(data){
                        alert(data);
                        location.reload();
                    } else{
                        alert('근무형태 삭제에 실패 했습니다.');
                    }
                }
            });
        }
    });

    $('#btn_ww_delete').click(function(){
        //var wwid = $('#wwid').val();

        var wwid = $('input[name=id]').val();
        alert(wwid);
        if(confirm("정말 삭제 하시겠습니까?") == true) {
            $.ajax({
                url:"/admin/Datahandle/ww_delete",  
                method:"post",  
                data:{wwid:wwid},
                success:function(data){
                    if(data){
                        alert(data);
                        location.reload();
                    } else{
                        alert('근무편성 삭제에 실패 했습니다.');
                    }
                }
            });
        }
    });


    $('#btn_rank_delete').click(function(){
        //var wwid = $('#wwid').val();

        var rank_id = $('input[name=id]').val();
        if(confirm("정말 삭제 하시겠습니까?") == true) {
            $.ajax({
                url:"/admin/Datahandle/rank_delete",  
                method:"post",  
                data:{rank_id:rank_id},
                success:function(data){
                    if(data){
                        alert(data);
                        location.reload();
                    } else{
                        alert('직급 삭제에 실패 했습니다.');
                    }
                }
            });
        }
    });
   
   $('#submit_ment').click(function(){
    var aid = $('#ment_aid').val();
    var bid = $('#ment_bid').val();
    var ntid = $('#ment_ntid').val();
    var body = $('#ment_body').val();

    console.log('bid : ' , bid);    
    console.log('ntid : ' , ntid);
    // return;

    $.ajax({
        url:"/board/board/ment_save",
        method:"post",
        data:{aid:aid,bid:bid,ntid:ntid,body:body},
        success:function(data) {
            console.log('data' , data);
            $('#ment_add').append(data);
            location.reload();  
        }
    });
   });

   $('.reply_ment_btn').click(function(){
      var mid = $(this).attr("id"); 
      $('#div_reply_ment'+mid).slideToggle();
   });

   $('.submit_reply_ment').click(function(){
      var aid = $('#ment_aid').val();
      var bid = $('#ment_bid').val();
      var ntid = $('#ment_ntid').val();
      var parent_ment_id = $(this).attr("id");
      var body = $('#reply_ment_body'+parent_ment_id).val();
     $.ajax({
            url:"/board/board/ment_save",
            method:"post",
            data:{aid:aid,bid:bid,ntid:ntid,parent_ment_id:parent_ment_id,body:body},
            success:function(data) {
                console.log('data' , data);
                $('#reply_ment_add'+parent_ment_id).append(data);
                location.reload();  
            }
        });
   });

   $('.delete_ment_btn').click(function(){
      var mid = $(this).attr("id"); 
      confirm("정말 삭제하시겠습니까?");

      $.ajax({
            url:"/board/board/ment_delete",
            method:"post",
            data:{mid:mid},
            success:function(data) {
                location.reload();
            }
        });
      

     
   });

   $('.btn_bt_delete').click(function(){
        var btid = $('input[name=btid]').val();
      if(confirm("정말 삭제 하시겠습니까?") == true) {
        $.ajax({
            url:"/admin/board_manage/bt_delete",
            method:"post",
            data:{btid:btid},
            success:function(data) {
                location.reload();                
            }
        });
      }

   });


    $('.btn_board_delete').click(function(){
        var id = $('input[name=id]').val();
      if(confirm("정말 삭제 하시겠습니까?") == true) {
        $.ajax({
            url:"/admin/board_manage/delete",
            method:"post",
            data:{id:id},
            success:function(data) {
                location.reload();                
            }
        });
      }

   });

    $( ".district_change" ).change(function() {

      var selelct_opt = $(this).val();

      console.log('selelct_opt : ' , selelct_opt);      
      $.ajax({
            url:"/escalate/contractor/getworkplace",
            method:"post",
            data:{did:selelct_opt},
            success:function(data) {

                // console.log('data' , data);

                $('#new_contractor_wpid').empty();

                var list = data.parkinglot;
                // console.log('list :' , list.length);
                for(var count = 0; count < list.length; count++){     

                    var option = $("<option name='workplace' value="+list[count].wpid+">"+list[count].title+"</option>");
                    // console.log('option : ' , option);
                    $('#new_contractor_wpid').append(option);
                }
            }
      });

    });   


    $( ".district_change_inandout" ).change(function() {

      var selelct_opt = $(this).val();

      console.log('selelct_opt : ' , selelct_opt);      
      $.ajax({
            url:"/escalate/contractor/getworkplace",
            method:"post",
            data:{did:selelct_opt},
            success:function(data) {

                //console.log('data' , data);

                $('#inandout_wpid').empty();

                var list = data.parkinglot;
                // console.log('list :' , list.length);
                for(var count = 0; count < list.length; count++){     

                    var option = $("<option name='workplace' value="+list[count].wpid+">"+list[count].title+"</option>");
                    // console.log('option : ' , option);
                    $('#inandout_wpid').append(option);
                }
            }
      });

    });   




    $('.bt_status').click(function(){  

        var id = $(this).attr("id"); 

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        $.ajax({  
                url:"/admin/board_manage/bt_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();
                    } else{
                        location.reload();
                    }
                }
        });  
    });


    $('.bl_status').click(function(){  

        var id = $(this).attr("id"); 

        var this_id = '#'+id;

        //console.log('this id' , this_id);

        // check / non check 여부

        var checked = $('input:checkbox[id="'+id+'"]').is(":checked") ;

        //console.log('checked:' , checked);        

        var str = id.split('_');

        var id = str[1];

        var status = 0;

        if(checked == true){
            status = 1;
        }

        $.ajax({  
                url:"/admin/board_manage/bl_status",  
                method:"post",  
                data:{id:id , status:status},  
                success:function(data){  
                    //console.log('data' , data);
                    // return;
                    if(!data.status){
                         alert(data.msg);
                         location.reload();                    
                        
                    } else{
                        location.reload();
                    }
                }
        });  
    });

    
    $('.contractor_extend').click(function(){  
        console.log('extend');


        if(confirm('보고하시겠습니까? 최종 보고 전에 근무 지역과 주차장을 확인해 주세요') == true){
        var list = [];

        var did  = $('#new_contract_did').val();
        var wpid = $('#new_contractor_wpid').val();

        var access_type = $('#access_type').val();
        var ecid = $('#ecid').val();


        var proc = 1;

        // 먼저 유효성 검사를 합니다. 그래야 오류 ..
        $('#contract_extend_tbl tr').each(function(i) {
            //console.log('tr :');
          //alert('tr');
          //Cool jquery magic that lets me iterate over all the td only in this row

            var arr = {};
          
            var car_ = '#' + i + '_car_no';
            var car_no = $(car_).val();

            if(!car_no) return true;

            var ret = check_car_no(car_no);

            if(false == ret) {
                alert(i+1+'번째를 확인하세요');
                proc = 0;
                return false;
            }

            var pay_date_ = '#' + i + '_date';
            var pay_date = $(pay_date_).val();        


            var amount_ = '#' + i + '_amount';
            var amount = $(amount_).val();  

            if(!amount) {
                alert(i+1+'번째를 금액을 확인 하세요');
                proc = 0;
                return false;
            }


            var bNumber = Number.isInteger(amount);


            if(bNumber) {
                alert(i+1+'번째를 금액의 숫자를 확인 하세요');
                proc = 0;
                return false;
            }



            var etc_  = '#' + i + '_etc';  
            var etc = $(etc_).val();    


            var car_owner_ =  '#' + i + '_car_owner';  
            var car_owner = $(car_owner_).val();    


            var tel_number_ = '#' + i + 'tel_number';          
            var tel_number = $(tel_number_).val();        

            console.log('i : ' , i);
            console.log('car_ : ' , car_);
            console.log('car_no : ' , car_no);
            console.log('pay_date : ' , pay_date);
            console.log('amount_ : ' , amount_);
            console.log('amount : ' , amount);
            console.log('etc : ' , etc);
            //console.log('bNumber : ' , bNumber);

            console.log('tel_number : ' , tel_number);


            arr["car_no"] = car_no;
            arr["pay_date"] = pay_date;
            arr["amount"] = amount;
            arr["etc"] = etc;
            arr["car_owner"] = car_owner;
            arr["tel_number"] = tel_number;

            list.push(arr);

        });  
        // console.log('list ' , list);  
        // return;

        if(!proc) return false;
        
        var msg = '';

        for(var j = 0; j < list.length; j++){
            var arr = list[j];
            

           //console.log('arr : ' , arr);
           if(arr.car_no){
                $.ajax({  
                        url:"/escalate/contractor/extend_save",  
                        method:"post",  
                        async: false,
                        data:{
                            ecid:ecid,
                            car_no:arr.car_no , 
                            pay_date : arr.pay_date , 
                            amount: arr.amount, 
                            car_owner:arr.car_owner,
                            tel_num:arr.tel_number,    
                            etc:arr.etc,
                            did:did,
                            wpid:wpid}
                            ,  
                        success:function(data){  
                            // console.log('data' , data);
                            // console.log('j' , j);
                            // console.log('length' , list.length);

                            // $('#worktemplate_modal').html(data);  
                            // $('#wtpModal').modal("show");  

                             if(data.status){
                                //alert(data.msg);
                                //msg += (j) + '번째 저장에 성공 하였습니다.\n';
                                
                                alert(data.car_no+ ' 저장에 성공하였습니다 ');
                                //proc = 1;
                                //window.location.href = '/escalate/contractor/extend_list';
                                //location.reload();
                                if(j+1 == list.length){
                                    move_extend_list(access_type);
                                }
                            } else{
                                //msg += (j) + ' 번째 저장에 실패 하였습니다.\n';
                                //proc = 0;
                                alert(data.car_no+ ' 저장에 실패하였습니다. ');
                                if(j+1 == list.length){
                                    move_extend_list(access_type);
                                }

                                //location.reload();
                            }

                        }  
                });  

           }
        }

        }

    });

    function move_extend_list(access_type){
        if(access_type == 'admin'){
              window.location.href = '/admin/contractor/extend_list';    
        } else{
              window.location.href = '/escalate/contractor/extend_list';    
        }

    }

    function sum_coupon() {
        var half_coupon_amount = $('#half_coupon_amount').val();
        var hour_coupon_amount = $('#hour_coupon_amount').val();
        var onehalf_coupon_amount = $('#onehalf_coupon_amount').val();
        var twohour_coupon_amount = $('#twohour_coupon_amount').val();
        var total_coupon_amount = Number(half_coupon_amount) + Number(hour_coupon_amount) + Number(onehalf_coupon_amount) + Number(twohour_coupon_amount);
        $('#total_coupon_amount').val(total_coupon_amount);
    }


    $('#half_coupon').change(function(){
        var number = $('#half_coupon').val();

        var wpid = $('#new_contractor_wpid').val();

        

        $.ajax({  
                url:"/escalate/coupon/getinfo",  
                method:"post",  
                data:{
                    wpid:wpid
                },  
                success:function(data){  
                    console.log('data' , data);
                    var value = data.info.halfhour_coupon_amount;
                    var price = number * value;

                    console.log('price:' , price);
                    if(!price){
                        $('#half_coupon').val(price);                                    
                    }
                    $('#half_coupon_amount').val(price);

                    sum_coupon();

                    
                  

                }
        });


        

    });

     

    $('#hour_coupon').change(function(){
        var number = $('#hour_coupon').val();

        var wpid = $('#new_contractor_wpid').val();

        //console.log('number:' , number);

        $.ajax({  
                url:"/escalate/coupon/getinfo",  
                method:"post",  
                data:{
                    wpid:wpid
                },  
                success:function(data){  
                    console.log('data' , data);
                    var value = data.info.hour_coupon_amount;
                    var price = number * value;

                    console.log('price:' , price);
                    if(!price){
                        $('#hour_coupon').val(price);                                    
                    }
                    $('#hour_coupon_amount').val(price);
                    sum_coupon();

                }
        });

    });

    $('#onehalf_coupon').change(function(){
        var number = $('#onehalf_coupon').val();

        var wpid = $('#new_contractor_wpid').val();

        $.ajax({  
                url:"/escalate/coupon/getinfo",  
                method:"post",  
                data:{
                    wpid:wpid
                },  
                success:function(data){  
                    console.log('data' , data);
                    var value = data.info.onehalf_coupon_amount;
                    var price = number * value;

                    console.log('price:' , price);
                    if(!price){
                        $('#onehalf_coupon').val(price);                                    
                    }
                    $('#onehalf_coupon_amount').val(price);
                    sum_coupon();

                }
        });


        //console.log('number:' , number);
        //$('#onehalf_coupon_amount').val(number * 1440);

    });

    $('.btn_delete_cancel').click(function(){
        var id = $(this).attr("id");


        console.log('id :' , id);

        $.ajax({  
            url:"/escalate/contractor/cancel_delete",  
            method:"post",  
            data:{id:id},  
            success:function(data){
                console.log(data); 
                alert(data);
                location.reload(); 
            }
        });  
    });
    

    $('#twohour_coupon').change(function(){
        var number = $('#twohour_coupon').val();
        var wpid = $('#new_contractor_wpid').val();

        $.ajax({  
                url:"/escalate/coupon/getinfo",  
                method:"post",  
                data:{
                    wpid:wpid
                },  
                success:function(data){  
                    console.log('data' , data);
                    var value = data.info.twohour_coupon_amount;
                    var price = number * value;

                    console.log('price:' , price);
                    if(!price){
                        $('#twohour_coupon').val(price);                                    
                    }
                    $('#twohour_coupon_amount').val(price);
                    sum_coupon();

                }
        });

        //console.log('number:' , number);
        

    });

    // $('.btn_modify_cancel').click(function(){
    //     var id = $(this).attr("id");

    //     $.ajax({  
    //         url:"/escalate/contractor/cancel_modify",  
    //         method:"post",  
    //         data:{id:id},  
    //         success:function(data){  
              
    //         }
    //     });  

    // });
    

    // $('.btn_modify_change').click(function(){
    //     var ccid = $(this).attr("id");

    //     $.ajax({  
    //         url:"/escalate/contractor/change_",  
    //         method:"post",  
    //         data:{ccid:ccid},  
    //         success:function(data){  
              
    //         }
    //     });  

    // });
    
    $('.btn_delete_change').click(function(){
        var id = $(this).attr("id");

        $.ajax({  
            url:"/escalate/contractor/change_delete",  
            method:"post",  
            data:{id:id},  
            success:function(data){
                console.log(data); 
                alert(data);
                location.reload(); 
            }
        });  
    });

    // $('.btn_delete_coupon').click(function(){
    //     var id = $(this).attr("id");

    //     $.ajax({  
    //         url:"/admin/contractor/coupon_delete",  
    //         method:"post",  
    //         data:{id:id},  
    //         success:function(data){
    //             console.log(data); 
    //             alert(data);
    //             location.reload(); 
    //         }
    //     });  

    // });

    var coupon_datatable = $('#coupon_datatable').DataTable({
         // 표시 건수기능 숨기기
        lengthChange: true,
            
        searching: true,
            // 정렬 기능 숨기기
        ordering: true,
            // 정보 표시 숨기기
        info: true,
            // 페이징 기능 숨기기
        paging: true

    });



    coupon_datatable.on('click', '.btn_delete_coupon', function() {
         var id = $(this).attr("id");
        if(confirm('정말로 삭제하시겠습니까?')== true) {
            $.ajax({  
                url:"/admin/contractor/coupon_delete",  
                method:"post",  
                data:{id:id},  
                success:function(data){
                    console.log(data); 
                    alert(data);
                    location.reload(); 
                }
            });  
        }

        
    });





    $('.daily_input').change(function(){
        var daily_cash = Number($('#daily_cash').val());
        var daily_card = Number($('#daily_card').val());
        var daily_v_transfer = Number($('#daily_v_transfer').val());
        var daily_tmoney = Number($('#daily_tmoney').val());
        var daily_kakaot = Number($('#daily_kakaot').val());
        var daily_etc = Number($('#daily_etc').val());
        var daily_total;
        daily_total = daily_cash + daily_card + daily_v_transfer + daily_tmoney + daily_kakaot + daily_etc;
        
        $('#daily_total').val(daily_total);
    });

    // $('.daily_input_amount').change(function(){
    //     $('').val()
    // });

       $('.daily_input_amount').change(function(){
        var daily_cash_amount = Number($('#daily_cash_amount').val());
        var daily_card_amount = Number($('#daily_card_amount').val());
        var daily_v_transfer_amount = Number($('#daily_v_transfer_amount').val());
        var daily_tmoney_amount = Number($('#daily_tmoney_amount').val());
        var daily_kakaot_amount = Number($('#daily_kakaot_amount').val());
        var daily_etc_amount = Number($('#daily_etc_amount').val());
        var daily_total_amount;
        daily_total_amount = daily_cash_amount + daily_card_amount + daily_v_transfer_amount + daily_tmoney_amount + daily_kakaot_amount + daily_etc_amount;
        
        $('#daily_total_amount').val(daily_total_amount);
    });


    $('.monthly_input').change(function(){
        var monthly_cash = Number($('#monthly_cash').val());
        var monthly_card = Number($('#monthly_card').val());
        var monthly_transfer = Number($('#monthly_transfer').val());
        var monthly_web = Number($('#monthly_web').val());
        var monthly_v_transfer = Number($('#monthly_v_transfer').val());
        var monthly_etc = Number($('#monthly_etc').val());
        var monthly_total;
        monthly_total = monthly_cash + monthly_card + monthly_transfer + monthly_web + monthly_v_transfer + monthly_etc;
        
        $('#monthly_total').val(monthly_total);
    });


    $('.monthly_input_amount').change(function(){
        var monthly_cash_amount = Number($('#monthly_cash_amount').val());
        var monthly_card_amount = Number($('#monthly_card_amount').val());
        var monthly_transfer_amount = Number($('#monthly_transfer_amount').val());
        var monthly_web_amount = Number($('#monthly_web_amount').val());
        var monthly_v_transfer_amount = Number($('#monthly_v_transfer_amount').val());
        var monthly_etc_amount = Number($('#monthly_etc_amount').val());
        var monthly_total_amount;
        monthly_total_amount = monthly_cash_amount + monthly_card_amount + monthly_transfer_amount + monthly_web_amount + monthly_v_transfer_amount + monthly_etc_amount;
        
        $('#monthly_total_amount').val(monthly_total_amount);
    });

    $('.coupon_input').change(function(){
        var half_coupon = Number($('#half_coupon').val());
        var hour_coupon = Number($('#hour_coupon').val());
        var onehalf_coupon = Number($('#onehalf_coupon').val());
        var twohour_coupon = Number($('#twohour_coupon').val());
        var total_coupon;
        total_coupon = half_coupon + hour_coupon + onehalf_coupon + twohour_coupon;
        
        $('#total_coupon').val(total_coupon);

    

    });

    $('.coupon_input_amount').change(function(){
        var half_coupon_amount = Number($('#half_coupon_amount').val());
        var hour_coupon_amount = Number($('#hour_coupon_amount').val());
        var onehalf_coupon_amount = Number($('#onehalf_coupon_amount').val());
        var twohour_coupon_amount = Number($('#twohour_coupon_amount').val());
        var total_coupon_amount;
        total_coupon_amount = half_coupon_amount + hour_coupon_amount + onehalf_coupon_amount + twohour_coupon_amount;
        
        $('#total_coupon_amount').val(half_coupon_amount);
    });

    
});


