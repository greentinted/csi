<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_library {
	public static function getRecognizeColors() {
		$list =  SQL::DB('db')->fetchAll('SELECT * FROM tRECOGNIZE_COLOR');


		return $list;

	}

	public static function getRecognizeColor($rcid) {
		return  SQL::DB('db')->fetchRow('SELECT * FROM tRECOGNIZE_COLOR WHERE rcid= ?' , [$rcid]);
	}


	public static function getWorkPlacesWithColor(){

		$list = Work_library::getWorkPlaces();

		$cognize_color_list = Admin_library::getRecognizeColors();

		foreach ($list as $key => &$value) {
			# code...
			//$value['color'] = 
			if(!$value['rcid']){
				$value['color'] = '#000000';
				$value['color_title'] = '';
			} else {
				foreach ($cognize_color_list as $key => $value_) {
					# code...
					if($value['rcid'] == $value_['rcid']){
						$value['color'] = $value_['color'];
						$value['color_title'] = $value_['title'];

					}
				}
			}
		}

		return $list;
	}

	// public static function getWorkAssignWithColor(){

	// 	$list = Account_library::getWorkAssigns();

	// 	$cognize_color_list = Admin_library::getRecognizeColors();

	// 	foreach ($list as $key => &$value) {
	// 		# code...
	// 		//$value['color'] = 
	// 		if(!$value['rcid']){
	// 			$value['color'] = '#000000';
	// 			$value['color_title'] = '';
	// 		} else {
	// 			foreach ($cognize_color_list as $key => $value_) {
	// 				# code...
	// 				if($value['rcid'] == $value_['rcid']){
	// 					$value['color'] = $value_['color'];
	// 					$value['color_title'] = $value_['title'];

	// 				}
	// 			}
	// 		}
	// 	}

	// 	return $list;
	// }1




	public static function getWorkStyleWithColor(){
		$list = Account_library::getWorkStyles();
		$cognize_color_list = Admin_library::getRecognizeColors();
	}

	public static function getNavigationInfos($ptid = 1 , $status = 1 ){
		return SQL::DB('db')->fetchAll('SELECT * FROM tUSER_NAVIGATION  WHERE ptid = ? AND status = ? ORDER BY order_no', [$ptid , $status]);
	}


	public static function updateNavigationInfo($unid , $data){
		$cond = ['unid' => $unid];
		$rs = SQL::DB('db')->query('UPDATE tUSER_NAVIGATION SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;
	}


	public static function getNavigationInfosForAdmin($ptid = 1){
		return SQL::DB('db')->fetchAll('SELECT * FROM tUSER_NAVIGATION  WHERE ptid = ? ORDER BY order_no', [$ptid]);
		foreach ($list as $key => &$value) {
			# code...
			//$value['color'] = 
			if(!$value['rcid']){
				$value['color'] = '#000000';
				$value['color_title'] = '';
			} else {
				foreach ($cognize_color_list as $key => $value_) {
					# code...
					if($value['rcid'] == $value_['rcid']){
						$value['color'] = $value_['color'];
						$value['color_title'] = $value_['title'];

					}
				}
			}
		}

		return $list;
	}

		public static function getWorkWhenWithColor(){
		$list = Account_library::getWorkWhen();
		$cognize_color_list = Admin_library::getRecognizeColors();

		foreach ($list as $key => &$value) {
			# code...
			//$value['color'] = 
			if(!$value['rcid']){
				$value['color'] = '#000000';
				$value['color_title'] = '';
			} else {
				foreach ($cognize_color_list as $key => $value_) {
					# code...
					if($value['rcid'] == $value_['rcid']){
						$value['color'] = $value_['color'];
						$value['color_title'] = $value_['title'];
					}
				}
			}
		}

		return $list;
	}

	// public static function getWorkWhenWithColor($ptid = 1 , $status = 1){
	// 	return SQL::DB('db')->fetchAll('SELECT ww.wwid , ww.title , tc.color as color FROM tWORKWHEN as ww , tRECOGNIZE_COLOR as tc WHERE ww.status = ? AND ww.ptid= ? AND ww.rcid = tc.rcid', [$status , $ptid]);

	// }

	public static function getWorkTypeWithColor($is_admin, $status = 1){
		if($is_admin){
			return SQL::DB('db')->fetchAll('SELECT wt.wtid , wt.title ,wt.status,  wt.is_vacation, wt.is_duplicate, tc.color as color FROM tWORKTYPE as wt , tRECOGNIZE_COLOR as tc WHERE wt.rcid = tc.rcid');

		} else {
			return SQL::DB('db')->fetchAll('SELECT wt.wtid , wt.title ,wt.status,  wt.is_vacation, wt.is_duplicate , tc.color as color FROM tWORKTYPE as wt , tRECOGNIZE_COLOR as tc WHERE wt.status = ? AND wt.rcid = tc.rcid', [$status]);

		}
	}
	public static function getNavigationInfo($unid = 1){
		return SQL::DB('db')->fetchRow('SELECT * FROM tUSER_NAVIGATION  WHERE unid = ?', [$unid]);
	}


	public static function getWhiteips($is_admin=false, $ptid = 1 ,$status = 1){
		if($is_admin){
			return SQL::DB('db')->fetchAll('SELECT * FROM tWHITE_IP  WHERE ptid = ?', [$ptid]);
		} else {
			return SQL::DB('db')->fetchAll('SELECT * FROM tWHITE_IP  WHERE status = ? AND ptid = ?', [$status ,$ptid]);
		}
		

	}

	public static function getWhiteip($wiid){
		return SQL::DB('db')->fetchRow('SELECT * FROM tWHITE_IP  WHERE wiid = ?', [$wiid]);
	}

	public static function updateWhiteIp($wiid, $data){
		$cond = ['wiid' => $wiid];
		$rs = SQL::DB('db')->query('UPDATE tWHITE_IP SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;
	}

	public static function insertWhiteIp($data){
		$rs = SQL::DB('db')->query('INSERT INTO tWHITE_IP (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

		return $rs;

	}

	public static function insertWorkTimeHistory($data) {
		$rs = SQL::DB('db')->query('INSERT INTO tCHANGE_WORKING_TIME (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

		return $rs;
	}

	public static function deleteWhiteIp($wiid) {
		$rs = SQL::DB('db')->query('DELETE FROM tWHITE_IP WHERE wiid = ?',[$wiid]);

		return $rs;
	}

	


	public static function getWhiteipByRemoteAddress($remote_address, $ptid , $status =1 ){
		return SQL::DB('db')->fetchRow('SELECT * FROM tWHITE_IP  WHERE address = ? AND ptid = ? AND status = ?', [$remote_address , $ptid , $status]);
	}

	// public static function getBoardLists($ptid){
	// 	return SQL::DB('db')->fetchAll('SELECT * FROM tBOARD_LIST  WHERE ptid = ? ', [$ptid]);
	// }

	// // public static function getBoardInfo($blid){
	// // 	return SQL::DB('db')->fetchRow('SELECT * FROM tBOARD_LIST  WHERE blid = ? ', [$blid]);
	// // }

	// public static function getBoardInfo($blid){
	// 	return SQL::DB('db')->fetchRow('SELECT bl.blid, bl.btid, bt.typename, bl.title, bl.status, bl.placing from tBOARD_LIST bl, tBOARD_TYPE bt where bl.btid = bt.btid AND blid = ?', [$blid]);
	// }

	// public static function getBoardTypes($ptid){
	// 	return SQL::DB('db')->fetchAll('SELECT * FROM tBOARD_TYPE  WHERE ptid = ? ', [$ptid]);
	// }

	// public static function getBoardType($btid){
	// 	return SQL::DB('db')->fetchRow('SELECT * FROM tBOARD_TYPE  WHERE btid = ? ', [$btid]);
	// }

	// public static function getBoardTypeList($ptid){
	// 	$list = SQL::DB('db')->fetchAll('SELECT * FROM tBOARD_TYPE  WHERE ptid = ? ', [$ptid]);

	// 	$data = array();

	// 	foreach($list as $key => $value) {
	// 		$data[$value['btid']] = $value['typename'];
	// 	}

	// 	return $data;
	// }





}
