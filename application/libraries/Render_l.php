<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Render_l
{
    private $inc = '_inc';
    private $meta = array(
        'company'       => 'sdc'
        ,'title'        => '고객서비스혁신'
        ,'author'       => '성남도시개발공사'
        ,'description'  => '고객서비스혁신'
        ,'keywords'     => '성남도시개발공사'
        ,'image'        => ''
        ,'url'          => null
    );

    public function method()
    {
        parent::__construct();
    }

    public function json($param)
    {
        header('Content-Type: application/json');
        echo json_encode($param);
    }

    public function html($param = null)
    {
        $CI = & get_instance();

        $this->meta['url'] = base_url();

        $detail = false;
        if(isset($param['detail'])) {
            $detail = $param['detail'];
        }

        $html = array(
            'head'      => $this->_meta($detail)
            ,'title'    => $this->meta['title']
            ,'header'   => $CI->load->view($this->inc . '/header', null, true)
            ,'section'  => $param['section']
        );

        $CI->load->view($this->inc . '/html', $html);
    }

    public function popup($param = null) {
        $CI = & get_instance();

        $detail = false;
        if(isset($param['detail'])) {
            $detail = $param['detail'];
        }

        $html = array(
            'head'      => $this->_meta($detail)
            ,'title'    => $this->meta['title']
            ,'header'   => null
            ,'section'  => $param['section']
            ,'footer'   => null
            ,'position' => $param['position']
        );

        $CI->load->view($this->inc . '/html', $html);
    }

    private function _meta($detail = fail)
    {
        //상세페이지의 경우 메타 데이터 수정
        if(isset($detail)) {
            foreach ($this->meta as $k => $v) {
                if(isset($detail[$k])) {
                    $this->meta[$k] = $detail[$k];
                }
            }
        }
        $meta = array(
            'title'                     => $this->meta['title']
            ,'author'                   => $this->meta['author']
            ,'description'              => $this->meta['description']
            ,'keywords'                 => $this->meta['keywords']

            ,'og:type'                  => 'article'
            ,'og:url'                   => $this->meta['url']
            ,'og:title'                 => $this->meta['title']
            ,'og:site_name'             => $this->meta['title']
            ,'og:image'                 => $this->meta['image']
            ,'og:description'           => $this->meta['description']
            ,'og:locale'                => 'ko'
            ,'og:locale:alternate'      => 'ko_KR'

            ,'article:section'          => $this->meta['company']
            ,'article:author'           => $this->meta['author']
            ,'article:publisher'        => $this->meta['title']
            ,'article:published_time'   => ''
            ,'article:tag'              => $this->meta['keywords']
        );

        $str = null;

        foreach ($meta as $k => $v) {
            $attr = 'name';
            if(strpos($k, ':') !== FALSE) {
                $attr = 'property';
            }
            $str .= '<meta '. $attr .'="' . $k . '" content="' . $v . '" />' . "\n";
        }

        return $str;
    }

}
