<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arduino_library {

	public static function getSensingPlacesRawData(){
		
			return SQL::DB('iot')->fetchAll('SELECT * FROM tLOCATION WHERE status = 0 ');
	}


	public static function getAvgTempAndHumi($lid , $did ){

		return  SQL::DB('iot')->fetchRow('SELECT avg(temp) as tempature , avg(humi) as humiditiy, avg(pm25) as pm25  from tRECORD WHERE did = ? order by lid desc limit ? , ? ' , [$did , 0 , 10]);

	}

	public static function getDeviceAll(){

		return  SQL::DB('iot')->fetchAll('SELECT * FROM tLOCATION as lc, tDEVICE as dc WHERE  lc.lid = dc.lid');
		 
	}


	public static function getAllRecordlist(){

	return  SQL::DB('iot')->fetchAll('SELECT * FROM ARDUINO.tRECORD order by cre_date desc limit 20');
	 
	}

	public static function getRecordByLid($lid){
		return  SQL::DB('iot')->fetchAll('SELECT * FROM ARDUINO.tRECORD where lid = ? order by lid desc limit ? , ?' , [$lid , 0, 10]);

	}

	// public static function updateAccountLeave($aid , $data){
	// 	$cond = ['aid' => $aid];
	// 	$rs = SQL::DB('db')->query('UPDATE tACCOUNT_LEAVE SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

	// 	return $rs;
	// }



	// public static function insertAccount($data){
	// 	return SQL::DB('db')->query('INSERT INTO tACCOUNT (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));		
	// }

}
