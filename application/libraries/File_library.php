<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class File_library {
	public static function reArrayFiles(&$file_post) {

		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);

		// debug_log($_FILES);
		// throw new Exception("Error Processing Request", 409);

		// debug_log(['file_post' => $file_post]);
		// debug_log(['file_count' => $file_count]);
		// debug_log(['file_keys' => $file_keys]);

		for ($i = 0; $i < $file_count; $i++) {
			foreach ($file_keys as $key) {
				if (isset($file_post[$key]) && is_array($file_post[$key])) {
					$file_ary[$i][$key] = $file_post[$key][$i];
				} else {
					$file_ary[$i][$key] = $file_post[$key];
				}
			}
		}

		// debug_log(['file_ary' => $file_ary]);

		return $file_ary;

	}

	public static function uploadAttachedFile($file_key = '' , $folder ,$is_show_error = true, $is_image = true , $is_update = false ) {


		if (isset($_FILES[$file_key]['name'])) {
			if($_FILES[$file_key]['name']){
				$temp = explode("." , $_FILES[$file_key]['name']);
				//debug_log($temp);
				$ext = end($temp);
				$newfilename = strtolower(RandString(10)) . RandNum(20) . '.' . $ext;

				// if( !in_array($ext, $allowed_ext) ) {
				// 	throw new Exception("jpg,png,gif 파일만 업로드 가능합니다. => " . $ext, 409);
				// 	//return null;
				// }

				// 개발과정이 끝나면 수정하여야 함.
				$upload_dir = ASSETSPATH."/files/" ;


				$fullname = $upload_dir . $newfilename;
				$moved = move_uploaded_file($_FILES[$file_key]['tmp_name'], $fullname);
				// debug_log($fullname);
				// debug_log($moved);
				// debug_log($upload_dir);

				return $newfilename;

			} else {
				return '';
			}
		}

	}


	public static function uploadFile($file_key = '' , $folder ,$is_show_error = true, $is_image = true , $is_update = false ) {

		$allowed_ext = array('jpg','jpeg','png','gif');

		// debug_log($file_key);
		// debug_log($_FILES[$file_key]);
		// debug_log($_FILES[$file_key]['name']);

		if (isset($_FILES[$file_key]['name'])) {
			if($_FILES[$file_key]['name']){
				$temp = explode("." , $_FILES[$file_key]['name']);
				//debug_log($temp);
				$ext = end($temp);
				$newfilename = strtolower(RandString(10)) . RandNum(20) . '.' . $ext;

				if( !in_array($ext, $allowed_ext) ) {
					throw new Exception("jpg,png,gif 파일만 업로드 가능합니다. => " . $ext, 409);
					//return null;
				}

				// 개발과정이 끝나면 수정하여야 함.
				$upload_dir = ASSETSPATH."/images/profile/" ;


				$fullname = $upload_dir . $newfilename;
				$moved = move_uploaded_file($_FILES[$file_key]['tmp_name'], $fullname);
				// debug_log($fullname);
				// debug_log($moved);

				return $newfilename;

			} else {
				return '';
			}

			
		}

		throw new Exception("유효한 파일이 아닙니다."); 

	}


	public static function uploadImgFile($file_key = '' , $folder ,$is_show_error = true, $is_image = true , $is_update = false){

		$allowed_ext = array('jpg','jpeg','png','gif');

		// debug_log($file_key);
		// debug_log($_FILES[$file_key]);
		//debug_log($_FILES[$file_key]['name']);

		if (isset($_FILES[$file_key]['name'])) {
			if($_FILES[$file_key]['name']){
				$temp = explode("." , $_FILES[$file_key]['name']);
				//debug_log($temp);
				$ext = end($temp);
				$newfilename = strtolower(RandString(10)) . RandNum(20) . '.' . $ext;

				if( !in_array($ext, $allowed_ext) ) {
					throw new Exception("jpg,png,gif 파일만 업로드 가능합니다. => " . $ext, 409);
					//return null;
				}

				// 개발과정이 끝나면 수정하여야 함.
				$path = '/' . rand(1,9999);
				$path .= '/' .rand(1,9999);
				$path .= '/' .rand(1,9999);

				$upload_dir = ASSETSPATH."/images".$path ;

				$test_dir = mkdir($upload_dir,0700,true);






				$fullname = $upload_dir ."/" .$newfilename;
				$moved = move_uploaded_file($_FILES[$file_key]['tmp_name'], $fullname);

				// debug_log($upload_dir);
				// debug_log($fullname);
				// debug_log($moved);

				$arr = [];

				if($moved){
					
					$arr['path'] = $path;
					$arr['filename'] = $newfilename;

					// debug_log($arr);

					return $arr;
				} else{
					return $arr; 
				}

				//return $newfilename;

			} else {
				return '';
			}

			
		}

		throw new Exception("유효한 파일이 아닙니다."); 


	}

	public static function getUrl($path){
		$url = 'http://'.$_SERVER['HTTP_HOST'] . '/assets/images'.$path;

		debug_log($url);
		return $url;
	}

	public static function uploadCSVFile($file_key = '' , $folder ,$is_show_error = true, $is_image = false , $is_update = false ) {

		$allowed_ext = array('csv');

		// debug_log($file_key);
		// debug_log($_FILES[$file_key]);
		// debug_log($_FILES[$file_key]['name']);

		if (isset($_FILES[$file_key]['name'])) {
			if($_FILES[$file_key]['name']){
				$temp = explode("." , $_FILES[$file_key]['name']);
				//debug_log($temp);
				$ext = end($temp);

				if( !in_array($ext, $allowed_ext) ) {
					throw new Exception("jpg,png,gif 파일만 업로드 가능합니다. => " . $ext, 409);
					//return null;
				}

				return true;;

			} else {
				return '';
			}

			
		}

		throw new Exception("유효한 파일이 아닙니다."); 



	}

	public static function insertFileDB($data){
		return SQL::DB('csi')->query('INSERT INTO tFILE (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

	}

}