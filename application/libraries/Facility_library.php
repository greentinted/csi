<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Facility_library {

	public static function getFaciltiyList($status = 1, $view = 1){

		return  SQL::DB('csi')->fetchAll('SELECT fc_id , title , fut_code , address , lat , lng , did , contact FROM tFACILITY where status = ? and p_view = ?' , [$status , $view]);


		// return  SQL::DB('csi')->fetchAll('SELECT fc_id , title , fut_code , address , lat , lng , did FROM tFACILITY where status = ?' , [$status]);

	}


	public static function getMDustNowList($status = 1, $view = 1){

		return  SQL::DB('csi')->fetchAll('SELECT sd_code,create_date, temp , pm10 , humi , pm25
			FROM(
				SELECT sd_code,create_date, temp , pm10 , humi , pm25
				FROM tSENSOR_RECORD
				ORDER BY create_date DESC
			) AS A
		GROUP BY sd_code');

		// return  SQL::DB('csi')->fetchAll('SELECT fc_id , title , fut_code , address , lat , lng , did FROM tFACILITY where status = ?' , [$status]);
	}

	public static function getFaciltyInfo($sd_code){
		return  SQL::DB('csi')->fetchRow('SELECT f.title, f.fut_code, f.description,  d.sl_id  , l.fc_id , f.lat , f.lng , f.address 
		FROM tSENSOR_DEVICE as d , tSENSOR_LOCATION as l , tFACILITY as f   
		where d.sd_code = ? and l.sl_id = d.sl_id and l.fc_id = f.fc_id 
		group by f.fc_id' , [$sd_code]);


	}


	public static function getFaciltyFromAid($aid){
		return  SQL::DB('csi')->fetchRow('SELECT * FROM tACCOUNT_ADD
		WHERE aid = ?', [$aid]);
	}

	public static function getFaciltyFromFcid($fc_id){
		return  SQL::DB('csi')->fetchRow('SELECT fut_code
		FROM tFACILITY
	WHERE fc_id = ?', [$fc_id]);

	}

	public static function getFaciltyInfoByFutCode($fut_code , $status = 1){

		if($fut_code == 'NOPL'){
			return SQL::DB('csi')->fetchAll('SELECT f.fc_id , f.title , f.contact, p.address_road , p.address_no , f.lat , f.lng , p.building_type , p.building_height , p.control_vendor , p.daily_reconcile_type , p.monthly_reconcile_type , p.p_avb_cnt ,p.d_avb_cnt , p.preg_avb_cnt , p.monthly_fare , p.daily_fare , p.basic_fare , p.add_fare ,p.operating_time , p.electric_charge_number as echarger_cnt
			FROM tFACILITY as f , tPARKINGLOT_ETC as p 
			WHERE f.fc_id = p.fc_id  AND f.status = ? AND f.fut_code = ?' , [$status ,  $fut_code]);



		} else if($fut_code == 'NSPL'){

			return SQL::DB('csi')->fetchAll('SELECT title , address , lat , lng , description as avb FROM tFACILITY WHERE fut_code = ? AND status = ? ' , [$fut_code , $status]);
		} else {
			return '';
		}

		

	}


	public static function getFaciltyInfoBydcode($dcode , $status = 1){

			return SQL::DB('csi')->fetchAll('SELECT fc_id, title, fut_code, dcode, did
			FROM tFACILITY as f 
			WHERE dcode = ?  AND status = ?' , [$dcode, $status]);	

	}




	public static function types(){
		return  SQL::DB('csi')->fetchAll('SELECT title, fct_code , description FROM tFACILITY_UNIQUE_TYPE where status = ?' , [true]  );

	}


	public static function getImgs($fc_id){
		return  SQL::DB('csi')->fetchAll('SELECT ff_id, fc_id, path, filename FROM tFACILITY_FILE where status = ? AND fc_id = ?' , [true , $fc_id]  );

	}

}
