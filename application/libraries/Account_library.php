<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account_library {
	public static function getAccount($aid = '') {
		$account_row = static::getUserRow($aid);
		$Authority = array();
		$Authority["read"]["Super_Admin"] = "admin|admin_dashboard|";
		$Authority["read"]["Admin"] = "admin|admin_dashboard|";


		$Authority["read"]["Publisher"] = "admin";
		$Authority["write"]["Publisher"] = "admin";


		if ($account_row) {
			$account_row = array_change_key_case($account_row);

			if ($account_row['authority'] && isset($Authority['read'][$account_row['authority']])) {

				$read_tmp_grant = explode('|', $Authority['read'][$account_row['authority']]);
				$account_row['read_grant_list'] = array_flip($read_tmp_grant);
			}

			if ($account_row['authority'] && isset($Authority['write'][$account_row['authority']])) {
				$write_tmp_grant = explode('|', $Authority['write'][$account_row['authority']]);
				$account_row['write_grant_list'] = array_flip($write_tmp_grant);
			}
		}
		// debug_var($admin_row);
		// exit;
		return $account_row;
	}

	public static function getUserRow($aid){
		$account =  SQL::DB('csi')->fetchRow('SELECT a.id , a.email , a.contact_number , a.password, a.aid , a.name , a.admin_type ,  a.birth , a.address , a.gender , b.ref_id , b.fct_code , b.user_type,b.fc_id  , c.title , c.fut_code, d.title, c.title as facility FROM tACCOUNT as a , tACCOUNT_ADD as b , tFACILITY as c , tDIVISION as d WHERE a.aid = ? AND a.aid = b.aid AND b.fc_id = c.fc_id AND c.dcode = d.dcode', [$aid]);
		
		return $account;
	}

	public static function getUserRowByID($id){
		$account =  SQL::DB('csi')->fetchRow('SELECT id FROM tACCOUNT WHERE id = ?', [$id]);
		
		return $account;

	}

	public static function getBelongDivision($futcode){
		return SQL::DB('csi')->fetchAll('SELECT fc_id, title FROM tFACILITY WHERE fut_code =?',[$futcode]);
	}

	public static function setBelongDepartment($aid){
		return SQL::DB('csi')->fetchRow('SELECT a.aid as aid, b.title as title, a.fc_id as fc_id, a.fct_code as fct_code, b.dcode as dcode, b.did as did FROM tACCOUNT_ADD as a, tFACILITY as b  WHERE a.fc_id = b.fc_id AND a.aid = ?',[$aid]);
	}

	public static function getUserType(){
		return SQL::DB('csi')->fetchAll('SELECT DISTINCT user_type FROM tACCOUNT_ADD');
	}

	public static function getDivisionList(){
		return SQL::DB('csi')->fetchAll('SELECT DISTINCT title, dcode FROM tDIVISION');
	}

	public static function getDivisionFacility($dcode){
		return SQL::DB('csi')->fetchAll('SELECT dcode, title FROM tFACILITY where dcode = ?',[$dcode]);
	}

	public static function getDivisionFacility1(){
		return SQL::DB('csi')->fetchAll('SELECT dcode, fc_id, title FROM tFACILITY ORDER BY dcode desc');
	}

	public static function getAccountID($aid){
		return  SQL::DB('db')->fetchRow('SELECT ID FROM tADMIN_ACCOUNT WHERE aid = ?', [$aid]);
		
	}

	public static function getWorkPlacesData($ptid = 1 , $status = 1){
		$list = [];
		$list = SQL::DB('db')->fetchAll('SELECT * FROM tWORKPLACE WHERE status = ? AND ptid = ?' , [$status , $ptid]);

		$data = array();

		foreach($list as $key => $value) {
			$data[$value['wpid']] = $value['title'];
		}


		return $data;
	}


	public static function getSuperAdminData(){
		$list = [];
		$list = SQL::DB('db')->fetchAll('SELECT * FROM tACCOUNT WHERE user_type = ?'  , ['super_admin']);
		$data = array();

		foreach($list as $key => $value) {
			$data[$value['aid']] = $value['id'];
		}
		return $data;

	}

	public static function getAccountRawList($ptid , $status=1 , $start=0, $end=1000){
		$list =  SQL::DB('db')->fetchAll('SELECT * FROM tACCOUNT WHERE ptid = ? AND status = ? order by aid desc LIMIT ?, ? ', [$ptid, $status , $start , $end]);

		//.return $list;
	}

	public static function getAccountList_($status=1 , $start=0, $end=1000 ){

		$list =  SQL::DB('csi')->fetchAll('SELECT *, a.address as address1 FROM tACCOUNT as a , tACCOUNT_ADD as b , tFACILITY as c  WHERE a.status = ? AND a.aid = b.aid AND b.fc_id = c.fc_id'  , [$status]);




		//$work_templates = Work_library::getWorkingTemplates($status, $ptid);


		//foreach ($list as $key => &$value) {
			# code...

			//$work_template = Work_library::getWorkTemplateRow($value['wtlt_id']);
		//}

		return $list;


	}

	public static function getAccountListData($fct_code=' ', $status=1 , $admin_type='user', $start=0, $end=10000 ){

		 // $list =  SQL::DB('csi')->fetchAll('SELECT * FROM tACCOUNT  WHERE status = ? AND admin_type not in (?) order by aid desc LIMIT ?, ? ', [$status , $admin_type, $start , $end]);

		$list2 = SQL::DB('csi')->fetchAll('SELECT a.aid as aid, a.name as name, b.fct_code as fct_code, d.title as title FROM tACCOUNT as a, tACCOUNT_ADD as b, tFACILITY as c, tDIVISION as d WHERE a.aid = b.aid AND b.fc_id = c.fc_id AND c.dcode = d.dcode AND b.fct_code = ? AND a.status = ? AND admin_type not in (?) order by a.name ASC LIMIT ?, ?',[$fct_code, $status , $admin_type, $start, $end]);
		 // debug_var($list2);
		 // exit;

		$data = array();

		foreach($list2 as $key => $value) {
			$data[$value['aid']] = $value['name'];
		}

		 // debug_var($data);
		 // exit;
		return $data;

	}

		public static function getAdminTypeList($type){
		$list =  SQL::DB('csi')->fetchAll('SELECT DISTINCT admin_type FROM tACCOUNT
		WHERE admin_type not in (?)',[$type]);

		// foreach ($list as $key => &$value) {
		// 	# code...

		// 	$work_template = Work_library::getWorkTemplateRow($value['wtlt_id']);

		// 	// debug_var($work_template);
		// 	// exit;

		// 	$leave_info = self::getLeveInfo($value['aid']);
		// 	$value['leave_info'] = $leave_info;


		// 	if($value['ptid']){
		// 		$value['ptid_name'] = Work_library::getParkingLotNameByID($value['ptid']);
		// 	} else {
		// 		$value['ptid_name'] = '';
		// 	}

		// 	if($value['wpid']){
		// 		$value['wpid_name'] = Work_library::getWorkingPlaceNameByID($value['wpid']);
		// 	} else {
		// 		$value['wpid_name'] = '';
		// 	}

		// 	if($value['waid']){
		// 		$value['waid_name'] = Work_library::getWorkingAssignNameByID($value['waid']);
		// 	} else {
		// 		$value['waid_name'] = '';
		// 	}
		// 	if(isset($work_template['wwid'])){
		// 		$value['wwid_name'] = Work_library::getWorkingWhenNameByID($work_template['wwid']);
			
		// 	} else {
		// 		$value['wwid_name'] = '';
		// 	}

		// 	if(isset($work_template['wsid'])){
		// 		$value['wsid_name'] = Work_library::getWorkingStyleNameByID($work_template['wsid']);
		// 	} else {
		// 		$value['wsid_name'] = '';
		// 	}
		// }

		// $end = get_time();
		// $time = $end - $start;
		// debug_var($list);
		// debug_var($time);
		// exit;


		return $list;


	}

	public static function getAccountList($status=1 , $start=0, $end=1000 ){
		$list =  SQL::DB('csi')->fetchAll('SELECT * FROM tACCOUNT   WHERE status = ? order by aid desc LIMIT ?, ? ', [$status , $start , $end]);

		// foreach ($list as $key => &$value) {
		// 	# code...

		// 	$work_template = Work_library::getWorkTemplateRow($value['wtlt_id']);

		// 	// debug_var($work_template);
		// 	// exit;

		// 	$leave_info = self::getLeveInfo($value['aid']);
		// 	$value['leave_info'] = $leave_info;


		// 	if($value['ptid']){
		// 		$value['ptid_name'] = Work_library::getParkingLotNameByID($value['ptid']);
		// 	} else {
		// 		$value['ptid_name'] = '';
		// 	}

		// 	if($value['wpid']){
		// 		$value['wpid_name'] = Work_library::getWorkingPlaceNameByID($value['wpid']);
		// 	} else {
		// 		$value['wpid_name'] = '';
		// 	}

		// 	if($value['waid']){
		// 		$value['waid_name'] = Work_library::getWorkingAssignNameByID($value['waid']);
		// 	} else {
		// 		$value['waid_name'] = '';
		// 	}
		// 	if(isset($work_template['wwid'])){
		// 		$value['wwid_name'] = Work_library::getWorkingWhenNameByID($work_template['wwid']);
			
		// 	} else {
		// 		$value['wwid_name'] = '';
		// 	}

		// 	if(isset($work_template['wsid'])){
		// 		$value['wsid_name'] = Work_library::getWorkingStyleNameByID($work_template['wsid']);
		// 	} else {
		// 		$value['wsid_name'] = '';
		// 	}
		// }

		// $end = get_time();
		// $time = $end - $start;
		// debug_var($list);
		// debug_var($time);
		// exit;


		return $list;


	}
	public static function getAccountListOperator($status=1 , $admin_type='user', $start=0, $end=1000 ){
		$list =  SQL::DB('csi')->fetchAll('SELECT * FROM tACCOUNT   WHERE status = ? AND admin_type not in (?)  order by aid desc LIMIT ?, ? ', [$status ,$admin_type , $start , $end]);
		return $list;


	}

	public static function setLogAccountLog($account_row) {

		$data = [];
		// 일반 유저 ..		
		// debug_var($account_row);
		// exit;

		$data['aid'] = $account_row['aid'];
		$data['signin_ip'] = REMOTE_ADDR;
		$data['activity'] = 'login';
		$data['user_type']  = $account_row['user_type'];
		$data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		// SQL::DB('db')->query('INSERT INTO tLOG_LOGINOUT (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));		

		$ses = RandString(20);

		//ses update //

		SQL::DB('csi')->query('UPDATE tADMIN_ACCOUNT SET ses=? WHERE aid = ?', [$ses, $account_row['aid']]);		
		
		return $ses;
	}

	public static function insertAccount($data){
		return SQL::DB('csi')->query('INSERT INTO tACCOUNT (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));
	}
	public static function insertAccountAdd($data){
		return SQL::DB('csi')->query('INSERT INTO tACCOUNT_ADD (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));
	}

	public static function insertAccountETC($etc_data){
		return SQL::DB('csi')->query('INSERT INTO tACCOUNT_ETC (' . implode(',', array_keys($etc_data)) . ') VALUES(' . db_array_to_values($etc_data) . ')', array_values($etc_data));		
	}

	public static function insertAccountLeave($leave_data){
		return SQL::DB('db')->query('INSERT INTO tACCOUNT_LEAVE (' . implode(',', array_keys($leave_data)) . ') VALUES(' . db_array_to_values($leave_data) . ')', array_values($leave_data));		

	}

	public static function setLogOutAccountLog($account_row) {

		$data = [];

		// 일반 유저 ..		

		$data['aid'] = $account_row['aid'];
		$data['signin_ip'] = REMOTE_ADDR;
		$data['activity'] = 'logout';
		$data['ptid'] = $account_row['ptid'];
		$data['user_type']  = $account_row['user_type'];
		$data['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
		$data['mac_address'] = $_SERVER['HTTP_USER_AGENT'];
		SQL::DB('db')->query('INSERT INTO tLOG_LOGINOUT (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));		

		$ses = '';

		//ses update //

		SQL::DB('db')->query('UPDATE tACCOUNT SET ses=? WHERE aid = ?', [$ses, $account_row['aid']]);		
		
		return $ses;
	}

	public static function getRank($status=1){
		return SQL::DB('db')->fetchAll('SELECT rid , title , status FROM tRANK WHERE status = ?', [$status]);
	}


	public static function getWorkPlaceRow($wpid , $ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchRow('SELECT wp.wpid as wpid , wp.title as title  , wp.ch  as ch, wp.rcid , wp.did, dt.title as region dt.did as did FROM tWORKPLACE as wp , tDISTRICT as dt  WHERE wp.status = ? AND wp.ptid = ? AND wp.did = dt.did AND wp.wpid = ? ', [$status , $ptid , $wpid]);
	}

	// public static function getWorkAssigns($ptid = 1 , $status = 1){
	// 	return SQL::DB('db')->fetchAll('SELECT wa.waid as waid , wa.title as title  , wa.rcid , pt.title as pttitle FROM tWORKASSIGN as wa , tPARKTYPE as pt  WHERE wa.status = ? AND wa.ptid = ? AND wa.ptid = pt.ptid', [$status , $ptid]);
	// }

	public static function getWorkAssignRow($waid , $ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchRow('SELECT wa.waid as waid , wa.title as title , wa.rcid , pt.title as pttitle FROM tWORKASSIGN as wa , tPARKTYPE as pt WHERE wa.status = ? AND wa.ptid = ? AND wa.ptid = pt.ptid AND wa.wsid = ? ', [$status , $ptid , $waid]);
	}



//	select wp.wpid , wp.title, wp.ch , wp.ptid  dt.title from tWORKPLACE as wp, tDISTRICT as dt WHERE wp.did = dt.did AND wp.status = 1 AND ptid = 1;

	public static function getWorkAssign($ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchAll('SELECT waid , title FROM tWORKASSIGN  WHERE status = ? AND ptid = ?', [$status , $ptid]);

	}

	public static function getWorkStyle($ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchAll('SELECT wsid , title FROM tWORKSTYLE  WHERE status = ? AND ptid = ?', [$status , $ptid]);

	}

		public static function getWorkWhen($ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchAll('SELECT ww.wwid as wwid , ww.title as title  , ww.rcid , pt.title as pttitle FROM tWORKWHEN as ww , tPARKTYPE as pt  WHERE ww.status = ? AND ww.ptid = ? AND ww.ptid = pt.ptid', [$status , $ptid]);
	}

		public static function getWorkStyles($ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchAll('SELECT ws.wsid as wsid , ws.title as title  , ws.rcid , pt.title as pttitle FROM tWORKSTYLE as ws , tPARKTYPE as pt  WHERE ws.status = ? AND ws.ptid = ? AND ws.ptid = pt.ptid', [$status , $ptid]);
	}

	public static function getWorkStyleRow($wsid , $ptid = 1 , $status = 1){
		return SQL::DB('db')->fetchRow('SELECT ws.waid as wsid , ws.title as title , ws.rcid , pt.title as pttitle FROM tWORKSTYLE as ws , tPARKTYPE as pt WHERE ws.status = ? AND ws.ptid = ? AND ws.ptid = pt.ptid AND ws.wsid = ? ', [$status , $ptid , $wsid]);
	}

	public static function getDistrictType($status = 1){

		$list = [];

		$list = SQL::DB('db')->fetchAll('SELECT did , title FROM tDISTRICT ' );

		$data = array();

		foreach ($list as $key => $value) {
			# code...

			//$title = $value['column_type_title'];
			//$data1 = array($title => $title);
			// debug_var($value);
			// exit;
			$data[$value['did']] = $value['title'];
			//array_push($data1, $data1);
		}

		return $data;

	// public static function getWorkAssign($status = 1){

	// 	$list = [];

	// 	$list = SQL::DB('db')->fetchAll('SELECT did , title FROM tDISTRICT ' );

	// 	$data = array();

	// 	foreach ($list as $key => $value) {
	// 		# code...

	// 		//$title = $value['column_type_title'];
	// 		//$data1 = array($title => $title);
	// 		// debug_var($value);
	// 		// exit;
	// 		$data[$value['did']] = $value['title'];
	// 		//array_push($data1, $data1);
	// 	}

	// 	return $data;

	}

	// public static function getWorkPlaceRow($wpid , $ptid = 1 , $status = 1){
	// 	return SQL::DB('db')->fetchRow('SELECT wp.wpid as wpid , wp.title as title  , wp.ch  as ch, wp.rcid , wp.did, dt.title as region FROM tWORKPLACE as wp , tDISTRICT as dt  WHERE wp.status = ? AND wp.ptid = ? AND wp.did = dt.did AND wp.wpid = ? ', [$status , $ptid , $wpid]);
	// }

	//	select wp.wpid , wp.title, wp.ch , wp.ptid  dt.title from tWORKPLACE as wp, tDISTRICT as dt WHERE wp.did = dt.did AND wp.status = 1 AND ptid = 1;





	public static function getAccountFullInfo($aid){

		//debug_var($aid);

		$account = Account_library::getUserRow($aid);



		// $account['profile_img'] = self::setProfileImg($account['profile_img']);

		
		// $account['parktype_info'] = Work_library::getParkingLotTypeRow($account['ptid']);
		// $account['rank_info'] = Work_library::getRankRow($account['rid']);
		// $account['wp_info'] = Work_library::getWorkplaceRow($account['wpid']);

		// $account['wtlt_info'] = Work_library::getWorkTemplateRow($account['wtlt_id']);


		// $account['wa_info'] = Work_library::getWorkAssignRow($account['waid']);
		// $account['ww_info'] = Work_library::getWorkWhenRow($account['wtlt_info']['wwid']);
		// $account['ws_info'] = Work_library::getWorkStyleRow($account['wtlt_info']['wsid']);

		// $account['leave_info'] = self::getLeveInfo($account['aid']);


		return $account;
	}

	public static function getLeveInfo($aid){
		return SQL::DB('db')->fetchRow('SELECT * FROM tACCOUNT_LEAVE WHERE aid = ? ', [$aid]);

	}


	public static function setProfileImg($img){
		$profile_img = '';
		if(!$img){
			$profile_img = base_url() .'assets/images/profile/human.png';
		} else {
			$profile_img = base_url() .'assets/images/profile/'. $img;
		}

		return $profile_img;

	}


	public static function updateAccountLeave($aid , $data){
		$cond = ['aid' => $aid];
		$rs = SQL::DB('db')->query('UPDATE tACCOUNT_LEAVE SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;
	}

	public static function updateUseLeave($aid, $use_data){
		$rs = SQL::DB('db')->query('UPDATE tACCOUNT_LEAVE SET use_leave_days = use_leave_days - ? WHERE aid = ?' , [$use_data , $aid]);

		return $rs; 
	}

	public static function insertBoard($data){
		$rs = SQL::DB('csi')->query('UPDATE tACCOUNT SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;
	}

	public static function updateMyinfo($rs){
		$rs = SQL::DB('csi')->query('$rs');

		return $rs; 
	}

	public static function updateAccount($aid,  $data){
		$cond = ['aid' => $aid];
		$rs = SQL::DB('csi')->query('UPDATE tACCOUNT SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;

	}
	public static function updateAccountAdd($aid,  $data){
		$cond = ['aid' => $aid];
		$rs = SQL::DB('csi')->query('UPDATE tACCOUNT_ADD SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;

	}


}
