<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ment_library {
	public static function getMents($bid , $parent_ment_id , $is_admin = true  ,$status = 1) {
		$list = [];

		$list = SQL::DB('csi')->fetchAll('SELECT a.mid , a.aid , a.bid , a.child_ment_count , a.parent_ment_id , a.body, a.create_time, a.is_admin , b.name FROM tMENT AS a , tACCOUNT AS b WHERE bid=? AND parent_ment_id = ? AND a.status = ? AND a.aid = b.aid ORDER BY create_time asc' , [$bid, $parent_ment_id , $status]);	
		return $list;
	}

	public static function insertMent($data) {
		return SQL::DB('csi')->query('INSERT INTO tMENT ('. implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));
	}

	public static function getMentInfo($mid) {
		$row = SQL::DB('csi')->fetchRow('SELECT * FROM tMENT WHERE mid = ? ' , [$mid]);	
		return $row;

	}

	public static function updateChildCount($parent_ment_id, $count) {
		return SQL::DB('csi')->query('UPDATE tMENT SET child_ment_count = ? WHERE mid = ?',[$count,$parent_ment_id]);
	}

	public static function deleteMent($mid) {
		return SQL::DB('csi')->query('UPDATE tMENT SET status = 0 WHERE mid = ?',[$mid]);
	}
	public static function fakedeleteMent($mid) {
		return SQL::DB('csi')->query('UPDATE tMENT SET body = "삭제된 댓글 입니다." WHERE mid = ?',[$mid]);
	}

	public static function updateMent($mid , $data) {
		$cond = ['mid' => $mid];
		$rs = SQL::DB('csi')->query('UPDATE tMENT SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

		return $rs;
	}

}
