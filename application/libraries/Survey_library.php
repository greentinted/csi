<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Survey_library {
	public static function getList() {

		return SQL::DB('csi')->fetchAll('SELECT * FROM tSURVEYCONF ORDER BY sc_id desc');

	}

	public static function getList1($ssid) {

		return SQL::DB('csi')->fetchAll('SELECT * FROM tSURVEYCONF  WHERE sc_id = ? ', [$ssid]);

	}

	public static function GetLastOrderNum() {

		return SQL::DB('csi')->fetchAll('SELECT * FROM tSURVEYCONF WHERE `order` = (SELECT max(`order`) FROM tSURVEYCONF)');
	}	

	public static function ExistsOrderNum($order) {

		return SQL::DB('csi')->fetchRow('SELECT EXISTS (SELECT * FROM tSURVEYCONF WHERE `order`=?) as success', [$order]);
	}	

	public static function IntoServeyConf($sql){
		return SQL::DB('csi')->query($sql);
	}

	public static function GetLastEndTime($status = 1) {

	return SQL::DB('csi')->fetchAll('SELECT MIN(`from`)as start_date, MAX(`to`) as end_date FROM tSURVEYCONF WHERE status = ?', [$status]);
	}	

	public static function GetCovid19List() {

		return SQL::DB('csi')->fetchAll('SELECT * FROM tCOVID19 ORDER BY cid desc ');

	}

	public static function GetCovid19List1($ssid) {

		return SQL::DB('csi')->fetchAll('SELECT * FROM tCOVID19  WHERE cid = ? ', [$ssid]);

	}

	public static function GetDivisionList($status = 1) {

		return SQL::DB('csi')->fetchAll('SELECT title FROM tDIVISION WHERE status = ?', [$status]);

	}

	public static function Latest_Mod_Date() {

		return SQL::DB('csi')->fetchAll('SELECT MAX(mod_date) as latest_date FROM tCOVID19 ');

	}

	public static function CovidDivisionTotal($status = 1) {

		return SQL::DB('csi')->fetchAll('SELECT distinct(dcode), sum(description) as total FROM tCOVID19 WHERE status = ? GROUP BY dcode', [$status]);

	}


	public static function CovidTotal($dcode, $status = 1) {

		return SQL::DB('csi')->fetchAll('SELECT description as people FROM tCOVID19 WHERE dcode = ? and status = ?', [$dcode, $status]);

	}

}
