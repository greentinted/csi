<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info_library {
	public static function getList($aid = 0 , $is_admin = false) {
		return SQL::DB('csi')->fetchAll('SELECT a.bid , a.title as title, b.title as pstitle, c.title as optitle, a.description, a.view_count, a.share , d.name  , a.create_date  , a.admin_aid , c.color_code as optitle_color , a.ps_code as ps_code FROM tBOARD as a , tPROCESSING_STATUS as b, tOPINIONTYPE as c , tACCOUNT as d WHERE  a.status = ? and a.optype_code = c.optype_code and a.ps_code = b.ps_code and a.writer_aid = d.aid ORDER BY a.create_date desc' , [true]);							
	}


	public static function updateViewCount($bid){
		$rs = SQL::DB('csi')->query('UPDATE tBOARD SET view_count = view_count+1 WHERE bid=?' , [$bid]);

		return $rs;
	}


	public static function getBoard($bid){
		return  SQL::DB('csi')->fetchRow('SELECT a.bid , a.title as title, b.title as pstitle, c.title as optitle, a.description, a.view_count, a.share , d.name  , a.create_date , a.writer_aid , b.ps_code ,a.share  ,a.optype_code, IF(DATE_SUB(NOW(), INTERVAL 3 DAY) <= a.create_date, 1, 0) AS new_icon FROM tBOARD as a , tPROCESSING_STATUS as b, tOPINIONTYPE as c , tACCOUNT as d WHERE a.bid=? and a.optype_code = c.optype_code and a.ps_code = b.ps_code and a.writer_aid = d.aid ORDER BY a.create_date desc' , [$bid]);						
	}


	public static function getBeforeAfterList($bid){
		$list = [];
		$list['prev'] = SQL::DB('csi')->fetchRow('SELECT bid, title  FROM tBOARD WHERE bid < ? AND status = ?  ORDER BY bid DESC LIMIT 1' , [$bid, true]);

		$list['next'] = SQL::DB('csi')->fetchRow('SELECT bid, title  FROM tBOARD WHERE bid > ? AND status = ?  ORDER BY bid  LIMIT 1' , [$bid, true]);


		return $list;

	}


	public static function getBoardRefAdminList($bid){
		return SQL::DB('csi')->fetchAll('SELECT b.name , a.bid ,a.brid , a.create_date FROM tBOARD_REPLY as a , tACCOUNT AS b WHERE b.aid = a.admin_aid and a.bid  = ?' , [$bid]);
	}

	public static function getBoardRefOperatorList($bid){

		return SQL::DB('csi')->fetchAll('SELECT b.name , a.bid ,a.brid ,a.operator_aid,  a.create_date FROM tBOARD_REPLY as a , tACCOUNT AS b WHERE b.aid = a.operator_aid and a.bid  = ?' , [$bid]);
	}

	public static function getBoardStatus($status=1){
		return SQL::DB('csi')->fetchAll('SELECT * FROM tPROCESSING_STATUS ORDER BY view asc');
	}


	public static function getTypeList($status = 1){
		return SQL::DB('csi')->fetchAll('SELECT * FROM tOPINIONTYPE WHERE status = ? ORDER BY view asc ' , [$status]);

	}

	public static function insertBoard($data){
		$rs = SQL::DB('csi')->query('INSERT INTO tBOARD (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

		return $rs;
	}


	public static function updateBoard($bid , $data){
		$cond = ['bid' => $bid];
		$rs = SQL::DB('csi')->query('UPDATE tBOARD SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));


		return $rs;
	}

}
