<?php
//mysql 클래스를 SQLAdaptorInterface에 맞게 구현함
//모두 prepared statement를 사용하게 구현함.
namespace MySQLAdaptor {
	require_once 'util.php';
	class DB {
		private $_connInfo, $_conn, $_error, $_sql, $_bind;

		private $_affectedRows = -1, $_matchedRows = -1, $_lastID = -1;

		public function __construct($connInfo) {
			$this->_connInfo = $connInfo;
		}

		public function isHealthy() {
			return $this->_connect();
		}

		public function error() {
			return $this->_error ? $this->_error : ($this->_conn ? $this->_conn->error : null);
		}

		public function affectedRows() {
			return max($this->_matchedRows, $this->_affectedRows);
		}

		public function lastID() {
			return $this->_lastID;
		}

		public function sendError() {
			$ci = &get_instance();
			// if(!IS_REAL) $ci->lib_gearman->gearman_dev_client();
			// else $ci->lib_gearman->gearman_master_client();

			$message = '';
			if (PROD_MODE != 'REAL') {
				$message = '[DEV]' . $message;
			}

			$d = new \Datetime();
			$d->setTimeZone(new \DateTimeZone('Asia/Seoul'));
			$server_addr = isset($_SERVER['SERVER_ADDR']) ? "[" . $_SERVER['SERVER_ADDR'] . "]" : '';
			$message .= $d->format('c') . " " . $server_addr;
			$message .= "\nerror : " . $this->_error . "\nsql : " . $this->_sql;
			if ($this->_bind && is_array($this->_bind)) {
				$message .= "\nbind : " . array2string($this->_bind);
			} else {
				$message .= "\nbind : " . $this->_bind;
			}
			$message .= "\n" . debug_backtrace_string();

			//if (!IS_REAL) {
				debug_log($message, 'db_error');
			//}
			//\Log_library::AddLogDb($message, 'db_error');

			// \DLog::log('mysql_error',['error'=>$this->_error,'bind'=>$this->_bind,'debug_backtrace_string'=>debug_backtrace_string()],'ci_log');

			// $tg_to = 'donpush_db_error';
			// if(strpos($message,'Duplicate') !== false ){
			//     $tg_to = 'donpush_db_duplicate_error';
			// }

			// $ci->lib_gearman->do_job_background('sendTelegramKr', serialize(['msg'=>$message,'to'=>$tg_to]));
		}

		private function _connect() {
			if ($this->_conn) {
				return true;
			}

			try {

				$this->_conn = @new \mysqli(
					($this->_connInfo['persistent'] ? 'p:' : '') . $this->_connInfo['host'],
					$this->_connInfo['user'],
					$this->_connInfo['pass'],
					$this->_connInfo['db'],
					$this->_connInfo['port']
				);
			} catch (Exception $e) {
				//debug_var($e);
				exit();
			}

			if ($this->_conn->connect_error) {
				// debug_var('connect_error');
				$this->_error = $this->_conn->connect_error;

				//debug_var($this->_error);

				$this->sendError();
				// \Log::error($this->_conn->connect_error, null, __FILE__, __LINE__);
				return false;
			}

			if ($this->_connInfo['persistent']) {
				if (!$this->_conn->ping()) {
					$this->_conn->close();
					if (!$this->_conn->real_connect()) {
						return false;
					}

				}
			}

			$this->_conn->set_charset($this->_connInfo['charset']);

			return true;
		}

		private function _prepare($sql, $bind = null) {

			if ($this->_conn->more_results()) {
				$this->_conn->next_result();
			}

			$_t = \ExternalUtil::utime();

			// debug_var([$tm, 'MySQL', [$tm, $sql], __FILE__, __LINE__]);

			$_sql = strtoupper(str_replace(['`', "'", '"'], '', $sql));
			// debug_log($_sql);

			$_type = '';
			$_table = 'UNKNOWN';
			if (substr($_sql, 0, 6) == 'SELECT') {
				$_type = 'R';

				$_tmp_arr = explode('FROM', $_sql);
				if (isset($_tmp_arr[1])) {
					$_tmp1_arr = explode(' ', trim($_tmp_arr[1]));

					if (isset($_tmp1_arr[0])) {
						$_table = trim($_tmp1_arr[0]);
					}
				}

			} else if (substr($_sql, 0, 6) == 'INSERT') {
				$_type = 'C';

				// INSERT INTO TAD_MENT (AD_KEY,CREATE_TIME,BODY,M_TIME,NICK_NAME) VALUES(?,?,?,?,?)

				$_tmp_arr = explode('(', $_sql);
				if (isset($_tmp_arr[0])) {
					$_tmp1_arr = explode(' ', trim($_tmp_arr[0]));
					if (isset($_tmp1_arr[2])) {
						$_table = trim($_tmp1_arr[2]);
					}
				}

			} else if (substr($_sql, 0, 6) == 'UPDATE') {
				$_type = 'U';
				$_tmp_arr = explode('UPDATE', $_sql);
				if (isset($_tmp_arr[1])) {
					$_tmp1_arr = explode(' ', trim($_tmp_arr[1]));
					if (isset($_tmp1_arr[0])) {
						$_table = trim($_tmp1_arr[0]);
					}
				}

			} else if (substr($_sql, 0, 6) == 'DELETE') {
				$_type = 'D';
				$_tmp_arr = explode('FROM', $_sql);
				if (isset($_tmp_arr[1])) {
					$_tmp1_arr = explode(' ', trim($_tmp_arr[1]));
					if (isset($_tmp1_arr[0])) {
						$_table = trim($_tmp1_arr[0]);
					}
				}

			} else {
				$_type = 'N';
			}

			// debug_var([$_type,$_table,$_sql]);
			// exit;

			$d = new \DateTime();

			$d->setTimeZone(new \DateTimeZone(TIMEZONE_OFFSET_STR));
			
			$_save_time = $d->format('YmdHi');

			$_log_key = 'MySQL:' . $this->_connInfo['db'] . ':' . $_type . ':' . $_table;

			//debug_var($_save_time);
			//debug_var($_log_key);

			//\MCache::inc($_log_key . ':' . $_save_time, 1, 864000);

			// if($_type == 'C') debug_log_master_one($_log_key,'db_log');

			//\MCache::del('MySQL_LOG');
			$_c_list = \MCache::get('MySQL_LOG');
			$_c_list = '';
			if (!$_c_list) {
				$_c_list = [];
				$_c_list[$this->_connInfo['db']][$_table] = true;
				//\MCache::set('MySQL_LOG', $_c_list);
			} else {
				if (!isset($_c_list[$this->_connInfo['db']][$_table])) {
					$_c_list[$this->_connInfo['db']][$_table] = true;

					//debug_var($_c_list);
					//exit;
					//\MCache::set('MySQL_LOG', $_c_list);
				}
			}

			// debug_var($_c_list);
			// exit;

			$stmt = $this->_conn->prepare($sql);
			if ($stmt) {
				$cnt = $bind ? count($bind) : 0;
				if ($bind && $stmt->param_count > 0 && $stmt->param_count == $cnt) {
					$arg = [str_repeat('s', $cnt)];
					foreach ($bind as &$v) {
						$arg[] = &$v;
					}

					// dump($bind, $arg);
					if (!call_user_func_array(array($stmt, 'bind_param'), $arg)) {
						$stmt->close();
						return false;
					}
				}
				if ($stmt->execute()) {
					$this->_affectedRows = $stmt->affected_rows;
					$this->_lastID = $stmt->insert_id;
					if ($this->_conn->info && preg_match('/Rows matched:\s*([0-9]+)/i', $this->_conn->info, $m)) {
						$this->_matchedRows = (int) $m[1];
					} else {
						$this->_matchedRows = 0;
					}

					$tm = \ExternalUtil::utime() - $_t;
					// \Log::db($tm, 'MySQL', [$tm, $sql], __FILE__, __LINE__);

					return $stmt;
				}
			}
			$this->_error = $this->_conn->error;
			$this->_sql = $sql;
			$this->_bind = $bind;
			$this->sendError();

			// \Log::error($this->_conn->error, [$this->_connInfo['db'], $sql, $bind], __FILE__, __LINE__);
			if ($stmt) {
				$stmt->close();
			}

			@$this->_conn->close();
			$this->_conn = false;
			return false;
		}

		public function query($sql, $bind = null) {

			// debug_log($sql);
			// debug_log($bind);



			if (!$this->_connect()) {
				return false;
			}

			if (!($stmt = $this->_prepare($sql, $bind))) {
				return false;
			}
			

			$stmt->close();
			return true;
		}

		public function fetchAll($sql, $bind = null) {
			// exit;

			if (!$this->_connect()) {
				return false;
			}

			if (!($stmt = $this->_prepare($sql, $bind))) {
				return false;
			}

			$data = [];
			$var = [];
			$meta = $stmt->result_metadata();
			if ($meta) {
				foreach (\ExternalUtil::extractObject($meta->fetch_fields(), 'name') as $name) {
					$var[] = &$data[$name];
				}

				call_user_func_array(array($stmt, 'bind_result'), $var);
			}

			$rows = [];
			while ($stmt->fetch()) {
				$row = [];
				foreach ($data as $k => $v) {
					$row[$k] = $v;
				}

				$rows[] = $row;
			}
			$stmt->close();


			//debug_var($rows);
			//exit;

			return $rows;
		}

		public function fetchRow($sql, $bind = null) {
			//debug_log($sql);
			//debug_log($bind);
			// exit;
			if (!$this->_connect()) {
				// debug_var('unconnected');
				return false;
			}

			if (!($stmt = $this->_prepare($sql, $bind))) {
				return false;
			}

			$data = [];
			$var = [];
			$meta = $stmt->result_metadata();
			//debug_var($meta->fetch_fields());
			//exit;

			foreach (\ExternalUtil::extractObject($meta->fetch_fields(), 'name') as $name) {
				$var[] = &$data[$name];
			}

			// debug_var($stmt);
			// debug_var($var);

			// exit;

			call_user_func_array(array($stmt, 'bind_result'), $var);
			$row = [];
			if ($stmt->fetch()) {
				foreach ($data as $k => $v) {
					$row[$k] = $v;
				}
			}
			//debug_var($row);
			// exit;

			$stmt->close();
			return $row;
		}

		public function result($sql, $bind = null) {

			// debug_var($sql);
			// debug_var($bind);
			// exit;

			if (!$this->_connect()) {
				return false;
			}



			if (!($stmt = $this->_prepare($sql, $bind))) {
				return false;
			}

			$data = [];
			$var = [];
			$meta = $stmt->result_metadata();
			$fields = \ExternalUtil::extractObject($meta->fetch_fields(), 'name');
			foreach ($fields as $name) {
				$var[] = &$data[$name];
			}

			call_user_func_array(array($stmt, 'bind_result'), $var);
			$stmt->fetch();
			$val = $data[$fields[0]];
			$stmt->close();
			return $val;
		}
	}
}
