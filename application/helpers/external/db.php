<?php
class SQL {
	public static $config;

	public static function DB($name) {

		static $conn = [];
		if (!isset($conn[$name])) {
			require_once 'db.mysql.php';
			// print_r(static::$config);
			// exit;

			$conn[$name] = new MySQLAdaptor\DB(static::$config[$name]);

			// debug_var($conn[$name]);
		}

		return $conn[$name];
	}
}
// $real_rds = '172.16.0.20';

SQL::$config = [
	'csi' => [
		'persistent' => true,
		'user' => MYSQL_USER,
		'pass' => MYSQL_PASS,
		'db' => 'CUSTOMER_SERVICE_INNOVATION',
		'charset' => 'utf8mb4',
		'host' => MYSQL_HOST,
		'port' => 3306,
	],
	'parkinglot' => [
	  	'persistent' => true,
	  	'user' => MYSQL_USER1,
	  	'pass' => MYSQL_PASS1,
	  	'db' => 'PARKINGLOT',
	  	'charset' => 'utf8',
	  	'host' => MYSQL_HOST1,
	  	'port' => 3306,
	],
	// 'psa' => [
	//  	'persistent' => true,
	//  	'user' => MYSQL_USER,
	//  	'pass' => MYSQL_PASS,
	//  	'db' => 'PARKING_STANDARD',
	//  	'charset' => 'utf8',
	//  	'host' => MYSQL_HOST,
	//  	'port' => 3306,
	// ],

	// 'iot' => [
	//  	'persistent' => true,
	//  	'user' => MYSQL_USER,
	//  	'pass' => MYSQL_PASS,
	//  	'db' => 'arduino',
	//  	'charset' => 'utf8',
	//  	'host' => MYSQL_HOST,
	//  	'port' => 3306,
	// ]
];
	


