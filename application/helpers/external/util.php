<?php
define('PHP_INT32_MAX', 2147483647);

class ExternalUtil {
	public static function historyBack() {
		static::error();
	}

	public static function error($msg = '', $back = true) {
		echo ('<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8" /></head><body>');
		static::alert($msg);
		if ($back) {
			static::script('history.back();');
		}

		echo ('</body></html>');
		exit;
	}

	public static function headerNoCache() {
		header('Cache-control: no-cache,no-store,must-revalidate');
		header('Pragma: no-cache');
		header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
	}

	public static function headerExpire() {
		$expires = 60 * 60 * 24 * 14; //2주
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Pragma: public');
		header('Cache-Control: maxage=' . $expires);
		header('Expires: ' . gmdate('D, d M Y H:i:s', time() + $expires) . ' GMT');
	}

	public static function redirect($url, $redirect2noSSL = false) {
		if ($url) {
			if ($redirect2noSSL) {
				if (preg_match('/^https?:\/\//i', $url)) {
					$url = preg_replace('/^https?:\/\//i', 'http://', $url);
				} else {
					$url = 'http://' . $_SERVER['HTTP_HOST'] . $url;
				}

			}

			header('Location: ' . $url);
		}
		exit;
	}

	public static function replace_url($url) {
		if ($url) {			
			//echo("<script>location.replace(".$url.");</script>"); 
			header('Location: ' . $url);

		}
		exit;
	}


	public static function redirectForm($url, $argv) {
		$redirect = $url . (preg_match('/\?/', $url) ? '&' : '?') . http_build_query($argv);
		echo ('<html><head><meta http-equiv="content-type" content="text/html; charset=utf-8" /><meta http-equiv="refresh" content="2;url=' . out($redirect) . '" />');
		echo ('</head><body onload="document.forms[0].submit();"><form action="' . $url . '" method="post">');
		foreach ($argv as $k => $v) {
			echo ('<input type="hidden" name="' . out($k) . '" value="' . out($v) . '" />');
		}

		echo ('</body></html>');
	}

	public static function alert($msg, $escape = false) {
		if (!$msg) {
			return false;
		}

		$msg = str_replace("\n", '\n', addslashes($msg));
		static::script('alert(\'' . $msg . '\');');
	}

	public static function script($script) {
		echo ('<script type="text/javascript" charset="UTF-8">' . $script . '</script>' . "\n");
	}

	public static function stripTags($string) {
		return preg_replace('/<[^>]+>/u', '', $string);
	}

	public static function extractList($array, $key) {
		$ret = array();
		if (!$array) {
			return $ret;
		}

		foreach ($array as $arr) {
			$ret[] = $arr[$key];
		}

		return $ret;
	}

	public static function extractObject($array, $key) {
		$ret = array();
		if (!$array) {
			return $ret;
		}

		for ($i = 0, $cnt = count($array); $i < $cnt; $i++) {
			$ret[] = $array[$i]->{$key};
		}
		return $ret;
	}

	public static function tryJson($str) {
		if (is_string($str) && ((substr($str, 0, 1) == '{' && substr($str, -1, 1) == '}') || (substr($str, 0, 1) == '[' && substr($str, -1, 1) == ']'))) {
			//json
			$obj = json_decode($str, true);
			if ($obj) {
				return $obj;
			}

		}
		return $str;
	}

	public static function arrayize($o) {
		if (is_array($o) && !$o[0]) {
			return array($o);
		}
		return $o;
	}

	public static function arrayListKeyLower($array) {
		if ($array) {
			foreach ($array as &$r) {
				$r = array_change_key_case($r);
			}

		}
		return $array;
	}

	public static function arrayListExtract($array, $key1, $key2) {
		$res = [];
		foreach ($array as $k => $v) {
			$res[$v[$key1]] = $v[$key2];
		}

		return $res;
	}

	public static function arrayKeySwap($array, $key) {
		if (!$array) {
			return null;
		}

		$newArray = array();
		for ($i = count($array); $i--;) {
			$newArray[(string) $array[$i][$key]] = &$array[$i];
		}

		return $newArray;
	}

	public static function getArrayFromList($array, $key, $val) {
		for ($i = count($array); $i--;) {
			if ($array[$i][$key] == $val) {
				return $array[$i];
			}

		}
	}

	public static function getObjectFromList($array, $key, $val) {
		for ($i = count($array); $i--;) {
			if ($array[$i]->{$key} == $val) {
				return $array[$i];
			}

		}
	}

	public static function validateArrayKey(&$array, $keys) {
		if (!$array) {
			return;
		}

		if (!is_array($keys)) {
			$keys = preg_split('/\s*,\s*/', $keys);
		}

		foreach ($array as $k => $v) {
			if (array_search($k, $keys) === false) {
				unset($array[$k]);
			}

		}
	}

	public static function validateArrayKeys(&$array, $keys) {
		if (!$array) {
			return;
		}

		if (!is_array($keys)) {
			$keys = preg_split('/\s*,\s*/', $keys);
		}

		foreach ($array as $i => $t) {
			foreach ($t as $k => $v) {
				if (array_search($k, $keys) === false) {
					unset($array[$i][$k]);
				}

			}
		}
	}

	public static function objectList2Array($array, $key) {
		if (!$array) {
			return null;
		}

		$newArray = array();
		for ($i = count($array); $i--;) {
			$newArray[(string) $array[$i]->$key] = &$array[$i];
		}

		return $newArray;
	}

	public static function arrayList2Array($array, $key) {
		if (!$array) {
			return null;
		}

		$newArray = array();
		for ($i = count($array); $i--;) {
			$newArray[(string) $array[$i][$key]] = &$array[$i];
		}

		return $newArray;
	}

	public static function object2Array($object) {
		if (!is_object($object)) {
			return $object;
		}

		$newArray = array();
		foreach ($object as $k => $v) {
			$newArray[$k] = $v;
		}

		return $newArray;
	}

	public static function isImagePath($fname) {
		return array_search(strtolower(array_pop(explode('.', $fname))), array('jpg', 'jpeg', 'gif', 'png', 'bmp', 'psd', 'swf', 'raw', 'esp', 'pcx', 'tif', 'tiff')) !== false;
	}

	public static function replace($str, $pos, $len, $rep) {
		return substr($str, 0, $pos) . $rep . substr($str, $pos + $len);
	}

	public static function preg_escape($str) {
		return str_replace('/', '\/', preg_quote($str));
	}

	public static function htmldecode($str) {
		$str = html_entity_decode($str, ENT_COMPAT, 'UTF-8');
		$str = preg_replace_callback('/&#[0-9]{1,5};/', '_htmldecode_html', $str);
		$str = preg_replace_callback('/&#x([0-9a-z]{4});/i', '_htmldecode_htmlx', $str);
		return $str;
	}

	public static function htmldecode_r(&$str) {
		if (is_string($str)) {
			$str = static::htmldecode($str);
		}

	}

	public static function xmlNamedEntityDecode($str) {
		//$str=html_entity_decode($str,ENT_COMPAT,'UTF-8');
		$str = preg_replace_callback('/&(lt|gt|amp|apos|quot|#39|#039|#x0027);/ui', '_xmlNamedEntityDecode', $str);
		return $str;
	}

	public static function strtolowerUTF8($str) {
		return preg_replace_callback('/[A-Z]+/u', '_strtolowerUTF8', $str);
	}

	public static function burnCookie($cookie, $expire = 0) {
		@header('P3P: CP="NOI CURa ADMa DEVa TAIa OUR DELa BUS IND PHY ONL UNI COM NAV INT DEM PRE"');
		// clog(array('COOKIE SET'=>$cookie, 'COOKIE_EXPIRE'=>$expire));
		if ($expire == 'forever') {
			$expire = strtotime('1 year');
		}

		foreach ($cookie as $name => $value) {
			$_COOKIE[$name] = $value;
			@setcookie($name, $value, $expire, '/');
		}
	}

	public static function encrypt($plain_text) {
		return base64_encode(openssl_encrypt($plain_text, 'aes-256-cbc', DEF_ENC_KEY, true, str_repeat(chr(0), 16)));
	}

	public static function decrypt($base64_text) {
		$is_encoded = preg_match('~%[0-9A-F]{2}~i', $base64_text);
		if ($is_encoded) {
			$base64_text = urldecode($base64_text);
		}
		return openssl_decrypt(base64_decode($base64_text), 'aes-256-cbc', DEF_ENC_KEY, true, str_repeat(chr(0), 16));
	}

	public static function utime() {
		$time = explode(' ', microtime());
		return $time[1] . substr($time[0], 2, 3);
	}

	public static function strtotime($time) {
		// static $tz;
		// if (!$tz)
		$tz = new DateTimeZone(TIMEZONE_OFFSET_STR);

		return date_timestamp_get(date_create($time, $tz));
	}

	public static function inTimeRange($s, $e, $now = null) {
		if (!$now) {
			$now = time();
		}

		return static::strtotime($s) <= $now && $now <= static::strtotime($e);
	}

	public static function inLocalTimeRange($s, $e, $now = null) {
		if (!$now) {
			$now = time();
		}

		return strtotime($s) <= $now && $now <= strtotime($e);
	}

	public static function u2time($time) {
		return substr($time, 0, -3);
	}

	public static $_base62 = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	public static function base62encode($num) {
		$out = '';
		for ($t = floor(log10($num) / log10(62)); $t >= 0; $t--) {
			$a = floor($num / pow(62, $t));
			$out = $out . static::$_base62[$a];
			$num = $num - ($a * pow(62, $t));
		}
		return $out;
	}

	public static function base62decode($num) {
		$out = 0;
		$len = strlen($num) - 1;
		for ($t = 0; $t <= $len; $t++) {
			$out = $out + strpos(static::$_base62, $num[$t]) * pow(62, $len - $t);
		}
		return $out;
	}

	public static function trimArray($arr) {
		$d = array();
		foreach ($arr as $k => $v) {
			if ($v !== null) {
				$d[$k] = $v;
			}

		}
		return $d;
	}

	public static function clog($msg) {
		static $fp = false;
		if (!$fp) {
			$fname = '/tmp/php.' . $_SERVER['HTTP_HOST'] . '.log.' . date('Ymd');
			$fp = @fopen($fname, 'a');
			@chmod($fname, 0666);
			if (!$fp) {
				return;
			}

			static::clog("\n\n" . 'X-------------------------------------------------------------------------------------------X');
			static::clog($_SERVER['PHP_SELF'] . ' ' . date('r'));
		}
		if (is_string($msg)) {
			fwrite($fp, $msg);
		} else {
			fwrite($fp, var_export($msg, true));
		}

		fwrite($fp, "\n");
	}

	public static function cleanText($var = '') {
		return trim(strip_tags($var));
	}

	public static function is_empty($var, $allow_false = false, $allow_ws = false) {
		$var = static::cleanText($var);
		if (!isset($var) || is_null($var) || ($allow_ws == false && trim($var) == "" && !is_bool($var)) || ($allow_false === false && is_bool($var) && $var === false) || (is_array($var) && empty($var))) {
			return true;
		} else {
			return false;
		}
	}

}
