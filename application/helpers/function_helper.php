<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

function basic_caution($msg){
	$str = "<script>";
	$str .= "alert('{$msg}');";
	$str .= "</script>";
	echo("$str");
	exit;
}

function basic_caution_no_exit($msg){
	$str = "<script>";
	$str .= "alert('{$msg}');";
	$str .= "</script>";
	echo("$str");
	exit;
}


function back_caution($msg){
	$str = "<script>";
	$str .= "alert('{$msg}');";
	$str .= "history.back();";
	$str .= "</script>";
 	echo("$str");
	exit;
}


// 해당 메시지를 띄우고 해당 URL 로 이동
function goto_caution($msg, $url){
 $str = "<script>";
 $str .= "alert('{$msg}');";
 $str .= "location.href = '{$url}';";
 $str .= "</script>";
 echo("$str");
 exit;
}
 
// 해당 메시지를 띄우고 창을 종료 시킴
function close_caution($msg){
 $str = "<script>";
 $str .= "alert('{$msg}');";
 $str .= "window.close();";
 $str .= "</script>";
 echo("$str");
 exit;
}
 
// 해당 주소로 그냥 보냄
function just_go($url){
 $str = "<script>";
 $str .= "location.href = '{$url}';";
 $str .= "</script>";
 echo("$str");
 exit;
}

function modal_popup($modal_id){
	$str = "<script type='text/javascript'>";
	$str .= "$(document).ready(function(){";
	$str .= "$('{modal_id}').modal('show');";
	$str .= "});";
	$str .= "</script>;";
	echo("$str");
	exit;

}

function getRemoveApmTime($datetime){

	$t_date = explode(" " , $datetime);

	$apm  = $t_date[2];

	$time = explode(":" , $t_date[1]);
	$time_t = $time[0];
	if($apm == '오후'){
		$time_t += 12;

	}
	$a_time = $time_t.":".$time[1].":".$time[2];
	$datetime = $t_date[0] .' '.$a_time;

	return $datetime;
}


function dayOftheWeek($date){
	$dayoftheweek = array("일","월","화","수","목","금","토");

	return $dayoftheweek[date('w' , strtotime($date))];
}


function debug_backtrace_string() {
	$stack = '';
	$i = 1;
	$trace = debug_backtrace();
	// debug_var($trace);
	// exit;
	// unset($trace[0]); //Remove call to this function from stack trace
	foreach ($trace as $node) {
		$stack .= "#$i ";
		if (isset($node['file'])) {
			$stack .= $node['file'];
		}
		if (isset($node['line'])) {
			$stack .= "(" . $node['line'] . "): ";
		}
		// $stack .= "#$i ".$node['file'] ."(" .$node['line']."): ";
		if (isset($node['class'])) {
			$stack .= $node['class'] . "->";
		}
		$stack .= $node['function'] . "()" . PHP_EOL;
		$i++;
	}
	return $stack;
}

function getDayByWeeks($from){
	// set current date
	$date = $from;
	// parse about any English textual datetime description into a Unix timestamp 
	$ts = strtotime($date);
	// calculate the number of days since Monday
	$dow = date('w', $ts);
	$offset = $dow - 1;
	if ($offset < 0) {
	    $offset = 6;
	}
	// calculate timestamp for the Monday
	$ts = $ts - $offset*86400;
	$list = [];
	// loop from Monday till Sunday 
	for ($i = 0; $i < 7; $i++, $ts += 86400){
	     $list[] = date("Y-m-d", $ts);

	}

	return $list;
}

function get_time() {
    list($usec, $sec) = explode(" ", microtime());
    return ((float)$usec + (float)$sec);
}


// function getYesterDay(){
// 	$date = new DateTime();
// 	$date->modify("-1 day");	

// 	return $date->format('Y-m-d');
// }

function  check_valid_leave($schedule_row , $request_date){

	//debug_var($schedule_row);
	$from = new DateTime($schedule_row['from_']);
	$to = new DateTime($schedule_row['to_']);
	$deadline = new DateTime($schedule_row['deadline']);

	$rd = new DateTime($request_date);
	$now = new DateTime();

	// debug_var($rd);
	// debug_var($now);
	// exit;

	// debug_var($from);
	// debug_var($to);
	// debug_var($rd);
	// debug_var($deadline);
	//exit;



	// 먼저 날짜에 속하는지 여부
	if($from <= $rd && $to >= $rd){

   	 //debug_var($from);
		  // debug_var($to);
		  // exit;
		// debug_var($rd);
		// debug_var($now);
		// debug_var($deadline);
		//exit;

		if($now > $deadline){
			return 0;	
		}

	}

	return 1;

}

function format_phone($phone) {
	// note: making sure we have something
	// if (!isset($phone{3})) {return '';}
	// note: strip out everything but numbers
	$phone = preg_replace("/[^0-9]/", "", $phone);
	$length = strlen($phone);
	switch ($length) {
	case 7:
		return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
		break;
	case 9:
		return preg_replace("/([0-9]{2})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
		break;
	case 10:
		return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "$1-$2-$3", $phone);
		break;
	case 11:
		return preg_replace("/([0-9]{3})([0-9]{4})([0-9]{4})/", "$1-$2-$3", $phone);
		break;
	default:
		return $phone;
		break;
	}
}
function return_select($key = '', $val = '') {
	if ($key == $val) {
		return "selected=\"select\"";
	}
}
function return_checked($key = '', $val = '') {
	// debug_log($key);
	// debug_log($val);
	// return;


	if ($key == $val) {
		return "checked";
	}
}
function _trim($value = '') {
	return trim(strip_tags($value));
}

function aasort(&$array, $key) {
	$sorter = array();
	$ret = array();
	reset($array);
	foreach ($array as $ii => $va) {
		if (isset($va[$key])) {
			$sorter[$ii] = $va[$key];
		} else {
			$sorter[$ii] = $ii;
		}
	}
	asort($sorter);
	foreach ($sorter as $ii => $va) {
		$ret[$ii] = $array[$ii];
	}
	$array = $ret;
}

function array2string($data, $new_line = "\n") {
	$log_a = "";
	if ($data && is_array($data)) {
		foreach ($data as $key => $value) {
			if (is_array($value)) {
				$log_a .= "[" . $key . "] => (" . array2string($value) . ") " . $new_line;
			} else {
				$log_a .= "[" . $key . "] => " . $value . $new_line;
			}
		}
	}
	return $log_a;
}
function array_null_string($rs = '') {
	if (is_array($rs)) {
		foreach ($rs as $key => $value) {
			if (is_array($value)) {
				$rs[$key] = array_null_string($value);
			} else if (is_null($value)) {
				$rs[$key] = '';
			}

		}
	} else if (is_null($rs)) {
		$rs = '';
	}

	return $rs;
}

function db_array_to_values($array = []) {
	$field_v = '';
	foreach ($array as $key => $value) {
		$field_v .= '?,';
	}
	$field_v = substr($field_v, 0, -1);

	// debug_var($field_v);
	// exit;

	return $field_v;
}
function db_array_to_set($array = [], $where = '=', $and = ',') {
	$field_v = '';
	foreach ($array as $key => $value) {
		$field_v .= $key . ' ' . $where . ' ? ' . $and . ' ';
	}
	$field_v = substr(trim($field_v), 0, -strlen($and));
	return $field_v;
}


function db_array_to_set_reserved_words($array = [], $where = '=', $and = ',') {
	$field_v = '';
	foreach ($array as $key => $value) {
		$field_v .= "`" . $key . "`" . $where . ' ? ' . $and . ' ';
	}
	$field_v = substr(trim($field_v), 0, -strlen($and));
	return $field_v;
}


function db_array_to_set_add($array = [], $where = '=', $and = ',') {
	$field_v = '';
	foreach ($array as $key => $value) {
		$field_v .= $key . ' ' . $where . $key . '+ ? ' . $and . ' ';
	}
	$field_v = substr(trim($field_v), 0, -strlen($and));
	return $field_v;
}

function db_ngram_query($keyword = '', $field = '') {
	// 특수문자 지움
	$qry = '';
	$keyword = preg_replace("/[#\&\+\-%@=\/\\\:;,\.'\"\^`~\_|\!\?\*$#<>()\[\]\{\}]/i", "", $keyword);
	$keyword = preg_split('@(<[^<>]+>|&[a-z\d]+;|\pC|\pM|\pP|\pS|\pZ)+@u', $keyword);
	if ($keyword && is_array($keyword) && count($keyword) > 0) {
		$_keyword = '+' . implode(' +', $keyword);

		$qry = ' match(' . $field . ') against(? in boolean mode)';
	}
	return $qry;
}

function datetime_local($value = '') {
	$date = new DateTime($value);
	$date->setTimeZone(new DateTimeZone(TIMEZONE_OFFSET_STR));
	return $date;
}

function isset_return_str(&$value = '', $ext = '') {
	if (isset($value)) {
		return $value;
	} else {
		return $ext;
	}
}

function human_time_sec($seconds, $ext_hour = ':', $ext_min = ':', $is_show_sec = true) {
	$hours = floor($seconds / 3600);
	$mins = floor($seconds / 60 % 60);
	$secs = floor($seconds % 60);
	$str = '';
	if ($hours) {
		$str .= $hours . $ext_hour;
	}
	if ($mins) {
		if ($str) {
			$str .= '';
		}
		$str .= $mins . $ext_min;
	}
	if ($is_show_sec) {
		if ($str) {
			$str .= '';
		}
		if (!$secs) {
			$secs = '0';
		}
		$secs = sprintf('%02d', $secs);
		$str .= $secs . '';
	}
	return $str;
}

function fileUploaded($key = '') {
	if (empty($_FILES)) {
		return false;
	}
	$file = $_FILES[$key];
	if (is_array($file['error'])) {
		foreach ($file['error'] as $key => $value) {
			if ($value != 0) {
				return false;
			}
		}
	} else {
		if ($file['error'] != 0) {
			return false;
		}
	}
	return true;
}

function date_ymd() {
	return date('Y-m-d H:i:s');
}
function return_date_format($date = '', $format = 'Y-m-d H:i:s') {
	try {
		if ($date) {
			if (is_numeric($date)) {
				$d = new DateTime('@' . $date);
			} else {
				$d = new DateTime($date);
			}
		} else {
			$d = new DateTime();
		}
		return $d->format($format);

	} catch (Exception $e) {
		return '';
	}
}

function only_number($s = '') {
	return preg_replace("/[^0-9]/", "", $s);
}

function RandString($len) {
	$return_str = "";
	for ($i = 0; $i < $len; $i++) {
		mt_srand((double) microtime() * 1000000);
		$return_str .= substr('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', mt_rand(0, 61), 1);
	}
	return $return_str;
}

function RandNum($len) {
	// $return_str = "";
	// whele(1)
	// 	mt_srand((double) microtime() * 1000000);
	// 	$n = substr('123456789', mt_rand(0, 9), 1);
	// 	$return_str .= $n;
	// }
	$ci = &get_instance();
	$ci->load->helper('string');
	return random_string('nozero', $len);
}

function rand_code($len = '2', $type = '1') {
	$return_str = "";
	// md5(microtime())
	if ($type == '1') {
		$charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789';
	} else {
		$charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	}
	$charset_len = strlen($charset) - 1;
	for ($i = 0; $i < $len; $i++) {
		mt_srand((double) microtime() * 1000000);
		$ran = mt_rand(0, $charset_len);
		// debug_var($ran);
		$return_str .= substr($charset, $ran, 1);
	}
	return $return_str;
}

if (!function_exists('getallheaders')) {
	function getallheaders() {
		$headers = '';
		foreach ($_SERVER as $name => $value) {
			if (substr($name, 0, 5) == 'HTTP_') {
				// $headers[str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($name, 5)))))] = $value;
			}
		}
		return $headers;
	}
}

function return_date_text($t = '') {
	if ($t < 60) {
		$txt = " second ago";
	} elseif ($t >= 60 && $t < 3600) {
		$txt = floor($t / 60) . " minute ago";
	} elseif ($t >= 3600 && $t < 86400) {
		$txt = floor($t / 3600) . " hour ago";
		// } elseif ($t >= 86400 && $t < 2419200) {
		// 	$txt = floor($t / 86400) . "일";
	} else {
		$txt = '';
	}
	return $txt;
}

// 1분 이내 => 몇 초전
// 1시간 이내 => xx분전
// 1일 이내 => xx시간전
// 1일 이상 => 5월 8일
// 작년 이전 => 2016년 12월 13일
function getDifferenceDate($timestamp, $reference = null) {

	if ($reference === null) {
		$reference = time();
	}

	$timestamp = ($timestamp instanceof DateTime) ? $timestamp->getTimestamp() : (is_numeric($timestamp) ? (int) $timestamp : strtotime($timestamp));
	$reference = ($reference instanceof DateTime) ? $reference->getTimestamp() : (is_numeric($reference) ? (int) $reference : strtotime($reference));

	$delta = $reference - $timestamp;

	if ($delta >= 0) {
		if ($delta >= 31536000) {
			return date('j, M ,Y', $timestamp);
		} else if ($delta >= 86400) {
			//return date('n월 d일', $timestamp);
			return date('j, M', $timestamp);
		} else {
			return return_date_text($delta);
		}
		// foreach ($formats as $format) {
		// if ($delta < $format[0]) {

		// return sprintf($format[1], round($delta / $format[2]));
		// }
		// }
	} else {
		// foreach ($futureformats as $format) {
		// if (-$delta < $format[0]) {
		return 'few seonds ago';
		// return sprintf($format[1], round($delta / $format[2]));
		// }
		// }
	}
}
// function ishanishan($str) {
// 	$for = mb_strlen($str);
// 	$newstr = '';
// 	for ($i = 0; $i < $for; $i++) {
// 		// $s = $i;// * 2;
// 		$char = mb_substr($str, $i, 1);
// 		$newstr .= ($char >= '가' && $char <= '힝') ? $char : "";
// 	}
// 	return $newstr;
// }

// function hankeycheck($str) {
// 	$for = mb_strlen($str);
// 	for ($i = 0; $i < $for; $i++) {
// 		$arr[] = mb_substr($str, $i, 2);
// 	}

// // 	$for = strlen($str) / 2;
// 	// for($i=0;$i<$for-1;$i++){
// 	// $arr[] = substr($str,$i*2,4);

// 	return $arr;
// }

function RandString2($len) {
	$return_str = "";
	for ($i = 0; $i < $len; $i++) {
		mt_srand((double) microtime() * 1000000);
		$return_str .= substr('abcdefghjkmnpqrstuvwxyz', mt_rand(0, 23), 1);
		// $return_str .= substr('0123456789', mt_rand(0,9), 1);
	}
	return $return_str;
}

function RandString3($len) {
	$return_str = "";
	for ($i = 0; $i < $len; $i++) {
		mt_srand((double) microtime() * 1000000);
		$return_str .= substr('0123456789abcdefghjkmnpqrstuvwxyz', mt_rand(0, 33), 1);
		// $return_str .= substr('0123456789', mt_rand(0,9), 1);
	}
	return $return_str;
}

// function money_format($money){
//     return number_format($money);
//}


function replace_language_name($language){
	if($language == 'en'){
		return 'english';
	} else if($language == 'ko'){
		return 'korean';
	}
	return 'english';
}


function get_invite_code($mem_count = 0) {

	$code = '';

	if ($mem_count < 100000) {
		// 경우의 수 2,300,000

		$code = RandString2(1) . rand(10000, 99999);
		// 뒷 다섯자리 숫자 00000 ~ 99999 중에서 랜덤하게 선택

	} else if ($mem_count < 1000000) {
		// 경우의 수 25,000,000

		$code = RandString2(1) . RandString3(2) . rand(100, 999);
		// 첫글짜를 알파벳 i l o 를 제외한 23글자 중에서 랜덤하게 선택
		// 두번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 세번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 뒷 세자리 숫자 000 ~ 999 중에서 랜덤하게 선택

	} else if ($mem_count < 10000000) {
		// 경우의 수 272,761,830

		$code = RandString2(1) . RandString3(4) . rand(0, 9);
		// 첫글짜를 알파벳 i l o 를 제외한 23글자 중에서 랜덤하게 선택
		// 두번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 세번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 네번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 다섯번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 뒷 한자리 숫자 0 ~ 9 중에서 랜덤하게 선택

	} else {
		// 경우의 수 약 12억 5천만

		$code = RandString3(6);
		// 첫글짜를 알파벳 i l o 를 제외한 23글자와 숫자 1~9 중에서 랜덤하게 선택
		// 두번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 세번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 네번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 다섯번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
		// 엿섯번째 글짜를 알파벳 i l o 를 제외한 23글자와 숫자 0~9 중에서 랜덤하게 선택
	}

	return $code;
}

function mm_check($mm = '', $is_web = false) {
	if ($mm == 'nesis') {
		return true;
	}
	// 기획의도
	// 앱이 아닌, 다른 알수 없는 방법으로 API를 호출하는 것을 막기 위해, 앱에서만 발생시킬 수 있는 난수값을 암호화해서 서버로 보내고, 그 값을 서버에서 복호화 해서 문제 없을때만 정상적으로 API를 처리하도록 합시다.
	// (1) 앱
	// {현재폰시각(UTC)}^aresjoy
	// 위 값을 aes256으로 암호화 해서
	// 로그인 / 비번찾기 / 회원가입 API 호출시
	// mm 이라는 파라메타에 값을 넎어서 올려 보냅시다.
	// (2) API
	// 앱에서 올려보낸 mm 값을 aes256으로 풀어서
	// 그 값을 추출해 냈을때,
	// 서버시간(UTC)에 비해, mm 값에 포함된 시간의 차이가 600초 이내가 아니면, API 진행하지 말고 에러 응답.
	// "올바른 이용이 아닙니다. 돈푸시 앱을 이용하세요."
	$is_success = false;
	
	$dec_mm = ExternalUtil::decrypt($mm);

	// debug_var($dec_mm);
	// debug_var($mm);
	// debug_var(urldecode($mm));
	// exit;

	if ($dec_mm) {
		$dec_list = explode('^', $dec_mm);
		if (isset($dec_list[0]) && $dec_list[1]) {
			if ($dec_list[1] == 'aresjoy') {

				if ($is_web) {
					$is_success = true;
				} else {
					$app_time = only_number($dec_list[0]);
					$now_time = time() - 32400; // utc시간으로..
					$sec = 600;
					if ($now_time - $app_time <= $sec) {
						$is_success = true;
					}
				}
			}
		}
	}
	return $is_success;
}

function isInWeb() {
	if (isset($_SERVER['HTTP_USER_AGENT'])) {
		$user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		if (strpos($user_agent, 'android_skinmatch') !== false || strpos($user_agent, 'ios_skinmatch') !== false) {
			return true;
		}
	}
	return false;
}

function isIos()
{
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $user_agent = strtolower($_SERVER['HTTP_USER_AGENT']);
        if (strpos($user_agent, APP_UA_IOS) !== false) {
            return true;
        }
    }
    return false;
}


function sms_send($msg = '', $phone = '') {
	if (!$msg || !$phone) {
		return false;
	}

	// return true;

	// if (!IS_REAL) {
	// 	send_telegram($phone . "\n" . $msg, 'donpush_sms');
	// 	return true;
	// }

	// $now_d = new DateTime();
	// // $now_d->setTimeZone(new DateTimeZone('Asia/Seoul'));

// $bind_data = [];
	// $bind_data[] = 0; //SCHEDULE_TYPE
	// $bind_data[] = $msg; //SMS_MSG
	// $bind_data[] = $now_d->format('YmdHis'); //'%Y%m%d%H%i%s'; //저장날자
	// $bind_data[] = $now_d->format('YmdHis'); //'%Y%m%d%H%i%s'; //발송날자
	// $bind_data[] = '15999248'; //발신번호
	// $bind_data[] = $phone; //수신번호

	// $rs = SQL::DB('sms')->query('INSERT INTO SMS_SEND(SCHEDULE_TYPE, SMS_MSG, SAVE_TIME, SEND_TIME, CALLBACK, CALLEE_NO) VALUES(?,?,?,?,?,?)', $bind_data);

	// if ($rs) {
	// 	$c_cache = 'sms_send:' . $now_d->format('Ymd');
	// 	MCache::inc($c_cache);

	// 	$c_cache = 'sms_send:' . $now_d->format('Ym');
	// 	MCache::inc($c_cache);
// }
	return $rs;
}

// 2019-05-01  의 경우 2019-05 로 return ;
function getMonthByDate($date){
	//$str = $date.split('-');
	$str = explode( '-', $date );

	$month = $str[0]. '-' .$str[1];

	return $month;

}

// 이번달이 2019-04 월 경우 2019-03-01 나 2019-03-31 일로 
function getPrevMonth($first){
	$d = mktime(0,0,0, date("m"), 1, date("Y")); //이번달 1일

	$prev_month = strtotime("-1 month", $d); //한달전

	if($first){
		return date("Y-m-01", $prev_month ); //지난달 1일	
	}  else {
		return  date("Y-m-t", $prev_month ); //지난달 말일	
	}


}

class TimingHelper {

    private $start;

    public function __construct() {
        $this->start = microtime(true);
    }

    public function start() {
        $this->start = microtime(true);
    }

    public function segs() {
        return microtime(true) - $this->start;
    }

    public function time() {
        $segs = $this->segs();
        $days = floor($segs / 86400);
        $segs -= $days * 86400;
        $hours = floor($segs / 3600);
        $segs -= $hours * 3600;
        $mins = floor($segs / 60);
        $segs -= $mins * 60;
        $microsegs = ($segs - floor($segs)) * 1000;
        $segs = floor($segs);

        return 
            (empty($days) ? "" : $days . "d ") . 
            (empty($hours) ? "" : $hours . "h ") . 
            (empty($mins) ? "" : $mins . "m ") . 
            $segs . "s " .
            $microsegs . "ms";
    }

}


class Memcached
{
    // Predefined Constants
    // See: http://php.net/manual/en/memcached.constants.php
    // Defined in php_memcached.c
    const OPT_COMPRESSION = -1001;
    const OPT_SERIALIZER = -1003;
    // enum memcached_serializer in php_memcached
    const SERIALIZER_PHP = 1;
    const SERIALIZER_IGBINARY = 2;
    const SERIALIZER_JSON = 3;
    // Defined in php_memcached.c
    const OPT_PREFIX_KEY = -1002;
    // enum memcached_behavior_t in libmemcached
    const OPT_HASH = 2;     //MEMCACHED_BEHAVIOR_HASH
    // enum memcached_hash_t in libmemcached
    const HASH_DEFAULT = 0;
    const HASH_MD5 = 1;
    const HASH_CRC = 2;
    const HASH_FNV1_64 = 3;
    const HASH_FNV1A_64 = 4;
    const HASH_FNV1_32 = 5;
    const HASH_FNV1A_32 = 6;
    const HASH_HSIEH = 7;
    const HASH_MURMUR = 8;
    // enum memcached_behavior_t in libmemcached
    const OPT_DISTRIBUTION = 9;     // MEMCACHED_BEHAVIOR_DISTRIBUTION
    // enum memcached_server_distribution_t in libmemcached
    const DISTRIBUTION_MODULA = 0;
    const DISTRIBUTION_CONSISTENT = 1;
    // enum memcached_behavior_t in libmemcached
    const OPT_LIBKETAMA_COMPATIBLE = 16;    // MEMCACHED_BEHAVIOR_KETAMA_WEIGHTED
    const OPT_BUFFER_WRITES = 10;           // MEMCACHED_BEHAVIOR_BUFFER_REQUESTS
    const OPT_BINARY_PROTOCOL = 18;         // MEMCACHED_BEHAVIOR_BINARY_PROTOCOL
    const OPT_NO_BLOCK = 0;                 // MEMCACHED_BEHAVIOR_NO_BLOCK
    const OPT_TCP_NODELAY = 1;              // MEMCACHED_BEHAVIOR_TCP_NODELAY
    const OPT_SOCKET_SEND_SIZE = 4;         // MEMCACHED_BEHAVIOR_SOCKET_SEND_SIZE
    const OPT_SOCKET_RECV_SIZE = 5;         // MEMCACHED_BEHAVIOR_SOCKET_RECV_SIZE
    const OPT_CONNECT_TIMEOUT = 14;         // MEMCACHED_BEHAVIOR_CONNECT_TIMEOUT
    const OPT_RETRY_TIMEOUT = 15;           // MEMCACHED_BEHAVIOR_RETRY_TIMEOUT
    const OPT_SEND_TIMEOUT = 19;            // MEMCACHED_BEHAVIOR_SND_TIMEOUT
    const OPT_RECV_TIMEOUT = 20;            // MEMCACHED_BEHAVIOR_RCV_TIMEOUT
    const OPT_POLL_TIMEOUT = 8;             // MEMCACHED_BEHAVIOR_POLL_TIMEOUT
    const OPT_CACHE_LOOKUPS = 6;            // MEMCACHED_BEHAVIOR_CACHE_LOOKUPS
    const OPT_SERVER_FAILURE_LIMIT = 21;    // MEMCACHED_BEHAVIOR_SERVER_FAILURE_LIMIT
    // In php_memcached config, define HAVE_MEMCACHED_IGBINARY default 1,
    // then use ifdef define HAVE_IGBINARY to 1.
    const HAVE_IGBINARY = 1;
    // In php_memcached config, define HAVE_JSON_API default 1,
    // then use ifdef define HAVE_JSON to 1.
    const HAVE_JSON = 1;
    // Defined in php_memcached.c, (1<<0)
    const GET_PRESERVE_ORDER = 1;
    // enum memcached_return_t in libmemcached
    const RES_SUCCESS = 0;                  // MEMCACHED_SUCCESS
    const RES_FAILURE = 1;                  // MEMCACHED_FAILURE
    const RES_HOST_LOOKUP_FAILURE = 2;      // MEMCACHED_HOST_LOOKUP_FAILURE
    const RES_UNKNOWN_READ_FAILURE = 7;     // MEMCACHED_UNKNOWN_READ_FAILURE
    const RES_PROTOCOL_ERROR = 8;           // MEMCACHED_PROTOCOL_ERROR
    const RES_CLIENT_ERROR = 9;             // MEMCACHED_CLIENT_ERROR
    const RES_SERVER_ERROR = 10;            // MEMCACHED_SERVER_ERROR
    const RES_WRITE_FAILURE = 5;            // MEMCACHED_WRITE_FAILURE
    const RES_DATA_EXISTS = 12;             // MEMCACHED_DATA_EXISTS
    const RES_NOTSTORED = 14;               // MEMCACHED_NOTSTORED
    const RES_NOTFOUND = 16;                // MEMCACHED_NOTFOUND
    const RES_PARTIAL_READ = 18;            // MEMCACHED_PARTIAL_READ
    const RES_SOME_ERRORS = 19;             // MEMCACHED_SOME_ERRORS
    const RES_NO_SERVERS = 20;              // MEMCACHED_NO_SERVERS
    const RES_END = 21;                     // MEMCACHED_END
    const RES_ERRNO = 26;                   // MEMCACHED_ERRNO
    const RES_BUFFERED = 32;                // MEMCACHED_BUFFERED
    const RES_TIMEOUT = 31;                 // MEMCACHED_TIMEOUT
    const RES_BAD_KEY_PROVIDED = 33;        // MEMCACHED_BAD_KEY_PROVIDED
    const RES_CONNECTION_SOCKET_CREATE_FAILURE = 11;    // MEMCACHED_CONNECTION_SOCKET_CREATE_FAILURE
    // Defined in php_memcached.c
    const RES_PAYLOAD_FAILURE = -1001;
    /**
     * Dummy option array
     *
     * @var array
     */
    protected $option = array(
        Memcached::OPT_COMPRESSION  => true,
        Memcached::OPT_SERIALIZER   => Memcached::SERIALIZER_PHP,
        Memcached::OPT_PREFIX_KEY   => '',
        Memcached::OPT_HASH         => Memcached::HASH_DEFAULT,
        Memcached::OPT_DISTRIBUTION => Memcached::DISTRIBUTION_MODULA,
        Memcached::OPT_LIBKETAMA_COMPATIBLE => false,
        Memcached::OPT_BUFFER_WRITES    => false,
        Memcached::OPT_BINARY_PROTOCOL  => false,
        Memcached::OPT_NO_BLOCK     => false,
        Memcached::OPT_TCP_NODELAY  => false,
        // This two is a value by guess
        Memcached::OPT_SOCKET_SEND_SIZE => 32767,
        Memcached::OPT_SOCKET_RECV_SIZE => 65535,
        Memcached::OPT_CONNECT_TIMEOUT  => 1000,
        Memcached::OPT_RETRY_TIMEOUT    => 0,
        Memcached::OPT_SEND_TIMEOUT     => 0,
        Memcached::OPT_RECV_TIMEOUT     => 0,
        Memcached::OPT_POLL_TIMEOUT     => 1000,
        Memcached::OPT_CACHE_LOOKUPS    => false,
        Memcached::OPT_SERVER_FAILURE_LIMIT => 0,
    );
    /**
     * Last result code
     *
     * @var int
     */
    protected $resultCode = 0;
    /**
     * Last result message
     *
     * @var string
     */
    protected $resultMessage = '';
    /**
     * Server list array/pool
     *
     * I added array index.
     *
     * array (
     *  host:port:weight => array(
     *      host,
     *      port,
     *      weight,
     *  )
     * )
     *
     * @var array
     */
    protected $server = array();
    /**
     * Socket connect handle
     *
     * Point to last successful connect, ignore others
     * @var resource
     */
    protected $socket = null;
    /**
     * Add a serer to the server pool
     *
     * @param   string  $host
     * @param   int     $port
     * @param   int     $weight
     * @return  boolean
     */
    public function addServer($host, $port = 11211, $weight = 0)
    {
        $key = $this->getServerKey($host, $port, $weight);
        if (isset($this->server[$key])) {
            // Dup
            $this->resultCode = Memcached::RES_FAILURE;
            $this->resultMessage = 'Server duplicate.';
            return false;
        } else {
            $this->server[$key] = array(
                'host'  => $host,
                'port'  => $port,
                'weight'    => $weight,
            );
            $this->connect();
            return true;
        }
    }
    /**
     * Add multiple servers to the server pool
     *
     * @param   array   $servers
     * @return  boolean
     */
    public function addServers($servers)
    {
        foreach ((array)$servers as $svr) {
            $host = array_shift($svr);
            $port = array_shift($svr);
            if (is_null($port)) {
                $port = 11211;
            }
            $weight = array_shift($svr);
            if (is_null($weight)) {
                $weight = 0;
            }
            $this->addServer($host, $port, $weight);
        }
        return true;
    }
    /**
     * Connect to memcached server
     *
     * @return  boolean
     */
    protected function connect()
    {
        $rs = false;
        foreach ((array)$this->server as $svr) {
            $error = 0;
            $errstr = '';
            $rs = @fsockopen($svr['host'], $svr['port'], $error, $errstr);
            if ($rs) {
                $this->socket = $rs;
            } else {
                $key = $this->getServerKey(
                    $svr['host'],
                    $svr['port'],
                    $svr['weight']
                );
                $s = "Connect to $key error:" . PHP_EOL .
                    "    [$error] $errstr";
                error_log($s);
            }
        }
        if (is_null($this->socket)) {
            $this->resultCode = Memcached::RES_FAILURE;
            $this->resultMessage = 'No server avaliable.';
            return false;
        } else {
            $this->resultCode = Memcached::RES_SUCCESS;
            $this->resultMessage = '';
            return true;
        }
    }
    /**
     * Delete an item
     *
     * @param   string  $key
     * @param   int     $time       Ignored
     * @return  boolean
     */
    public function delete($key, $time = 0)
    {
        $keyString = $this->getKey($key);
        $this->writeSocket("delete $keyString");
        $s = $this->readSocket();
        if ('DELETED' == $s) {
            $this->resultCode = Memcached::RES_SUCCESS;
            $this->resultMessage = '';
            return true;
        } else {
            $this->resultCode = Memcached::RES_NOTFOUND;
            $this->resultMessage = 'Delete fail, key not exists.';
            return false;
        }
    }
    /**
     * Retrieve an item
     *
     * @param   string  $key
     * @param   callable    $cache_cb       Ignored
     * @param   float   $cas_token          Ignored
     * @return  mixed
     */
    public function get($key, $cache_cb = null, $cas_token = null)
    {
        $keyString = $this->getKey($key);
        $this->writeSocket("get $keyString");
        $s = $this->readSocket();
        if (is_null($s) || 'VALUE' != substr($s, 0, 5)) {
            $this->resultCode = Memcached::RES_FAILURE;
            $this->resultMessage = 'Get fail.';
            return false;
        } else {
            $s_result = '';
            $s = $this->readSocket();
            while ('END' != $s) {
                $s_result .= $s;
                $s = $this->readSocket();
            }
            $this->resultCode = Memcached::RES_SUCCESS;
            $this->resultMessage = '';
            return unserialize($s_result);
        }
    }
    /**
     * Get item key
     *
     * @param   string  $key
     * @return  string
     */
    public function getKey($key)
    {
        return addslashes($this->option[Memcached::OPT_PREFIX_KEY]) . $key;
    }
    /**
     * Get a memcached option value
     *
     * @param   int     $option
     * @return  mixed
     */
    public function getOption($option)
    {
        if (isset($this->option[$option])) {
            $this->resultCode = Memcached::RES_SUCCESS;
            $this->resultMessage = '';
            return $this->option[$option];
        } else {
            $this->resultCode = Memcached::RES_FAILURE;
            $this->resultMessage = 'Option not seted.';
            return false;
        }
    }
    /**
     * Return the result code of the last operation
     *
     * @return  int
     */
    public function getResultCode()
    {
        return $this->resultCode;
    }
    /**
     * Return the message describing the result of the last opteration
     *
     * @return  string
     */
    public function getResultMessage()
    {
        return $this->resultMessage;
    }
    /**
     * Get key of server array
     *
     * @param   string  $host
     * @param   int     $port
     * @param   int     $weight
     * @return  string
     */
    protected function getServerKey($host, $port = 11211, $weight = 0)
    {
        return "$host:$port:$weight";
    }
    /**
     * Get list array of servers
     *
     * @see     $server
     * @return  array
     */
    public function getServerList()
    {
        return $this->server;
    }
    /**
     * Read from socket
     *
     * @return  string|null
     */
    protected function readSocket()
    {
        if (is_null($this->socket)) {
            return null;
        }
        return trim(fgets($this->socket));
    }
    /**
     * Store an item
     *
     * @param   string  $key
     * @param   mixed   $val
     * @param   int     $expt
     * @return  boolean
     */
    public function set($key, $val, $expt = 0)
    {
        $valueString = serialize($val);
        $keyString = $this->getKey($key);
        $this->writeSocket(
            "set $keyString 0 $expt " . strlen($valueString)
        );
        $s = $this->writeSocket($valueString, true);
        if ('STORED' == $s) {
            $this->resultCode = Memcached::RES_SUCCESS;
            $this->resultMessage = '';
            return true;
        } else {
            $this->resultCode = Memcached::RES_FAILURE;
            $this->resultMessage = 'Set fail.';
            return false;
        }
    }
    /**
     * Set a memcached option
     *
     * @param   int     $option
     * @param   mixed   $value
     * @return  boolean
     */
    public function setOption($option, $value)
    {
        $this->option[$option] = $value;
        return true;
    }
    /**
     * Set memcached options
     *
     * @param   array   $options
     * @return  bollean
     */
    public function setOptions($options)
    {
        $this->option = array_merge($this->option, $options);
        return true;
    }
    /**
     * Increment numeric item's value
     *
     * @param string $key           The key of the item to increment.
     * @param int    $offset        The amount by which to increment the item's value.
     * @param int    $initial_value The value to set the item to if it doesn't currently exist.
     * @param int    $expiry        The expiry time to set on the item.
     *
     * @return mixed                Returns new item's value on success or FALSE on failure.
     */
    public function increment($key, $offset = 1, $initial_value = 0, $expiry = 0)
    {
        if (($prevVal = $this->get($key))) {
            if (!is_numeric($prevVal)) {
                return false;
            }
            $newVal = $prevVal + $offset;
        } else {
            $newVal = $initial_value;
        }
        $this->set($key, $newVal, $expiry);
        return $newVal;
    }
    /**
     * Write data to socket
     *
     * @param   string  $cmd
     * @param   boolean $result     Need result/response
     * @return  mixed
     */
    protected function writeSocket($cmd, $result = false)
    {
        if (is_null($this->socket)) {
            return false;
        }
        fwrite($this->socket, $cmd . "\r\n");
        if (true == $result) {
            return $this->readSocket();
        }
        return true;
    }
}

class MCache {
	//memcached
	protected static function conn() {
		static $conn;
		if (!$conn) {
			// if (!class_exists('Memcached')) {
			//     include ("memcached.php");
			// }			
			$conn = new Memcached();
			$conn->addServers([
				[COMMON_MCACHE_HOST1, COMMON_MCACHE_PORT],
			]);
			// debug_log_master(COMMON_MCACHE_HOST1, 'HTTP_UNAUTHORIZED');
			$conn->setOption(Memcached::OPT_BINARY_PROTOCOL, true);
			$conn->setOption(Memcached::OPT_PREFIX_KEY, MCACHE_KEY);
		}
		return $conn;
	}

	public static function getConn() {
		return static::conn();
	}

	public static function set($k, $v, $ttl = 0) {
		// debug_var($k);
		// debug_var($v);


		return static::conn()->set($k, $v, $ttl);
	}

	public static function get($k) {
		return static::conn()->get($k);
	}

	public static function mset($ks, $ttl = 0) {
		return static::conn()->setMulti($ks, $ttl);
	}

	public static function mget($ks) {

		if (count($ks) < 100) {
			return static::conn()->getMulti($ks);
		}
		$result = [];
		while ($ks) {
			$_ks = array_splice($ks, 0, 95);
			$r = static::conn()->getMulti($_ks);
			if ($r && is_array($r)) {
				$result = array_merge($result, $r);
			}
		}
		return $result;

		// return static::conn()->getMulti($ks);
	}


	public static function del($k) {
		return static::conn()->delete($k);
	}

	public static function inc($k, $v = 1, $ttl = 0) {
		return static::conn()->increment($k, (int) $v, (int) $v, $ttl);
	}

	public static function dec($k, $v = 1, $ttl = 0) {
		return static::conn()->decrement($k, (int) $v, -((int) $v), $ttl);
	}

	public static function getAllKeys() {
		return static::conn()->getAllKeys();
	}

	public static function error() {
		return static::conn()->getResultMessage();
	}

	public static function errno() {
		return static::conn()->getResultCode();
	}
}

class Mongo {
	protected static function client() {
		static $client;
		if (!$client) {
			$client = new MongoDB\Driver\Manager(MONGODB_HOST);
		}
		return $client;
	}
	public static function DB($name, $collection) {
		// $collection = static::client()->{$name}->{$collection};
		// return $collection;
	}

	public static function write($name, $collection, $data) {
		$bulk = new MongoDB\Driver\BulkWrite;
		$bulk->insert($data);
		static::client()->executeBulkWrite($name . '.' . $collection, $bulk);
	}

	public static function getclient() {
		return static::client();
	}
}

class Redis_Cache{

	protected static function conn(){
		static $conn;
		static $try_count;
		if(!$conn){
			if($try_count > 3) return '';
			$conn = new Redis();
			try{
				$tt = $conn->connect(REDIS_HOST , REDIS_PORT);		
				
				if(!$tt){
					$try_count++;
					$conn = '';
				} else{
					$try_count = 0;
				}
			} catch(Exception $e){
				$conn = '';
			}			
		}
		return $conn;
	}

	public static function get($key){
		if(!static::conn()) return '';
		return static::conn()->GET($key);
	}	

	public static function set($k, $v, $ttl = 10) {
		if(static::conn()){		
			static::conn()->SET($k, $v);
			if($ttl){
				static::conn()->EXPIRE ($v , $ttl);
			}
		}
	}
}

class CallCache{

	protected static function conn(){
		static $conn;
		if(!$conn){
			$ci = &get_instance();
			$ci->load->driver('cache', array('adapter' => 'apc', 'backup' => 'file' , 'key_prefix' => '')); 
			$conn = $ci->cache;
		}

		return $conn;
	}

	public static function getConn(){
		return static::conn();
	}

	public static function set($k, $v , $ttl = 60){
		
		return  static::conn()->save($k, $v, $ttl);


	}

	public static function get($k){
		$ds = static::conn()->apc->is_supported();
		debug_var($ds);
		exit;

		$dd = static::conn()->get($k);	

		debug_var($dd);
		return $dd;
	}
}

require_once 'external/util.php';
require_once 'external/db.php';
