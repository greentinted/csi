<?php
defined('BASEPATH') OR exit('No direct script access allowed');

function make_html_header($data = []) {
	$html = '<thead><tr>';
	foreach ($data as $key => $value) {
		if (is_array($value)) {
			$html .= '<th width="' . $value[0] . '">' . $value[1] . '</th>';
		} else {
			$html .= '<th>' . $value . '</th>';
		}
	}
	$html .= '</tr></thead>';
	return $html;
}
function make_html_tr($data = []) {
	$html = '<tr>';
	foreach ($data as $key => $value) {
		$html .= '<td>' . $value . '</td>';
	}
	$html .= '</tr>';
	return $html;
}

function make_html_overlay_helper($title){
		$content = '<div class="wrap">' . 
            '    <div class="info">' . 
            '        <div class="title">' .  $title . 
            '            <div class="close" onclick="closeOverlay()" title="닫기"></div>' . 
            '        </div>' . 
            '        <div class="body">' . 
            '            <div class="img">' .
            '                <img src="http://cfile181.uf.daum.net/image/250649365602043421936D" width="73" height="70">' .
            '           </div>' . 
            '            <div class="desc">' . 
            '                <div class="ellipsis">제주특별자치도 제주시 첨단로 242</div>' . 
            '                <div class="jibun ellipsis">(우) 63309 (지번) 영평동 2181</div>' . 
            '                <div><a href="http://www.kakaocorp.com/main" target="_blank" class="link">홈페이지</a></div>' . 
            '            </div>' . 
            '        </div>' . 
            '<div id="chartContainer" style="height: 300px; width: 100%;"></div>'.
            '    </div>' .    
            '</div>';

        $content += '<script type="text/javascript">';

        $content += ' var chart = new CanvasJS.Chart("chartContainer", {';

        $content += '           title:{';
        $content += '          text: "Fruits sold in First Quarter" ';             
        $content += '}, ';
        $content += 'data: [//array of dataSeries ';             
        $content += '{ //dataSeries object ';

                         /*** Change type "column" to "bar", "area", "line" or "pie"***/
        $content += 'type: "column",';
        $content += 'dataPoints: [';
        $content += '{ label: "banana", y: 18 },';
        $content += '{ label: "orange", y: 29 },';
        $content += '{ label: "apple", y: 40 },                                ';    
        $content += '{ label: "mango", y: 34 },';
        $content += '{ label: "grape", y: 24 }';
        $content += ']';
        $content += '}';
        $content += ']';
        $content += '});';
                      

        $content += 'chart.render();';

        $content += '</script>';


        return $content;

}

function make_html_template_change(){
	
	$html = "";
  	$html .= "<script>";

	$html .=  "$('#wtlt_id').on('change', function (e) {";
	$html .=  "var val = $(e.target).val();";
	$html .=  "var wruid = $('#wruid').val();";
	$html .=  "var wrid = $('#id').val();";
	// $html .=  "alert(wruid);";
	$html .=  "if(wruid == undefined){wruid=0;}";
	// $html .=  "var text = $(e.target).find('option:selected').text();";
	// $html .=  "var name = $(e.target).attr('name');";



	// $html .= "alert(val);";
	// $html .= "alert(wrid);";
	// $html .= "alert(wruid);";

	$html .= "$.ajax({";
    $html .= "url:'/home/workrecord/worktemplate_change',";

    $html .= "method:'post',";  
    $html .= "data:{id:val,wruid:wruid,wrid:wrid},";
    $html .= "success:function(data){";	            

    $html .= "if(data.status === true){";
    // $html .= "alert(data.status);";
    // $html .=  "console.log(data);";
                 // $('#monthly_calendar_detail').html(data.output);  
                 // $('#dataModal').modal("show");
	//$html .= "$('#wpid').html('');";

    // $html .= "alert('val : ' +val)";

	$html .= "if(val == 1000){
		//alert(data.is_edit);
		if(data.is_edit == true){
			//alert(1);
   			$('#etc_calendar_edit_detail').html(data.output);    			
		} else {
			//alert(2);
			$('#etc_calendar_detail').html(data.output);
		}
	} else {
		//alert(data.is_edit);

		if(data.is_urgent == true){
			if(data.is_edit == true){
				//alert(1);
	   			$('#etc_calendar_edit_detail').html(data.output);    			
			} else {
				//alert(2);
				$('#etc_calendar_detail').html(data.output);
			}

		} else{
			if(data.is_edit == true){
				//alert(data.output);
	   			$('#etc_calendar_edit_detail').html('');    			
			} else {
				//alert(4);
				$('#etc_calendar_detail').html('');
			}

		}

	}";


    //$html .= "alert(data.html);";
	

	//$html .= "$('#wpid').html(data.html);";



	$html .= "}";
    $html .= "}";


             // $('#monthly_calendar_detail').html(data);  
             // $('#dataModal').modal("show");  

	$html .="});";
	$html .="});";

  		
	$html .=   "</script>";

	return $html;

}

function make_html_district_change(){

	$html= "";
  	$html .= "<script>";

	$html .=  "$('#did_did').on('change', function (e) {";
	$html .=  "var val = $(e.target).val();";
	// $html .=  "var text = $(e.target).find('option:selected').text();";
	// $html .=  "var name = $(e.target).attr('name');";

	// $html .= "alert(val);";

	$html .= "$.ajax({";
    $html .= "url:'/home/workrecord/district_change',";

    $html .= "method:'post',";  
    $html .= "data:{did:val},";
    $html .= "success:function(data){";	            

    $html .= "if(data.status==true){";
    //$html .=  "alert(data.msg);";
                 // $('#monthly_calendar_detail').html(data.output);  
                 // $('#dataModal').modal("show");
	//$html .= "$('#wpid').html('');";

    //$html .= "$('#wpid').html('');";
    //$html .= "alert(data.html);";
	$html .= "$('#wpid').html(data.html);";


	$html .= "} else {";
	$html .= " alert(data.msg);";
    //location.reload();
    $html .="}}";

    // $('#monthly_calendar_detail').html(data);  
    // $('#dataModal').modal("show");  

	$html .="});";
	$html .="});";

  		
	$html .=  "</script>";

	return $html;

}

function make_html_input_text($data = []) {
	$html = '';
	$f_w = '';
	$textarea_rows = '10';

	// debug_var($data);
	// exit;

	foreach ($data as $key => $value) {
		//debug_var($value['name']);
		//debug_var($value['placeholder']);
		// exit;

		

		// 신규 입력이 없어도 죽이지는 말자.. 
		if(!isset($value['name_desc'])){
			$name_desc = $value['name'];
		} else {
			$name_desc = $value['name_desc'];
		}

		$html .= '<div class="form-group row">';
			$html .= '    <label for="' . $value['name'] . '" ' . $f_w . ' class="col-sm-2  text-right form-control-label">' . $name_desc . '</label>';


		//debug_var($html);
		//exit;

		if (isset($value['message'])) {
			$html .= '    <div class="col-xs-2">';
		} else {
			$html .= '    <div class="col-sm-10">';
		}

		if ($value['type'] == 'textarea') {
			if (isset($value['textarea_rows'])) {
				$textarea_rows = $value['textarea_rows'];
			}
			$html .= '        <textarea class="form-control" rows="' . $textarea_rows . '" name="' . $value['name'] . '" placeholder="' . $value['placeholder'] . '">' . $value['value'] . '</textarea>';
		}

		else if($value['type'] == 'text_hidden'){
			$html .= '        <input type="' . $value['type'] . '"  class="form-control" name="' . $value['name'] . '" id="' . $value['name'] . '" placeholder="' . $value['placeholder'] . '" value="' . $value['value'] . '"  readonly>';

		}

		else if($value['type'] == 'color'){
		

		} else if ($value['type'] == 'checkbox') {
			$html .= '        <div class="form-control-label text-left"><label class="md-check"><input type="checkbox" name="' . $value['name'] . '" value="Y" ' . return_checked($value['value'], 'Y') . '><i class="indigo"></i>' . $value['placeholder'] . '</label></div>';

		} else if ($value['type'] == 'date') {
			$html .= '        <input type="text"  class="form-control" name="' . $value['name'] . '" id="' . $value['name'] . '" placeholder="' . $value['placeholder'] . '" value="' . $value['value'] . '" ui-jp="datetimepicker" ui-options="{locale: \'ko\',format: \'YYYY-MM-DD HH:mm\'}" >'; 

		} else {

			$html .= '        <input type="' . $value['type'] . '"  class="form-control" name="' . $value['name'] . '" id="' . $value['name'] . '" placeholder="' . $value['placeholder'] . '" value="' . $value['value'] . '">';

			//debug_var($value['name']);

			//debug_var($html);
			//exit;
		}

		if ($value['type'] == 'file' && isset($value['img']) && $value['img']) {
			$html .= '<div class="form-group row">';
			// $html .= '    <label for="" class="col-sm-2 form-control-label"></label>';
			$html .= '    <div class="col-sm-10 text-left">';
			$html .= '        <img src="' . $value['img'] . '" width="100">';
			$html .= '    </div>';
			$html .= '</div>';
		}
		$html .= '    </div>';

		if (isset($value['message']) && $value['message']) {
			$html .= '    <div class="col-xs-7 form-control-label">';
			$html .= $value['message'];
			$html .= '</div>';
		}

		$html .= '</div>';
	}

	//debug_var($html);
	//exit;
	return $html;
}

function make_html_input_checkbox($name = '', $data = [], $is_row = true) {
	$html = '';
	if ($is_row) {
		$html .= '<div class="form-group row">';
		$html .= '	<label for="' . $name . '" class="col-sm-2 text-right form-control-label">' . $name . '</label>';
		$html .= '    <div class="col-sm-10">';
	}

	$html .= '        <div class="form-control-label text-left">';
	foreach ($data as $key => $value) {
		$html .= '<label class="md-check"><input type="checkbox" name="' . $value['name'] . '" value="Y" ' . return_checked($value['value'], 'Y') . '><i class="indigo"></i>' . $value['placeholder'] . '</label>&nbsp;&nbsp;&nbsp;&nbsp;';
	}
	$html .= '   	</div>';

	if ($is_row) {
		$html .= '    </div>';
		$html .= '</div>';
	}

	return $html;
}

function make_html_input_select_array($name = '', $placeholder = '', $data = [], $select_value = '', $is_row = true) {
	$html = '';
	if ($is_row) {
		$html .= '<div class="form-group row">';
		$html .= '	<label for="' . $name . '" class="col-sm-2 text-right form-control-label">' . $placeholder . '</label>';
		$html .= '    <div class="col-sm-10">';
	}

	$html .= '		<select id="' . $name . '" name="' . $name . '" class="form-control select2" ui-jp="select2" ui-options="{theme: \'bootstrap\'}">';
	$html .= '    		<option value="">Select Type.</option>';

	//debug_var($data);

	foreach ($data as $key => $value) {
		// debug_var($value);
		// debug_var($key);
		//debug_var($select_value);
		$html .= '    	<option value="' . $key . '" ' . return_select($key, $select_value) . '>' . $value . '</option>';
	}
	$html .= '   	</select>';

	if ($is_row) {
		$html .= '    </div>';
		$html .= '</div>';
	}
	exit;

	return $html;

}

function make_html_input_color_select($name = '', $placeholder = '', $data = [], $select_value = '', $is_row = true) {
	$html = '';
	if ($is_row) {
		$html .= '<div class="form-group row">';
		$html .= '	<label for="' . $name . '" class="col-sm-2 text-right form-control-label">' . $placeholder . '</label>';
		$html .= '    <div class="col-sm-10">';
	}

	$html .= '		<select id="' . $name . '" name="' . $name . '" class="form-control select2" ui-jp="select2" ui-options="{theme: \'bootstrap\'}">';


	foreach ($data as $key => $value) {
		//debug_var($value);
		//exit;
		//debug_var($key);
		//debug_var($select_value);

		// $html .= '    	<option style = "'.$value['color'].'" . value="' . $key . '" ' . return_select($key, $select_value) . '>' . $value['title'] . ' 	'  . '</option>';
		
		$html .= ' <option style="color:'.$value['color'].'" value="'.$value['rcid'].'"'.return_select($value['rcid'], $select_value).'>&#9724; '.$value['title'].'</option>
		';
	}

	$html .= '   	</select>';

	if ($is_row) {
		$html .= '    </div>';
		$html .= '</div>';
	}
	//exit;

	// debug_var($html);
	// exit;

	return $html;

}


function make_html_input_select($name = '', $placeholder = '', $data = [], $select_value = '', $is_row = true) {
	$html = '';
	if ($is_row) {
		$html .= '<div class="form-group row">';
		$html .= '	<label for="' . $name . '" class="col-sm-2 text-right form-control-label">' . $placeholder . '</label>';
		$html .= '    <div class="col-sm-10">';
	}

	$html .= '		<select id="optionlist1" name="' . $name . '" class="form-control select2" ui-jp="select2" ui-options="{theme: \'bootstrap\'}">';


	// foreach ($data as $key => $value) {
		// debug_var($value);
		// debug_var($key);
		// debug_var($select_value);

		$html .= '  <option id="" value=" ">부서를 선택하세요.</option>';
		// $html .= '    	<option id="" value="' . $key . '" ' . return_select($key, $select_value) . '>' . $value . '</option>';
	// }

	$html .= '   	</select>';

	if ($is_row) {
		$html .= '    </div>';
		$html .= '</div>';
	}
	//exit;

	return $html;
}

function make_html_input_select2($name = '', $placeholder = '', $data = [], $select_value = '', $is_row = true) {
	$html = '';

	if ($is_row) {
		$html .= '<div class="form-group row">';
		$html .= '	<label for="' . $name . '" class="col-sm-2 text-right form-control-label">' . $placeholder . '</label>';
		$html .= '    <div class="col-sm-10">';
	}

	$html .= '		<select id="taskOption" name="' . $name . '" class="form-control select2 get_divisions" ui-jp="select2" ui-options="{theme: \'bootstrap\'}">';


	foreach ($data as $key => $value) {
		// debug_var($value);
		//debug_var($key);
		//debug_var($select_value);

		$html .= '    	<option value="' . $key . '" ' . return_select($key, $select_value) . '>' . $value . '</option>';
	}

	$html .= '   	</select>';

	if ($is_row) {
		$html .= '    </div>';
		$html .= '</div>';
	}
	//exit;

	$html .= '


<script type="text/javascript">
	$(".get_divisions").change(function() {
		var selelct_opt = $(".get_divisions").val();
		console.log(selelct_opt);

  	$.ajax({
        url:"/admin/Board/operator_division",
        method:"post",
        data:{fct_code:selelct_opt},
        success:function(data) {

        $("#optionlist1").empty();
        var list = data.account_list;

			if(Object.keys(list).length == 0){
				var option = $("<option> 등록된 담당자가 없습니다.</option>");
				$("#optionlist1").append(option);
			}
			else{
				for(var count = 0; count < Object.keys(list).length; count++){	
	    		var option = $("<option value="+Object.keys(list)[count]+">"+Object.values(list)[count]+"</option>");

			$("#optionlist1").append(option);
			}		
	 			}		
		}
	});

	})
</script> ';

	return $html;
}


/* End of file html_make_helper.php */
/* Location: ./application/helpers/html_make_helper.php */