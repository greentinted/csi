
<style>
img {
  vertical-align: middle;
  max-width:100%;
}

.media-body{
  padding-left:30px;
}

  
</style>


<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?=$board_detail['title']?></h2> <h2 style="font-size: 15px">&nbsp&nbsp(작성자 : <?=$board_detail['writer']?>)<small>읽음 : <?=$view_count?></small></h2>

        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
              <?php if($board_detail['writer_aid'] == $aid) { ?>
            <ul class="dropdown-menu" role="menu">
                <li><a href="/board/board/write?bid=<?=$board_detail['bid']?>" class="">게시물 수정</a></li>
           </ul>
              <?php } ?>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
            <div class="col-md-12 col-sm-12 col-xs-12">
              <?php if($board_detail['is_attch_file']) { ?>
                  <p style="float:right">
                    <a class="btn btn-app" href="<?=$board_detail['attch_file_dir']?>" download><i class="fa fa-save"></i><?=$board_detail['is_attch_file']?></a>
                  </p>
              <?php } ?>

                <div style="padding-left:15px; padding-right: 15px; float:left; text-align: left; padding-bottom: 15px; max-width: 100%; width:auto; ">
                    <p>
                        <?=$board_detail['description']?>
                    </p>
                </div>
          </div>
          
      </div>
    </div>

  <div class="x_panel">
      <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
          <h2><small class="pull-right"></small> 댓글 </h2>
        </div> 
         <div class="comments-list">
                <div class="media">
                <?php foreach ($ment_list as $key => $value) :?>
                    <p class="pull-right"><small><?=$value['create_time']?></small></p>
                    <img src="<?=$value['profile_img']?>" class="avatar" alt="Avatar">
                    </a>
                    <div class="media-body">
                        
                      <h4 class="media-heading user_name"><?=$value['writer']?></h4>
                      
                      <br><p   style="font-size:20px;height: auto;
                        resize:both;overflow: auto;"><?=nl2br($value['body'])?></p>

                      <p><small>
                        <a id="<?=$value['mid']?>" class="reply_ment_btn" >댓글달기</a>
                        <?php if($aid == $value['aid']) { ?>
                           <a id="<?=$value['mid']?>" class="delete_ment_btn" >&nbsp&nbsp삭제하기</a>
                        <?php } ?>
                      </small></p>
                      <hr>

                      <div class="input-group" id="div_reply_ment<?=$value['mid']?>" style="display: none">

                        <textarea id="reply_ment_body<?=$value['mid']?>" class="form-control" style="height:70px; font-size:20px"></textarea>


                          <span class="input-group-btn">
                        <button type="button" id="<?=$value['mid']?>" class="btn btn-primary submit_reply_ment" style="height: 70px;width: 100px;font-size: 20px;">작성</button>
                        </span>
                      </div>
                  
                      <?php if(isset($value['c_list'])): ?>
                      <?php foreach ($value['c_list'] as $key1 => $value1): ?>
                        <div class="reply">
                          <p class="pull-right"><small><?=$value1['create_time']?></small></p>
                          <img src="<?=$value1['profile_img']?>" class="avatar" alt="Avatar">
                          <div class="media-body">                              
                            <h4 class="media-heading user_name"><?=$value1['writer']?></h4>                            
                            <br>&nbsp&nbsp<p><?=nl2br($value1['body'])?></p>
                            <?php if($aid == $value1['aid']) { ?>
                              <p><small><a id="<?=$value1['mid']?>" class="delete_ment_btn" >삭제하기</a></small></p>
                            <?php } ?>
                          <hr>
                          </div>
                        </div>
                        <?php endforeach ?>
                      <?php endif ?>
                      <div id="reply_ment_add<?=$value['mid']?>"> </div>

                    </div>
                <?php endforeach?>
                <div id="ment_add"></div>
                    <p class="pull-right"><small></small></p>
                    </a>
                    <div class="media-body">
                        
                      <h4 class="media-heading user_name"><?=$user_id?></h4>
                      
                      <br>
                      <div class="input-group">
                        <textarea id="ment_body" class="form-control" style="height:70px; font-size:20px"></textarea>

<!--                         <input type="text" id="ment_body" class="form-control" style="height:70px; font-size:20px">
 -->                        <span class="input-group-btn">
                        <button type="button" id="submit_ment" class="btn btn-primary" style="height: 70px;width: 100px;font-size: 20px;">작성</button>
                        </span>
                      </div>
                      <hr>
                        <input type="hidden" id="ment_bid" value="<?=$board_detail['bid']?>">
                        <input type="hidden" id="ment_aid" value="<?=$aid?>">
                  </div>
              </div>         
            </div>
        </div>
      </div>
  <!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column

