<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">

				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br/>
				<form id="content_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/board/board/save" enctype="multipart/form-data">
					<input type="hidden" name="bid" value="<?=$bid?>">
					<input type="hidden" name="blid" value="<?=$blid?>">
					<input type="hidden" name="ptid" value="<?=$ptid?>">
					<input type="hidden" name="writer_aid" value="<?=$writer_aid?>">
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">제목 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$title?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">작성자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="writer" name="writer" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$writer?>"  readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">첨부파일
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" id="file" name="upload_file" class="form-control col-md-7 col-xs-12" placeholder="" ><?=$attch_file_dir?>
							<span class="fa fa-file form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>






					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 내용 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">

			            <textarea id="summernote" name="content"><?=$content?></textarea>

						</div>
					</div>





					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button type="button" id="content-save-btn" class="btn btn-success">저장</button>
							<button class="btn btn-primary" type="reset">리셋</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>



<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column