<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>게시판 <small><?=$parking_type_str?></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="
            button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/board/board/write?blid=<?=$blid?>" class="">게시물 작성하기</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>#</th>
              <th>구분</th>
              <th>제목</th>
              <th>읽음</th>
              <th>작성자</th>
              <th>작성일</th>              
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$key+1?></td>
                <td><?=$value['bname']?></a></td>
                <td><a href="/board/board/detail?bid=<?=$value['bid']?>"><?=$value['title']?></a></td>
                <td><?=$value['view_count']?></td>
                <td><?=$value['writer']?></td>
                <td><?=$value['create_date']?></td>
              </tr>
            <?php endforeach?>    
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>