<!------------------------------------모달창 실행------------------------------>
<div class="container">

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 id="modal-title" class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <table class="table">
                    <tr>
                        <td>사용자명</td>
                        <td><input class="form-control" id="userName" type="text"></td>
                    </tr>
                    <tr>
                        <td>내용</td>
                        <td><textarea class="form-control" id="contents" rows="10"></textarea></td>
                    </tr>                    
                </table>
            </div>
            <div class="modal-footer">
                <button id="modalSubmit" type="button" class="btn btn-success">Submit</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      
    </div>
  </div>
  
</div>    
<!-------------------------------------------모달창으로 게시판 참여 끝 ---------------------->

                  content +=    '    <ul>';
                  content +=    '        <li class="down" style="height:20px"> * 실내 공기질 측정값이 환경부의 대기 환경...</li>';

                  content +=    '        <li class="down" style="height:20px"> * 야탑 1환승 이용중인데 조도가 너무 어두...</li>';

                  content +=    '        <li class="down" style="height:20px"> * 지하 주차장 자주 이용하는 고객인데요 ...</li>';
                  content +=    '        <li class="down" style="height:20px; align:right;"><button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">참여하기</button></li>'