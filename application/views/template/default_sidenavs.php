<div class="col-md-3 left_col menu_fixed">
	<div class="left_col scroll-view">
 		<div class="navbar nav_title" style="border: 0;">
			<a href="<?php echo base_url(); ?>" class="site_title"><i class="fa fa-paw"></i> <span style="font-size: 20px;"><?php echo '고객서비스혁신' ?></span></a>
		</div>

		<div class="clearfix"></div>
		<!-- menu profile quick info -->
		<div class="profile clearfix">
			<div class="profile_pic">
				<img src="<?php echo base_url('assets/images/user.jpg') ?>" alt="..." class="img-circle profile_img">
			</div>
			<div class="profile_info">
				<span>Welcome,</span>
				<h2><?=$account['id']?></h2>
			</div>
		</div>
		<!-- /menu profile quick info -->
		<br>
		<!-- Sidebar Menu -->
		<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
			<div class="menu_section">
				<h3>게시글 관리</h3>
				<ul class="nav side-menu">
					<li><a><i class="fa fa-home"></i> 민원사항 <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
							<?php if($account['admin_type'] == 'super_admin' || $account['admin_type'] == 'admin'):?>
								<li><a href="<?php echo base_url('admin/board/index') ?>">담당자 관리(어드민만 노출됨)</a></li>
								<li><a href="<?php echo base_url('admin/surveyconf/') ?>">평가일정 조정</a></li>
								<li><a href="<?php echo base_url('admin/covid19/') ?>">코로나 현황관리</a></li>
								<li><a href="<?php echo base_url('admin/information/') ?>">공지사항 관리</a></li>

							<?php elseif($account['admin_type'] == 'operator'):?>
							<li><a href="<?php echo base_url('admin/board/operator_list') ?>">게시글 관리</a></li>


							<?php endif?>	

						</ul>
					</li>
					<li><a><i class="fa fa-users"></i> 계정관리  <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						<?php if($account['admin_type'] == 'super_admin' || $account['admin_type'] == 'admin'):?>		
							<li><a href="<?php echo base_url('admin/account/list') ?>">계정리스트</a></li>
						<?php endif?>		
						</ul>
					</li>
					<li><a><i class="fa fa-institution"></i> 시설물관리 <span class="fa fa-chevron-down"></span></a>
						<ul class="nav child_menu">
						</ul>
					</li>



				</ul>
			</div>
		</div>
		<!-- /Sidebar Menu -->

		<!-- menu footer buttons -->
		<div class="sidebar-footer hidden-small">
			<a data-toggle="tooltip" data-placement="top" title="Settings">
				<span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="FullScreen">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Lock">
				<span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
			</a>
			<a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
				<span class="glyphicon glyphicon-off" aria-hidden="true"></span>
			</a>
		</div>
		<!-- /menu footer buttons -->
	</div>
</div>