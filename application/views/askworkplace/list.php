<style>
	body{
		font-size:19px;
	}
 </style>
<script type="text/javascript">
		function getTime() { 
		now = new Date(); 
		deadline = document.getElementById("deadline").value;
		dday = new Date(deadline);
		// dday = new Date(2019,06,14,18,00,00);
		// alert(dday); 
		// 원하는 날짜, 시간 정확하게 초단위까지 기입.
		days = (dday - now) / 1000 / 60 / 60 / 24; 
		daysRound = Math.floor(days); 
		hours = (dday - now) / 1000 / 60 / 60 - (24 * daysRound); 
		hoursRound = Math.floor(hours); 
		minutes = (dday - now) / 1000 /60 - (24 * 60 * daysRound) - (60 * hoursRound); 
		minutesRound = Math.floor(minutes); 
		seconds = (dday - now) / 1000 - (24 * 60 * 60 * daysRound) - (60 * 60 * hoursRound) - (60 * minutesRound); 
		secondsRound = Math.round(seconds); 

		timer_text = "(마감시간까지  "+daysRound+"일 "+hoursRound+"시간 "+minutesRound+"분 "+secondsRound+"초 남았습니다.)";
		document.getElementById("clock").innerHTML = timer_text;

		// document.getElementById("left_days").innerHTML = daysRound; 
		// document.getElementById("left_hours").innerHTML = hoursRound; 
		// document.getElementById("left_minutes").innerHTML = minutesRound; 
		// document.getElementById("left_seconds").innerHTML = secondsRound; 
		newtime = window.setTimeout("getTime();", 1000); 
		timeflag = dday - now;

		if(timeflag <= 0) {
			alert("근무지 신청기간이 지났습니다");
			window.clearTimeout(newtime);
			location.href='/admin/draw_admin/set_end_state';
			// history.go(-1);
		}
	} 	
</script>

<input type="hidden" id="deadline" value="<?=$deadline?>" />


<div class="row">
<SPAN style="padding-left:30px;"></SPAN></div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>주차장 신청<small></small></h2>				
				<span style="FONT: bold 12px;  COLOR: #5b1e29" id="clock"></span>
				<SCRIPT>getTime();</SCRIPT>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a class="btn btn-warning" href="/admin/account/write">계정 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="ask_parkinglot_datatable" class="table table-striped table-bordered bulk_action dataTable no-footer dtr-inline collapsed">
					<thead>
						<tr>
							<th>구분</th>
							<th>지역구</th>
							<th>주차장명</th>
							<th>정원</th>
							<th>신청인원</th>
							<th>신청명단</th>
							<th>확정명단</th>
							<th>선택</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['dtitle']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['ch']?></td>	
							<td><?=$value['curr_cnt']['curr_cnt']?></td>	
							<td> 
								<?php if(isset($value['names'])):?>
									<?php foreach ($value['names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-primary"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['name_list']['name_list']):?>
										<button type="button" class="btn btn-round btn-primary"><?=$value['name_list']['name_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	


							<td> 
								<?php if(isset($value['fixed_names'])):?>
									<?php foreach ($value['fixed_names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-success"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['fixed_list']['fixed_list']):?>
										<button type="button" class="btn btn-round btn-success"><?=$value['fixed_list']['fixed_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	


							
							<form role="form" id="ask_btn_form" action="">
							<input type="hidden" id="wpid" name="wpid">
							<!-- 신청한 주차장은 취소버튼, 나머지는 버튼 비활성화 -->
							<?php 
							if($setting == "end") {
							?>
								<td><input type="button" value="마감" id="<?=$value['wpid']?>" class="btn btn-danger"></td>
							<?php
							} else {
								if($fixed_user_button_flag == "fixed_user"){
							?>
								<td><input type="button" value="선택불가" id="<?=$value['wpid']?>" class="btn btn-danger"></td>
							<?php

								} else { 
									if($ask_wpid) {
										if($ask_wpid == $value['wpid']) { 
								?>
										<td><input type="button" value="취소" id="<?=$value['wpid']?>" class="btn btn-success ask_cancel"></td>
								<?php 
										} else { 
								?>
										<td><input type="button" name="view" value="선택불가" id="<?=$value['wpid']?>" class="btn btn-warning"></td>
								<?php 
										}
									} else {
							?>
									<?php if($value['complete_flag'] == 1) { ?>
									<td><input type="button" value="마감" id="<?=$value['wpid']?>" class="btn btn-danger"></td>
									<?php } else {?>
									<td><input type="button" name="view" value="신청" id="<?=$value['wpid']?>" class="btn btn-primary ask_parkinglot"></td>
								<?php } ?>
							<?php
										}
									}
								}
							
							?>
							</form>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

 <div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">주차장 선택</h4>  
                </div>  
                <form role="form" id="ask_parkinglot_form" action="">
	                <div class="modal-body" id="ask_message">  

	                </div>  
	                <div class="modal-footer">  
	                     <button type="button" class="btn btn-primary ok_btn" data-dismiss="modal">확인 </button>
	                     <!-- <button type="button" class="btn btn-default" data-dismiss="modal">취소</button>   -->

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
						
