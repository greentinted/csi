<div class="row">

  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel  tile fixed_height_320">
      <div class="x_title">
        <h2>설문조사 요약</h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>타이틀</th>
              <th>결과</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>#</td>              
              <td>설문조사타이틀</td>              
              <td><?=$result[0]['title']?></td>              
              </tr>

            <tr>
              <td>#</td>              
              <td>참여자수</td>              
              <td><?=$result[1]['cnt']?>인</td>              
            </tr>

            <tr>
              <td>#</td>              
              <td>문항수</td>              
              <td><?=$result[2]['survey_cnt']?>문항</td>              
            </tr>

            <tr>
              <td>#</td>              
              <td>참여기간</td>              
              <td></td>              
            </tr>

          </tbody>
        </table>
      </div>
    </div>
  </div>


  <?php foreach ($sur as $key => $value) :?>
  
  <div class="col-md-4 col-sm-4 col-xs-12">
    <div class="x_panel tile fixed_height_320 overflow_hidden">
      <div class="x_title">
        <h2><?=$key+1?>.<?=$value['title']?></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <?php if($value['is_input_text']):?>
        <table class="table table-striped">
          <thead>
            <tr>
              <th>#</th>
              <th>NO</th>
              <th>답변</th>
              
            </tr>
          </thead>
          <tbody>
            <?php foreach ($value['participants'] as $key1 => $value1):?>
            <tr>
              <td>#</td>              
              <td><?=$key1+1?></td>              
              <td><?=$value1['user_input']?></td>              
              
            </tr>
          <?php endforeach?>
          </tbody>
        </table>

        <?php else:?>
        <table class="" style="width:100%">
          <tr>
            <th style="width:37%;">
              <p></p>
            </th>
            <th>
              <div class="col-lg-7 col-md-7 col-sm-7 col-xs-7">
                <p class="">답변</p>
              </div>
              <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5">
                <p class="">점유율(%)</p>
              </div>
            </th>
          </tr>
          <tr>
            <td>
              <canvas id="myChart_<?=$key?>"  height="140" width="140" style="margin: 15px 10px 10px 0"></canvas>
            </td>
            <td>
              <table class="tile_info" id="_tile_info_<?=$key?>">
                <?php foreach ($value['a_list'] as $key1 => $value1):?>
                <tr id="<?=$key?>_tr_<?=$key1?>">
                  <td>
                    <p><i class="<?=$value1['i_class']?>"></i><?=$value1['title']?> </p>
                  </td>
                  <?php if(isset($value1['share'])):?>
                    <td><?=$value1['share']?></td>
                  <?php else:?>
                    <td>0</td>
                  <?php endif?>
                </tr>
                <?php endforeach?>
              </table>
            </td>
          </tr>
        </table>

        <?php endif?>
      </div>
    </div>
  </div>
<?php endforeach?>
</div>

<div id="ssModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">설문조사 추가 / 수정</h4>  
                </div>  
                <form role="form" id="ss_edit" action="/admin/survey/ss_save">
                  <div class="modal-body" id="ss_detail">  

                  </div>  
                  <div class="modal-footer">  

                       <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

                       <button type="submit" class="btn btn-primary">저장 </button>  
                  </div>  
            </form>
           </div>  
      </div>  
 </div>  

<script>
$(document).ready(function(){
  for(var i = 0; i < 20; i++){
    var id = "myChart_"+i;
    var elem = document.getElementById(id);
    //console.log('elem : ' , elem); 
    if(elem == null) continue;
    var ctx = document.getElementById(id).getContext('2d');

    var tbl_id = '_tile_info_'+i;
    var table = document.getElementById(tbl_id);

    if(table == null) continue;

    //console.log('table : ' , table);
    var labels = [];
    var data = [];

    for( var j = 0; j < table.rows.length; j++){
      var col = table.rows[j].cells[1];

      console.log('col :' , col.innerHTML);

      data.push(col.innerHTML)
      //labels.push('A');

    }

    console.log('labels : ' , labels);

    var myChart = new Chart(ctx, {
      type: 'pie',
      data: {
        
        datasets: [{
          backgroundColor: [
            "blue",
            "red",
            "green",
            "black",
            "purple",
            "aero"
          ],
          data: data
        }],
        labels:  [],
      }
    });  


  }
           
})
</script>