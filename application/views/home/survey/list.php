<div class="row">
  <div class="col-md-8 col-sm-8 col-xs-12 col-md-offset-2">
    <div class="x_panel">
        <div class="x_title">
          <h2><?=$active_schedule['title']?> <small><?=$parking_type_str?></small>
          </h2>
          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/home/survey/submit"">

          <ul class="list-unstyled timeline" id="survey_ul">
            <input type="hidden" name="aid" value="<?=$aid?>">
            <input type="hidden" name="ssid" value="<?=$ssid?>">
            <?php foreach ($list as $key => $value):?>
            <li class="survey_li">
              <div class="block">
                <div class="tags">
                  <a href="" class="tag">
                    <span>Step-<?=$key+1?></span>
                  </a>
                </div>
                <div class="block_content">
                  <h2 class="title">
                    <a><?=$key+1?>.<?=$value['title']?></a>
                  </h2>
                  <?php if(!$value['is_input_text']):?>
                    <?php foreach ($value['a_list'] as $key1 => $value1) :?>
                      <div class="radio">
                        <label>
                          <input type="radio" required="required" id="options_radio_<?=$key?>" name="options_radio_<?=$key?>" value="<?=$value1['said']?>"> <?=$value1['title']?>
                        </label>
                      </div>

                    <?php endforeach?>
                  <?php else:?>
                    <textarea id="message_<?=$key?>" class="form-control" name="message_<?=$key?>" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.."
                      data-parsley-validation-threshold="10" required="required" ></textarea>

                  <?php endif?>  
                </div>
              </div>
            </li>
            <?php endforeach?>
          <!-- </ul> -->
        <div class="" style="text-align: center">
          <button type="reset" class="btn btn-warning">리셋</button>
          <button type="submit" class="btn btn-info">제출</button>
        </div>

        </form>
        </div>
      </div>

    </div>
</div>