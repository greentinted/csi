<div class="row">	
	<input type="hidden" id="workrecord_aid" name="workrecordname_aid" value="<?=$aid?>">
	<input type="hidden" id="default_month" name="default_month_name" value="<?=$default_month?>">
	<div class="col-md-7 col-sm-7 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>기본정보 <small><?=$account['parktype_info']['title']?> 소속 </small></h2>
				      <a style="margin-left: 15px;"class="btn btn-danger" href="/board/board/index?blid=18"><b>일일근무배치표 확인</b></a>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-striped jambo_table bulk_action">
						<thead>
							<tr class="headings">
								<th class="column-title">#</th>
								<th class="column-title">성명</th>
								<th class="column-title">직급</th>
								<th class="column-title">입사일자</th>
								<th class="column-title">근무지</th>
								<th class="column-title">근무지정</th>
								<th class="column-title">근무형태</th>
							</tr>
						</thead>

						<tbody>
							<tr class="even pointer">
								<td class=" "><?=$account['aid']?></td>
								<td class=" "> <?=$account['id']?> </td>
								<?php if(isset($account['rank_info']['title'])):?>
									<td class=" "> <?=$account['rank_info']['title']?> 
									</td>
								<?php else:?>
									<td></td>
								<?php endif?>

								<td class=" "> <?=$account['join_date']?> </td>
								<?php if(isset($account['wp_info']['title'])):?>
									<td class=" "> <?=$account['wp_info']['title']?></td>
								<?php else:?>
									<td></td>
								<?php endif?>
								<?php if(isset($account['wa_info']['title'])):?>
									<td class=" "> <?=$account['wa_info']['title']?></td>
								<?php else:?>
									<td></td>
								<?php endif?>
								<?php if(isset($account['ws_info']['title'])):?>
									<td class=" "> <?=$account['ws_info']['title']?></td>
								<?php else:?>
									<td></td>
								<?php endif?>
								
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-5 col-sm-5 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>출퇴근 정보 <small><?=$account['parktype_info']['title']?> 소속 </small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-striped jambo_table bulk_action">
						<thead>
							<tr class="headings">
								<th class="column-title">시간</th>
								<th class="column-title">ACTION</th>
							</tr>
						</thead>

						<tbody>
							<tr class="even pointer">
								<td id="clock">
									<script>
									 setInterval(function(){
								             
									            var timer = new Date();
									            var h = timer.getHours();
									            var m = timer.getMinutes();
									            var s = timer.getSeconds();
									            document.getElementById('clock').innerHTML = h + ":" + m + ":" + s;
									        },1000);
	     							</script>
     							</td>

								<td class=""> <button id="workstart_<?=$aid?>_<?=$crid?>" style="" class="btn btn-info work_check">출근체크(<?=$work_start_time?>)</button>
									<button id="workend_<?=$aid?>_<?=$crid?>" style="" class="btn btn-info work_check">퇴근체크(<?=$work_end_time?>)</button>
								</td>				
					

		
								
							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 내역 <small><?=$account['parktype_info']['title']?> 소속 </small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<div class="table-responsive">
					<table class="table table-striped jambo_table bulk_action" id="tr_work_confirm">
					<thead>
						<tr>
							<th>제출(月)</th>
							<th>근무(日)</th>
							<th>조정근무(日)</th>
							<th>휴무근무(日) </th>
							<th>휴일근무(日)</th>
							<th>무급휴무(日)</th>
							<th>연차(日)</th>
							<th>주휴(日)</th>
							<th>청원휴가(日) </th>
							<th>병가(日)</th>
							<th>휴직(日)</th>
							<th>기타(日)</th>
							<!-- <th>시간외(時)</th> -->
							<th>야간(時)</th>
							<th>휴일(時)</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td id="reg_month"><?=$reg_month?></td>
							<td id="working_days"><?=$working_days?></td>
							<td id="urgent_work"><?=$urgent_days?></td>							
							<td id="working_off_days"><?=$working_off_days?></td>
							<td id="working_holiday_days"><?=$working_holiday_days?></td>
							<td id="nopay_off_days"><?=$nopay_off_days?></td>			
							<td id="vacation_days"><?=$vacation_days?></td>			
							<td id="">0</td>							
							<td id="emergency_leave_days"><?=$emergency_leave_days?></td>
							<td id="sickleave_days"><?=$sickleave_days?></td>							
							<td id="restoff_days"><?=$restoff_days?></td>
							<td id="etc_days"><?=$etc_days?></td>
							<!-- <td id="overtime_hour"><?=$overtime_hour?></td> -->
							<td id="night_hour"><?=$night_hour?></td>
							<td id="holiday_hour"><?=$holiday_hour?></td>

							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>


	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월별근무상황</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div id="dataModal" class="modal fade">  
			      <div class="modal-dialog">  
			           <div class="modal-content">  
			                <div class="modal-header">  
			                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
			                     <h4 class="modal-title">근무 확인</h4>  
			                </div>  
			                <form role="form" id="monthly_calendar_edit" action="/home/workrecord/save">
				                <div class="modal-body" id="monthly_calendar_detail">  


				                </div>  
				                <div class="modal-body" id="etc_calendar_detail">  
								</div>				                
				                <div class="modal-footer">

				                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

				                     <button type="submit" class="btn btn-primary">저장 </button>  

				                </div>  

			            </form>
			           </div>  
			      </div>  
			 </div>  

			<div id="dataEditModal" class="modal fade">  
			      <div class="modal-dialog">  
			           <div class="modal-content">  
			                <div class="modal-header">  
			                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
			                     <h4 class="modal-title">근무 수정</h4>  
			                </div>  
			                <form role="form" id="monthly_calendar_edit_" action="/home/workrecord/save/edit">
				                <div class="modal-body" id="monthly_calendar_edit_detail">  
				                </div>  
				                <div class="modal-body" id="etc_calendar_edit_detail">  
				                </div>  

				                <div class="modal-footer">  

 				                	<?php if($user_type != 'operator'):?>
				                    	<button type="button" class="btn btn-danger wr_delete">삭제</button>  
				                    <?php endif?>

									

				                     <button id="btn-shutdown" type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

				                     <button type="submit" class="btn btn-primary">저장 </button> 
				                </div>  
			            </form>
			           </div>  
			      </div>  
			 </div>  

	        <div class="row">
	            <div class="col-lg-12 text-center">
	                <div id="calendar" class="col-centered <?=$default_month?>" style="font-size:1.3em;">
	                </div>
	            </div>				
	        </div>

    <!-- /.container -->

    <!-- jQuery Version 1.11.1 -->
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>


<script type="text/javascript">

$(document).ready(function() {

	$('.confirm_workrecord').click(function(){  
		//console.log('confirm work record')
		// $('#tr_work_confirm tr').each(function() {
		    var reg_month = $('#tr_work_confirm #reg_month').text();
		    var working_days = $('#tr_work_confirm #working_days').text();
		    var working_off_days = $('#tr_work_confirm #working_off_days').text();
		    var nopay_off_days = $('#tr_work_confirm #nopay_off_days').text();
		    var vacation_days = $('#tr_work_confirm #vacation_days').text();
		    var emergency_leave_days = $('#tr_work_confirm #emergency_leave_days').text();

		    var sickleave_days = $('#tr_work_confirm #sickleave_days').text();
		    var restoff_days = $('#tr_work_confirm #restoff_days').text();
		    var etc_days = $('#tr_work_confirm #etc_days').text();
		    var overtime_hour = $('#tr_work_confirm #overtime_hour').text();
		    var night_hour = $('#tr_work_confirm #night_hour').text();
		    var holiday_hour = $('#tr_work_confirm #holiday_hour').text();

		    var r = confirm("제출하시겠습니까?");
		    if(r == true){
			$.ajax({  
	                url:"/home/workrecord/submit_work_record",  
	                method:"post",  
	                data:{
	                	aid : $('#workrecord_aid').val(),
	                	reg_month:reg_month ,
	                	working_days:working_days ,
	                	working_off_days:working_off_days ,
	                	
	                	nopay_off_days:nopay_off_days ,
	                	vacation_days:vacation_days ,
	                	emergency_leave_days:emergency_leave_days ,
	                	
	                	sickleave_days:sickleave_days ,
	                	restoff_days:restoff_days ,
	                	etc_days:etc_days ,
	                	overtime_hour:overtime_hour ,
	                	night_hour:night_hour ,
	                	holiday_hour:holiday_hour ,

	                },  
	                success:function(data){  
	                	// console.log('data' , data);

	                     // $('#rank_detail').html(data);  
	                     // $('#dataModal').modal("show");  
	                }  
	        });  

		    }




		    //console.log('reg_month' , reg_month);

		    // var customerI = $(this).find("td:second").html();    
		    // console.log('customerId' , customerI);

		// });

	});
	$('#calendar').fullCalendar({
		//console.log($this);
		header: {

			left: 'prev,next today',
			center: 'title',
			right: 'month,basicWeek',
			
		},
		locale:'ko',
		defaultDate: $('#default_month').val(),
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		selectable: true,
		selectHelper: true,
		select: function(start, end) {
			

			var id = 0;
			var start = moment(start).format('YYYY-MM-DD HH:mm:ss');
			//var end = moment(end).format('YYYY-MM-DD HH:mm:ss');
			var end = moment(start).add("1" , "days").format('YYYY-MM-DD HH:mm:ss')
			//var end = 

			//var endtemp = moment(start).format('YYYY-MM-DD');


			var rdate = moment(start).format('YYYY-MM-DD');
			var aid = $('#workrecord_aid').val();


			//console.log('start ' , start);
			//console.log('end ' , end);

			

			$.ajax({  
	                url:"/home/workrecord/write",  
	                method:"post",  
	                data:{id:id , rdate:rdate, start:start, end:end , aid:aid},  
	                success:function(data){  
	                	//console.log('data :' , data);
	        			        		
		            	if(data.status==true){
			                $('#monthly_calendar_detail').html(data.output);  
			                if(data.is_urgent){
			                	$('#etc_calendar_detail').html(data.urgent);  
			             	} else {
			                	$('#etc_calendar_detail').html('');  
			             	}
			                 $('#dataModal').modal("show");		            		
			            } else {
			             	alert(data.msg);
			             	//location.reload();
			            }


	                     // $('#monthly_calendar_detail').html(data);  
	                     // $('#dataModal').modal("show");  
	                }  
	        });  


		},
		eventRender: function(event, element) {
			element.bind('dblclick', function() {
				start = event.start.format('YYYY-MM-DD HH:mm:ss');
				if(event.end){
					end = event.end.format('YYYY-MM-DD HH:mm:ss');
				}else{
					end = start;
				}
				var rdate = moment(start).format('YYYY-MM-DD');
				
				id =  event.id;

				var aid = $('#workrecord_aid').val();

				// console.log('id : ' , id);
				// console.log('start : ' , start);
				// console.log('end : ' , end);
				
				// Event = [];
				// Event[0] = id;
				// Event[1] = start;
				// Event[2] = end;
				
				$.ajax({
		            url:"/home/workrecord/write",  
		            method:"post",  
		            data:{id:id , rdate:rdate,  start:start, end:end , aid:aid},  
		            success:function(data){  
		            	//console.log('data' , data);
		            	//alert(data);

		            	if(data.status==true){
			                 $('#monthly_calendar_edit_detail').html(data.output);
			                 
			                 if(data.is_urgent){
				                $('#etc_calendar_edit_detail').html(data.urgent);  

			                 } else{
				                $('#etc_calendar_edit_detail').html('');

			                 }

			                 $('#dataEditModal').modal("show");		            		
			            } else {
			             	alert(data.msg);
			             	location.reload();
			            }

  
		            }  
				});



			});
		},
		eventDrop: function(event, delta, revertFunc) { // si changement de position

			//edit(event);

		},
		eventResize: function(event,dayDelta,minuteDelta,revertFunc) { // si changement de longueur

		//edit(event);

		},
		events: [
		
		
		
		<?php foreach($events as $event): 
			
			//print_r($event);
			$start = explode(" ", $event['start']);
			//debug_var($start);
			//exit;
			$end = explode(" ", $event['end']);
			if($start[1] == '00:00:00'){
				$start = $start[0];
			}else{
				$start = $event['start'];
			}
			if($end[1] == '00:00:00'){
				$end = $end[0];
			}else{
				$end = $event['end'];
			}
		?>
			{
				id: '<?php echo $event['wrid']; ?>',
				title: '<?php echo $event['title']; ?>',
				start: '<?php echo $start; ?>',
				end: '<?php echo $end; ?>',
				color: '<?php echo $event['color']; ?>',
			},
		<?php endforeach; ?>
		]
	});

	$('.fc-prev-button').click(function(){
	   //alert('prev is clicked, do something');

	   goWorkRecord();

	});

	$('.fc-next-button').click(function(){
	   //alert('nextis clicked, do something');
	   goWorkRecord();


	});	





	function goWorkRecord(){
	   var b = $('#calendar').fullCalendar('getDate');

	   var temp = (b.format('L'));

	   var str = temp.split('.');

	   var month = str[0] + '-' + str[1] + '-' + str[2];


	   var aid = $('#workrecord_aid').val();


	   //var aid = $('#workrecord_aid').val();
	   window.location.href = '/home/workrecord?aid=' +aid+'&month='+month;
	   //var aid = $('#workrecord_aid').val();

	}
	
	function edit(event){
		// start = event.start.format('YYYY-MM-DD HH:mm:ss');
		// if(event.end){
		// 	end = event.end.format('YYYY-MM-DD HH:mm:ss');
		// }else{
		// 	end = start;
		// }
		
		// id =  event.id;

		// console.log('id : ' , id);
		
		// Event = [];
		// Event[0] = id;
		// Event[1] = start;
		// Event[2] = end;
		

		// $.ajax({
  //           url:"/home/workrecord/write",  
  //           method:"post",  
  //           data:{id:id , start:start, end:end},  
  //           success:function(data){  
  //           	//console.log('data' , data);

  //                // $('#monthly_calendar_detail').html(data);  
  //                $('#dataModal').modal("show");  
  //           }  
		// });
	}
	
});

</script>
