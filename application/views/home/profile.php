<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>프로필 변경</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" method="post" action="/home/profile/reset" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data">


					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="<?=$aid?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">패스워드 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="password" id="passwd" name="passwd" class="form-control col-md-7 col-xs-12" placeholder="패스워드">
							<span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">패스워드 확인 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="password" id="passwd_check" name="passwd_check" class="form-control col-md-7 col-xs-12" placeholder="패스워드 확인">
							<span class="fa fa-lock form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> 이메일  
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="email" id="email_address" name="email_address" class="form-control col-md-7 col-xs-12" value="<?=$email?>">
							<span class="fas fa-envelope form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_img"> 프로필 이미지 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" id="profile_img" name="profile_img" class="form-control col-md-7 col-xs-12">
							<!-- 이미지 소스 필요.. ( 수정 작업 등) -->
							<img src="<?=$profile_img?>" width="100">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone_number"> 휴대폰 번호 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="cellphone_number" name="cellphone_number" class="form-control col-md-7 col-xs-12" pattern="\d{3}-\d{3,4}-\d{4}" value="<?=$cellphone?>">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="contact_number"> 비상연락처 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="contact_number" name="contact_number" class="form-control col-md-7 col-xs-12" placeholder="02-222-2222" value="<?=$contact_number?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>



					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="submit" class="btn btn-success">저장</button>
						</div>
					</div>

					

				</form>
			</div>
		</div>
	</div>
</div>
