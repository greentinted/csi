<!-- First Section one Column -->
<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>기본정보 <small><?=$account['parktype_info']['title']?> 소속</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control has-feedback-left" id="inputSuccess3" placeholder="김동동" value="<?=$account['real_name']?>">
					<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control" id="inputSuccess5" placeholder="고정" value="<?=$account['wp_info']['title']?>">
					<span class="fa fa-align-left form-control-feedback right" aria-hidden="true"></span>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control has-feedback-left" id="inputSuccess3" placeholder="김동동" value="<?=$account['wa_info']['title']?>">
					<span class="fa fa-refresh form-control-feedback left" aria-hidden="true"></span>
				</div>



				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control" id="inputSuccess4" placeholder="3교대" value="<?=$account['ws_info']['title']?>">
					<span class="fa fa-repeat form-control-feedback right" aria-hidden="true"></span>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control has-feedback-left" id="inputSuccess2" placeholder="일반8급" value="<?=$account['rank_info']['title']?>">
					<span class="fa fa-info-circle form-control-feedback left" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control" id="inputSuccess4" placeholder="" value="<?=$account['join_date']?>">
					<span class="fa fa-calendar form-control-feedback right" aria-hidden="true"></span>
				</div>

				<div class="clearfix"></div>

				<div class="x_title" style="padding-top: 75px;">
					<h2>휴가 정보 <small><?=$account['parktype_info']['title']?> 소속</small></h2>
					<div class="clearfix"></div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">발생 연차</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" class="form-control" readonly="readonly" placeholder="Read-Only Input" value="<?=$account['leave_info']['leave_days']?>일">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">사용연차</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" class="form-control" readonly="readonly" placeholder="Read-Only Input" value="<?=$account['leave_info']['use_leave_days']?>일">
					</div>
				</div>

				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">잔여연차</label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" class="form-control" readonly="readonly" placeholder="Read-Only Input" value="<?=$account['leave_info']['leave_days'] - $account['leave_info']['use_leave_days']?>일">
					</div>
				</div>
			</div>
		</div>

	</div>

	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>연차 마감 <small><small><?=$account['parktype_info']['title']?> 소속</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<?php foreach ($vac_deadlines as $key => $value): ?>

				
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">마감기간 </label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" class="form-control" disabled="disabled" placeholder="2019-05-01~2019-05-12" value="<?=$value['schedule']?>">
					</div>
				</div>



					
				<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12">마감일 </label>
					<div class="col-md-9 col-sm-9 col-xs-12">
						<input type="text" class="form-control" disabled="disabled" placeholder="2019-05-01" value="<?=$value['deadline']?>">
					</div>
				</div>

				
				<?php endforeach ?>

			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>휴가 신청 <small><?=$account['parktype_info']['title']?> 소속</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<form class="form-horizontal form-label-left" method="post" action="/home/appforvacation/save/vacation">

					<div class="form-group">
						<label class="col-sm-3 control-label">신청기간</label>

						<div class="col-sm-9">
	                        <form class="form-horizontal">
	                          <fieldset>
	                            <div class="control-group">
	                              <div class="controls">
	                                <div class="input-prepend input-group">
	                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                                  <input type="text" name="vacation_period" id="vacation_period" class="form-control"  value="" />
	                                </div>
	                              </div>
	                            </div>
	                          </fieldset>
	                        </form>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">휴가종류</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select id="select_worktype" name="select_worktype" class="form-control" tabindex="-1">
									<option value="0">Choose option</option>
									<?php foreach ($worktypes as $key => $value): ?>
										<option value="<?=$value['wtid']?>"><?=$value['title']?></option>
									<?php endforeach?>    						

								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">외출등의 시간 입력</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<select id="select_offhour" name="select_offhour" class="form-control" tabindex="-1">
									<option value="0">Choose option</option>
									<option value="1">01:00</option>
									<option value="2">02:00</option>
									<option value="3">03:00</option>
									<option value="4">04:00</option>
								</select>
							</div>
						</div>						

						<div class="form-group">
							<label class="control-label col-md-3 col-sm-3 col-xs-12">공가등의 사유</label>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<input type="text" id="aid" name="reason" class="form-control col-md-7 col-xs-12" placeholder="공가의 경우 사유를 입력하세요(ex) 교육 등" value="">

							</div>
						</div>						


						<div class="form-group">
							<div class="col-md-6 col-sm-6 col-xs-12">
								<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="<?=$account['aid']?>">
							</div>
						</div>

						<div class="ln_solid"></div>
						<div class="form-group">
							<div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
								<button type="reset" class="btn btn-primary">리셋</button>
								<button type="submit" class="btn btn-success">신청</button>
							</div>
						</div>



					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>휴가 신청 내역 <small><?=$account['parktype_info']['title']?> 소속</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="vr_datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>요청휴가일</th>
							<th>종류</th>												
							<th>승인날짜</th>							
							<th>상태</th>
							<th>삭제</th>							
							<th>신청사유</th>							
							<th>거절사유</th>							
						</tr>
					</thead>
	

					<tbody>
						<?php foreach ($vacations as $key => $value): ?>
						<tr>
							<td><?=$value['vrid']?></td>
							<td><?=$value['request_date']?></td>
							<?php if($value['is_duplicate']):?>
								<td><?=$value['wt_title']?> / <?=$value['off_hour']?> 시간 차감</td>							
							<?php else:?>
								<td><?=$value['wt_title']?>
							<?php endif?>
							
							<td><?=$value['approve_date']?></td>
							<?php if($value['v_status'] == 'request'):?>
								<td>요청 <a href="javascript:;"><i class="fa fa-child"></i></td>
							<?php elseif($value['v_status'] == 'approve'):?>
								<td>승인 <a href="javascript:;"><i class="fa fa-check"></i></td>
							<?php elseif($value['v_status'] == 'reject'):?>
								<td>거절 <a href="javascript:;"><i class="fa fa-ban"></i></td>
							<?php endif?>

							<?php if($value['v_status'] == 'approve'):?>
								<td><button id="<?=$value['vrid']?>" class="btn btn-success" >완료</button></td>
							<?php else:?>	
								<td><a onclick="return confirm('정말로 삭제하시겠습니까? ')" href="/home/appforvacation/delete?id=<?=$value['vrid']?>&aid=<?=$account['aid']?>" class="btn btn-danger">삭제</a></td>
							<?php endif?>							
							<td><?=$value['reason']?></td>
							<td><?=$value['reject_reason']?></td>
						</tr>
						<?php endforeach?>    						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 



