<style>
input{border:0px solid #000; margin:0; background:transparent; width:100%}
table tr td{border-right:1px solid #000; border-bottom:1px solid #000;}
table{background: #fff none repeat scroll 0 0;
    border-left: 1px solid #000;
    border-top: 1px solid #000;}
    table tr:nth-child(even){background:#eee;}
    table tr:nth-child(odd){background:#eee;}
</style>    

<div class="x_panel">
			<h2>시간대별 이용차량 조사표</h2>
	<div class="x_title">
	</div>
</div>

  <div class="table-responsive">
    <table class="table table-bordered table-condensed" cellpadding="0" cellspacing="0">
    <thead>
        <tr class="headings">
            <th class="column-title" style="text-align: center">조사요일</th>
            <th class="column-title" style="text-align: center">11시</th>
            <th class="column-title" style="text-align: center">16시</th>
            <th class="column-title" style="text-align: center">20시</th>
            <th class="column-title" style="text-align: center">24시</th>
            <th class="column-title" style="text-align: center">비고</th>
        </tr>
    </thead>
      <tbody>
        <tr>
          <td><input type="text" class="form-control" value="월" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="화" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="수" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="목" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="금" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="토" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
        <tr>
          <td><input type="text" class="form-control" value="일" readonly style="text-align: center"/></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
          <td><input type="text" class="form-control" /></td>
        </tr>
      </tbody>
    </table>