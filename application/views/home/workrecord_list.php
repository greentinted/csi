<div class="row">
	<input type="hidden" id="workrecord_aid" name="workrecordname_aid" value="<?=$aid?>">
	<input type="hidden" id="default_month" name="default_month_name" value="<?=$default_month?>">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무상활 리스트 형 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

						<label class="col-sm-4 control-label">조회 월</label>

						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-btn">
									<button id="prev_workrecord_btn" type="button" class="btn btn-primary">◀</button>
								</span>
								<input id="viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
								<span class="input-group-btn">
									<button id="next_workrecord_btn" type="button" class="btn btn-primary">▶</button>
								</span>

							</div>
						</div>




				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>근무일</th>
							<th>근무장소</th>
							<th>근무타입</th>
							<th>타입</th>
							<th>출근시간</th>
							<th>퇴근시간</th>						
							<th>근무시작시간</th>						
							<th>근무종료시간</th>						
							<th>근무시간</th>
							<th>시간외시간</th>		
							<th>야간시간</th>						
							<th>휴일시간</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
							<tr>
								<th><?=$value['wrid']?></th>
								<td><?=$value['rdate']?></td>
								<td><?=$value['wpid_name']?></td>
								<td><?=$value['wtid_name']?></td>
								<td><?=$value['type']?></td>
								<td><?=$value['work_start_time']?></td>
								<td><?=$value['work_end_time']?></td>
								<?php if(isset($value['official_work_start'])):?>
									<td><?=$value['official_work_start']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
								<?php if(isset($value['official_work_end'])):?>
									<td><?=$value['official_work_end']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
								<?php if(isset($value['working_time'])):?>
									<td><?=$value['working_time']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
								<?php if(isset($value['overtime'])):?>								
									<td><?=$value['overtime']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
								<?php if(isset($value['nighttime'])):?>																	
									<td><?=$value['nighttime']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>

								<?php if(isset($value['holidaytime'])):?>										
								<td><?=$value['holidaytime']?></td>
								<?php else:?>
									<td></td>
								<?php endif;?>
							</tr>
						<?php endforeach?>    
					</tbody>
				</table>

			</div>
		</div>
	</div>
</div>