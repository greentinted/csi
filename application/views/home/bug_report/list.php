<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>주차업무포털 의견 게시 <small><?=$parking_type_str?></small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="
            button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="/home/bug_report/write?aid=<?=$aid?>&is_admin=<?=$is_admin?>" class="">리포트 작성하기</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>#</th>
              <th>제목</th>
              <th>작성자</th>
              <th>중요도</th>
              <th>빈도</th>
              <th>상태</th>              
              <th>작업자</th>              
              <th>보고일자</th>              
              <th>편집</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$value['bpid']?></td>
                <td><a href="/home/bug_report/write?bpid=<?=$value['bpid']?>"><?=$value['title']?></a></td>
                <td><?=$value['reporter_name']?></td>
                <td><?=$value['importance']?></td>
                <?php if($value['frequency'] == 'A'):?>
                  <td>항상</td>  
                <?php elseif($value['frequency'] == 'B'):?>  
                  <td>부분항상</td>  
                <?php else:?>
                  <td>간헐적</td>  
                <?php endif?>                 

                <?php if($value['status'] == 'REPORT'):?>
                  <td>보고</td>  
                <?php elseif($value['status'] == 'DONE'):?>  
                  <td>완료</td>  
                <?php elseif($value['status'] == 'FIXING'):?>    
                  <td>수정중</td>  
                <?php else:?>
                  <td>재발생</td>  
                <?php endif?>                 


                <td><?=$value['worker_name']?></td>
                <td><?=$value['create_date']?></td>
                <td><a a href="/home/bug_report/write?bpid=<?=$value['bpid']?>&aid=<?=$aid?>&&is_admin=<?=$is_admin?>" class="btn btn-info">수정</a></td>
              </tr>
            <?php endforeach?>    
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>