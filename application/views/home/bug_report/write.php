<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">

				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="bug_report_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/home/bug_report/save" enctype="multipart/form-data">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">제목 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$title?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">  아이디
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="bpid" name="bpid" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$bpid?>"  readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true" readonly></span>

						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="is_admin" name="is_admin" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$is_admin?>">

						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">작성자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="reporter" name="reporter" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$reporter?>"  readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">중요도</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="_importance" class="select2_single form-control" name="importance">
								<option value="0">Please input</option>
								<?php foreach ($importance_list as $key => $value): ?>
									<option value="<?=$key?>"<?=return_select($key, $importance)?>><?=$value?></option>
								<?php endforeach?>    						
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">발생빈도 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="_frequency" class="select2_single form-control" name="frequency">
								<option value="0">Please input </option>
								<?php foreach ($frequency_list as $key => $value): ?>
									<option value="<?=$key?>"<?=return_select($key, $frequency)?>><?=$value?></option>
								<?php endforeach?>    						
							</select>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">상태 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="_frequency" class="select2_single form-control" name="status">
								<option value="0">Please input</option>
								<?php foreach ($bug_status_list as $key => $value): ?>
									<option value="<?=$key?>"<?=return_select($key, $status)?>><?=$value?></option>
								<?php endforeach?>    						
							</select>
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">작업자 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="_worker" class="select2_single form-control" name="worker">
								<option value="0">Please input</option>
								<?php foreach ($worker_list as $key => $value): ?>
									<option value="<?=$key?>"<?=return_select($key, $worker)?>><?=$value?></option>
								<?php endforeach?>    						
							</select>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 재현 방법 이나 의견 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">

			            <textarea id="summernote" name="body"><?=$reproduce?></textarea>

						</div>
					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="button" id="bug_report-save-btn" class="btn btn-success">저장</button>
						</div>
					</div>

				</form>
			</div>
		</div>
	</div>
</div>



<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column