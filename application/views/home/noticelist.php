<div class="row">
	<div class="col-md-10 col-sm-10 col-xs-12 col-md-offset-1">
		<div class="x_panel">
			<div class="x_title">
				<h2>공지사항 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
					<ul class="messages">
						<?php foreach ($list as $key => $value) :?>
							<li>
							<a href="/home/notice/detail?ntid=<?=$value['ntid']?>">
								<img src="<?=$value['profile_img']?>" class="avatar" alt="Avatar">
								<div class="message_date">
								<?php if($value['main_img_url']):?>
								<img src="<?=$value['main_img_url']?>" class="avatar" alt="Avatar" style="width:100px;height:100px;">
								<?php else:?>
								<img src="/assets/images/no-image.jpg" class="avatar" alt="Avatar" style="width:100px;height:100px;">

								<?php endif?>
								</div>
								<div class="message_wrapper">
								<h4 class="heading"><?=$value['writer']?></h4>
								<blockquote class="message"><?=$value['title']?></blockquote>
									<br>
								<p class="url">
									<span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
									<a href="#"><i class="fa fa-calendar"></i> <?=$value['create_date']?> </a>

									<span fs1 text-info" aria-hidden="true" style="margin-left:50px">읽음 : <?=$value['view_count']?></span> 
								</p>
							</div>
							</a>
						</li>
					<?php endforeach?>
					</ul>

	
				<?php /*
				<table class="table">
					<thead>
						<tr>
							<th>#</th>
							<th>제목</th>
							<th>작성자</th>
							<th>New</th>
							<th>읽음</th>
							<th>작성일</th>							
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
							<tr>
								<th scope="row"><?=$value['ntid']?></th>
								<td><a href="/home/notice/detail?ntid=<?=$value['ntid']?>"><?=$value['title']?></a></td>
								<td><?=$value['id']?></td>
								
								<?php if($value['is_new']) :?>
									<td><button class="bnt btn-warning">new</button></td>
								<?php else :?>										
									<td>

									</td>
								<?php endif?>


								<td><?=$value['view_count']?></td>
								<td><?=$value['create_date']?></td>
							</tr>
						<?php endforeach?>    
					</tbody>
				</table>
				*/?>
			</div>
		</div>
	</div>
</div>