<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="clearfix"></div>
      </div>

       <form id="inandout_search" action="/admin/stdb/inandout" metho="POST">  
        <div class="form-group">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <select class="select2_single form-control district_change_inandout" id="district_change_inandout" tabindex="1" name="inandout_did">
                <option name="did" value="0">지역구 선택</option>
              <?php foreach ($districts as $key => $value): ?>
                <option name="did" value="<?=$value['did']?>" <?=return_select($value['did'], $did)?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <select id="inandout_wpid" class="select2_single form-control" name="inandout_wpid" tabindex="1">
                <option name="wpid" value="0">주차장 선택</option>

              <?php foreach ($wp_list as $key => $value): ?>
                <option name="wpid" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $wpid)?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="input-group">
                <input type="date" name="date" id="inandout_date" class="form-control" style="width:250px;height:40px;" value="<?=$s_time?>" />
              </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="input-group">
                <input type="text" name="search_keyword"  id="inandout_searchkeyword" style="width:250px;height:40px;" value="<?=$search_keyword?>" placeholder="차량 번호 검색 ex)11가1111"/>
              </div>

            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
                <button type="submit" class="btn btn-primary" id="inandout_query_btn">조회하기</button>
            </div>
          </div>

          <div class="col-md-2 col-sm-2 col-xs-12">
<!--             <button id="btn_inandout_dwn" class="btn_success">엑셀 파일 다운로드</button> 
 -->  
            <a href="/admin/stdb/download_inandout/<?=$s_time?>"class="btn-success">파일 다운로드_<?=$s_time?></a>

          </div>
      </form>

      </div>

      <div class="x_content">

        <table class="table table-striped table-bordered bulk_action" style="font-size: 13px; table-layout:fixed; word-break:break-all;">
          <thead>
            <tr>
              <th>#</th>
              <th>지역</th>
              <th>주차장</th>
              <th>차량번호</th>
              <th>타입</th>
              <th>입출차</th>
              <th>시간</th>
              <th>관제업체</th>           
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$value['io_id']?></td>
                <td><?=$value['did_title']?></td>
                <td><?=$value['wp_title']?></td>
                <td><?=$value['car_no']?></td>
                <td><?=$value['t_type']?></td>
                <td><?=$value['io_type']?></td>
                <td><?=$value['e_time']?></td>
                <td><?=$value['vendor_name']?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
    </div>
  </div>
  <footer class="dker p-a" style="margin-left: 0px;">
      <?=$pagination?>
  </footer>



<style>
    .table tr td:nth-child(6){ width:350px; }
</style>


