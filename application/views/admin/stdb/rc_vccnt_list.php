<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="clearfix"></div>
      </div>
       <form id="vccnt_search" action="/admin/stdb/reconcile_vccnt" metho="POST">  
        <div class="form-group">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="input-group">
                <input type="date" name="date" id="rc_vccnt_date" class="form-control" style="width:250px;height:40px;" value="<?=$s_time?>" />
              </div>
            </div>  
            <div class="col-md-2 col-sm-2 col-xs-12">
                  <button type="submit" class="btn btn-primary" id="">조회하기</button>
              </div>

        </div>

      </form>

      <div class="x_content">
        <table id="reconcile_datatable" class="table table-striped table-bordered bulk_action" style="font-size: 17px">
          <thead>
            <tr>
              <th>#</th>
              <th>지역</th>
              <th>주차장</th>
              <th>거래금액</th>
              <th>수수료</th>
              <th>VAT</th>
              <th>에스크로 FEE</th>              
              <th>에스크로 VAT</th>              
              <th>정산금액</th>
              <th>POS 업체</th>              
              <th>거래종류</th>
              <th>거래일시</th>              
              <th>차량번호</th>
              <th>정기권유무</th>
              <th>관제업체</th>           
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$key+1?></td>
                <td><?=$value['did_title']?></td>
                <td><?=$value['wp_title']?></td>
                <td><?=$value['trans_amt']?></td>
                <td><?=$value['general_fee']?></td>
                <td><?=$value['vat']?></td>
                <td><?=$value['escro_fee']?></td>
                <td><?=$value['escro_vat']?></td>
                <td><?=$value['sttlmnt_amt']?></td>                
                <td><?=$value['cd_vendor']?></td>                
                <td><?=$value['ps_title']?></td>
                <td><?=$value['tr_id']?></td>
                <td><?=$value['car_no']?></td>
                <td><?=$value['ticket_type']?></td>
                <td><?=$value['vendor_name']?></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


