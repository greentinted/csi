<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <div class="clearfix"></div>
      </div>
       <form id="credit_search" action="/admin/stdb/reconcile_credit" metho="POST">  
        <div class="form-group">
            <div class="col-md-2 col-sm-2 col-xs-12">
              <select class="select2_single form-control district_change_inandout" id="rc_credit" tabindex="1" name="rc_credit_did">
                <option name="rc_credit_did" value="0">지역구 선택</option>
              <?php foreach ($districts as $key => $value): ?>
                <option name="rc_credit_did" value="<?=$value['did']?>" <?=return_select($value['did'], $did)?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <select id="inandout_wpid"class="select2_single form-control" name="rc_credit_wpid" tabindex="1">
                <option name="rc_credit_wpid" value="0">주차장 선택</option>

              <?php foreach ($wp_list as $key => $value): ?>
                <option name="rc_credit_wpid" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $wpid)?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="input-group">
                <input type="date" name="date" id="rc_credit_date" class="form-control" style="width:250px;height:40px;" value="<?=$s_time?>" />
              </div>
            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
              <div class="input-group">
                <input type="text" name="search_keyword"  id="rc_credit_searchkeyword" style="width:250px;height:40px;" value="<?=$search_keyword?>" placeholder="차량 번호 검색 ex)11가1111"/>
              </div>

            </div>

            <div class="col-md-2 col-sm-2 col-xs-12">
                <button type="submit" class="btn btn-primary" id="inandout_query_btn">조회하기</button>
            </div>
            </div>
          <div class="col-md-2 col-sm-2 col-xs-12">
<!--             <button id="btn_inandout_dwn" class="btn_success">엑셀 파일 다운로드</button> 
 -->  
            <a href="/admin/stdb/download_credit/<?=$s_time?>"class="btn-success">엑셀 파일 다운로드</a>

          </div>

          

        </form>
        </div>
        <div class="x_content">
        <div id="loading_bar" class="loader"  style="display: block; width:120px; margin:0 auto; position:relative;display:none;"></div>

      </div>

      </div>

      <div class="x_content">
        <table class="table table-striped table-bordered bulk_action" style="font-size: 13px; table-layout:fixed; word-break:break-all;">
          <thead>
            <tr>
              <th>#</th>
              <th>지역</th>
              <th>주차장</th>
              <th>거래금액</th>
              <th>수수료</th>
              <th>VAT</th>              
              <th>정산금액</th>
              <th>POS 업체</th>              
              <th>거래종류</th>
              <th>거래일시</th>              
              <th>발급사</th>              
              <th>매입사</th>
              <th>차량번호</th>
              <th>정기권유무</th>
              <th>관제업체</th>
              <th>PMCODE</th>           
              <th>TID</th>              
              <th>OTID</th>              
              <th>MID</th>              

            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$value['rcs_id']?></td>
                <td><?=$value['did_title']?></td>
                <td><?=$value['wp_title']?></td>
                <td><?=$value['trans_amt']?></td>
                <td><?=$value['general_fee']?></td>
                <td><?=$value['vat']?></td>
                <td><?=$value['sttlmnt_amt']?></td>                                                
                <td><?=$value['cd_vendor']?></td>                
                <td><?=$value['ps_title']?></td>
                <td><?=$value['tr_id']?></td>
                <td><?=$value['app_title']?></td>
                <?php if(isset($value['acqu_title'])):?>
                  <td><?=$value['acqu_title']?></td>
                <?php else:?>
                  <td>Unknown</td>
                <?php endif?>
                <td><?=$value['car_no']?></td>
                <td><?=$value['ticket_type']?></td>
                <td><?=$value['vendor_name']?></td>
                <td><?=$value['pm_code']?></td>                
                <td><?=$value['tid']?></td>                
                <td><?=$value['otid']?></td>                
                <td><?=$value['mid']?></td>                
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>

    </div>

  </div>

</div>
<footer class="dker p-a">
    <?=$pagination?>
</footer>

<style>
    .table tr td:nth-child(6){ width:350px; }
</style>

<script type="text/javascript">
  
</script>