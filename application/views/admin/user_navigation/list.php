<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>유저 네비게이션 <small> <?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="navigation_get">유저화면 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>타이틀</th>
							<th>상태</th>
							<th>ORDER</th>
							<th>link</th>
							<td>비고</td>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input type="checkbox" class="js-switch un_status" id = "<?=$value['unid']?>" checked /> 활성화
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input type="checkbox" class="js-switch un_status"  id = "<?=$value['unid']?>"/> 비활성화
								</label>								
								</td>
							<?php endif?>

							<td><?=$value['order_no']?></td>					
							<td><?=$value['link']?></td>
							<td><?=$value['description']?></td>

							<td><input type="button" name="view" value="EDIT" id="<?=$value['unid']?>" class="btn btn-primary navigation_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

 <div id="user_dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">유저 화면 추가/수정</h4>  
                </div>  
                <form role="form" id="navigation_edit" action="/admin/usernavigation/save">
	                <div class="modal-body" id="navigation_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
