
<style type="text/css">
@keyframes progress {
  from {
    width: 0;
  }
  to {
    width: 100%;
  }
}
</style>

<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>주차장 추첨</small></h2>
				<div><input type="button" id="draw_btn" name="draw_btn" value="추첨하기" class="btn btn-primary navbar-right draw_parkinglot"></div>

				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="draw_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>구분</th>
							<th>지역구</th>
							<th>주차장명</th>
							<th>정원</th>
							<th>신청인원</th>
							<th>신청명단</th>
							<th>확정명단</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['dtitle']?></td>
							<td><?=$value['ch']?></td>	
							<td><?=$value['curr_cnt']['curr_cnt']?></td>	
							<td> 
								<?php if(isset($value['names'])):?>
									<?php foreach ($value['names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-primary"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['name_list']['name_list']):?>
										<button type="button" class="btn btn-round btn-primary"><?=$value['name_list']['name_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	


							<td> 
								<?php if(isset($value['fixed_names'])):?>
									<?php foreach ($value['fixed_names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-success"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['fixed_list']['fixed_list']):?>
										<button type="button" class="btn btn-round btn-success"><?=$value['fixed_list']['fixed_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <h4 class="modal-title">근무지 추첨 중 </h4>  
                </div>  
	                <div class="modal-body" id="draw_process_modal">  
	                	    <progress width="100" value="50" max="50" style="width:95%; animation: progress 5s ease-in-out forwards;"></progress> 
	                </div>  
	                <div class="modal-footer"></div>  
            </form>
           </div>  
      </div>  
 </div>  

