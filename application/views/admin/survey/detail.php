<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">

        <h2><?=$mtitle?> <small>서비스평가 속성을 입력하세요.</small></h2>

          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
  <!--           <li><a class="dropdown-item" href="#">Settings 1</a>
            </li>
            <li><a class="dropdown-item" href="#">Settings 2</a>
            </li> -->
          </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
          </div>
          <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="save">

<!--           <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align ">#</label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                <input type="text" class="form-control" readonly="readonly" placeholder="Read-Only Input">
              </div>
          </div>
 -->
          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">회차
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">

                  <?php if($order):?>
                    <input type="text" class="form-control" readonly="readonly" name="order" value="<?=$order?>"> 
                  <?php else:?>
                      <input type="text" class="form-control" placeholder="회차를 입력하세요." name="order" value=""> 
                  <?php endif?>
   <!--              <input type="text" class="form-control" readonly="readonly" name="order" value="<?=$order?>"> -->
              </div>
          </div>


          <div class="item form-group">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">시작기간 <span class="required">*</span>
            </label>

              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="" aria-describedby="start_date" name="start_date" value="<?=$start_date?>" style="color: defult">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>

              <!-- <span id="inputSuccess2Status" class="sr-only">(success)</span> -->
            </div>
          </div>


          <div class="item form-group">
            <label class="col-md-3 col-sm-3 label-align" for="">종료기간 <span class="required">*</span>
            </label>
              <div class="col-md-6 xdisplay_inputx form-group row has-feedback">
                <input type="text" class="form-control has-feedback-left" id="single_cal3" placeholder="" aria-describedby="end_date" name="end_date" value="<?=$end_date?>">
              <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
              <!-- <span id="inputSuccess2Status" class="sr-only">(success)</span> -->
            </div>
          </div>

          <div class="item form-group">
              <label class="col-md-3 col-sm-3 label-align" for="first-name">설명 
              </label>
                <div class="col-md-6 col-sm-6 ">
                <textarea id="message" required="required" class="form-control" name="message" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"><?=$description?></textarea>
                </div>
          </div>


          <div class="item form-group">
          <label class="col-md-3 col-sm-3 label-align">활성화</label>
          <div class="col-md-6 col-sm-6 " id="switchText" >
          <div id="status" class="btn-group" data-toggle="buttons">
          <input type="checkbox" class="js-switch" id="toggle_status" name="status" checked />
          </div>
          </div>
          </div>

<!-- 시작날짜가 종료날짜보다 늦을경우 -->

<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript">    
$(document).ready(function() {
  $('.btn-success').click(function(){


         
        var startDate = $( "input[name='start_date']" ).val(); //2017-12-10
        var startDateArr = startDate.split('/');
         
        var endDate = $( "input[name='end_date']" ).val(); //2017-12-09
        var endDateArr = endDate.split('/');
                 
        var startDateCompare = new Date(startDateArr[0], parseInt(startDateArr[1])-1, startDateArr[2]);
        var endDateCompare = new Date(endDateArr[0], parseInt(endDateArr[1])-1, endDateArr[2]);
         
          if(startDateCompare.getTime() > endDateCompare.getTime()) {  
              $( "input[name='start_date']" ).css("color","red");
              alert("시작날짜와 종료날짜를 확인해 주세요.");
              return false;
          }
      });
});

</script>

        <div class="ln_solid"></div>
        <div class="item form-group">
        <div class="col-md-6 col-sm-6 offset-md-3">
        <button class="btn btn-primary" type="button">취소</button>
        <button class="btn btn-primary" type="reset">리셋</button>
        <button class="btn btn-success" type="submit">저장</button>
        </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

