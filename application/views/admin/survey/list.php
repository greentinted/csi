<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title ">
      <h2>평가일정 조정 <small>수정·생성 시 <span style="color:green">상태</span>는 기본으로 <span style="color:red">활성화</span> 됩니다.</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
            <li><a class="dropdown-item" href="/admin/surveyconf/detail">평가일정 생성하기</a>
            </li>              
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr>
              <!-- <th>#</th> -->
              <th>회차</th>
              <th>평가시작기간</th>
              <th>평가종료기간</th>
              <th>메모</th>
              <th>상태</th>
              <th>수정하기</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
            <tr>
             <!--  <td><?=$key+1?></td>     -->          
              <td><?=$value['order']?></td>
              <td><?=$value['from']?></td>
              <td><?=$value['to']?></td>
              <td><?=$value['description']?></td>
              <td>
                  <?php if($value['status'] == 1):?>
                    <input type="checkbox" class="js-switch" disabled="disabled" checked="checked"/>
                  <?php else:?>
                    <input type="checkbox" class="js-switch" disabled="disabled"/>
                  <?php endif?>

              </td>
              <td><a href="/admin/surveyconf/detail?ssid=<?=$value['sc_id']?>" class="btn btn-primary">수정하기</a></td>
           
            </tr>
            <?php endforeach?>    
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

