
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무기록 수정 내역  </h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->
				<form class="form-horizontal form-label-left" method="POST" id="work_history" action="/admin/change_workingrecord/">

					<div class="form-group">
						<label class="col-sm-4 control-label">조회기간</label>

						<div class="col-sm-3">
							<div class="input-group">
								
	                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                            <input type="text" name="history_work_query_period" id="history_work_query_period" class="form-control"  value="<?=$history_work_query_str?>" />
			                        
								<span class="input-group-btn">
										<button type="button" class="btn btn-primary" id="history_work_query_btn">조회하기</button>
								</span>
							</div>
						</div>
					</div>
					<div class="divider-dashed"></div>

				</form>



				<table id="change_working_record_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>수정 어드민</th>
							<th>대상 작업자</th>
							<th>날짜</th>
							<th>주차처</th>
							<th>타입</th>
							<th>원본 아이디</th>
							<th>원본</th>
							<th>수정내역</th>
							<th>조정 근무 </th>
							<th>조정근무 수정 내역</th>
							
						</tr>
					</thead>


					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>

							<td><?=$value['cwrh_id']?></td>
							<td><?=$value['admin_id']?></td>					
							<td><?=$value['worker_id']?></td>					
							<td><?=$value['create_date']?></td>
							<?php if($value['ptid'] == 1):?>
								<td>노외 주차처</td>
							<?php elseif($value['ptid'] == 2):?>
								<td>노상 주차처</td>
							<?php else:?>
								<td><?=$value['ptid']?></td>
							<?php endif;?>
							<td><?=$value['type']?></td>
							<td><?=$value['origin_wrid']?></td>
							<td><?=$value['origin']?></td>
							<td><?=$value['dest']?></td>
							<td><?=$value['urgent_origin']?></td>
							<td><?=$value['urgent_dest']?></td>

							
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

