<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2 style="padding-top: 10px">월정기 변경 처리 내역 <small></small></h2>
        <a href="/escalate/contractor/change_"><button type="button" class="btn btn-success  navbar-right">입력화면으로 돌아가기</button></a>
        <div class="clearfix"></div>
      </div>

      <form id="period_form" action="/admin/contractor/change_list" metho="POST">  

      <div class="col-sm-12">
      <div class="form-group">
            <label class="col-sm-4 control-label"><small></small></label>

            <div class="col-sm-3">
              <div class="input-group">
                              <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                              <input type="text" class="query_period" name="query_period" id="approve_vac_query_period" class="form-control" value="<?=$query_period?>" />
              
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" id="approve_vac_query_btn">조회하기</button>
                </span>
              </div>
            </div>
          </div>
          </form>

      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action" style="font-size: 17px">
          <thead>
            <tr>
              <th>#</th>
              <th>처리일</th>
              <th>처리자</th>              
              <th>지역</th>
              <th>주차장</th>
              <th>소유자</th>
              <th>연락처</th>
              <th>변경날짜</th>
              <th>전 차량번호</th>
              <th>전 감면유형</th>
              <th>전 차종</th>
              <th>전 요금</th>
              <th>현 차량번호</th>
              <th>현 감면유형</th>
              <th>현 차종</th>
              <th>현 요금</th>
              <?php if($user_type != "operator") { ?>
                <th>Action</th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_list as $key => $value): ?>
              <tr>
                <td id="ccid"><?=$value['ccid']?></td>
                <td><?=$value['create_date']?></td>
                <td><?=$value['confirm_id']?></td>
                <td><?=$value['district_title']?></td>
                <td><?=$value['workplace_title']?></td>
                <td><?=$value['car_owner']?></td>
                <td><?=$value['tel_num']?></td>
                <td><?=$value['change_date']?></td>
                <td><?=$value['before_car_no']?></td>
                <td><?=$value['before_dis_title']?></td>
                <td><?=$value['before_car_type']?></td>
                <td><?=$value['before_fee']?></td>
                <td><?=$value['after_car_no']?></td>
                <td><?=$value['after_dis_title']?></td>
                <td><?=$value['after_car_type']?></td>
                <td><?=$value['after_fee']?></td>
                <?php if($user_type != "operator") { ?>
                  <td>
                    <a class="btn btn-info" href="/escalate/contractor/change_?ccid=<?=$value['ccid']?>">수정</a>
                    <a class="btn btn-danger" onclick="return confirm('삭제하시겠습니까 ?');" href="/escalate/contractor/change_delete?ccid=<?=$value['ccid']?>">삭제</a>

                  </td>
                <?php } ?> 
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>


