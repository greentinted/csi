<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 입력 리스트<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>


	
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

					<div class="form-group">
						<label class="col-sm-4 control-label">조회기간</label>

						<div class="col-sm-3">
							<div class="input-group">
								
	                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                            <input type="text" name="e_q_period" id="e_q_period" class="form-control"  value="<?=$viewing_from?>" />
			                        
								<span class="input-group-btn">
										<button type="button" class="btn btn-primary e_query_btn" >조회하기</button>
								</span>
							</div>
						</div>
					</div>
					<div class="divider-dashed"></div>

				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>작업자</th>
							<th>주차장</th>	
							<th>차량번호</th>						
							<th>금액</th>
							<th>결제일</th>
							<th>등록일</th>							
							<th>수정</th>
							<th>삭제</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['ecid']?></td>
							<td><?=$value['user']['id']?></td>
							<td><?=$value['wpid_title']?></td>
							<td><?=$value['car_no']?></td>
							<td><?=$value['amount']?></td>							
							<td><?=$value['pay_date']?></td>
							<td><?=$value['create_date']?></td>
							<td><a class="btn btn-info" href="/escalate/contractor/extend_?ecid=<?=$value['ecid']?>&aid=<?=$value['aid']?>&rdate=<?=$value['rdate']?>&did=<?=$value['did']?>&wpid=<?=$value['wpid']?>&access_type=admin">수정</a></td>	

							<td><a onclick="return confirm('정말로 삭제하시겠습니까? ')" href="/admin/contractor/extend_delete?ecid=<?=$value['ecid']?>" class="btn btn-danger">삭제</a></td>

							<?php /*

							<td><input type="button"  name="view" class="btn btn-danger init_password" id="<?=$value['aid']?>" value="비번초기화"></td>						

							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




