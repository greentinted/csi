<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>휴가 스케쥴 등록 <small> <?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="vc_get" id="">휴가 스케쥴 등록</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>#</th>
							<th>From</th>
							<th>to</th>
							<th>데드라인</th>
							<th>상태</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['from_']?></td>
							<td><?=$value['to_']?></td>
							<td><?=$value['deadline']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="survey_<?=$value['lars_id']?>"type="checkbox" class="js-switch vc_status" checked /> 활성화
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="survey_<?=$value['lars_id']?>" type="checkbox" class="js-switch vc_status" /> 비활성화
								</label>								
								</td>
							<?php endif?>

							<td><input type="button" name="view" value="수정" id="<?=$value['lars_id']?>" class="btn btn-primary vc_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

 <div id="vc_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">스케쥴 등록</h4>  
                </div>  
                <form role="form" id="navigation_edit" action="/admin/vacation_schedule/save">
	                <div class="modal-body" id="vc_detail">  
						<div class="form-group row">    
							<label for="id"  class="col-sm-2  text-right form-control-label">아이디</label>    
							<div class="col-sm-10">        
								<input type="text_hidden"  class="form-control" name="vs_id" id="vs_id" placeholder="id" readonly>    
							</div>
						</div>
						<div class="form-group row">
						    <label for="from"  class="col-sm-2  text-right form-control-label">from</label>    
						    <div class="col-sm-10">        
								<div class="row">
							        <!-- <div class='col-sm-6'> -->
							            <div class="form-group">
							                <div class='input-group date datetimepicker_duration_leave' >
							                    <input type='text' class="form-control"  id='vs_from' name='vs_from' placeholder="2019-01-01 12:00:00" />
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
							            </div>
							        <!-- </div> -->
							    </div>
							</div>						    	
						</div>
						<div class="form-group row">
						    <label for="from"  class="col-sm-2  text-right form-control-label">to</label>    
						    <div class="col-sm-10">        
								<div class="row">
							        <!-- <div class='col-sm-6'> -->
							            <div class="form-group">
							                <div class='input-group date datetimepicker_duration_leave'  >
							                    <input type='text' class="form-control" id='vs_to' name='vs_to' placeholder="2019-01-01 12:00:00"/>
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
							            </div>
							        <!-- </div> -->
							    </div>
							</div>						    	
						</div>
					    <div class="form-group row">
						    <label for="from"  class="col-sm-2  text-right form-control-label">deadline</label>    
						    <div class="col-sm-10">        
								<div class="row">
							        <!-- <div class='col-sm-6'> -->
							            <div class="form-group">
							                <div class='input-group date datetimepicker_duration_leave'  >
							                    <input type='text' class="form-control" id='vs_deadline' name='vs_deadline' placeholder="2019-01-01 12:00:00"/>
							                    <span class="input-group-addon">
							                        <span class="glyphicon glyphicon-calendar"></span>
							                    </span>
							                </div>
							            </div>
							        <!-- </div> -->
							    </div>
							</div>						    	
						</div>
						    <div class="form-group row">    
						    	<label for="status"  class="col-sm-2  text-right form-control-label">status</label>    
						    	<div class="col-sm-10">        
						    		<input type="text"  class="form-control" name="vs_status" id="vs_status" placeholder="1 : 보이기, 0 : 보이지 않기 ">    
						    	</div>
						    </div>
	                </div>  
	                <div class="modal-footer">  

	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                    <button type="submit" class="btn btn-primary"> 저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
