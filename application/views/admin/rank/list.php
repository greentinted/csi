<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Default Example <small>Users</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="rank_get">직급 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>직급</th>							
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($rank as $key => $value): ?>
						<tr>
							<td><?=$value['rid']?></td>
							<td><?=$value['title']?></td>							
							<td><input type="button" name="view" value="EDIT" id="<?=$value['rid']?>" class="btn btn-primary rank_get"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">직급 추가/수정</h4>  
                </div>  
                <form role="form" id="rank_edit" action="/admin/rank/save">
	                <div class="modal-body" id="rank_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button> 

	                     <button type="button" class="btn btn-danger rank_delete">삭제 </button>

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

