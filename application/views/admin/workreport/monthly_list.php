<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월말보고자료  <small></small></h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class=""></a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">
				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->
				<form id="workreport_form" class="form-horizontal form-label-left" method="GET" id="workreport" action="/admin/workreport/monthly">
						<label class="col-sm-4 control-label">조회 월</label>

						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-btn">
									<button id="prev_btn" type="button" class="btn btn-primary">◀</button>
								</span>
								<input id="viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
								<span class="input-group-btn">
									<button id="next_btn" type="button" class="btn btn-primary">▶</button>
								</span>

							</div>
						</div>


				</form>


				<table id="admin_monthly" class="table table-striped table-hovered">
					<thead>
						<tr>
							<th>#</th>
							<th>성명</th>
							<th>입사일자</th>
							<th>지역</th>
							<th>근무지</th>
							<th>근무지정</th>
							<th>근로형태</th>
							<th>근무</th>
							<th>휴무근무</th>
							<th>휴일근무</th>
							<th>시간외</th>
							<th>휴일</th>
							<th>야간</th>
							<th>발생연차</th>
							<th>전월누계</th>
							<th>당월사용</th>
							<th>잔여연차</th>							
						</tr>
					</thead>


					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['aid']?></td>
							<td><?=$value['id']?></td>
							<td><?=$value['join_date']?></td>
							<?php if(isset($value['did_title'])):?>
								<td><?=$value['did_title']?></td>		
							<?php else:?>
								<td></td>		
							<?php endif?>
							<?php if(isset($value['wpid_name'])):?>
								<td><?=$value['wpid_name']?></td>		
							<?php else:?>
								<td></td>
							<?php endif?>	

							<td><?=$value['waid_name']?></td>
							<td><?=$value['wsid_name']?></td>
							<td><?=$value['working_count']?></td>
							<td><?=$value['working_off_days']?></td>
							<td><?=$value['working_holiday_days']?></td>
							
							<td><?=$value['working_overtime']?></td>
							<td><?=$value['working_holiday']?></td>
							<td><?=$value['working_night']?></td>
							
							<td><?=$value['leave_days']?></td>
							<td><?=$value['this_month_vacation']?></td>
							<td><?=$value['prev_month_vacation']?></td>
							<td><?=$value['remain_vacation']?></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript" src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>


<script type="text/javascript">
	$(function(){

	});
</script>						
