
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월말보고자료  <small></small></h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class=""></a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->


				<form id="workreport_form_" class="form-horizontal form-label-left" method="GET" id="workreport" action="/admin/workreport/monthly_daily">
						<label class="col-sm-4 control-label">조회 월</label>

						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-btn">
									<button id="daily_prev_btn" type="button" class="btn btn-primary">◀</button>
								</span>
								<input id="viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
								<span class="input-group-btn">
									<button id="daily_next_btn" type="button" class="btn btn-primary">▶</button>
								</span>

							</div>
						</div>
				</form>

				<table id="example" class="table table-striped table-bordered">
				</table>
				<div class="x_content">
				<div id="loading_bar" class="loader"  style="display: block; width:120px; margin:0 auto; position:relative;display:none;"></div>

			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
$(document).ready( function () {


	var viewing_month = $('#viewing_month').val();

	//console.log('viewing_month' , viewing_month);

	$('#loading_bar').show();

    $.ajax({  
            url:"/admin/workreport/ajax_monthly_daily_",  
            method:"post",  
            data:{from:viewing_month},  
            dataType: 'json',  
            success:function(resData){  
            	//console.log('resData' , resData);

            	$('#loading_bar').hide();

            	add_columns = [
				    {
				        title: '#',
				    },
				    {
				        title: '성명',
				    },
				    {
				        title: '휴근(1.5)',
				    },
				    {
				        title: '연장(1.5)',
				    },
				    {
				        title: '야근(0.5)',
				    },

				    {
				        title: '휴근연장(0.5)',
				    },
				    {
				        title: '근무',
				    },

            	];

			    for(var i = 0; i < resData.month_list.length; i++){
			    	var date = resData.month_list[i];
			    	var str = date.day +'\n('+date.dayofweek+')'; 
			    	var arr = {title:str};

			    	add_columns.push(arr);


			    }

			    //console.log('add_columns' , add_columns);
			    //console.log('resData' , resData.list);


				var data = [
 			    ];				

 			    for(var j =0; j < resData.list.length; j++){

 			    	var account = resData.list[j];

 			    	//console.log('account.aid' , account.aid)

 			    	for(var kk = 0; kk < 4; kk++){

	 			    	var ac_data = [];
	 			    	ac_data.push(account.aid);
	 			    	ac_data.push(account.id);
	 			    	ac_data.push(account.holiday);
	 			    	ac_data.push(account.overtime);
	 			    	ac_data.push(account.nighttime);
	 			    	ac_data.push(account.holiday_overtime);

	 			    	
	 			    	//console.log('jj',jj);

	 			    	if(kk == 0){
	 			    		ac_data.push('휴근');
	 			    	}
	 			    	else if(kk == 1){
	 			    		ac_data.push('연장');
	 			    	}
	 			    	else if(kk == 2){
	 			    		ac_data.push('야근');
	 			    	}
	 			    	else if(kk == 3){
	 			    		ac_data.push('휴근연장');
	 			    	}

	 			    	for( var k = 0; k < account.work_record.length; k++){
	 			    		var work_record = account.work_record[k];

	 			    		//ac_data.push('휴근연장');	
		 			    	if(kk == 0){
		 			    		ac_data.push(work_record.holiday_workhour);
		 			    	}
		 			    	else if(kk == 1){
		 			    		ac_data.push(work_record.overtime_workhour);
		 			    	}
		 			    	else if(kk == 2){
		 			    		ac_data.push(work_record.nighttime_workhour);
		 			    	}
		 			    	else if(kk == 3){
		 			    		ac_data.push(work_record.holiday_overtime);
		 			    	}

	 			    	}
 			    	//console.log('ac_data' , ac_data);

 			    	data.push(ac_data);

 			    	}

 			    }

 			    //console.log('data' , data);
  				var table = $('#example').DataTable({
			        lengthChange: true,
			        responsive: false,
			        searching: true,
			            // 정렬 기능 숨기기
			        ordering: false,
			            // 정보 표시 숨기기

					dom: 'Bfrtip',
				    buttons: [
				        'copy', 'csv', 'excel', 'print'
				    ] ,     



				columns: add_columns,
				data: data,
				rowsGroup: [// Always the array (!) of the column-selectors in specified order to which rows groupping is applied
				            // (column-selector could be any of specified in https://datatables.net/reference/type/column-selector)
				    'first',
				    0,
				    1,
				    2,
				    3,
				    4,
				    5,
				],
				pageLength: '100',
				});
		}

	        
    });

});


	
</script>