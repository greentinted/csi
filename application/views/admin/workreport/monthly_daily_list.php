
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월말보고자료  <small></small></h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class=""></a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->

				<form id="workreport_form_" class="form-horizontal form-label-left" method="GET" id="workreport" action="/admin/workreport/monthly_daily">
						<label class="col-sm-4 control-label">조회 월</label>

						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-btn">
									<button id="daily_prev_btn" type="button" class="btn btn-primary">◀</button>
								</span>
								<input id="viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
								<span class="input-group-btn">
									<button id="daily_next_btn" type="button" class="btn btn-primary">▶</button>
								</span>

							</div>
						</div>
				</form>

				<table id="report_monthly_datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th rowspan="2">#</th>
							<th rowspan="2">성명</th>
							<th>휴근</th>
							<th>연장</th>
							<th>야근</th>
							<th>휴근연장</th>
							<th rowspan="2">근무</th>
							<?php foreach ($month_list as $key => $value) :?>
								<th><?=$value['day']?></th>								
							<?php endforeach?>    
						</tr>
						<tr>
							<th>1.5</th>
							<th>1.5</th>
							<th>0.5</th>
							<th>0.5</th>
							
							<?php foreach ($month_list as $key => $value) :?>
								<th><?=$value['dayofweek']?></th>								
							<?php endforeach?>    
							
						</tr>
					</thead>
						<?php foreach ($list as $key1 => $value): ?>

							
							<?php for($i =0; $i < 4; $i++):?>
							<tr>
							
							<!-- <?php if(0 == $i):?> -->
							<td rowspan="4"><?=$value['aid']?></td>
							<td rowspan="4"><?=$value['id']?></td>
							<td rowspan="4"><?=$value['holiday']?></td>
							<td rowspan="4"><?=$value['overtime']?></td>
							<td rowspan="4"><?=$value['nighttime']?></td>
							<td rowspan="4"><?=$value['holiday_overtime']?></td>
							<!-- <?php else:?> -->
<!-- 							<td style="display:none"></td>	
							<td style="display:none"></td>	
							<td style="display:none"></td>	
							<td style="display:none"></td>	
							<td style="display:none"></td>	
							<td style="display:none"></td>	
							<?php endif?>
 -->

							
							
								<?php if($i == 0):?>
									<td>휴근</td>
								<?php elseif($i == 1):?>	
									<td>연장</td>
								<?php elseif($i == 2):?>	
									<td>야근</td>
								<?php elseif($i == 3):?>	
									<td>휴근연장</td>
								<?php else:?>	
									<td>머지 ?</td>

								<?php endif;?>
								<?php foreach ($value['work_record'] as $key2 => $value1): ?>

									<?php if($i == 0):?>
										<td><?=$value1['holiday_workhour']?></td>				
									<?php elseif($i == 1):?>	
										<td><?=$value1['overtime_workhour']?></td>						
									<?php elseif($i == 2):?>	
										<td><?=$value1['nighttime_workhour']?></td>				
									<?php elseif($i == 3):?>	
										<td><?=$value1['holiday_overtime']?></td>				
									<?php endif;?>

								<?php endforeach ?>
							</tr>
							<?php endfor?>
							
						<?php endforeach?>    	
					</tbody>


				</table>


				<div class="x_content">
			</div>
		</div>
	</div>
</div>




