  <div class="x_panel">
      <div class="x_title">
        <h2 style="padding-top: 5px;">우선 배치 </h2>
        <div class="clearfix"></div>
                                 
                  <div class="clearfix"></div>
                  </div>
                  <span style="float:right">
                    <input type="button" value="랜덤 배치 테스트" id="draw_test_btn" class="btn btn-success">
                    <form id="draw_test_form">
                    </form>
                  </span>

                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th class="column-title" style="display: table-cell;">구분</th>
                            <th class="column-title" style="display: table-cell;">부서명</th>
                            <th class="column-title" style="display: table-cell;">이름</th>
                            <th class="column-title" style="display: table-cell;">근무지신청</th>
                            <th class="column-title" style="display: table-cell;">근무지</th>
                            <th class="column-title" style="display: table-cell;">신청</th>
                            <th class="column-title" style="display: table-cell;">배치</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($list as $key => $value) { ?>
                            <?php if($value['user_type'] == "operator") { ?>
                            <tr class="even pointer">
                              <td class="col-md-2"><?=$key+1?></td>
                                <?php if($value['ptid'] == 1) 
                                  {
                                    echo '<td>노외주차처</td>';
                                  }else {
                                    echo '<td>노상주차처</td>';

                                  } ?>
                              <td class="col-md-2"><?=$value['id']?></td>
                                <?php if($value['draw_flag'] == 1) 
                                  {
                                    echo '<td>근무지 확정</td>';
                                  }else {
                                    echo '<td>근무지 미확정</td>';

                                  } ?> 
                              <td>
                                 <select class="form-control" id="select_workplace<?=$value['aid']?>" name="select_workplace">
                                  <?php foreach ($wp_list as $key2 => $value2) : ?>
                                    <option value="<?=$wp_list[$key2]['wpid']?>"> <?=$wp_list[$key2]['title']?> </option>
                                  <?php endforeach ?>
                                 </select>
                              </td>
                              <td class="a-center col-md-2"><input type="button" name="dipose_btn" id = "<?=$value['aid']?>" value="신청하기" class="btn btn-warning dipose_btn"></td>

                              <td class="a-center col-md-2"><input type="button" name="dipose_btn" id = "<?=$value['aid']?>" value="배치하기" class="btn btn-danger confirm_btn"></td>
                              </tr>
                          <?php } ?>  
                        <?php } ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>


