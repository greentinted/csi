  <div class="x_panel">
      <div class="x_title">
        <h2 style="padding-top: 5px;">근무지 일괄배치</small></h2>
        <div><input type="button" id="draw_left" name="draw_btn" value="일괄배치" class="btn btn-primary navbar-right draw_left"></div>

        <div class="clearfix"></div>
                                 
                  <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <div class="table-responsive">
                      <table class="table table-striped jambo_table bulk_action">
                        <thead>
                          <tr class="headings">
                            <th>체크</th>
                            <th class="column-title" style="display: table-cell;">구분</th>
                            <th class="column-title" style="display: table-cell;">부서명</th>
                            <th class="column-title" style="display: table-cell;">이름</th>
                            <th class="column-title" style="display: table-cell;">근무지신청</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($list as $key => $value) { ?>
                            <?php if($value['user_type'] == "operator") { ?>
                            <tr class="even pointer">
                              <td class="a-center "><input type="checkbox" name="check_aid" id = "<?=$value['aid']?>" style="width: 15px; height: 15px;" checked></td>
                              <td><?=$key+1?></td>
                                <?php if($value['ptid'] == 1) 
                                  {
                                    echo '<td>노외주차처</td>';
                                  }else {
                                    echo '<td>노상주차처</td>';

                                  } ?>
                              <td><?=$value['id']?></td>
                                <?php if($value['draw_flag'] == 1) 
                                  {
                                    echo '<td>근무지 확정</td>';
                                  }else {
                                    echo '<td>근무지 미확정</td>';

                                  } ?>                            
                              </tr>
                          <?php } ?>  
                        <?php } ?>
                          </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <form id="draw_left_form" action = "/admin/draw_admin/draw_left" method="post">
                  <span id="aid_list_span"></span>
                  <input type="hidden" id = "aid_index" name="aid_index">
                </form>

