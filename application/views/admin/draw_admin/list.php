<!-- First Section one Column -->
<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>추첨기본정보 
				<?php if($ptid == 1) 
					{
						echo '<small>노외주차처</small>';
					}else {
						echo '<small>노상주차처</small>';
					} ?>
				</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
			<form class="form-horizontal form-label-left input_mask">

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<?php if($ptid == 1) 
					{
						echo '<input type="text" class="form-control has-feedback-left" value ="노외주차처" readonly="true">';
					}else {
						echo '<input type="text" class="form-control has-feedback-left" value ="노상주차처" readonly="true">';
					} ?>

					

					<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<?php if(isset($curr_drawinfo['title'])): ?>
						<input type="text" class="form-control" id="inputSuccess5" value="<?=$curr_drawinfo['title']?>" readonly="true">
					<?php else: ?>
						<input type="text" class="form-control" id="inputSuccess5" value="진행중이 추첨이 없습니다." readonly="true">
					<?php endif ?>
					<span class="fa fa-align-justify form-control-feedback right" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<input type="text" class="form-control has-feedback-left" id="inputSuccess3" value = "<?=$ask_cnt?>명 신청완료" readonly="true">
					<span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<?php if(isset($curr_drawinfo['round'])): ?>
						<input type="text" class="form-control" id="inputSuccess4" value="<?=$curr_drawinfo['round']?>차 추첨" readonly="true">
					<?php else: ?>
						<input type="text" class="form-control" id="inputSuccess4" value="진행중이 추첨이 없습니다." readonly="true">
					<?php endif ?>
					<span class="fa fa-refresh form-control-feedback right" aria-hidden="true"></span>
				</div>


				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
					<?php if(isset($setting)): ?>
						<input type="text" class="form-control has-feedback-left" id="inputSuccess2" value="<?=$setting?>" readonly="true">
					<?php else: ?>
						<input type="text" class="form-control has-feedback-left" id="inputSuccess2" value="" readonly="true">
					<?php endif ?>
					<span class="fa fa-table form-control-feedback left" aria-hidden="true"></span>
				</div>

				<div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
				</div>

	<!-- 추첨대상 주차장 개소 표시 -->			
				<div class="clearfix"></div>	
			</form>
			</div>
		</div>
	</div>
<!-- 등록신청기간 및 마감시간 설정 페이지 -->
	<div class="col-md-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>추첨진행상황<?php if($ptid == 1) 
					{
						echo '<small>노외주차처</small>';
					}else {
						echo '<small>노상주차처</small>';
					} ?>
						
					</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content" >
				<div class="form-group col-sm-12 col-md-12 col-xs-12" style="padding-top: 14px;">
					<label class="control-label col-sm-3 col-md-3 col-xs-3">추첨 대상 주차장</label>
					<div class="input-group col-sm-9 col-md-9 col-xs-9">
						<input type="text" class="form-control" value="<?=$parkinglot_cnt?>개소">
					</div>
				</div>

				<div class="form-group col-sm-12 col-md-12 col-xs-12">
					<label class="control-label col-sm-3 col-md-3 col-xs-3">추첨 완료 주차장</label>
					<div class="input-group col-sm-9 col-md-9 col-xs-9">
						<input type="text" class="form-control" value="<?=$complete_cnt?>개소">
					</div> 
				</div>

				<div class="form-group col-sm-12 col-md-12 col-xs-12">
					<label class="control-label col-sm-3 col-md-3 col-xs-3">추첨 진행 주차장</label>
					<div class="input-group col-sm-9 col-md-9 col-xs-9">
						<input type="text" class="form-control" value="<?=$drawing_cnt?>개소">
					</div>
				</div>

			</div>

					

				
			</div>
		</div>
	</div>



<!-- 페이지 리스트 출력 -->

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>추첨 회차 정보
					<?php if($ptid == 1) 
					{
						echo '<small>노외주차처</small>';
					}else {
						echo '<small>노상주차처</small>';
					} ?></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="drawlist_get">새 추첨 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="draw_admin_datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>구분</th>
							<th>부서명</th>
							<th>제목</th>
							<th>추첨 차수</th>
							<th>등록신청기간</th>
							<th>추첨상태</th>
							<th>설정</th>
							<th>다음회차</th>
							<th>일괄배치</th>
							<th>우선배치</th>
							<th>추첨기록</th>
							<th>초기화</th>							
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>								
								<?php if($value['ptid'] == 1) 
								{
									echo '<td>노외주차처</td>';
								}else {
									echo '<td>노상주차처</td>';

								} ?>
							<td><?=$value['title']?></td>
							<td><?=$value['round']?></td>
							<td><?=$value['start_date']?> ~ <?=$value['end_date']?></td>
							<?php if($value['setting'] == "start") 
								{
									echo '<td>신청시작</td>';
								} elseif($value['setting'] == "end") {
									echo '<td>신청마감</td>';

								} elseif($value['setting'] == "finish") {
									echo '<td>추첨종료</td>';
								}?>
							<td><input type="button" name="view" value="설정" id="<?=$value['dws_id']?>" class="btn btn-primary drawinfo_modify"></td>
							<td>
								<?php if($value['status'] == 1) { ?>
									<input type="button" name="view" value="<?=$value['round']+1?>회차 진행" id="<?=$value['dws_id']?>" class="btn btn-success next_draw">
								<?php } else {?>

								<?php } ?>				
							</td>
							<?php if($value['status'] == 1) { ?>
								<td><input type="button" name="view" value="일괄배치" id="move_draw_left" class="btn btn-info draw_left"></td>
							<?php } else {?>
								<td></td>
							<?php } ?>
							<?php if($value['status'] == 1) { ?>
								<td><input type="button" name="view" value="우선" id="<?=$value['dws_id']?>" class="btn btn-dark move_admin_askparkinglot"></td>	
							<?php } else {?>
								<td></td>
							<?php } ?>
							<td><input type="button" name="view" value="보기" id="<?=$value['dws_id']?>" class="btn btn-warning show_history"></td>
							<?php if($value['status'] == 1) { ?>
								<td><input type="button" name="view" value="초기화" id="<?=$value['dws_id']?>" class="btn btn-danger delete_all_btn"></td>
							<?php } else {?>
								<td></td>
							<?php } ?>	
							</tr>
						<?php endforeach?>    						
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div> 
<form id="history_dws_id_form">
	<input type="hidden" name="history_dws_id" id="history_dws_id">
	<input type="hidden" name="history_round" id="history_round" value="1">
</form>

 <div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">추첨회차 정보 추가/수정</h4>  
                </div> 

                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" id="draw_info_form" name="draw_info_form">
                    <input type="hidden" id="etc_table_initial" name="etc_table_initial">
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">구분</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="draw_index" name="draw_index" readonly="true">
                        </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">제목</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="draw_title" name="draw_title">
                        </div>
                      </div>
    				<div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">부서명</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
					
                       		<?php if($ptid == 1) 
								{
									echo '<input type="text" class="form-control" value ="노외주차처" readonly = "true">';
								}else {
									echo '<input type="text" class="form-control" value ="노상주차처" readonly = "true">';
								} 
							?>
                        </div>
                      </div>
                     <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">추첨차수</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control" id="draw_round" name="draw_round">
                            <option value="1">1차</option>
                            <option value="2">2차</option>
                            <option value="3">3차</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                      	<label class="control-label col-md-3 col-sm-3 col-xs-3">신청기간</label>
	                        <div class="col-md-9 col-sm-9 col-xs-9">
	                           <fieldset>
                            <div class="control-group">
                              <div class="controls">
                                <div class="input-prepend input-group">
                                  <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                                  <input type="text" style="width: 100%" name="draw_date" id="draw_period" class="form-control" value="">
                                </div>
                              </div>
                            </div>
                          </fieldset>          
	                        </div>
                        </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">사용여부</label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select class="form-control" id="draw_status" name="draw_status">
	                            <option value="1">사용</option>
	                            <option value="0">비사용</option>
	                          </select>
	                        </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">추첨상태</label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select class="form-control" id="draw_setting" name="draw_setting">
	                            <option value="start">신청시작</option>
	                            <option value="end">신청마감</option>
	                          </select>
	                        </div>
                      </div>

                 <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">마감일</label>
                         <div class="col-md-9 col-sm-9 col-xs-12">
							<div class='col-sm-6 col-xs-12 input-group date form_datetime' id = "deadline_date">
								<span class="input-group-addon">
	                        		<span class="fa fa-calendar"></span>
	                    		</span>	
								<input type="text" name="draw_deadline_date" required="required" class="form-control" id="draw_deadline_date" >			
						   </div>
						</div>
					</div>
	


                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">마감시간</label>
	                        <div class="col-md-9 col-sm-9 col-xs-12">
	                          <select class="form-control" id="draw_deadline_time" name="draw_deadline_time">
	                            <option value="10:00:00">10:00</option>
	                            <option value="12:00:00">12:00</option>
								<option value="14:00:00">14:00</option>
								<option value="16:00:00">16:00</option>
								<option value="18:00:00">18:00</option>
	                          </select>
	                        </div>
                      </div>
						
                      <div class="ln_solid"></div>
                  </div>

 
	                <div class="modal-footer">  
	                	<button type="submit" class="btn btn-primary" id="save_draw_info">저장 </button>  
	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  



 <div id="deleteModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">추첨회차 정보 초기화</h4>  
                </div> 

                <div class="x_content">
                    <br>
                     <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3" style="margin-top: 5px">추첨 차수</label>
                       <div class="col-md-9 col-sm-9 col-xs-12" style="margin-bottom: 30px;">
                       	  <input type="hidden" id="delete_dws_id">
                          <select class="form-control" id="delete_draw_round" name="delete_draw_round">
                            <option value="1">1차</option>
                            <option value="2">2차</option>
                            <option value="3">3차</option>
                            <option value="4">일괄배치</option>
                          </select>
                        </div>
                      </div>						
                  </div>

	                <div class="modal-footer">  
	                	<button type="submit" class="btn btn-primary" id="delete_all">초기화 </button>  
	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  



