<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>추첨 히스토리</small></h2>
				<div><input type="button" name="round4_button" value="일괄배치 결과" class="btn btn-primary navbar-right round4_button"></div>
				<div><input type="button" name="round3_button" value="3차 결과" class="btn btn-primary navbar-right round3_button"></div>
				<div><input type="button" name="round2_button" value="2차 결과" class="btn btn-primary navbar-right round2_button"></div>
				<div><input type="button" name="round1_button" value="1차 결과" class="btn btn-primary navbar-right round1_button"></div>
				
				<form id="round_id_form" name="round_id_form">
					<input type="hidden" id="history_dws_id" name="history_dws_id" value="<?=$dws_id?>">
					<input type="hidden" id="history_round" name="history_round">
				</form>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="history-datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>구분</th>
							<th>지역구</th>
							<th>주차장명</th>
							<th>정원</th>
							<th>신청인원</th>
							<th>신청명단</th>
							<th>확정명단</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['dtitle']?></td>
							<td><?=$value['ch']?></td>	
							<td><?=$value['curr_cnt']['curr_cnt']?></td>	
							<td> 
								<?php if(isset($value['names'])):?>
									<?php foreach ($value['names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-primary"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['name_list']['name_list']):?>
										<button type="button" class="btn btn-round btn-primary"><?=$value['name_list']['name_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	


							<td> 
								<?php if(isset($value['fixed_names'])):?>
									<?php foreach ($value['fixed_names'] as $key1 => $value1) :?>
										<button type="button" class="btn btn-round btn-success"><?=$value1?></button>
										<?php if(($key1+1) % 3 == 0):?>
											<br>
										<?php endif?>	

									<?php endforeach?>
								<?php else:?>
									<?php if($value['fixed_list']['fixed_list']):?>
										<button type="button" class="btn btn-round btn-success"><?=$value['fixed_list']['fixed_list']?></button>
									<?php else:?>
										
									<?php endif;?>
								<?php endif;?>
							</td>	
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <h4 class="modal-title">근무지 추첨 중 </h4>  
                </div>  
	                <div class="modal-body" id="draw_process_modal">  
	                	    <progress width="100" value="50" max="50" style="width:95%; animation: progress 5s ease-in-out forwards;"></progress> 
	                </div>  
	                <div class="modal-footer"></div>  
            </form>
           </div>  
      </div>  
 </div>  
