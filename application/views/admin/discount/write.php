<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$mtitle?> <small>관리자 기능</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/admin/discount/save" enctype="multipart/form-data">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">아이디 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="dis_id" name="dis_id" required="required" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$row['dis_id']?>" readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">퍼블릭 아이디 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="dis_public_id" name="dis_public_id" required="required" class="form-control col-md-7 col-xs-12" placeholder="행정 정보 공동 이용 아이디 , 커스텀은 100번대 사용할 것" value="<?=$row['dis_public_id']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">타이틀 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" placeholder="ex)경차" value="<?=$row['title']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_rate"> 할인 %  <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="discount_rate" name="discount_rate" required="required" class="form-control col-md-7 col-xs-12" min="1" max="100" value="<?=$row['discount_rate']?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status"> 상태   <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="status" name="status" required="required" class="form-control col-md-7 col-xs-12" min="0" max="1" value="<?=$row['status']?>" placeholder="0:보이지 않기 , 1: 보이기">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status"> 고정금액 여부   <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="is_fix_price" name="is_fix_price" required="required" class="form-control col-md-7 col-xs-12" min="0" max="1" value="<?=$row['is_fix_price']?>" placeholder="0:고정금액 아님  , 1: 고정금액">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status"> 고정금액    <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="fix_price" name="fix_price" class="form-control col-md-7 col-xs-12" min="0" max="10000000" value="<?=$row['fix_price']?>" placeholder="10000">
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="status"> 보이기 순서    <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="dis_order" name="dis_order" required="required" class="form-control col-md-7 col-xs-12" min="1" max="10000000" value="<?=$row['dis_order']?>" placeholder="보이기 순서">
						</div>
					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="submit" class="btn btn-success">제출</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column