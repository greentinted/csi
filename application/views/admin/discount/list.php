<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>감면 유형 설정<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="/admin/discount/write" class="">신규 입력으로 이동</a>
							</li>
						</ul>

				</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">


				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>퍼블릭 아이디</th>
							<th>타이틀</th>
							<th>할인%</th>
							<th>설명</th>
							<th>고정금액여부</th>
							<th>고정금액</th>
							<th>ORDER NO</th>
							<th>상태</th>
							<th>수정</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['dis_id']?></td>
							<td><?=$value['dis_public_id']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['discount_rate']?>%</td>
							<td><?=$value['discount_desc']?></td>
							<td><?=$value['is_fix_price']?></td>
							<td><?=$value['fix_price']?></td>
							<td><?=$value['dis_order']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
								<input id="discount_<?=$value['dis_id']?>" type="checkbox" class="js-switch discount_status" checked /> <span>활성화</span>
								</label>								
								</td>

							<?php else :?>
								<td>
								<label>

									<input id="discount_<?=$value['dis_id']?>" type="checkbox" class="js-switch discount_status" /> <span>비활성화</span>
								</label>								
								</td>

							<?php endif?>	
				
							<td><a class="btn btn-info" href="/admin/discount/write?dis_id=<?=$value['dis_id']?>">수정</a></td>	

							<?php /*

							<td><input type="button"  name="view" class="btn btn-danger init_password" id="<?=$value['aid']?>" value="비번초기화"></td>						

							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




