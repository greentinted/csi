<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2> 입력 리스트<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>


	
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

					<div class="form-group">
						<label class="col-sm-4 control-label">조회기간</label>

						<div class="col-sm-3">
							<div class="input-group">
								
	                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                            <input type="text" name="ic_q_period" id="ic_q_period" class="form-control"  value="<?=$viewing_from?>" />
			                        
								<span class="input-group-btn">
										<button type="button" class="btn btn-primary ic_query_btn" >조회하기</button>
								</span>
							</div>
						</div>
					</div>
					<div class="divider-dashed"></div>
				<br>
				<br>
				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>작업자</th>
							<th>주차장</th>
							<th>집계일</th>
							<th>등록일</th>
							
							<th>일차현금(매수)</th>
							<th>일차현금(원)</th>

							<th>일차카드(매수)</th>
							<th>일차카드(원)</th>

							<th>일차계좌이체(매수)</th>
							<th>일차계좌이체(원)</th>

							<th>일차T-머니교통(매수)</th>
							<th>일차T-머니교통(원)</th>

							<th>일차카카오-T(매수)</th>
							<th>일차카카오-T(원)</th>

							<th>일차기타(매수)</th>
							<th>일차기타(원)</th>

							<th>월차현금(매수)</th>
							<th>월차현금(원)</th>

							<th>월차카드(매수)</th>
							<th>월차카드 (원)</th>

							<th>월차계좌이체(매수)</th>
							<th>월차계좌이체 (원)</th>

							<th>월차웹연장(매수)</th>
							<th>월차웹연장 (원)</th>


							<th>월차가상계좌(매수)</th>
							<th>월차가상계좌 (원)</th>

							<th>월차기타(매수)</th>
							<th>월차기타(원)</th>

							<th>미납(건수)</th>
							<th>미납(원)</th>

							<th>비고</th>

							<th>수정</th>
							<th>삭제</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['ic_id']?></td>
							<td><?=$value['user']['id']?></td>
							<td><?=$value['wpid_title']?></td>
							<td><?=$value['income_date']?></td>
							<td><?=$value['create_date']?></td>
							
							<td><?=$value['daily_cash']?></td>
							<td><?=$value['daily_cash_amount']?></td>

							<td><?=$value['daily_card']?></td>
							<td><?=$value['daily_card_amount']?></td>

							<td><?=$value['daily_transfer']?></td>
							<td><?=$value['daily_transfer_amount']?></td>

							<td><?=$value['daily_tmoney']?></td>
							<td><?=$value['daily_tmoney_amount']?></td>

							<td><?=$value['daily_kakaot']?></td>
							<td><?=$value['daily_kakaot_amount']?></td>

							<td><?=$value['daily_etc']?></td>
							<td><?=$value['daily_etc_amount']?></td>

							<td><?=$value['monthly_cash']?></td>
							<td><?=$value['monthly_cash_amount']?></td>

							<td><?=$value['monthly_card']?></td>
							<td><?=$value['monthly_card_amount']?></td>


							<td><?=$value['monthly_transfer']?></td>
							<td><?=$value['monthly_transfer_amount']?></td>

							<td><?=$value['monthly_web']?></td>
							<td><?=$value['monthly_web_amount']?></td>

							<td><?=$value['monthly_v_transfer']?></td>
							<td><?=$value['monthly_v_transfer_amount']?></td>

							<td><?=$value['monthly_etc']?></td>
							<td><?=$value['monthly_etc_amount']?></td>

							<td><?=$value['unpaid']?></td>
							<td><?=$value['unpaid_amount']?></td>

							<td><?=$value['description']?></td>							


			
							<td><a class="btn btn-info" href="/escalate/daily_income/detail?ic_id=<?=$value['ic_id']?>&access_type=admin">수정</a></td>


							<td><a onclick="return confirm('정말로 삭제하시겠습니까? ')" href="/escalate/daily_income/delete?ic_id=<?=$value['ic_id']?>&access_type=admin" class="btn btn-danger">삭제</a></td>

							<?php /*

							<td><input type="button"  name="view" class="btn btn-danger init_password" id="<?=$value['aid']?>" value="비번초기화"></td>						

							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>미보고 주차장<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="white_ip_data_table" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>주차장명</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($unreport_list  as $key => $value): ?>
						<tr>
							<td><?=$value['wpid']?></td>
							<td><?=$value['title']?></td>
							<td></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




