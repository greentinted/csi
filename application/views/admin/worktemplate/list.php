<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>Default Example <small>Users</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="worktemplate_get">템플릿 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>근무형태</th>
							<th>근무편성</th>
							<th>평일</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>시간외시간</th>
							<th>야간시간</th>
							<th>휴무일</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>시간외시간</th>
							<th>야간시간</th>
							<th>휴일(공휴일)</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>휴일시간</th>
							<th>야간시간</th>							
							<th>Action</th>							
						</tr>
					</thead>


					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['wtlt_id']?></td>
							<td><?=$value['ws_title']?></td>
							<td><?=$value['ww_title']?></td>
							<td>근무</td>
							<td><?=$value['normal_work_start']?></td>
							<td><?=$value['normal_work_end']?></td>
							<td><?=$value['normal_work_hour']?></td>
							<td><?=$value['normal_work_overtime_hour']?></td>
							<td><?=$value['normal_work_night_hour']?></td>
							<td>휴무근무</td>
							<td><?=$value['off_work_start']?></td>
							<td><?=$value['off_work_end']?></td>
							<td><?=$value['off_work_hour']?></td>
							<td><?=$value['off_work_overtime_hour']?></td>
							<td><?=$value['off_work_night_hour']?></td>
							<td>휴일근무</td>
							<td><?=$value['holiday_work_start']?></td>
							<td><?=$value['holiday_work_end']?></td>
							<td><?=$value['holiday_work_hour']?></td>
							<td><?=$value['holiday_work_overtime_hour']?></td>
							<td><?=$value['holiday_work_night_hour']?></td>
							<td><input type="button" name="view" value="EDIT" id="<?=$value['wtlt_id']?>" class="btn btn-primary worktemplate_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">템플릿  추가 / 수정 </h4>  
                </div>  
                <form role="form" id="workplace_detail" action="/admin/worktemplate/save">
	                <div class="modal-body" id="workplace_modal">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
