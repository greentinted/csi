<style type="text/css">
	.selected{
		background-color: #acbad4;	
	}
	
</style>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>일일 연차 및 무급 휴무 신청 승인  <small> <?=$parking_lot['title']?></small></h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="approve_batch">일괄승인하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>

			<div class="x_content">

				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->

			<form class="form-horizontal form-label-left" method="POST" id="approve_vac" action="/admin/approve_vacation/">
					<div class="form-group">


						<label class="col-sm-4 control-label">조회기간</label>
						<div class="col-sm-3">
							<div class="input-group">
								
	                            <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
	                            <input type="text" name="approve_vac_query_period" id="approve_vac_query_period" class="form-control"  value="<?=$approve_vac_query_str?>" />
			                        
								<span class="input-group-btn">
										<button type="button" class="btn btn-primary" id="approve_vac_query_btn">조회하기</button>
								</span>
							</div>
						</div>
					</div>
					<div class="divider-dashed"></div>

				</form>


				<table id="appvac_datatable" class="table table-bordered display">
					<thead>
						<tr>
							<th>#</th>
							<th>아이디</th>
							<th>요청휴가일</th>
							<th>근무지</th>
							<th>근무지정</th>
							<th>근로형태</th>
							<th>발생연차</th>
							<th>전월누계</th>
							<th>잔여연차</th>
							
							<th>종류</th>
							<th>상태</th>
							<th>삭제</th>
							
							<th>사유</th>
							<th>거절사유</th>
							
						</tr>
					</thead>


					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>

							<td><?=$value['vrid']?></td>
							<td><?=$value['id']?></td>					
							<td><?=$value['request_date']?></td>
							<td  width=130><?=$value['wp_title']?></td>
			
							<td><?=$value['wa_title']?></td>
							<td><?=$value['ws_title']?></td>

							
							
							<td><?=$value['leave_days']?></td>
							<td><?=$value['pre_month']?></td>
							<td><?=$value['remain_leave_days']?></td>
							<?php if($value['is_duplicate']):?>
								<td><?=$value['wt_title']?> / <?=$value['off_hour']?> 시간 요청</td>							
							<?php else:?>
								<td><?=$value['wt_title']?>
							<?php endif?>

							<?php if($value['v_status'] == 'request'):?>
							<td><input type="button" name="view" value="승인" id="<?=$value['vrid']?>" class="btn btn-info approve_vacation"></td>
								
							<?php elseif($value['v_status'] == 'approve'):?>
							<td><a class="btn btn-primary" href="#">승인완료</a></td>

							<?php else :?>
							<td><a class="btn btn-danger" href="#">승인거절</a></td>
							<?php endif?>
							<td><a onclick="return confirm('정말로 삭제하시겠습니까? ')"  href="/admin/approve_vacation/delete?vrid=<?=$value['vrid']?>" class="btn btn-danger">삭제</a></td>


							<td width=130><?=$value['reason']?></td>
							<td><?=$value['reject_reason']?></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="app_vac_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">연차 승인</h4>  
                </div>  
                <form role="form" method="post" action="/admin/approve_vacation/save">
	                <div class="modal-body" id="approve_vac_modal">  
	                	
	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

                     	<button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div> 
</div> 

