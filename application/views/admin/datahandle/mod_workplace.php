<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무지 정보 수정</h2>
			<!-- 	<a href="/escalate/contractor/change_history"><button type="button" class="btn btn-success  navbar-right">변경 처리 내역</button></a> -->
				<!-- <ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul> -->
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="change_contractor_form" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" onsubmit="return checkContractForm();" action="/admin/datahandle/wp_save">

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="wpid"> <span class="required">아이디</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<?php if($workplace_row['wpid'] == 0) { ?>
								<input id="id" name="id" class="form-control col-md-7 col-xs-12" value="" readonly>
							<?php } else { ?>
								<input id="id" name="id" class="form-control col-md-7 col-xs-12" value="<?=$workplace_row['wpid']?>" readonly>
							<?php }	?>
							
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" name="did" id="did" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $workplace_row['did'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>

					<fieldset class="control-label">
					<legend>주차장 정보</legend>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="주소"> 주차장
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="title" name="title" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['title']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="주소"> 주소
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="description" name="description" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['description']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="정원"> 근무 정원
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="ch" name="ch" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['ch']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="주차면수"> 주차면수
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="parking_number" name="parking_number" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['parking_number']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="적정주차면수"> 적정주차면수
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="suit_parking_number" name="suit_parking_number" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['suit_parking_number']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="요금체계"> 요금체계
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="fare_system" name="fare_system" class="form-control" rows="10"><?=$workplace_row['fare_system']?></textarea>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 관제 회사
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="vendor_name" name="vendor_name" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['vendor_name']?>">
							<span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 전화 번호
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="tel_num" name="tel_num" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['tel_num']?>">
							<span class="fa fa-building form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="api"> API 사용여부
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="is_standapi" name="is_standapi" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['is_standapi']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="상태"> lat</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="lat" name="lat" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['lat']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="상태"> lng</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="lng" name="lng" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['lng']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
					<label class="control-label col-md-3 col-sm-3 col-xs-12" for="상태"> 지도</label>

					<div id="map" class="col-md-6 col-sm-6 col-xs-12" style="height:400px"></div>	
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="결제 단말기 번호"> 결제 단말기 번호
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="console_number" name="console_number" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['console_number']?>">
							<span class="fa fa-credit-card form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="평일시작"> 평일 근무 시작 / 끝
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="normal_day_start" name="normal_day_start" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['normal_day_start']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="normal_day_end" name="normal_day_end" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['normal_day_end']?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="토요일"> 토요일 근무 시작 / 끝
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="sat_day_start" name="sat_day_start" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['sat_day_start']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="sat_day_end" name="sat_day_end" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['sat_day_end']?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="일요일"> 일요일 근무 시작 / 끝
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="sun_start" name="sun_start" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['sun_start']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="sun_end" name="sun_end" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['sun_end']?>">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="상태"> 주차장 활성화</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="status" name="status" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['status']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>



				</fieldset>

					<fieldset class="control-label">
					<legend>쿠폰 정보</legend>



					


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="30분권"> 30분권
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="halfhour_coupon_amount" name="halfhour_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['halfhour_coupon_amount']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="1시간권"> 1시간권
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="hour_coupon_amount" name="hour_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['hour_coupon_amount']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
							</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="1.5시간권"> 1.5시간권
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="onehalf_coupon_amount" name="onehalf_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['onehalf_coupon_amount']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="2시간권"> 2시간권
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="twohour_coupon_amount" name="twohour_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['twohour_coupon_amount']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>
					
					</fieldset>

					<fieldset class="control-label">
					<legend>현장 PC 정보</legend>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="pc_spec"> PC SPEC
						</label>



 						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="computer_spec" name="computer_spec" class="form-control" rows="10"><?=$workplace_row['computer_spec']?></textarea>

						</div>
 					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="MAC"> MAC
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="mac_address" name="mac_address" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['mac_address']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="Windows"> Windows
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="windows" name="windows" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['windows']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="memsize"> memsize
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="memsize" name="memsize" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['memsize']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cpu speed"> cpu speed
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="cpu_speed" name="cpu_speed" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$workplace_row['cpu_speed']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>
				</fieldset>


					
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">						<button type="button" class="btn_wp_delete btn btn-danger" id="<?=$workplace_row['wpid']?>">삭제</button>
		
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="submit" class="btn btn-success">저장</button>
							<!-- <a href="/admin/datahandle/wp_delete?wpid=<?=$workplace_row['wpid']?>" class="btn btn-danger" id="<?=$workplace_row['wpid']?>">삭제</a> -->
						</div>
					</div>
				
					<input type="hidden" id="edit_type" name="edit_type" value="$cc_type">
				</form>
			</div>
		</div>
	</div>
</div>





<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=cc1194c66e4c14603df366fffb14bd46"></script>


<script type="text/javascript">

	//var container = document.getElementById('map');
	var lat = $('#lat').val();
	var lng = $('#lng').val();
	var title = $('#title').val();

	if(!lat){
		lat = 37.45051080;
	}

	if(!lng){
		lng = 127.13124040;
	}


    $.ajax({  
             url:"/admin/datahandle/get_wp",  
             method:"post",  
             data:{title:title},  
             dataType: 'json',  
              success:function(resData){  

            	console.log('resData' , resData.wp_list);
				var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
				    mapOption = { 
			        center: new kakao.maps.LatLng(lat, lng), // 지도의 중심좌표
			        level: 3 // 지도의 확대 레벨
			    };

            	var map = new kakao.maps.Map(mapContainer, mapOption); // 

            	var positions = [];

            	for(var i = 0; i < resData.wp_list.length; i++){
            		var data = resData.wp_list[i];
					var arr = {title: data.title , 
						lat : data.lat,
						lng : data.lng
						//latlng: new kakao.maps.LatLng(data.lat, data.lng)
					}            			

					positions.push(arr);
            	}


				// 마커 이미지의 이미지 주소입니다
				// var imageSrc = "http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/markerStar.png"; 
				    
				for (var i = 0; i < positions.length; i ++) {

	            		if(!positions[i].lat){
	            			// console.log('i : ' , i);
	            			// console.log('lat : ' , positions[i].lat);
	            			continue;
	            		}
	            		if(!positions[i].lng){
	            			continue;
	            		}


					var iwContent = '<div style="padding:5px;">'+positions[i].title+'</div>', // 인포윈도우에 표출될 내용으로 HTML 문자열이나 document element가 가능합니다


					    iwPosition = new kakao.maps.LatLng(positions[i].lat, positions[i].lng), //인포윈도우 표시 위치입니다
					    iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

					// 인포윈도우를 생성하고 지도에 표시합니다
					var infowindow = new kakao.maps.InfoWindow({
					    map: map, // 인포윈도우가 표시될 지도
					    position : iwPosition, 
					    content : iwContent,
					    removable : iwRemoveable
					});					
				    
				    // 마커 이미지의 이미지 크기 입니다
				    // var imageSize = new kakao.maps.Size(24, 35); 
				    
				    // // 마커 이미지를 생성합니다    
				    // var markerImage = new kakao.maps.MarkerImage(imageSrc, imageSize); 
				    
				    // // 마커를 생성합니다
				    // var marker = new kakao.maps.Marker({
				    //     map: map, // 마커를 표시할 지도
				    //     position: positions[i].latlng, // 마커를 표시할 위치
				    //     title : positions[i].title, // 마커의 타이틀, 마커에 마우스를 올리면 타이틀이 표시됩니다
				    //     image : markerImage // 마커 이미지 
				    // });
				}            	

        	}
        


    });          

</script>

