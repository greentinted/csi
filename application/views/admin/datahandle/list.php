<div class="row">	
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>WHITE IP<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a href="#" class="handle_whiteip">화이트 아이피 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="white_ip_data_table" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>IP ADDRESS</th>
							<th>타이틀</th>
							<th>상태</th>
							<th>description</th>							
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($whiteip_list as $key => $value): ?>
						<tr>
							<td><?=$value['wiid']?></td>
							<td><?=$value['address']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
								<input id="whiteip_<?=$value['wiid']?>" type="checkbox" class="js-switch whiteip_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="whiteip_<?=$value['wiid']?>" type="checkbox" class="js-switch whiteip_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>


							<td><?=$value['description']?></td>
							<td><input type="button"  name="view" class="btn btn-info handle_whiteip" id="<?=$value['wiid']?>" value="수정"></td>						
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>직급<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="rank_get">직급 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped">

					<thead>
						<tr>
							<th>아이디</th>
							<th>직급</th>							
							<th>상태</th>							
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($rank_list as $key => $value): ?>
						<tr>
							<td><?=$value['rid']?></td>
							<td><?=$value['title']?></td>							
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="rank_<?=$value['rid']?>" type="checkbox" class="js-switch rank_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="rank_<?=$value['rid']?>" type="checkbox" class="js-switch rank_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>


							<td><input type="button" name="view" value="수정" id="<?=$value['rid']?>" class="btn btn-info rank_get"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무지 리스트<small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="/admin/datahandle/wp_write" class="workplace_get">근무지 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table id="workplace_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>근무지</th>
							<th>지역</th>
							<th>정원</th>
							<th>상태</th>
							<th>주차면수</th>
							<th>windows</th>
							<th>메모리</th>
							<th>bit</th>

							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($wp_list as $key => $value): ?>
						<tr>
							<td><?=$value['wpid']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['region']?></td>
							<td><?=$value['ch']?></td>							
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="wp_<?=$value['wpid']?>" type="checkbox" class="js-switch wp_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="wp_<?=$value['wpid']?>" type="checkbox" class="js-switch wp_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>

							
							<td><?=$value['parking_number']?></td>				
							<td><?=$value['windows']?></td>				
							<td><?=$value['memsize']?></td>				
							<td><?=$value['bit']?></td>					

 							<td><input type="button" name="view" value="수정" id="<?=$value['wpid']?>" class="workplace_get btn btn-info"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 템플릿 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="wtp_get">템플릿 추가하기</a>
							</li>
							<?php /*
							<li><a href="/admin/datahandle/save_urgent_template" class="">템플릿 ( 조정 ) 추가하기</a>
							</li>
							*/ ?>

						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="wtp_datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>근무형태</th>
							<th>근무편성</th>
							<th>평일</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>시간외시간</th>
							<th>야간시간</th>
							<th>휴무일</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>시간외시간</th>
							<th>야간시간</th>
							<th>휴일(공휴일)</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>근무시간</th>
							<th>휴일시간</th>
							<th>야간시간</th>
							<th>상태</th>							
							<th>Action</th>							
						</tr>
					</thead>


					<tbody>
						<?php foreach ($wtp_list as $key => $value): ?>
						<tr>
							<td><?=$value['wtlt_id']?></td>
							<td><?=$value['ws_title']?></td>
							<td><?=$value['ww_title']?></td>
							<td>근무</td>
							<td><?=$value['normal_work_start']?></td>
							<td><?=$value['normal_work_end']?></td>
							<td><?=$value['normal_work_hour']?></td>
							<td><?=$value['normal_work_overtime_hour']?></td>
							<td><?=$value['normal_work_night_hour']?></td>
							<td>휴무근무</td>
							<td><?=$value['off_work_start']?></td>
							<td><?=$value['off_work_end']?></td>
							<td><?=$value['off_work_hour']?></td>
							<td><?=$value['off_work_overtime_hour']?></td>
							<td><?=$value['off_work_night_hour']?></td>
							<td>휴일근무</td>
							<td><?=$value['holiday_work_start']?></td>
							<td><?=$value['holiday_work_end']?></td>
							<td><?=$value['holiday_work_hour']?></td>
							<td><?=$value['holiday_work_overtime_hour']?></td>
							<td><?=$value['holiday_work_night_hour']?></td>
							
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="wtp_<?=$value['wtlt_id']?>" type="checkbox" class="js-switch wtp_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="wtp_<?=$value['wtlt_id']?>" type="checkbox" class="js-switch wtp_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>

							<td><input type="button" name="view" value="수정" id="<?=$value['wtlt_id']?>" class="btn btn-info wtp_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>주차처 타입 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="pt_get">주차처타입 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>아이디</th>
							<th>주차처</th>	
							<th>상태</th>							
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($pt_list as $key => $value): ?>
						<tr>
							<td><?=$value['pid']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="pt_<?=$value['pid']?>" type="checkbox" class="js-switch pt_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="pt_<?=$value['pid']?>" type="checkbox" class="js-switch pt_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>
							<td><input type="button" name="view" value="수정" id="<?=$value['pid']?>" class="btn btn-info pt_get"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>지역 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="district_get">지역 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>지역명</th>
							<th>상태</th>
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($dt_list as $key => $value): ?>
						<tr>
							<td><?=$value['did']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="dt_<?=$value['did']?>" type="checkbox" class="js-switch dt_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="dt_<?=$value['did']?>" type="checkbox" class="js-switch dt_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>

							<td><input type="button" name="view" value="수정" id="<?=$value['did']?>" class="btn btn-info district_get"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 종류 <small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="wt_get">근무종류 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table class="table table-striped">					
					<thead>
						<tr>
							<th>#</th>
							<th>근무종류</th>
							<th>휴가관련</th>
							<th>중복기제여부</th>
							<th>상태</th>
							<th>인식색</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($wt_list as $key => $value): ?>
						<tr>
							<td><?=$value['wtid']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['is_vacation']) :?>
								<td>휴가</td>
							<?php else :?>
								<td>근무</td>
							<?php endif?>
					
							<?php if($value['is_duplicate']) :?>
								<td>중복가능</td>
							<?php else :?>
								<td>중복안됨</td>
							<?php endif?>


							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="dt_<?=$value['wtid']?>" type="checkbox" class="js-switch wt_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="dt_<?=$value['wtid']?>" type="checkbox" class="js-switch wt_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>
							<td><?=$value['color']?> <br> <?php if(isset($value['color_title'])):?><?php endif;?>
								<div class="progress">
    								<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:100%; background-color:<?=$value['color']?> !important;">
    								</div>
								</div>
								
							</td>							
							<td><input type="button" name="view" value="EDIT" id="<?=$value['wtid']?>" class="btn btn-info wt_get">
							</td>

						</tr>

						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>컬러 리스트 <small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="rc_get">컬러 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="color_list_datatable" class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>이름</th>
							<th>인식색</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($rc_list as $key => $value): ?>
						<tr>
							<td><?=$value['rcid']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['color']?>
								<div class="progress">
    								<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:100%; background-color:<?=$value['color']?> !important;">
    								</div>
								</div>
							</td>

							<td><input type="button" name="view" value="EDIT" id="<?=$value['rcid']?>" class="btn btn-primary rc_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 지정 <small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="wa_get">근무지정 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>#</th>
							<th>근무지정</th>							
							<th>상태</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($wa_list as $key => $value): ?>
						<tr>
							<td><?=$value['waid']?></td>
							<td><?=$value['title']?></td>

							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="wa_<?=$value['waid']?>" type="checkbox" class="js-switch wa_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="wa_<?=$value['waid']?>" type="checkbox" class="js-switch wa_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>
							
							<td><input type="button" name="view" value="수정" id="<?=$value['waid']?>" class="btn btn-info wa_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 형태 <small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="ws_get">근무형태 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table class="table table-striped">
					<thead>
						<tr>
							<th>아이디</th>
							<th>근로형태</th>
							<th>상태</th>
							<th>근무 시간 입력 여부</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($ws_list as $key => $value): ?>
						<tr>
							<td><?=$value['wsid']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="ws_<?=$value['wsid']?>" type="checkbox" class="js-switch ws_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="ws_<?=$value['wsid']?>" type="checkbox" class="js-switch ws_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>
							<?php if($value['input_time']):?>
								<td>必要</td>
							<?php else:?>
								<td>不必要</td>	
							<?php endif?>	
							<td></td>
							<td><input type="button" name="view" value="수정" id="<?=$value['wsid']?>" class="btn btn-info ws_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 편성 <small><?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="ww_get">근무편성 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped">
					<thead>
						<tr>
							<th>아이디</th>
							<th>근무편성</th>
							<th>상태</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($ww_list as $key => $value): ?>
						<tr>
							<td><?=$value['wwid']?></td>
							<td><?=$value['title']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="ww_<?=$value['wwid']?>" type="checkbox" class="js-switch ww_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="ww_<?=$value['wwid']?>" type="checkbox" class="js-switch ww_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>
							
							<td><input type="button" name="view" value="수정" id="<?=$value['wwid']?>" class="btn btn-info ww_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>







<div id="ptModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">주차처타입 추가/수정</h4>  
                </div>  
                <form role="form" id="parking_type_edit" action="/admin/datahandle/pt_save">
	                <div class="modal-body" id="parking_type_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  


<div id="rankModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">직급 추가/수정</h4>  
                </div>  
                <form role="form" id="rank_edit" action="/admin/datahandle/rank_save">
	                <div class="modal-body" id="rank_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button> 

	                     <button type="button" class="btn btn-danger" id="btn_rank_delete">삭제 </button>

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

  <div id="wpModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무지 추가/수정</h4>  
                </div>  
                <form role="form" id="workplace_edit" action="/admin/datahandle/wp_save">
	                <div class="modal-body" id="workplace_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  



<div id="white_ip_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">whiteip 추가/수정</h4>  
                </div>  
                <form role="form" id="whiteip_edit" action="/admin/datahandle/whiteip_save">
	                <div class="modal-body" id="whiteip_modal_detail">  

	                </div>  
	                <div class="modal-footer">  
						 <button type="button" class="btn btn-danger" id="btn_delete_whiteip">삭제</button>

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button> 
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  


<div id="dtModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">지역 추가 수정 </h4>  
                </div>  
                <form role="form" id="workplace_edit" action="/admin/datahandle/dt_save">
	                <div class="modal-body" id="district_modal">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

 <div id="wtModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무종류 추가/수정</h4>  
                </div>  
                <form role="form" id="worktype_edit" action="/admin/datahandle/wt_save">
	                <div class="modal-body" id="worktype_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  
	                </div>  
            </form>
           </div>  
      </div>  
</div>  
<div id="rcModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">컬러 추가/수정</h4>  
                </div>  
                <form role="form" id="recognize_color_edit" action="/admin/datahandle/rc_save">
	                <div class="modal-body" id="recognize_color_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

<div id="wtpModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">템플릿  추가 / 수정 </h4>  
                </div>  
                <form role="form" id="worktemplate_detail" action="/admin/datahandle/wtp_save">
	                <div class="modal-body" id="worktemplate_modal">  

	                </div>  
	                <div class="modal-footer">  

 						<button type="button" class="btn btn-danger wtp_delete">삭제</button>
	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                    <button type="submit" class="btn btn-primary">저장 </button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
 <div id="waModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무지정 추가/수정</h4>  
                </div>  
                <form role="form" id="workassign_edit" action="/admin/datahandle/wa_save">
	                <div class="modal-body" id="workassign_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
<div id="wsModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무형태 추가/수정</h4>  
                </div>  
                <form role="form" id="workstyle_edit" action="/admin/datahandle/ws_save">
	                <div class="modal-body" id="workstyle_detail">  

	                </div>  
	                <div class="modal-footer">  
						 <button type="button" class="btn btn-danger" id="btn_ws_delete" data-dismiss="modal">삭제</button>  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
<div id="wwModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무시간 추가/수정</h4>  
                </div>  
                <form role="form" id="workwhen_edit" action="/admin/datahandle/ww_save">
	                <div class="modal-body" id="workwhen_detail">  

	                </div>  
	                <div class="modal-footer">  
						 <button type="button" class="btn btn-danger" id="btn_ww_delete">삭제</button>  
	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                     <button type="submit" class="btn btn-primary">저장 </button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  


