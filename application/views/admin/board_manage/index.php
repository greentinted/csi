<div class="row">	

<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>게시판 폴더 관리<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="bt_get">게시판 타입 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped">

					<thead>
						<tr>
							<th>아이디</th>
							<th>제목</th>							
							<th>상태</th>							
							<th>Action</th>
						</tr>
					</thead>

					<tbody>
						<?php foreach ($bt_list as $key => $value): ?>
						<tr>
							<td><?=$value['btid']?></td>
							<td><?=$value['typename']?></td>							
							<?php if($value['status']) :?>
								<td>
								<label>
									<input id="bt_<?=$value['btid']?>" type="checkbox" class="js-switch bt_status" checked /> <span>활성화</span>
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input id="bt_<?=$value['btid']?>" type="checkbox" class="js-switch bt_status" /> <span>비활성화</span>
								</label>								
								</td>
							<?php endif?>


							<td><input type="button" name="view" value="수정" id="<?=$value['btid']?>" class="btn btn-info bt_get"></td>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>



</div>


<div id="btModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">게시판 타입 추가/수정</h4>  
                </div>  
                <form role="form" id="board_type_edit" action="/admin/board_manage/bt_save">
	                <div class="modal-body" id="board_type_detail">  

	                </div>  
	                <div class="modal-footer">  
	                     <button type="submit" class="btn btn-primary">저장 </button>  
	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                     <button type="button" class="btn_bt_delete btn btn-danger" data-dismiss="modal">삭제</button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  


 <div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>게시판 관리 <small> <?=$parking_type_str?></small></h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="board_info_get">게시판 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table id="board_datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>타입</th>
							<th>타이틀</th>
							<th>순서</th>
							<th>상태</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
							<?php foreach ($list as $key => $value): ?>
							<tr>
								<td><?=$value['blid']?></td>
								<td><?=$value['typename']?></td>
								<td><?=$value['title']?></td>
								<td><?=$value['placing']?></td>
								<?php if($value['status']) :?>
									<td>
									<label>
										<input type="checkbox" class="js-switch bl_status" id = "bl_<?=$value['blid']?>" checked /> 활성화
									</label>								
									</td>
								<?php else :?>
									<td>
									<label>
										<input type="checkbox" class="js-switch bl_status"  id = "bl_<?=$value['blid']?>"/> 비활성화
									</label>								
									</td>
								<?php endif?>
								<td><input type="button" name="view" value="수정" id="<?=$value['blid']?>" class="btn btn-info board_info_get"></td>
							</tr>
							<?php endforeach?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

 <div id="user_dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">게시판 정보 추가/수정</h4>  
                </div>  
                <form role="form" id="boardinfo_edit" action="/admin/board_manage/save">
	                <div class="modal-body" id="board_info_detail">  

	                </div>  
	                <div class="modal-footer">  
	                     <button type="submit" class="btn btn-primary">저장 </button>  
   	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
   	                     <button type="button" class="btn btn_board_delete btn-danger" data-dismiss="modal">삭제</button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  
