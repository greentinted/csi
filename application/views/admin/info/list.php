<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>공지사항 관리 </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
            <li><a class="dropdown-item" href="/admin/information/detail">공지사항 추가하기</a>
            </li>              
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>#</th>              
              <th>제목</th>
              <th>구분</th>
              <th>게시기간</th>
              <th>읽음</th>
              <th>작성자</th>
              <th>작성일</th>
              <th>상태</th>
              <th>수정</th>              
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$key+1?></td>
                <td><a href="/home/participatory/detail?bid=<?=$value['bid']?>" style="text-decoration: underline;"><?=$value['title']?></a>
                  <?php if($value['new_icon'] == true):?>
                  <span class="badge bg-red">NEW!!</span>
                  <?php endif;?></td>
                <td><?=$value['optitle']?></a></td> 
                <td><?=$value['create_date']?></a></td>             
                <td><?=$value['view_count']?></td>
                <td><?=$value['name']?></td>
                <td><?=$value['create_date']?></td>
                <td><input type="checkbox" class="js-switch" disabled="disabled" checked="checked"/></td>
                  <td><a class="btn btn-warning" href="/admin/board/write?bid=<?=$value['bid']?>">수정</a></td>
              </tr>
            <?php endforeach?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

