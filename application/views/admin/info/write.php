<style type="text/css">
 fieldset 
	{
		border: 1px solid #ddd !important;
		margin: 0;
		xmin-width: 0;
		padding: 10px;       
		position: relative;
		border-radius:4px;
		background-color:#f5f5f5;
		padding-left:10px!important;
	}	
	
		legend
		{
			font-size:22px;
			font-weight:bold;
			margin-bottom: 0px; 
			width: 35%; 
			border: 1px solid #ddd;
			border-radius: 4px; 
			padding: 5px 5px 5px 10px; 
			background-color: #ffffff;
			text-align: center;
		}	
</style>


<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">

				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br/>
				<form id="content_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/admin/board/save" enctype="multipart/form-data">
					<input type="hidden" name="bid" value="<?=$row['bid']?>">
					<input type="hidden" name="writer_aid" value="<?=$row['writer_aid']?>">
					<input type="hidden" name="admin_aid" value="<?=$my_aid?>">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">제목 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$row['title']?>" readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">분류 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="optitle" name="optitle" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$row['optitle']?>"  readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">작성자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="writer" name="writer" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$row['name']?>"  readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">상태 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
								<select name="board_select_status" id="board_select_status"class="select2_single form-control" tabindex="1">
									<?php foreach ($board_status as $key => $value): ?>
										<option name="board_status" value="<?=$value['ps_code']?>" <?=return_select($value['ps_code'], $row['ps_code'])?>><?=$value['title']?></option>
									<?php endforeach ?>
								</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">성과 공유
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
								<select name="share_select_status" id="share_select_status"class="select2_single form-control">
									<option name="share_status" value="0" <?=return_select($row['share'], 0)?>>공유안함</option>
									<option name="share_status" value="1" <?=return_select($row['share'], 1)?>>공유</option>


								tabindex="1">
								</select>

						</div>
					</div>



					<?php if(!$is_operator):?>					

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						</label>
						<label class="control-label" ><font color="red"> ※ 담당자 지정은 추가 / 삭제만 가능하며 수정 즉시 서버에 저장됩니다. 착오 없으시길 바랍니다.</font></label>
					</div>

					<fieldset class="control-label">
					<legend>담당자 지정</legend>


	                <?php if(isset($row['operators'])):?>
	                  <?php foreach ($row['operators'] as $key => $value) :?>


						<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 민원처리담당자_<?=$key+1?>
						</label>

							<div class="col-md-3 col-sm-3 col-xs-12">
								<select id="operator_select_aid"class="select2_single form-control" tabindex="1">
									<?php foreach ($account_list as $key1 => $value1): ?>
										<option name="operator_<?=$key1?>" value="<?=$value1['aid']?>" <?=return_select($value1['aid'], $value['operator_aid'])?>><?=$value1['name']?></option>
									<?php endforeach ?>
								</select>
							</div>
							<div class="col-md-3 col-sm-3 col-xs-12">
								<a href="/admin/board/operator_delete?bid=<?=$row['bid']?>&brid=<?=$value['brid']?>" class="btn btn-danger">삭제</a>
							</div>
						</div>

						<?php endforeach ?>
		                <?php endif;?>
		                <div class="">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 민원처리 담당자 추가
						</label>

						<div class="col-md-3 col-sm-3 col-xs-12">	

						<input type="button" name="view" value="ADD" id="<?=$row['bid']?>" class="btn btn-info add_operator">												
		               	</div>
		               	</div>

					</fieldset>	
					
					<?php endif;?>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 내용 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">

			            <textarea id="summernote" name="content"><?=$row['description']?></textarea>

						</div>
					</div>



					
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">			
							<button class="btn btn-primary" type="reset">리셋</button>			
							<button type="submit" id="" class="btn btn-success">저장</button>
						
							
						</div>
					</div>

					<div class="ln_solid"></div>

				</form>
			</div>
		</div>
	</div>
</div>

 <div id="operator_dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">민원처리 담당자 추가</h4>  
                </div>  
                <form role="form" id="board_operator_edit" action="/admin/board/operator_save">
	                <div class="modal-body" id="board_add_operator">  
	                </div>
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

<!-- <script type="text/javascript">

	   $( "#board_add_operator" ).change(function() {

      var selelct_opt = $(this).val();

      console.log('selelct_opt : ' , selelct_opt);      
      $.ajax({
            url:"/admin/account/getfacilities",
            method:"post",
            data:{dcode:selelct_opt},
            success:function(data) {

                 // console.log('data' , data);
                $('#select_facilities').empty();

                var list = data.facilitylist;
                // console.log('list :' , list);
                for(var count = 0; count < list.length; count++){     

                    var option = $("<option name='select_facilities' value="+list[count].fc_id+">"+list[count].title+"</option>");
                      console.log('option : ' , option);
                    $('#select_facilities').append(option);
                }
            }
      });

    });   
    </script> -->

<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column