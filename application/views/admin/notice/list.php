<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>공지사항 리스트</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a class="btn btn-warning" href="/admin/notice/write">공지사항 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="datatable-buttons" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>NO</th>
							<th>타이틀</th>
							<th>작성자</th>
							<th>작성일</th>
							<th>New</th>
							<th>상태</th>
							<th>읽음</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['id']?></td>
							<td><?=$value['create_date']?></td>
							<td><?=$value['is_new']?></td>
							<?php if($value['status']) :?>
								<td>
								<label>
									<input type="checkbox" id="nt_<?=$value['ntid']?>" class="js-switch nt_status" checked /> 활성화
								</label>								
								</td>
							<?php else :?>
								<td>
								<label>
									<input type="checkbox" id="nt_<?=$value['ntid']?>"  class="js-switch nt_status" /> 비활성화
								</label>								
								</td>
							<?php endif?>

							<td><?=$value['view_count']?></td>


							<td><a class="btn btn-info" href="/admin/notice/write?ntid=<?=$value['ntid']?>">수정</a></td>	
					
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

