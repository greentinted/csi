<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2><?=$mtitle?> <small>관리자 기능</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="notice_form" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/admin/notice/save" enctype="multipart/form-data">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">아이디 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="id" id="ntid" name="ntid" required="required" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$ntid?>" readonly>
							<span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">작성자 ID<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$aid?>" readonly>
							<span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>





					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 제목 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="input" id="title" name="title" required="required" class="form-control col-md-7 col-xs-12" placeholder="근무지 추첨이 있습니다. " value="<?=$title?>">
							<span class="fa fa-envelope form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> 활성화 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="status" name="status" required="required" class="form-control col-md-7 col-xs-12" placeholder="1:  활성화  , 0 : 비활성화 " value="<?=$status?>">
							<span class="fa fa-heart form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">NEW ICON<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="is_new" name="is_new" required="required" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$is_new?>">
						<span class="fa fa-at form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">첨부파일
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" id="file" name="upload_file" class="form-control col-md-7 col-xs-12" placeholder="" ><?=$attch_file_dir?>
							<span class="fa fa-file form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>
					
					
					<div class="form-group">

						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">본문<span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">

			            <textarea id="summernote" name="body"><?=$description?></textarea>
						
						</div>

					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="button" id="notice-save-btn" class="btn btn-success">저장</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>




<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column