<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title ">
      <h2>코로나 현황 관리 <small>수정·생성 시 <span style="color:green">상태</span>는 기본으로 <span style="color:red">활성화</span> 됩니다.</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
            <li><a class="dropdown-item" href="/admin/covid19/detail">시설별 코로나현황 추가하기</a>
            </li>              
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <table class="table table-striped">
          <thead>
            <tr>
              <!-- <th>#</th> -->
              <th>소속부서</th>
              <th>시설명</th>
              <th>휴관시작기간</th>
              <th>휴관종료기간</th>
              <th>문의처</th>
              <th>코로나 현황</th>
              <th>방역활동</th>
              <th>비고</th>
              <th>갱신시각</th>
              <th>활성화</th>
              <th>수정하기</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
            <tr>
             <!--  <td><?=$key+1?></td>     -->  
              <td><?=$value['dcode']?></td>        
              <td><?=$value['name']?></td>
              <td><?=$value['date_from']?></td>
              <td><?=$value['date_to']?></td>
              <td><?=$value['contact']?></td>
              <td><?=$value['description']?></td>
              <td><?=$value['action']?></td>
              <td><?=$value['etc']?></td>
              <td><?=$value['mod_date']?></td>
              <td>
                  <?php if($value['status'] == 1):?>
                    <input type="checkbox" class="js-switch" disabled="disabled" checked="checked"/>
                  <?php else:?>
                    <input type="checkbox" class="js-switch" disabled="disabled"/>
                  <?php endif?>

              </td>
              <td><a href="/admin/covid19/detail?ssid=<?=$value['cid']?>" class="btn btn-primary">수정하기</a></td>
           
            </tr>
            <?php endforeach?>    
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

