<div class="clearfix"></div>
<div class="row">
  <div class="col-md-12 col-sm-12 ">
    <div class="x_panel">
      <div class="x_title">

        <h2><?=$mtitle?> <small>성남도시개발공사 코로나현황 정보를 입력하세요.</small></h2>

          <ul class="nav navbar-right panel_toolbox">
            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
            </li>
            <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
          <ul class="dropdown-menu" role="menu">
  <!--           <li><a class="dropdown-item" href="#">Settings 1</a>
            </li>
            <li><a class="dropdown-item" href="#">Settings 2</a>
            </li> -->
          </ul>
            </li>
            <li><a class="close-link"><i class="fa fa-close"></i></a>
            </li>
          </ul>
          <div class="clearfix"></div>
          </div>
          <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" action="save">

<!--           <div class="form-group row">
            <label class="col-form-label col-md-3 col-sm-3 label-align ">#</label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                <input type="text" class="form-control" readonly="readonly" placeholder="Read-Only Input">
              </div>
          </div>
 -->

          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">번호
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" readonly="" placeholder="자동으로 입력됩니다." name="cid" value="<?=$cid?>">
                    <span class="fa fa-list-ol form-control-feedback left" aria-hidden="true"></span> 
              </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">부서선택 <span class="required">*</span>
              <br/><span style="color: blue">코로나현황에 있는 4개 부서 이외에는 집계 안됨</span>
            </label> 
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
              <select name="dcode" id="select_division" class="select_dcode form-control has-feedback-left" tabindex="-1" required="required">
                <?php foreach ($division_list as $key => $value): ?>
                <option value="<?=$value['dcode']?>"<?=return_select($dcode, $value['dcode'])?>><?=$value['title']?></option>
                <?php endforeach?>                
              </select>
              <span class="fa fa-building-o form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">시설명 <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" required="required" name="name" value="<?=$name?>"> 
                    <span class="fa fa-pencil-square-o form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">휴관시작날짜  <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" required="required" name="start_date" placeholder="2020-05-01" value="<?=$start_date?>">
              <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>

                    <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">휴관종료날짜  <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" required="required" name="end_date" value="<?=$end_date?>">
              <span class="fa fa-calendar form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>



          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">문의처  <span class="required">*</span>
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" required="required" name="contact" value="<?=$contact?>">
              <span class="fa fa-phone form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>


          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">코로나 현황(확인자 인원)  <span class="required">*</span> <br/><span style="color: blue">숫자만 표기</span>
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" required="required" name="description" value="<?=$description?>">
                    <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>


          <div class="item form-group">
              <label class="col-md-3 col-sm-3 label-align" for="first-name">방역활동  <span class="required">*</span>
              </label>
                <div class="col-md-6 col-sm-6 ">
                <textarea id="message" required="required" class="form-control" name="action" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="Come on! You need to enter at least a 20 caracters long comment.." data-parsley-validation-threshold="10"><?=$action?></textarea>
                </div>
          </div>

          <div class="form-group row">
            <label class="col-md-3 col-sm-3 label-align" for="last-name">비고
            </label>
              <div class="col-md-6 col-sm-6 xdisplay_inputx form-group row has-feedback">
                    <input type="text" class="form-control has-feedback-left" name="etc" value="<?=$etc?>">
                    <span class="fa fa-check form-control-feedback left" aria-hidden="true"></span>
              </div>
          </div>


          <div class="item form-group">
          <label class="col-md-3 col-sm-3 label-align">활성화</label>
          <div class="col-md-6 col-sm-6 " id="switchText" >
          <div id="status" class="btn-group" data-toggle="buttons">
          <input type="checkbox" class="js-switch" id="toggle_status" name="status" checked />
          </div>
          </div>
          </div>

<!-- 시작날짜가 종료날짜보다 늦을경우 -->

<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
<script type="text/javascript">    
$(document).ready(function() {
  $('.btn-success').click(function(){


         
        var startDate = $( "input[name='start_date']" ).val(); //2017-12-10
        var startDateArr = startDate.split('/');
         
        var endDate = $( "input[name='end_date']" ).val(); //2017-12-09
        var endDateArr = endDate.split('/');
                 
        var startDateCompare = new Date(startDateArr[0], parseInt(startDateArr[1])-1, startDateArr[2]);
        var endDateCompare = new Date(endDateArr[0], parseInt(endDateArr[1])-1, endDateArr[2]);
         
          if(startDateCompare.getTime() > endDateCompare.getTime()) {  
              $( "input[name='start_date']" ).css("color","red");
              alert("시작날짜와 종료날짜를 확인해 주세요.");
              return false;
          }
      });
});

</script>

        <div class="ln_solid"></div>
        <div class="item form-group">
        <div class="col-md-6 col-sm-6 offset-md-3">
        <button class="btn btn-primary" type="button">취소</button>
        <button class="btn btn-primary" type="reset">리셋</button>
        <button class="btn btn-success" type="submit">저장</button>
        </div>
        </div>
        </form>
      </div>
    </div>
  </div>
</div>

