<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2> <small>관리자 기능</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/admin/account/save" enctype="multipart/form-data">

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">고유번호 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="고유번호는 자동으로 입력됩니다." value="<?=$aid?>" readonly>
							<span class="fa fa-list form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">이메일 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="email" name="email" required="required" class="form-control col-md-7 col-xs-12" placeholder="your@isdc.co.kr" value="<?=$email?>">
							<span class="fa fa-envelope-o form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_img"> 프로필 이미지 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" id="profile_img" name="profile_img"  class="form-control col-md-7 col-xs-12">
							<!-- 이미지 소스 필요.. ( 수정 작업 등) -->
							<!-- <img src="<?=$profile_img?>" width="100"> -->
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">계정(실명) <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="id" name="name" required="required" class="form-control col-md-7 col-xs-12" placeholder="실명  ex)김종훈" value="<?=$name?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
							<span id="result_id_msg"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">주소 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="address" name="address" required="required" class="form-control col-md-7 col-xs-12" placeholder="ex)경기도 성남시" value="<?=$address?>">
							<span class="fa fa-map-marker form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">전화번호 <span class="required">*</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="id" id="contact_number" name="contact_number" required="required" class="form-control col-md-7 col-xs-12" placeholder="ex)010-0000-1234" value="<?=$contact_number?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
  
						<div class="col-md-12 col-sm-12 col-xs-12">
							<label class="control-label col-sm-3 col-xs-12" for="birthday"> 생년월일 <span class="required">* </span>
							</label>
							<div class='col-sm-6 col-xs-12 input-group date form_datetime'>
								<input type="text" id="birthday" name="birthday" required="required" class="form-control" placeholder="860312" id="datetimepicker1" ui-jp="datetimepicker" value="<?=$birth?>">

								<span class="input-group-addon">
	                        		<span class="fa fa-calendar"></span>
	                    		</span>							
							</div>
						</div>
					</div>	

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">성별<span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="gender" id="gender" class="select2_single form-control" required="required">
						<!-- 	<option name="gender" value="<?=$gender?>"><?=$gender?></option> -->
								<?php foreach ($gender_list as $key => $value): ?>
									<option value="<?=$value?>"<?=return_select($gender, $value);
								?>><?=$key?></option>
								<?php endforeach ?>

							
							</select>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">관리자타입 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="admin_type" id="" class="admintype_change form-control" required="required">
								<?php foreach ($admin_type as $key => $value): ?>
									<option value="<?=$value['admin_type']?>"<?=return_select($account_type, $value['admin_type'])?>><?=$value['admin_type']?></option>
								<?php endforeach ?>    						

							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">유저타입 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="user_type" id="user_type" class="select2_single form-control" required="required">
								<?php foreach ($user_type as $key => $value): ?>
			<option value="<?=$value['user_type']?>"<?=return_select($account_type, $value['user_type'])?>>
				<?=$value['user_type']?></option>
								<?php endforeach ?>    						

							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						부서 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="division" id="select_division" class="select_dcode form-control" tabindex="-1" required="required">
								<?php foreach ($division as $key => $value): ?>
	<option value="<?=$value['dcode']?>"<?=return_select($title, $value['title'])?>><?=$value['title']?></option>
								<?php endforeach?>    						
							</select>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">담당시설물 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="facility" id="select_facilities" class="select2_single form-control" tabindex="-1" required="required">
							<?php if(isset($fc_id)):?>
	<option name="fut_list" value="<?=$fc_id?>"><?=$facility?></option>
							<?php else:?>
	<option name="fut_list" value="">부서 선택 후 담당시설을 선택하세요.</option>							<?php endif?> 
							</select>
						</div>
					</div>

<!----- 테스트  -->
<script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>
 
<!-- 중복아이디 체크 -->

<script type="text/javascript">
$(document).ready(function(){
    $('#id').keyup(function(){
    	if ( $('#aid').val() == 0){
        var id = $(this).val();
        //console.log("아이디:",id);
            // ajax 실행
            $.ajax({
                type : "POST",
                url : "/admin/account/check_duplicated_id",
                data:
                { id: id },
                success : function(result) {
                    //console.log("return:",result);
                    if (result == 1) {
                        $("#result_id_msg").html("사용 가능한 아이디(이름)입니다.");
                    } else {
                        $("#result_id_msg").html("아이디(이름)가 존재합니다.");
                    }
                }
            }); // end ajax
        } //end if
        else{
        	// 계정 수정할 경우 
        	// console.log($('#id').val());
        }
    }); // end keyup
});
</script>

<!-- 					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">테스트 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
-->
<!--
<select class=" select_dcode" name="select_dcode" id="select_dcode">
    <option value="BB">본부</option>
    <option value="TC">탄천</option>
    <option value="SN">성남</option>
    <option value="SJ">수정</option>
</select>
<select class="new_facilities" name="new_facilities" id="new_facilities">
    <option value="">선택</option>
</select> -->


<script type="text/javascript">

	   $( ".select_dcode" ).change(function() {

      var selelct_opt = $(this).val();

      console.log('selelct_opt : ' , selelct_opt);      
      $.ajax({
            url:"/admin/account/getfacilities",
            method:"post",
            data:{dcode:selelct_opt},
            success:function(data) {

                 // console.log('data' , data);
                $('#select_facilities').empty();

                var list = data.facilitylist;
                // console.log('list :' , list);
                for(var count = 0; count < list.length; count++){     

                    var option = $("<option name='select_facilities' value="+list[count].fc_id+">"+list[count].title+"</option>");
                      console.log('option : ' , option);
                    $('#select_facilities').append(option);
                }
            }
      });

    });   

$( ".admintype_change" ).change(function() {

var admin = ["admin"];
var guest = ["guest"];
var user = ["cjg","ceg"];

var selectItem = $(".admintype_change").val();
 
var changeItem;
  
if(selectItem == "admin"){
  changeItem = admin;
}
else if(selectItem == "operator" || selectItem == "user"){
  changeItem = user;

}
else if(selectItem == "guest"){
  changeItem =  guest;
}
 
$('#user_type').empty(); 
for(var count = 0; count < changeItem.length; count++){                
                var option = $("<option>"+changeItem[count]+"</option>");
                $('#user_type').append(option);
            }
 
});
</script>




<!-- <script type="text/javascript">
	function CheckDivision(e){
		console.log(e);
//     var $target = $("select[name='ctg_sub_name']");
     
//     $target.empty();
//     if(sParam == ""){
//         $target.append("<option value="">선택</option>");
//         return;
//     }
 
//     $.ajax({
//         type: "POST",
//         url: "/admin/account/write",
//         async: false,
//         data:{ dcode : sParam },
//         dataType: "json",
//         success: function(data) {
//             if(data.length == 0){
//                 $target.append("<option value="">선택</option>");
//             }else{
//                 $(data).each(function(i){
//                     $target.append("<option value="">"+  +"</option>");
//                 });
//             }
//         }, error:function(xhr){
//             console.log(xhr.responseText);
//             alert("지금은 시스템 사정으로 요청하신 작업을 처리할 수 없습니다.\n잠시 후 다시 이용해주세요.");
//             return;
//         }
//     });
 }
</script>
 -->

<!-- 					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">주차처 타입 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="select_parktype" id="_select_parktype" class="select2_single form-control" tabindex="-1" required="required">
								<option>Please Input</option>
								<?php foreach ($parktype_list as $key => $value): ?>
 									<?php if($key == Const_library:: SUPERTYPE_PARKINGLOT && $my_account['user_type'] != 'super_admin'):?>
										<?php continue;?>
									<?php else:?>
										<option value="<?=$key?>"<?=return_select($key, $ptid)?>><?=$value?></option>		
									<?php endif?>

								<?php endforeach?>    						
							</select>
						</div>
					</div> -->

<!-- 					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">유저 타입 <span class="required">*</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="select_usertype" id="_select_usertype" class="select2_single form-control" tabindex="-1" required="required">
								<option value="operator">Please Input</option>
								<?php if($my_account['user_type'] == 'super_admin'):?>									
									<option value="super_admin"<?=return_select("super_admin", $user_type)?>>슈퍼 관리자</option>									
									<option value="admin"<?=return_select("admin", $user_type)?>>관리자</option>	
									<option value="operator"<?=return_select("operator", $user_type)?>>일일근무자</option>	
								<?php elseif($my_account['user_type'] == 'admin'):?>
									<option value="admin"<?=return_select("admin", $user_type)?>>관리자</option>	
									<option value="operator"<?=return_select("operator", $user_type)?>>일일근무자</option>										
								<?php endif?>

							</select>
						</div>
					</div> -->

					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="submit" class="btn btn-success">제출</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column