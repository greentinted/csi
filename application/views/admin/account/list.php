<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>계정리스트<small>00</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a class="btn btn-warning" href="/admin/account/write">계정 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>aid</th>
							<th>이메일</th>
							<th>계정</th>
							<th>유저타입</th>
							<th>주소</th>
							<th>휴대폰</th>
							<!-- <th>부서명</th> -->
							<th>시설명</th>
							<th>가입일</th>
							<th>계정수정</th>
							<th>비번초기화</th>
							<th>계정삭제</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['aid']?></td>

							<td><a class="btn btn-warning" href="#"><?=$value['email']?></a></td>
							<!-- <td><?=$value['id']?></td> -->
							<td><?=$value['name']?></td>
							<td><a class="btn btn-info" href="#"><?=$value['user_type']?></a></td>	
							<td><?=$value['address1']?></td>
							<!-- <td><?=$value['contact']?></td> -->
							
							<td><?=$value['contact_number']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['create_date']?></td>

							<td><a class="btn btn-info" href="/admin/account/write?aid=<?=$value['aid']?>">계정수정</a></td>	
							<td><a href="/admin/account/init_passwd?aid=<?=$value['aid']?>" class="btn btn-success" onclick="return confirm('초기화 하시겠습니까?')">비번초기화</a></td>
							
							<td><a href="/admin/account/delete_account?aid=<?=$value['aid']?>" class="btn btn-danger" onclick="return confirm('계정을 삭제 하시겠습니까?')">계정삭제</a></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="init_passwdModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">정말로 초기화 하시겠습니까?</h4>  
                </div>  
                <form role="form" action="/admin/account/init_passwd">
	                <div class="modal-body" id="init_passwd_modal">  
	                	
	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

                     	<button type="submit" class="btn btn-danger">초기화 </button>  

	                </div>  
            </form>
           </div>  
      </div> 
</div> 



