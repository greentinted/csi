<div class="row">
	<input type="hidden" id="workhistory_aid" name="workhistory_aid" value="<?=$aid?>">

	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무자 기록 <small></small></h2>


				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a class="" href="javascript:commute_record(<?=$aid?>);">출퇴근 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>


			<div class="x_content">

				<!-- <div style="border-bottom: 1px solid #E6E9ED"> -->
				<form id="workreport_form" class="form-horizontal form-label-left" method="GET" id="workreport" action="/admin/account/working_history">
						<label class="col-sm-4 control-label">조회 월</label>

						<div class="col-sm-4">
							<div class="input-group">
								<span class="input-group-btn">
									<button id="prev_btn_history" type="button" class="btn btn-primary">◀</button>
								</span>
								<input id="viewing_month_history" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
								<span class="input-group-btn">
									<button id="next_btn_history" type="button" class="btn btn-primary">▶</button>
								</span>

							</div>
						</div>

				</form>


				<table id="working_history_datatable" class="table table-striped table-bordered working_history_datatable">
					<thead>
						<tr>
							<th>#</th>
							<th>날짜</th>
							<th>성명</th>
							<th>지역</th>
							<th>근무지</th>
							<th>근로형태</th>
							<th>근무</th>
							<th>출근시간</th>
							<th>퇴근시간</th>
							<th>수정</th>
						</tr>
					</thead>


					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['wrid']?></td>
							<td><?=$value['rdate']?></td>
							<td><?=$account['id']?></td>
							<?php if(isset($value['wp_info']['did_title'])):?>
								<td><?=$value['wp_info']['did_title']?></td>
							<?php else:?>
								<td></td>							
							<?php endif?>
							<?php if(isset($value['wp_info']['title'])):?>
								<td><?=$value['wp_info']['title']?></td>
							<?php else:?>
								<td></td>							
							<?php endif?>							
							<?php if(isset($value['ws_info']['title'])):?>
								<td><?=$value['ws_info']['title']?></td>
							<?php else:?>
								<td></td>							
							<?php endif?>

							<?php if(isset($value['wt_info']['title'])):?>
								<td><?=$value['wt_info']['title']?></td>
							<?php else:?>
								<td></td>							
							<?php endif?>
							<td><?=$value['work_start_time']?></td>
							<td><?=$value['work_end_time']?></td>
							<td><button id="<?=$value['wrid']?>" type="button" class="btn btn-primary btn_get_history_info">수정</button></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

 <div id="history_modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">근무상황 수정</h4>  
                </div> 
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" id="form_save_working_history" name="form_save_working_history">
                    <input type="hidden" id="etc_table_initial" name="etc_table_initial" readonly="true">
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">#</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_crid" name="history_modal_crid" readonly="true">
                        </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">날짜</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_rdate" name="history_modal_rdate" readonly="true">
                        </div>
                      </div>
    				<div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">성명</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_id" name="history_modal_id" readonly="true">
                        </div>
                      </div>

    				<div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">aid</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_aid" name="history_modal_aid" readonly="true">
                        </div>
                      </div>

                     <div class="form-group" style="width: 85%">
                       <label class="control-label col-md-3 col-sm-3 col-xs-3">지역</label>
                       <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" id="history_modal_did_title" name="history_modal_did_title" readonly="true">
                        </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                      	<label class="control-label col-md-3 col-sm-3 col-xs-3">근무지</label>
	                        <div class="col-md-9 col-sm-9 col-xs-9">
	                            <input type="text" class="form-control" id="history_modal_wp_title" name="history_modal_wp_title" readonly="true">
                            </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">근로형태</label>
	                        <div class="col-md-9 col-sm-9 col-xs-9">
                            <input type="text" class="form-control" id="history_modal_ws_title" name="history_modal_ws_title" readonly="true">
                            </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">근무</label>
	                        <div class="col-md-9 col-sm-9 col-xs-9">
	                            <input type="text" class="form-control" id="history_modal_wt_title" name="history_modal_wt_title" readonly="true">
                            </div>
                      </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">출근시간</label>
						<div class='input-group' >
							<input type='text' class="form-control"  id='history_modal_work_start_time' name='history_modal_work_start_time' placeholder="2019-01-01 12:00:00" />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					  </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">퇴근시간</label>
						<div class='input-group' >
						<input type='text' class="form-control"  id='history_modal_work_end_time' name='history_modal_work_end_time' placeholder="2019-01-01 12:00:00" />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
							 </span>
						</div>
                      </div>
                      <div class="ln_solid"></div>
                  </div>
	                <div class="modal-footer">  
	                	<button type="button" class="btn btn-primary" id="btn_save_working_history">저장 </button>  
	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                </div>  
            </form>
           </div>  
      </div>  
 </div>  


 <div id="history_modal_new" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">출퇴근 기록 추가</h4>  
                </div> 
                <div class="x_content">
                    <br>
                    <form class="form-horizontal form-label-left" id="form_save_working_history" name="form_save_working_history">
                    <input type="hidden" id="etc_table_initial" name="etc_table_initial" readonly="true">
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">#</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_new_crid" name="history_modal_new_crid" readonly="true">
                        </div>
                      </div>


    				<div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">성명</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_new_id" name="history_modal_new_id" readonly="true" >
                        </div>
                      </div>

                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">날짜</label>
                        <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="date" class="form-control" id="history_modal_new_rdate" name="history_modal_new_rdate" placeholder="2019-09-01">
                        </div>
                      </div>

    				<div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">aid</label>
                       <div class="col-md-9 col-sm-9 col-xs-9">
                          <input type="text" class="form-control" id="history_modal_new_aid" name="history_modal_new_aid" readonly="true">
                        </div>

                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">출근시간</label>
						<div class='input-group' >
							<input type='text' class="form-control"  id='history_modal_new_work_start_time' name='history_modal_new_work_start_time' placeholder="2019-01-01 12:00:00" />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					  </div>
                      <div class="form-group" style="width: 85%">
                        <label class="control-label col-md-3 col-sm-3 col-xs-3">퇴근시간</label>
						<div class='input-group' >
						<input type='text' class="form-control"  id='history_modal_new_work_end_time' name='history_modal_new_work_end_time' placeholder="2019-01-01 12:00:00" />
							<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
							 </span>
						</div>
                    <div class="ln_solid"></div>

 
	                <div class="modal-footer">  
	                	<button type="button" class="btn btn-primary" id="btn_save_working_history_new">저장 </button>  
	                    <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  
	                </div>  
            </form>
           </div>  
      </div>  
</div>  


<script>
	function commute_record(aid){
		// console.log('ddfsafsd :' , aid);

        $.ajax({  
                url:"/admin/account/get_info",  
                method:"post",  
                data:{aid:aid},  
                success:function(data){
                    //console.log(data.account); 
                    $('#history_modal_new_crid').val(0);
                    $('#history_modal_new_rdate').val('');
                    $('#history_modal_new_id').val(data.account.id);
                    $('#history_modal_new_aid').val(data.account.aid);

                    $('#history_modal_work_new_start_time').val('');
                    $('#history_modal_work_new_end_time').val('');

                    $('#history_modal_new').modal("show"); 
  
                }  
        });  
	}
</script>