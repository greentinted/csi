<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>인식색 리스트</h2>

				<ul class="nav navbar-right panel_toolbox">					
					<!-- <li><a class="btn btn-success" href="/column/write" style="width:66px; height:38px;"> New </a></li> -->
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="recognize_color_get">인식색 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<p class="text-muted font-13 m-b-30">
				</p>
				<table id="datatable" class="table table-striped table-bordered">
					<thead>
						<tr>
							<th>아이디</th>
							<th>이름</th>
							<th>인식색</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['rcid']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['color']?>
								<div class="progress">
    								<div class="progress-bar" role="progressbar" aria-valuenow="30" aria-valuemin="0" aria-valuemax="100" style="width:100%; background-color:<?=$value['color']?> !important;">
    								</div>
								</div>
							</td>

							<td><input type="button" name="view" value="EDIT" id="<?=$value['rcid']?>" class="btn btn-primary recognize_color_get"></td>

						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="dataModal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">인식색 추가/수정</h4>  
                </div>  
                <form role="form" id="recognize_color_edit" action="/admin/recognize_color/save">
	                <div class="modal-body" id="recognize_color_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button>  

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

