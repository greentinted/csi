<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>업로드 테스트  <small>관리자 기능</small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">Settings 1</a>
							</li>
							<li><a href="#">Settings 2</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="/parking_portal/example/upload_test" enctype="multipart/form-data">

					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="profile_img"> 프로필 이미지 </label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="file" id="profile_img" name="profile_img" class="form-control col-md-7 col-xs-12">
							<!-- 이미지 소스 필요.. ( 수정 작업 등) -->
						</div>
					</div>

					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">Reset</button>
							<button type="submit" class="btn btn-success">Submit</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">
	$(function(){
		$('.form_datetime').datetimepicker();
	});
</script>						



<!-- /First Section one Column -->
<!-- Second Section 2 columns -->
<!-- /Third Section One Column



	<div class="form-group row">    <label for="start_time"  class="col-sm-2  text-right form-control-label">근무 시작 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="start_time" id="start_time" placeholder="14:00" value="">    </div></div><div class="form-group row">    <label for="end_time"  class="col-sm-2  text-right form-control-label">근무 종료 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="end_time" id="end_time" placeholder="22:00" value="">    </div></div><div class="form-group row">    <label for="working_time"  class="col-sm-2  text-right form-control-label">근무 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="working_time" id="working_time" placeholder="8" value="">    </div></div><div class="form-group row">    <label for="overtime"  class="col-sm-2  text-right form-control-label">시간외 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="overtime" id="overtime" placeholder="1" value="">    </div></div><div class="form-group row">    <label for="nighttime"  class="col-sm-2  text-right form-control-label">야간 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="nighttime" id="nighttime" placeholder="2" value="">    </div></div><div class="form-group row">    <label for="holidaytime"  class="col-sm-2  text-right form-control-label">휴일 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="holidaytime" id="holidaytime" placeholder="5" value="">    </div></div><div class="form-group row">    <label for="start_time"  class="col-sm-2  text-right form-control-label">근무 시작 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="start_time" id="start_time" placeholder="14:00" value="">    </div></div><div class="form-group row">    <label for="end_time"  class="col-sm-2  text-right form-control-label">근무 종료 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="end_time" id="end_time" placeholder="22:00" value="">    </div></div><div class="form-group row">    <label for="working_time"  class="col-sm-2  text-right form-control-label">근무 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="working_time" id="working_time" placeholder="8" value="">    </div></div><div class="form-group row">    <label for="overtime"  class="col-sm-2  text-right form-control-label">시간외 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="overtime" id="overtime" placeholder="1" value="">    </div></div><div class="form-group row">    <label for="nighttime"  class="col-sm-2  text-right form-control-label">야간 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="nighttime" id="nighttime" placeholder="2" value="">    </div></div><div class="form-group row">    <label for="holidaytime"  class="col-sm-2  text-right form-control-label">휴일 시간</label>    <div class="col-sm-10">        <input type="text"  class="form-control" name="holidaytime" id="holidaytime" placeholder="5" value="">    </div></div>