<style type="text/css">
	body{
		font-size: 14px;
	}
}
</style>

<div class="row">	

	<div class="x_content">

	<label class="col-sm-4 control-label"></label>

	<div class="col-sm-4">
		<div class="input-group">
			<span class="input-group-btn">
				<button id="prev_db_btn" type="button" class="btn btn-primary">◀</button>
			</span>
			<input id="db_viewing_day" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
			<span class="input-group-btn">
				<button id="next_db_btn" type="button" class="btn btn-primary">▶</button>
			</span>
		</div>
	</div>
	</div>

	<div class="col-md-6 col-sm-6 col-xs-12">
		<div class="x_panel tile fixed_height_400">
			<div class="x_title">
				<h2>수익금 요약</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table class="table table-striped">
		          <thead>
		            <tr>
		              <th>#</th>
		              <th>일차건수</th>
		              <th>일차금액</th>
		              <th>#</th>
		              <th>월차건수</th>
		              <th>월차금액</th>

		            </tr>
		          </thead>
         		  <tbody>
		            <tr>
		              <td>일차합계</td>              
		              <td><?=money_format($daily_total)?>(건)</td>              
		              <td><?=money_format($daily_total_amount)?>(원)</td>              
		              <td>월차합계</td>             
		              <td><?=money_format($monthly_total)?>(건)</td>              
		              <td><?=money_format($monthly_total_amount)?>(원)</td>
  	                </tr>

		            <tr>
		              <td>현금</td>              
		              <td><?=money_format($daily_cash)?>(건)</td>              
		              <td><?=money_format($daily_cash_amount)?>(원)</td>

		              <td>현금</td>             
		              <td><?=money_format($monthly_cash)?>(건)</td>              
		              <td><?=money_format($monthly_cash_amount)?>(원)</td>

  	                </tr>

		            <tr>
		              <td>신용카드</td>              
		              <td><?=money_format($daily_card)?>(건)</td>              
		              <td><?=money_format($daily_card_amount)?>(원)</td>
		              <td>신용카드</td>             
		              <td><?=money_format($monthly_card)?>(건)</td>              
		              <td><?=money_format($monthly_card_amount)?>(원)</td>

  	                </tr>

		            <tr>
		              <td>계좌이체</td>              
		              <td><?=money_format($daily_transfer)?>(건)</td>              
		              <td><?=money_format($daily_transfer_amount)?>(원)</td>
		              <td>계좌이체</td>             
		              <td><?=money_format($monthly_transfer)?>(건)</td>              
		              <td><?=money_format($monthly_transfer_amount)?>(원)</td>
  	                </tr>

		            <tr>
		              <td>T-MONEY</td>              
		              <td><?=money_format($daily_tmoney)?>(건)</td>              
		              <td><?=money_format($daily_tmoney_amount)?>(원)</td>		              <td>웹연장</td>             
		              <td><?=money_format($monthly_web)?>(건)</td>              
		              <td><?=money_format($monthly_web_amount)?>(원)</  	                </tr>

		            <tr>
		              <td>카카오 T</td>              
		              <td><?=money_format($daily_kakaot)?>(건)</td>              
		              <td><?=money_format($daily_kakaot_amount)?>(원)</td>
		              <td>가상계좌</td>             
		              <td><?=money_format($monthly_v_transfer)?>(건)</td>              
		              <td><?=money_format($monthly_v_transfer_amount)?>(원)</td>
  	                </tr>

		            <tr>
		              <td>기타</td>              
		              <td><?=money_format($daily_etc)?>(건)</td>              
		              <td><?=money_format($daily_etc_amount)?>(원)</td>
		              <td>기타</td>             
		              <td><?=money_format($monthly_etc)?>(건)</td>              
		              <td><?=money_format($monthly_etc_amount)?>(원)  	                </tr>

		            <tr>
		              <td>미납</td>              
		              <td><?=money_format($unpaid)?>(건)</td>              
		              <td><?=money_format($unpaid_amount)?>(원)</td>
		              <td></td>             
		              <td></td>               
		              <td></td>              
  	                </tr>

				    </tbody>
		        </table>

			</div>
		</div>
	</div>

	<div class="col-md-6 col-sm-4 col-xs-12">
		<div class="x_panel tile fixed_height_400">
			<div class="x_title">
				<h2>WEEKLY SALES REPORT</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<canvas id="chartJSContainer"></canvas>
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 현황</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<div class="chart_plot_01"></div>
			</div>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>입출차 현황</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>
	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>휴가자 리스트</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>수익금 미보고 리스트</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>출근 미보고 근무자</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>근무 미보고 리스트</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>

	<div class="col-md-4 col-sm-4 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>기타</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#" class="">자세히 보기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
			</div>
		</div>
	</div>


</div>

<script type="text/javascript" src="<?php echo base_url('vendors/jquery/dist/jquery.min.js') ?>"></script>

<script type="text/javascript">
	$(function(){

		var db_viewing_day = $('#db_viewing_day').val();

        $.ajax({  
                url:"/admin/dashboard/getweeklydata",  
                method:"post",  
                data:{day:db_viewing_day},  
                success:function(data){

              	    var options = {
				      type: 'line',
				      data: {
				        labels: data.day_list,
				        datasets: [
				            {
				              label: '# of Daily CreditCard',
				              data: [data.statics_list[0].daily_total_amount, 
				              		data.statics_list[1].daily_total_amount, 
				              		data.statics_list[2].daily_total_amount,
				              		data.statics_list[3].daily_total_amount,
				              		data.statics_list[4].daily_total_amount,
				              		data.statics_list[5].daily_total_amount,
				              		data.statics_list[6].daily_total_amount,],
				            		borderWidth: 1
				            },  
				                {
				                    label: '# of ddddd',
					              	data: [data.statics_list[0].monthly_total_amount, 
				              		data.statics_list[1].monthly_total_amount, 
				              		data.statics_list[2].monthly_total_amount,
				              		data.statics_list[3].monthly_total_amount,
				              		data.statics_list[4].monthly_total_amount,
				              		data.statics_list[5].monthly_total_amount,
				              		data.statics_list[6].monthly_total_amount,],
				            		borderWidth: 1
				                }
				            ]
				      },
				      options: {
				        scales: {
				            yAxes: [{
				            ticks: {
				                reverse: false
				            }
				          }]
				        }
				      }
				    }

				    var ctx = document.getElementById('chartJSContainer').getContext('2d');
				    new Chart(ctx, options);    

                    // console.log('data' , data);
                     // $('#workstyle_detail').html(data);  
                     // $('#wsModal').modal("show");  
                }  
        });  		
	});
</script>						


