<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월 정기 신규 입력<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
				</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<label class="col-sm-4 control-label">조회 날짜</label>

				<div class="col-sm-4">
					<div class="input-group">
						<span class="input-group-btn">
							<button id="prev_nca_btn" type="button" class="btn btn-primary">◀</button>
						</span>
						<input id="nca_viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
						<span class="input-group-btn">
							<button id="next_nca_btn" type="button" class="btn btn-primary">▶</button>
						</span>

					</div>
				</div>

				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>날짜</th>
							<th>차량번호</th>
							<th>금액</th>
							<th>결제일</th>
							<th>감면유형</th>
							<th>주차장</th>
							<th>소유자</th>
							<th>차종</th>
							<th>수정</th>
							
					</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key?></td>
							<td><?=$value['rdate']?></td>
							<td><?=$value['car_no']?></td>
							<td><?=$value['amount']?></td>
							<td><?=$value['pay_date']?></td>
							<td><?=$value['discount_title']?></td>
							<td><?=$value['wpid_title']?></td>
							<td><?=$value['car_owner']?></td>
							<td><?=$value['car_type']?></td>							
				
							<td><a class="btn btn-info" href="/escalate/contractor/new_?n_id=<?=$value['n_id']?>">수정</a></td>	

							<?php /*

							<td><input type="button"  name="view" class="btn btn-danger init_password" id="<?=$value['aid']?>" value="비번초기화"></td>						

							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




