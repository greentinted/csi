<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>민원 처리 </h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action">
          <thead>
            <tr>
              <th>#</th>              
              <th>제목</th>
              <th>구분</th>
              <th>상태</th>
              <th>읽음</th>
              <th>작성자</th>
              <th>민원처리지정자</th>
              <th>민원처리담당자</th>
              <th>작성일</th>
              <th>수정</th>              
            </tr>
          </thead>
          <tbody>
            <?php foreach ($list as $key => $value): ?>
              <tr>
                <td><?=$key+1?></td>
                <td><a href="/home/participatory/detail?bid=<?=$value['bid']?>"><?=$value['title']?>
                  <?php if($value['new_icon'] == true):?>
                  <span class="badge bg-red">NEW!!</span>
                  <?php endif;?></a></td>
                <td><?=$value['optitle']?></a></td>
                <td><?=$value['pstitle']?></a></td>
                <td><?=$value['view_count']?></td>
                <td><?=$value['name']?></td>
                <td><?=$value['admin_name']?></td>                
                <?php if(isset($value['operators'])):?>
                  <td>
                  <?php foreach ($value['operators'] as $key1 => $value1) :?>
                    <button type="button" class="btn btn-round btn-success"><?=$value1['name']?></button>
                    <?php if(($key1+1) % 3 == 0):?>
                      <br>
                    <?php endif?> 

                  <?php endforeach?>
                  </td>
                <?php else:?>
                  <td></td>
                <?php endif;?>

                <td><?=$value['create_date']?></td>
                  <td><a class="btn btn-warning" href="/admin/board/write?bid=<?=$value['bid']?>&operator=<?=$account['aid']?>">수정</a></td>
              </tr>
            <?php endforeach?> 
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>