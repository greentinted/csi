<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>WHITE IP<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">							
							<li><a href="#" class="handle_whiteip">근무지 추가하기</a>
							</li>
						</ul>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<table id="datatable-buttons" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>IP ADDRESS</th>
							<th>타이틀</th>
							<th>description</th>
							<th>status</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['wiid']?></td>
							<td><?=$value['address']?></td>
							<td><?=$value['title']?></td>
							<td><?=$value['description']?></td>
							<td><?=$value['status']?></td>
		
							<td><input type="button"  name="view" class="btn btn-info handle_whiteip" id="<?=$value['wiid']?>" value="수정"></td>						
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div id="white_ip_Modal" class="modal fade">  
      <div class="modal-dialog">  
           <div class="modal-content">  
                <div class="modal-header">  
                     <button type="button" class="close" data-dismiss="modal">&times;</button>  
                     <h4 class="modal-title">whiteip 추가/수정</h4>  
                </div>  
                <form role="form" id="whiteip_edit" action="/admin/whiteip/save">
	                <div class="modal-body" id="whiteip_modal_detail">  

	                </div>  
	                <div class="modal-footer">  

	                     <button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>  

	                     <button type="submit" class="btn btn-primary">저장 </button> 

	                </div>  
            </form>
           </div>  
      </div>  
 </div>  

