<style type="text/css">

html {
    -ms-text-size-adjust: 100%;
    -webkit-text-size-adjust: 100%;
}

div {
    display: block;
}

#contents {
    position: relative;
    z-index: 10;
    padding: 30px 0;
    word-break: keep-all;
}
.tabNav {
    display: none;
    position: relative;
    margin-bottom: 30px;
}
.themeColor, .themeBtn, .tB01, #snb, #gnbNavM, #gnbNav.fixed, #lnbNav h2, .tnb .choiced a, .virtSelect a, .paging span.current, .ss-controls > a, .popupHeader, .calendar td.today .today, .mobileAppWrap .infoList::before, .step-list li::before, .dream_step li dt, .dream_step li::before, .dream_step1 li::before {
    background-color: #6ea209 !important;
}

.tnb {
    border-left: 1px solid #e4e4e4;
}
.tnb li {
    overflow: hidden;
    float: left;
    position: relative;
    margin-top: -1px;
}
li {
    display: list-item;
    text-align: -webkit-match-parent;
}

ol, ul {
    list-style: none;
}
h1, h2, h3, h4, h5, h6 {
    margin: 0;
    padding: 0;
    font-weight: 500;
    line-height: 1.1;
}
h4 {
    display: block;
    margin-block-start: 1.33em;
    margin-block-end: 1.33em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    font-weight: bold;
}
.htitle {
    margin: 30px 0 12px;
    padding-top: 8px;
    font-weight: normal;
    font-size: 1.533em;
    line-height: 1.304;
    color: #333;
    background-position: 0 0;
    background-repeat: no-repeat;
}
.htitle {
    background-image: url(/include/image/jw/common/bul_htitle.png);
}
.htitle:first-child, .htitle:nth-of-type(1), .stitle:first-child, .stitle:nth-of-type(1) {
    margin-top: 0;
}

table {
    display: table;
    border-collapse: separate;
    box-sizing: border-box;
    border-spacing: 2px;
    border-color: grey;
}
table {
    width: 100%;
    border-collapse: separate;
    border-spacing: 0;
    font-size: inherit;
}
.board-view {
    table-layout: fixed;
    border-top: 1px solid #333;
    empty-cells: show;
}
caption {
    display: table-caption;
    text-align: -webkit-center;
}
caption, legend {
    overflow: hidden;
    width: 0;
    height: 0;
    line-height: 0;
    text-indent: -9999em;
}
strong, b {
    font-weight: 600;
}
p {
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
}
colgroup {
    display: table-column-group;
}
col {
    display: table-column;
    
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}
th {
    display: table-cell;
    vertical-align: inherit;
    font-weight: bold;
    text-align: -internal-center;
    width: 15%;
}
th, td {
    font-size: inherit;
    line-height: inherit;
    word-break: normal;
}
.board-view th, .board-view td {
    padding: 10px;
    text-align: left;
    border-bottom: 1px solid #e2e2e2;
}
.board-view th {
    font-weight: normal;
    text-align: center;
    background-color: #f8f8f8;
}
.board-view tr:first-child th, .board-view tr:first-child td {
    border-top: none;
}

td {
    display: table-cell;
    vertical-align: inherit;
}
.board-view th, .board-view td {
    padding: 10px;
    text-align: left;
    border-bottom: 1px solid #e2e2e2;
}
.board-view td {
    text-align: left;
}
.board-view tr:first-child th, .board-view tr:first-child td {
    border-top: none;
}
.btnGroup {
    overflow: hidden;
    position: relative;
    margin: 30px auto;
    text-align: center;
}
.btn {
    display: inline-block;
    padding: 8px 15px;
    color: #fff;
    text-align: center;
    letter-spacing: -1px;
    vertical-align: middle;
    box-sizing: border-box;
    background-color: #555;
}



@media screen and (min-width: 1000px){
#contents {
    min-height: 600px;
}
.btnGroup .btn {
    min-width: 100px;
    padding: 8px 20px;
    color: #fff;
}
}

</style>




<div id="contents" class="contentArea">
                    
                    <!-- 게시글 상세화면 -->
                    <div class="boardWrap">
                        <table class="board-view">
                            <caption><strong>묻고답하기 게시글 상세정보</strong><p>구분, 제목, 작성자, 작성일, 첨부파일 제공 표</p></caption>
                            <colgroup>
                                <col style="width:15%">
                                <col>
                            </colgroup>
                            <tbody>
                                
                                <tr>
                                    <th scope="row">제목</th>
                                    <td>열람실좌석표 출력시</td>
                                </tr>
                                <tr>
                                    <th scope="row">작성자</th>
                                    <td>
                                        박*호
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">작성일</th>
                                    <td>2020-05-20</td>
                                </tr>
                    
                                <tr>
                                    <td colspan="2" class="content"><h4>
                                        
                                        성남의 자랑 중원도서관 <br>답변 감사합니다.<br>다름이 아니오라<br>2020 년 6월 30일 이후 열람실이용시<br>자리배치 건의드립니다.<br>기존의 시스템을 이용시<br>2미터 간격을 유지할 수 있도록<br>열람실좌석표 출력시<br><br>1        2         3        4<br>-----------------------------<br>5        6         7       8<br><br>월요일은 1-6-3-8<br>화요일은 5-2-7-4<br>수요일은 1-6-3-8<br>목요일은 5-2-7-4<br>금요일은 1-6-3-8<br>토요일은 5-2-7-4<br>일요일은 1-6-3-8<br>월요일은 5-2-7-4<br>......<br>혹은 더욱더 강화된 사회적 열람실 자리배치 거리(1-8 또는 5-4) 환영합니다.<br>격일제로 열람실좌석을 이용하게하는것을 건의드립니다.<br>감사합니다.<br>
                                       </h4>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- //게시글 상세화면 -->

                    <h4 class="htitle">리플영역</h4>
                    <!-- 게시글 답변
                    https://www.epeople.go.kr/api/thk/pbsb/PbsbsrpnPrpslDetail.npaid?ideaRegNo=1AC-2001-1000138 -->
                    <div class="boardWrap">
                        <table class="board-view">
                            <caption><strong>묻고답하기 게시글 상세정보</strong><p>답변자, 답변일 제공 표</p></caption>
                            <colgroup>
                                <col style="width:15%">
                                <col>
                            </colgroup>
                            <tbody>
                                
                                        <tr>
                                            <th scope="row">답변자</th>
                                            <td>운영자</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">답변일</th>
                                            <td>2020-05-21 15:46</td>
                                        </tr>
                                        <tr>
                                            
                                            
                                            <td colspan="2" class="content">○ 중원도서관을 이용해 주시고, 또한 열람실 운영과 관련하여 소중한 의견 내주셔서
<br>   감사드립니다.
<br>
<br>○ 열람실 좌석 배치와 관련하여 제안해주신 의견은 충분히 고려하여
<br>   개관 이후 상황에 맞는 열람실 운영이 되도록 노력하겠습니다.
<br>
<br>○ 기타 건의사항이나 문의사항은 평생학습지원팀(☎752-3913, 내선 604번)으로 연락주시면
<br>   친절하게 답변해 드리겠습니다. 감사합니다.</td>
                                        </tr>
                                    
                            </tbody>
                        </table>
                    </div>
                    <!-- //게시글 답변 -->
                    <div class="btnGroup">
                        <a href="#none" id="listBtn" class="btn list">목록</a>
                        
                    </div>

                    <!-- End Of the Real Contents-->
                </div>