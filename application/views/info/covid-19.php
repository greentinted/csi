<style type="text/css">
	
html, body, div, span, applet, object, iframe, h1, h2, h3, h4, h5, h6, p, blockquote, pre, a, abbr, acronym, address, big, cite, code, del, dfn, em, ins, kbd, q, s, samp, small, strike, strong, sub, sup, tt, var, b, u, i, center, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, table, caption, tbody, tfoot, thead, tr, th, td, article, aside, canvas, details, embed, figure, figcaption, footer, header, hgroup, menu, nav, output, ruby, section, summary, time, mark, audio, video {
    margin: 0;
    padding: 0;
    border: 0;
    font-size: 100%;
    font: inherit;
}
.logo img {
	padding-top: 25px;
}
a, a:focus, a:hover {
    text-decoration: none;
}
a, button {
    cursor: pointer;
}
/*@media (min-width: 1180px)
.container > div {
    width: 1180px;
    margin: 0 auto;
    padding: 20px 0;
    padding-bottom: 60px;
}
@media (min-width: 1180px)
.content {
    float: right;
    width: 100%;
}*/

.contact_cover .box_line2:first-child{
	margin-left: 0px;
}

.etc_box .tit {
    display: block;
    width: 98%;
    background-color: #e4f1ff;
    font-size: 30px;
    line-height: 1.25em;
    font-weight: 400;
    color: black;
    text-align: center;
}
.etc_box .tit {
    display: block;
    padding: 7px;
    }

@media (min-width: 1180px){
.p_dp_n {}
    display: none;
}

 [class^="s_listin_"] {
    font-size: 14px;
    line-height: 22px;
    margin-top: 8px;
}
[class^="s_listin_"] > li {
    position: relative;
    padding: 4px 0 4px 0;
    color: #444;
}
.s_listin_dot > li {
    padding-left: 16px;
}
.s_listin_dot > li:before {
    content: "";
    display: block;
    width: 4px;
    height: 4px;
    border-radius: 2px;
    background: #000;
    position: absolute;
    left: 6px;
    top: 11px;
}
.w_bold {
    font-weight: 600;
}

.notice {
    position: relative;
    display: block;
    font-size: 16px;
    color: #777;
    text-align: center;
}
.contact {
    position: relative;
    display: block;
    font-size: 16px;
    color: #337ab7;
    text-align: center;
    font-weight: 600;
    line-height: 30px;
}


.contact_cover .box_line2 {
	display: inline-block;
	float: left;
	text-align: center;
    border: 1px solid #6d88b7;
    margin-bottom: 30px;
    width: 15.9%;
  	margin-left: 20px;
}

ol, ul {
    list-style: none;
}


.sub_top .st_navigation ul::after {
    display: block;
    content: '';
    clear: both;
}

.sub_top .st_title {
    padding: 12px 0;
    border-bottom: 2px solid #000;
}
.fl_l {
    float: left;
}
.sub_top .st_title * {
    vertical-align: top;
}
.sub_top .st_title > div {
    padding: 8px 0;
}
.sub_top .st_title h3 {
    display: block;
    font-weight: 500;
    font-size: 32px;
    line-height: 40px;
    letter-spacing: -0.08em;
    color: #333;
}
.sub_top .st_title::after {
    display: block;
    content: '';
    clear: both;
}


.sub_top .st_navigation ul {
    padding: 4px 0;
}
.sub_top .st_navigation ul li {
    float: left;
    padding: 2px 8px 2px 12px;
    background: url(../image/sub_cmn/sub_tb_icon.png) no-repeat -40px -15px;
}
.sub_top .st_navigation ul li:first-child {
    background: none;
    padding-left: 0;
    padding-right: 4px;
}
.sub_top .st_navigation ul li a {
    display: block;
    font-size: 14px;
    line-height: 22px;
    color: #666;
}
.sub_top .st_navigation ul li:first-child a {
    width: 20px;
    height: 22px;
    background: url(../image/sub_cmn/sub_tb_icon.png) no-repeat 2px 2px;
}
.hdn {
    position: absolute;
    left: 0;
    top: 0;
    width: 0;
    height: 0;
    overflow: hidden;
}
[class^="s_title"] {
    position: relative;
    display: block;
    font-weight: 500;
    letter-spacing: -0.048em;
}
.s_title_in2::before {
    display: inline-block;
    content: '';
    width: 9px;
    height: 16px;
    margin-left: -20px;
    margin-right: 7px;
    background-color: #405985;
    border-right: 4px solid #47c1d1;
}
.s_title_in2 {
    padding-left: 20px;
    font-size: 22px;
    line-height: 26px;
    color: #222;
    margin-top: 24px;
}
.s_title_in3::before {
    display: inline-block;
    content: '';
    width: 12px;
    height: 12px;
    margin-left: -16px;
    margin-right: 8px;
    border: 3px solid #47a8d1;
}

.s_title_in3 {
    padding-left: 20px;
    font-size: 18px;
    line-height: 24px;
    color: #222;
    margin-top: 16px;
}
.s_descript {
    display: block;
    font-size: 14px;
    line-height: 22px;
    color: #444;
    padding: 2px 0;
    margin-top: 8px;
    margin-bottom: 10px;
}
[class^="box_"] {
    position: relative;
    padding: 14px 24px;
    margin-top: 22px;
}
.box_toggle {
    padding: 0;
    margin: 0;
}
.box_toggle .title_ac {
    display: block;
    width: 100%;
    margin: 0;
    padding-bottom: 7px;
    color: #607aa6;
    font-weight: 600;
}
media (min-width: 768px)
.box_toggle .title_ac.hdn {
    display: none;
}

.box_toggle .title_ac.m {
    display: none;
}
.box_toggle .inner {
    position: relative;
    width: 100%;
    padding: 0;
    height: auto;
    line-height: inherit;
}
@media (min-width: 768px)
.box_toggle .inner {
    padding-left: 12px;
}
.mgt16 {
    margin-top: 16px !important;
}
.data_table {
    margin-top: 8px;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
.data_table * {
    vertical-align: middle;
}
.data_table table {
    table-layout: fixed;
    border-collapse: separate;
    width: 100%;
    border-top: 1px solid #333;
    border-left: 1px solid #ddd;
}
.data_table table.num {
    border-top: 2px solid #6d88b7;
}
.data_table table th, .data_table table td {
    border-right: 1px solid #ddd;
    border-bottom: 1px solid #ddd;
    text-align: center;
    padding: 10px 16px;
}
.data_table table th {
    color: #222;
}
.data_table table thead th {
    font-size: 13px;
    line-height: 21px;
    font-weight: 500;
    letter-spacing: -0.048em;
    background: #f8f8f8;
    border-bottom: 1px solid #bbb;
}
.data_table table.num th {
    color: #405985;
}
.data_table table.num thead th {
    font-size: 14px;
    line-height: 1.5em;
    font-weight: 600;
    letter-spacing: -0.048em;
    background: #f7faff;
    border-bottom: 1px solid #ddd;
}
.data_table table.num thead > tr:first-child th[rowspan], .data_table table.num thead > tr:not(:first-child) th[rowspan] {
    border-bottom: 1px solid #6d88b7;
}

.data_table table.num thead > tr:last-child th {
    border-bottom: 1px solid #6d88b7;
}
.data_table table td {
    color: #444;
}
.data_table table tbody th, .data_table table tbody td {
    font-size: 14px;
    line-height: 22px;
    position: relative;
}
.remark_area {
    width: : 100%;
    margin-top: 8px;
}
.remark_area > p {
    font-size: 14px;
    line-height: 22px;
    color: #444;
    padding: 2px 0;
}
.ta_r {
    text-align: right;
}
.mgt8 {
    margin-top: 8px !important;
}
.i_descript {
    display: block;
    font-size: 12px;
    line-height: 20px;
    color: #666;
    padding: 2px 0;
    margin-top: 4px;
}
.txt_ntc_bold {
    font-weight: 600;
    color: #dd0000;
}

.box_line1 {
    border: 1px solid #6d88b7;
    margin-bottom: 30px;
    padding: 10px 14px 10px 14px;
}

.contact_cover{
	float: left;
	display: inline-block;
	width: 100%;
    border: 0px solid #6d88b7;
    margin-bottom: 0px;
    height: auto;
    text-align: center;
}

@media screen and (max-width: 1620px) {


.contact_cover .box_line2 {
	display: inline-block;
	float: left;
	text-align: center;
    border: 1px solid #6d88b7;
    margin-bottom: 30px;
    width: 13.25%;
  	margin-left: 20px;
}
}

/*=======================모바일 영역 ============================*/

@media screen and (max-width: 640px) {

[class^="box_"] {
    position: relative;
    padding: 10px 6px;
    margin-top: 10px;
}

.etc_box .tit {
    display: block;
    width: 96%;
    background-color: #e4f1ff;
    font-size: 30px;
    line-height: 1.25em;
    font-weight: 400;
    color: black;
    text-align: center;
}

.contact_cover {
    float: relative;
    display: inline-block;
    width: 100%;
    border: 0px solid #6d88b7;
    margin-bottom: 0px;
    height: auto;
    text-align: center;
}
.contact_cover .box_line2:first-child {
    margin-left: 5px;
}

.contact_cover .box_line2 {
    display: inline-block;
    float: left;
    text-align: center;
    border: 1px solid #6d88b7;
    margin-bottom: 1px;
    /* width: 32.3%; */
    width: 28%;
    margin-left: 5px;
}
.notice {
    position: relative;
    display: block;
    font-size: 15px;
    color: #777;
    text-align: center;
}
.contact {
    position: relative;
    display: block;
    font-size: 15px;
    color: #337ab7;
    text-align: center;
    font-weight: 600;
    line-height: 20px;
    letter-spacing :-1px;
}

.box_toggle {
    padding: 0;
    margin-top: -10px;
}

}


</style>

<div id="contents" class="contentArea">

<div id="content" class="content">
					<div>

		<!-- 			<div class="sub_top">
                            <div class="st_navigation">
                                <ul>
                                    <li><a href="/"><span class="hdn">홈</span></a></li>
                                    <li><a href="javascript:void(0);" onclick="javascript:fn_goMenu('/bdBoardList.do', '1', '11', '');">발생동향</a></li>
                                    <li><a href="javascript:void(0);" onclick="javascript:fn_goMenu('/bdBoardList.do', '1', '14', '');">국외 발생 현황</a></li>
                                </ul>
                            </div>
                            <div class="st_title">
                                <div class="fl_l">
                                    <h3>성남도시개발공사 코로나 현황</h3>
                                </div>
                            </div>
                        </div> -->

							<div class="box_line1">
								<div class="etc_box">
									<p class="tit">성남도시개발공사 시설물 코로나 현황 안내</p>
								</div>
							</div>

                        <!-- s: content -->                        
                       <h4 class="s_title_in2">성남도시개발공사 코로나바이러스감염증-19 발생 현황</h4>
                     
						<h5 class="s_title_in3">부서별 코로나 발생 현황</h5>
						<p class="s_descript">COVID-19 환자 총 0명( 06.03. 09시 기준)</p>
				
						<div class="box_toggle">
							<div class="inner">

								<div class="data_table mgt16">
							<table class="num">
								<caption><span class="hdn">기관 및 부서로 분류</span></caption>
								<colgroup>
									<col style="width: 29%">
									<col style="width: 29%">
									<col style="width: 42%">
								</colgroup>
								<thead>
									<tr>
										<th colspan="2" scope="col">기관 및 부서</th>
										<th scope="col">환자발생 수</th>
									</tr>
								</thead>
								<tbody>
			
														<tr>
															<th rowspan="4" scope="row">성남도시개발공사</th>
															<td class="w_bold">탄천종합운동장</td>
															<td>0명</td>
														</tr>

													
														<tr>
															<td class="w_bold">성남종합운동장</td>
															<td>0명</td>
														</tr>
		
													
													
														<tr>
															<td class="w_bold">중원도서관</td>
															<td>0명</td>
														</tr>
			
													
														<tr>
															<td class="w_bold">수정도서관</td>
															<td>0명</td>
														</tr>
		
										<tr>
											<th colspan="2" scope="row">합계</th>
											<td class="w_bold">0명</td>
										</tr>
								</tbody>
							</table>
								</div>
								 
<!-- 								<div class="remark_area">
								<p>* 부서별 발생 현황 </p>
								</div> -->
							</div>		 <!-- END inner -->				
						</div>			<!-- END box_toggle -->				

						<div class="board_top mgt16">							
							<div class="fl_l">
								 <h5 class="s_title_in3">시설별 확진 환자 발생 현황</h5>
							 	<p class="s_descript">COVID-19 환자 총 0명( 06.03. 09시 기준)</p>
							</div>
						</div>     
												
						<div class="data_table mgt16 tbl_scrl_mini2 mini">
							<table class="num minisize">
								<caption><span class="hdn">국외 발생현황표 - 지역 및 국가, 확진환자 발생수(사망)으로 구성</span></caption>
								<colgroup>
									<col style="width: 15%">
									<col style="width: 25%">
									<col style="width: 13%">
									<col style="width: 15%">
									<col style="width: 18%">

								</colgroup>
								<thead>
									<tr>
										<th scope="col">시설명</th>										
										<th scope="col">휴관기간</th>
										<th scope="col">문의처</th>
										<th scope="col">코로나 현황</th>
										<th scope="col">방역활동</th>
										<th scope="col">비고</th>
	
									</tr>
								</thead>
								<tbody>
								
				
											<tr>
												<th scope="row">탄천종합운동장</th>
												<td>2020.2.20. ~ 별도 공지일까지</td>
												<td>031-725-9398</td>
												<td>확진 0명</td>
												<td>시설물 방역 지속추진</td>
												<td>안전</td>
											</tr>
										
											<tr>
												<th scope="row">성남종합운동장</th>
												<td>2020.2.20. ~ 별도 공지일까지</td>
												<td>031-725-9398</td>
												<td>확진 0명</td>
												<td>시설물 방역 지속추진</td>
												<td>안전</td>
											</tr>
										
											<tr>
												<th scope="row">중원도서관</th>
												<td>2020.2.20. ~ 별도 공지일까지</td>
												<td>031-725-9398</td>
												<td>확진 0명</td>
												<td>시설물 방역 지속추진</td>
												<td>안전</td>
											</tr>
										
											<tr>
												<th scope="row">수정도서관</th>
												<td>2020.2.20. ~ 별도 공지일까지</td>
												<td>031-725-9398</td>
												<td><span class="txt_ntc_bold">*</span> N/A</td>
												<td>시설물 방역 지속추진</td>
												<td>안전</td>
											</tr>														
								</tbody>
							</table>						
						</div>				
	<p class="i_descript mgt8 ta_r"><span class="txt_ntc_bold">*</span> N/A표시는 미집계를 의미함</p>
                        <!-- e: content -->                        
                        
<!---------------------아래 전화번호 ------------------->
<!-- <div class="box_line2">
	<ul class="s_listin_dot">
		<li><span class="w_bold">"확진환자의 이동경로 등 정보공개 안내(2판)('20.4.12)"</span>에 따라  확진자가 마지막 접촉자와 접촉한 날로부터 14일 경과 시, 이동경로에 대한 부분은 공개되지 않음을 알려드립니다. </li>
	</ul>
	<p class="notice">해당 시간대에 아래 시설을 방문하신 분은 증상이 없어도 진단검사를 꼭 받아주세요.</p>			
</div>
<div class="box_line2">
	<p class="notice">해당 시간대에 아래 시설을 방문하신 분은 증상이 없어도 진단검사를 꼭 받아주세요.</p>			
</div> -->
<h5 class="s_title_in3">문의전화</h5>
<div class="contact_cover">

	<div class="box_line2">
		<p class="notice">질병관리본부</p>
		<p class="contact">1339</p>	
	</div>
	<div class="box_line2">
		<p class="notice">수정구보건소</p>
		<p class="contact">031-729-3870</p>	
	</div>
	<div class="box_line2">
		<p class="notice">중원구보건소</p>
		<p class="contact">031-729-3776~7</p>	
	</div>
	<div class="box_line2">
		<p class="notice">분당구보건소</p>
		<p class="contact">031-729-3990</p>	
	</div>
	<div class="box_line2">
		<p class="notice">지역콜센터</p>
		<p class="contact">지역국번 + 120</p>	
	</div>
</div>


                        </div>
                    </div>


	</div><!-- END content -->

</div> <!-- END contents -->