<style type="text/css">
	
@media screen and (min-width: 1000px){
#contents {
    min-height: 600px;
}
.pageSearch .schForm .schKwd {
    min-width: 320px;
}

}

#contents {
    position: relative;
    z-index: 10;
    padding: 30px 0;
    word-break: keep-all;
}
.tabNav {
    display: none;
    position: relative;
    margin-bottom: 30px;
}
.tnb {
    border-left: 1px solid #e4e4e4;
}

.tnb li {
    overflow: hidden;
    float: left;
    position: relative;
    margin-top: -1px;
}

ol, ul {
    list-style: none;
}

.themeBG, .tB02, #gnb > li.fbC > ul, #lnb .submenu, .summaryDesc, .calHead {
    background-color: #f2f8e7 !important;
}
.summaryDesc {
    margin-bottom: 30px;
    padding: 7px;
    background: url(/include/image/common/bg_pattern_desc.png) repeat;
}
.summaryDesc .innerBox {
    overflow: hidden;
    position: relative;
    padding: 20px;
    background-color: #fff;
}
.summaryDesc .innerBox ul {
    display: block;
    list-style-type: disc;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
    padding-inline-start: 40px;
}

form {
    display: block;
    margin-top: 0em;
}

fieldset {
    display: block;
    margin-inline-start: 2px;
    margin-inline-end: 2px;
    padding-block-start: 0.35em;
    padding-inline-start: 0.75em;
    padding-inline-end: 0.75em;
    padding-block-end: 0.625em;
    min-inline-size: min-content;
    border-width: 2px;
    border-style: groove;
    border-color: threedface;
    border-image: initial;
}
.blind {
    overflow: hidden;
    position: absolute;
    top: -9999em;
    left: -9999em;
    width: 1px;
    height: 1px;
    line-height: 1px;
    text-indent: -9999em;
}
legend {
    display: block;
    padding-inline-start: 2px;
    padding-inline-end: 2px;
    border-width: initial;
    border-style: none;
    border-color: initial;
    border-image: initial;
}

caption, legend {
    overflow: hidden;
    width: 0;
    height: 0;
    line-height: 0;
    text-indent: -9999em;
}

.board-list {
    table-layout: fixed;
    border-top: 2px solid #666;
    border-bottom: 1px solid #e2e2e2;
    empty-cells: show;
}
.board-list th, .board-list td {
    padding: 10px 5px;
    font-weight: normal;
    text-align: center;
    border-top: 1px solid #e2e2e2;
}

.board-list thead th {
    font-size: 1.067em;
}

.board-list thead tr:first-child th {
    border-top: 0;
}

th, td {
    font-size: inherit;
    line-height: inherit;
    word-break: normal;
}
thead {
    display: table-header-group;
    vertical-align: middle;
    border-color: inherit;
}
tbody {
    display: table-row-group;
    vertical-align: middle;
    border-color: inherit;
}


table {
    width: 100%;
    border-collapse: separate;
    border-spacing: 0;
    font-size: inherit;
}
tr {
    display: table-row;
    vertical-align: inherit;
    border-color: inherit;
}

p {
    display: block;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
}

caption {
    display: table-caption;
    text-align: -webkit-center;
}
colgroup {
    display: table-column-group;
}

.pagingWrap {
    overflow: hidden;
    position: relative;
    min-height: 35px;
    margin-top: 20px;
    text-align: center;
}
html, body, div, span, iframe, p, dl, dt, dd, ol, ul, li, fieldset, form, label, legend, input, textarea, button, select, table, th, td, article, aside, section, figure, figcaption, img {
    margin: 0;
    padding: 0;
    border: 0;
}
.pagingWrap .btn {
    position: absolute;
    bottom: 0;
    right: 0;
    padding: 0 14px 0 18px;
    line-height: 35px;
}
.pageSearch {
    overflow: hidden;
    margin: 0 0 30px;
    padding: 10px;
    text-align: center;
    background-color: #f4f4f4;
}
.pageSearch .schForm {
    display: inline-block;
    vertical-align: top;
}


.themeColor, .themeBtn, .tB01, #snb, #gnbNavM, #gnbNav.fixed, #lnbNav h2, .tnb .choiced a, .virtSelect a, .paging span.current, .ss-controls > a, .popupHeader, .calendar td.today .today, .mobileAppWrap .infoList::before, .step-list li::before, .dream_step li dt, .dream_step li::before, .dream_step1 li::before {
    background-color: #6ea209 !important;
}

.btn {
    display: inline-block;
    padding: 8px 15px;
    color: #fff;
    text-align: center;
    letter-spacing: -1px;
    vertical-align: middle;
    box-sizing: border-box;
    background-color: #555;
}

select {
    height: 36px;
    padding-right: 26px;
    padding-left: 4px;
    line-height: 36px;
    border: 1px solid #ddd;
    border-radius: 0;
    box-sizing: border-box;
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    background: #fff url(/assets/img/arr_select.png) 100% 50% no-repeat;
    color: #333;
}

option {
    font-weight: normal;
    display: block;
    white-space: pre;
    min-height: 1.2em;
    padding: 0px 2px 1px;
}

.form-ele {
    display: inline-block;
    width: 100%;
    vertical-align: middle;
    box-sizing: border-box;
}
.form-ele.auto {
    width: auto;
}
.form-ele.min {
    min-width: 100px;
}
.pageSearch .item .form-ele {
    vertical-align: top;
}

.btn.input {
    min-width: 70px;
    padding: 0 4px;
    line-height: 36px;
    border: 0;
}

.themeColor, .themeBtn, .tB01, #snb, #gnbNavM, #gnbNav.fixed, #lnbNav h2, .tnb .choiced a, .virtSelect a, .paging span.current, .ss-controls > a, .popupHeader, .calendar td.today .today, .mobileAppWrap .infoList::before, .step-list li::before, .dream_step li dt, .dream_step li::before, .dream_step1 li::before {
    background-color: #6ea209 !important;
}
.pageSearch .schForm .schKwd + .btn {
    padding: 0 10px;
    line-height: 36px;
}


input, textarea {
    resize: none;
    border: 1px solid #ddd;
    border-radius: 0;
    box-sizing: border-box;
    background: #fff;
}

input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="number"] {
    height: 36px;
    padding: 0 4px;
    line-height: 34px;
    border: 1px solid #ddd;
    box-sizing: border-box;
}

input[type="text"], img, select {
    font-size: 0.933em;
    vertical-align: middle;
}

.pageSearch .schForm .schKwd {
    width: 200px;
}

a {
    text-decoration: none;
    color: inherit;
}
.dash-list{
    margin: 0;
    padding: 0;
    border: 0;
}
.dot-list > li {
    margin: 2px 0;
}

</style>

<div id="contents" class="contentArea">
					
<!--Forced tab Show Que-->
<div class="tabNav">
	<div class="virtSelect"><a href="#script">탭메뉴</a></div>
	<ul class="tnb clearfix">
		<li></li>
	</ul>
</div>
<!--Forced tab Show Que-->
					<!--Real Contents Start-->

					
						<div class="summaryDesc">
							<div class="innerBox"><ul class="dot-list">
    <li><strong class="themeFC">개인정보</strong>가  불법적으로 이용되는 것을 막기 위해 이용자께서는 e-메일, 주소, 주민번호, 전화번호 등 개인정보에 관한 사항을 게시하는 것을 주의하시기 바랍니다.</li>
    <li>홈페이지의 건전한 운영을 위하여 아래 내용의 게시물은 별도 통지없이 관리자에 의해 <strong class="themeFC">삭제</strong>되고 있음을 알려드립니다.
        <ul class="dash-list">
            <li>국가 안전이나 보안에 위배되는 경우</li>
	<li>정치적 목적이나 성향이 있는 경우</li>
	<li>특정기관, 단체, 부서를 근거없이 비난하는 경우</li>
	<li>특정인을 비방하거나 명예훼손의 우려가 있는 경우</li>
	<li>영리목적의 상업성 광고, 저작권을 침해할 수 있는 내용</li>
	<li>욕설, 음란물 등 불건전한 내용</li>
	<li>동일인이라고 인정되는 사람이 동일 또는 유사내용을 반복하여 게재하는 도배성 글</li>
	<li>연습성, 오류, 장난성의 내용</li>
	<li>기타 해당 게시판의 취지와 부합하지 않을 경우 등</li>
        </ul>
    </li>
</ul></div>
						</div>
					

					<!-- 게시판 검색 -->
					<form name="searchForm" id="searchForm" method="get">
					<fieldset>
						<legend class="blind">게시글 검색 영역</legend>
						<div id="pageSearch">
							<div class="pageSearch">
								
									<span class="item">
										<select name="searchCategory" id="searchCategory" title="구분 선택" class="form-ele auto min">
											<option value="">전체</option>
											
												<option value="1">자료실</option>
											
												<option value="2">열람실</option>
											
												<option value="3">전자정보실</option>
											
												<option value="4">문화강좌</option>
											
												<option value="5">시설물관련</option>
											
												<option value="6">전산관련</option>
											
												<option value="7">기타</option>
											
										</select>
									</span>
								
								<span class="schForm">
									<select name="searchCondition" id="searchCondition" title="검색방법 선택" class="schSel">
										<option value="title">제목</option>
										<option value="contents">내용</option>
									</select>
									<input type="text" name="searchKeyword" id="searchKeyword" value="" title="검색어 입력" class="schKwd" placeholder="검색어 입력">
									<a href="#link" id="searchBtn" class="btn input search themeBtn">검색</a>
								</span>
							</div>
						</div>
					</fieldset>
					</form>
					<!-- //게시판 검색 -->
					<!-- 게시판 목록 -->
					<div class="boardWrap">
						<table class="board-list">
							<caption><strong>묻고답하기 게시글 목록</strong><p>번호, 구분, 제목, 작성자, 작성일, 답변 제공 표</p></caption>
							<colgroup>
								<col style="width:50px" class="mobileHide">
								
									<col style="width:100px" class="mobileHide">
								
								<col>
								<col style="width:70px">
								<col style="width:100px" class="mobileHide">
								<col style="width:70px">
							</colgroup>
							<thead>
								<tr>
									<th scope="col" class="mobileHide">번호</th>
									
										<th scope="col" class="mobileHide">구분</th>
									
									<th scope="col">제목</th>
									<th scope="col">작성자</th>
									<th scope="col" class="mobileHide">작성일</th>
									<th scope="col">답변</th>
								</tr>
							</thead>
							<tbody>
								
									
										<tr>
											<td class="mobileHide">
												713
											</td>
											
												<td class="mobileHide">시설물관련</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137817'); return false;">
													스마트도서관의 책
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												<img src="/include/image/jw/board/ico_new.png" alt="새 글">
											</td>
											<td>
												박*경
											</td>
											<td class="mobileHide">2020-05-31</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												712
											</td>
											
												<td class="mobileHide">자료실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137815'); return false;">
													대출한 책에 욕설이 적힌 쪽지를 남긴 악질 이용자 제재 요청
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												<img src="/include/image/jw/board/ico_new.png" alt="새 글">
											</td>
											<td>
												원*현
											</td>
											<td class="mobileHide">2020-05-31</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												711
											</td>
											
												<td class="mobileHide">자료실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137778'); return false;">
													예약대출
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												<img src="/include/image/jw/board/ico_new.png" alt="새 글">
											</td>
											<td>
												최*은
											</td>
											<td class="mobileHide">2020-05-28</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												710
											</td>
											
												<td class="mobileHide">시설물관련</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137746'); return false;">
													사물함 사용관련 문의드립니다 
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												
											</td>
											<td>
												P**********I
											</td>
											<td class="mobileHide">2020-05-25</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												709
											</td>
											
												<td class="mobileHide">열람실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137655'); return false;">
													열람실좌석표 출력시
												</a>
												
												
												
											</td>
											<td>
												박*호
											</td>
											<td class="mobileHide">2020-05-20</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												708
											</td>
											
												<td class="mobileHide">열람실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137602'); return false;">
													안우울한건의
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												
											</td>
											<td>
												박*호
											</td>
											<td class="mobileHide">2020-05-17</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												707
											</td>
											
												<td class="mobileHide">자료실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137533'); return false;">
													반납
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												
											</td>
											<td>
												남*아
											</td>
											<td class="mobileHide">2020-05-11</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												706
											</td>
											
												<td class="mobileHide">자료실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137532'); return false;">
													반납
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												
											</td>
											<td>
												남*기
											</td>
											<td class="mobileHide">2020-05-11</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												705
											</td>
											
												<td class="mobileHide">시설물관련</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137527'); return false;">
													사물함 사용관련하여 질문
												</a>
												
												
												
											</td>
											<td>
												장*수
											</td>
											<td class="mobileHide">2020-05-10</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
										<tr>
											<td class="mobileHide">
												704
											</td>
											
												<td class="mobileHide">열람실</td>
											
											<td class="title">
												<a href="#javascript" onclick="javascript:fnDetail('137510'); return false;">
													책걸상  교체
												</a>
												
												 <img src="/include/image/jw/board/ico_lock.png" alt="비밀글">
												
											</td>
											<td>
												장*아
											</td>
											<td class="mobileHide">2020-05-08</td>
											<td class="themeFC">
												
														완료
													
											</td>
										</tr>
									
								
							</tbody>
						</table>
					</div>
					
					<!-- //게시판 목록 -->
					<!-- 페이징 -->
					<div class="pagingWrap">
						
							<p class="paging">
								<a href="javascript:fnList(1);" class="btn-paging first"><span class="blind">맨 첫 페이지로 가기</span></a>
<a href="javascript:fnList(1);" class="btn-paging prev"><span class="blind">이전 10개 보기</span></a>
<span class="current">1</span>
<a href="javascript:fnList(2);">2</a>
<a href="javascript:fnList(3);">3</a>
<a href="javascript:fnList(4);">4</a>
<a href="javascript:fnList(5);">5</a>
<a href="javascript:fnList(6);">6</a>
<a href="javascript:fnList(7);">7</a>
<a href="javascript:fnList(8);">8</a>
<a href="javascript:fnList(9);">9</a>
<a href="javascript:fnList(10);">10</a>
<a href="javascript:fnList(11);" class="btn-paging next"><span class="blind">다음 10개 보기</span></a>
<a href="javascript:fnList(72);" class="btn-paging last"><span class="blind">맨 마지막 페이지로 가기</span></a>


							</p>
						
						<a href="#none" id="registBtn" class="btn write themeBtn">글쓰기</a>
					</div>
					<!-- //페이징 -->

					<!-- End Of the Real Contents-->
					



				</div>