
<style type="text/css">
.reply_content{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);font-weight: 400;font-size: 17px;box-sizing: border-box !important;color: #555;margin: 0;padding: 0;font-family: 'Nanum Gothic', dotum, Arial, sans-serif;line-height: 130%;position: relative;height: 93px;border: 1px solid #ddd;background: #f6f7f8;margin-top: 20px;}
.reply_content .txt_ar{position: relative;padding-right: 99px;padding-bottom: 0;}
.reply_content textarea {display: inline-block;outline: none;box-sizing: border-box !important;font: inherit;overflow: auto;line-height: 25px;font-family: 'Noto Sans KR', sans-serif;font-size: 16px;width: 100%;border: 0;resize: none;overflow-y: auto;padding: 10px 100px 10px 20px;height: 91px;background: #fff;color: #767676;}
.reply_content .file_att{color: #555;overflow: hidden;position: absolute;top: -1px;right: -1px;height: 93px;padding: 0;border: 0;}
.reply_content .at_wrap {color: #555;font-family: 'Nanum Gothic', dotum, Arial, sans-serif;overflow: hidden;position: relative;padding-left: 100px;}
.reply_content .total_n{float: none;position: absolute;bottom: 20px;left: 0;font-size: 15px;color: #808080;}
.reply_content button{box-sizing: border-box !important;font: inherit;overflow: visible;text-transform: none;-webkit-appearance: button;font-family: 'Noto Sans KR', sans-serif;font-size: 16px;cursor: pointer;border-width: 0;color: #fff;height: 93px;line-height: 93px;width: 100px;position: relative;top: auto;right: auto;background: #488bf8;}
</style>

<div id="contents">
    <div class="reply_content" id="replyContentNoDiv">
        <div class="txt_ar">
            <textarea class="doc_text" rows="2" placeholder="여러분의 의견을 입력해주세요. 주제와 무관한 댓글 악플은 삭제될 수 있습니다." title="내용입력"></textarea>
        </div>
        <div class="file_att">
            <div class="at_wrap">
                <span class="total_n" id="description" name="description"><!-- <b>0</b>/1000 --></span>
                <button class="sub_mit" type="button" id="submit_ment">등록</button>
            </div>
        </div> 
    </div>
</div>


<script type="text/javascript">
    //textarea 글자수 체크
$('.doc_text').keyup(function (e){
    var content = $(this).val();
    $('#description').html("("+content.length+"/200)");    //글자수 실시간 카운팅

    if (content.length > 200){
        alert("최대 200자까지 입력 가능합니다.");
        $(this).val(content.substring(0, 200));
        $('#description').html("(200/200)");
    }
});
</script>