<style>

select {
height: 36px;
padding-right: 26px;
padding-left: 4px;
/*line-height: 36px;*/
border: 1px solid #ddd;
border-radius: 0;
box-sizing: border-box;
-webkit-appearance: none;
-moz-appearance: none;
appearance: none;
background: #fff url(/assets/img/arr_select.png) 100% 50% no-repeat;
color: #333;
}

input, textarea {
    resize: none;
    border: 1px solid #ddd;
    border-radius: 0;
    box-sizing: border-box;
    background: #fff;
}

input[type="text"], input[type="password"], input[type="email"], input[type="tel"], input[type="number"] {
    height: 36px;
    padding: 0 4px;
    line-height: 34px;
    border: 1px solid #ddd;
    box-sizing: border-box;
}

input[type="text"], img, select {
    font-size: 0.933em;
    vertical-align: middle;
}

#header .global_menu{
    letter-spacing:0px;
    padding:0px 10px;
}
.event-list {
    overflow: hidden;
    margin-left: -20px;
}

.event-list li {
    float: left;
    width: 25%;
    margin-bottom: 20px;
}

.event-list li .cate.typeG, .event-list .typeG .thumb {
    background-color: #995955;
}

.event-list li .thumbBox .thumb img, .event-list li .thumbBox .thumb span {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.event-list li .thumbBox {
    margin-left: 20px;
    border: 1px solid #e4e4e4;
}
.event-list li .thumbBox .thumb {
    display: block;
    position: relative;
    padding-top: 68%;
    text-align: center;
    border-bottom: 1px solid #e4e4e4;
}
.event-list li .eventSummary {
    position: relative;
    margin: 12px;
    padding-top: 30px;
}

.event-list li .tit {
    overflow: hidden;
    height: 50px;
    line-height: 25px;
    word-break: keep-all;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
}
.event-list li .cate {
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    padding: 0 5px;
    color: #fff;
    background-color: #3f5f9c;
}

.event-list li .hits {
    position: absolute;
    top: 3px;
    right: 0;
    padding-left: 22px;
    line-height: 19px;
    color: #777;
    background: url(/assets/img/ico_so.png) left no-repeat; background-size: 20px auto;;
}
.event-list li .date {
    margin-top: 10px;
    padding-top: 10px;
    padding-left: 0;
    line-height: 1.2;
    color: #777;
    border-top: 1px solid #e4e4e4;
    /* background: url(/include/cmm_new/image/board/ico_sp.png) 0 9px no-repeat; */
}
.blind {
    overflow: hidden;
    position: absolute;
    top: -9999em;
    left: -9999em;
    width: 1px;
    height: 1px;
    line-height: 1px;
    text-indent: -9999em;
}
.event-list li .eventDate {
    position: relative;
    padding: 0 10px 8px;
    text-align: right;
    color: #aaa;
    font-size: 13px;
}

@media screen and (max-width:1620px ){
    #header .global_menu{
        bottom:95px;
    }
.event-list li {
    float: left;
    width: 33.3333%;
    margin-bottom: 20px;
}

}

@media screen and (max-width: 640px) {  /*모바일*/
    #header .global_menu{
        bottom:inherit;
    }
.event-list {
    overflow: hidden;
    margin-left: -20px;
    padding: 0px 0px 0px 3px;
}    

.event-list li {
    float: left;
    width: 99%;
    margin-bottom: 20px;
}

.write_table tr th label {
    line-height: 35px;
    font-size: 15px;
}
.write_table tr td .wFull {
    width: 97%;
}
.write_table tr th {
    width: 15%;
    height: 35px;
    padding: 5px;
    font-size: 12px;
    line-height: 35px;
}

}

/*contents*/
.margin5 {margin:5px;}
.margin10 {margin:10px;}
.imarginTop0 {margin-top:0 !important;}
.marginTop10 {margin-top:10px;}
.marginTop30 {margin-top:30px;}
.marginTop60 {margin-top:60px;}
.paddingBtm3 {margin-bottom:3px;}
.marginBtm30 {margin-bottom:30px; margin-left: 20px;}
.marginTopLeftBtm10 {margin-top:10px; margin-left:10px; margin-bottom:10px;}
.marginTopBtm3 {margin-top:3px; margin-bottom:3px;}
.marginTopBtm10 {margin-top:10px; margin-bottom:10px;}
.marginTop10Btm30 {margin-top:10px; margin-bottom:30px;}
.marginTop20Btm15 {margin-top:20px; margin-bottom:15px;}
.marginLeft10 {margin-left:10px;}
.marginTopBtm30 {margin-top:30px; margin-bottom:30px;}
.marginBtm10 {margin-bottom:10px;}
.padding20 {padding:20px;}
.padding1520 {padding:15px 20px;}
.paddingTop10 {padding-top:10px;}
.paddingTopBtm10 {padding-top:10px; padding-bottom:10px;}
.con01 {margin:20px 0 25px 15px;}
.p01 {margin-top:60px;}
.border01 {border: 1px solid #e4e4e4;}
.border02 {border-left: 1px solid #e4e4e4; border-right: 1px solid #e4e4e4; border-bottom: 1px solid #e4e4e4;}

/* hidden*/
.blind {display:none;}
.blind01 {position:absolute; width:0; height:0; overflow:hidden; font-size:0; line-height:0;}
.blindReader {display:block; width:0; height:0; line-height:0; text-indent:100%; white-space:nowrap; overflow:hidden;}
.clearfix:after {content:""; display:block; clear:both;}

/*table*/
.tbl01 {position:relative; border-top:2px solid #3c97d3; border-right:1px solid #e4e4e4; background-color:#fff;}
.tbl01 th {background-color:#f1f1f1; text-align:center;}
.tbl01 td {text-align:left;}
.tbl01 th, .tbl01 td {padding:8px 6px; border-left:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4; empty-cells:show;}
.tbl02 {position:relative; border-top:2px solid #3c97d3; border-right:1px solid #e4e4e4; background-color:#fff;}
.tbl02 th {background-color:#f1f1f1; text-align:center;}
.tbl02 td {text-align:center;}
.tbl02 th, .tbl02 td {padding:8px 6px; border-left:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4; empty-cells:show;}
.boardList01 {position:relative; border-top:2px solid #3c97d3; table-layout:fixed;}
.boardList01 th {background-color:#f1f1f1; text-align:center;}
.boardList01 th, .boardList01 td {padding:10px 6px; border-bottom:1px solid #e4e4e4;}
.boardList01 td.boardTitle {overflow:hidden; white-space:nowrap; text-overflow:ellipsis;}
.boardView01 {table-layout:fixed; border-top:2px solid #3c97d3; empty-cells:show;}
.boardView01 th {background-color:#f1f1f1; text-align:center;}
.boardView01 td {text-align:left;}
.boardView01 th, .boardView01 td {padding:8px 6px; border-bottom:1px solid #e4e4e4;}
.boardView01 td.textarea textarea {width:100%; height:300px; padding:5px;}
.boardView01 td textarea {resize:none; vertical-align:top; box-sizing:border-box;}
.boardList02 {border-top:2px solid #3c97d3; border-right:1px solid #e4e4e4;}
.boardList02 dl {border-bottom:1px solid #e4e4e4;}
.boardList02 dl dt {padding:15px;}
.boardList02 dl dt span {margin-right:7px; color:#3c97d3; font-weight:600;}
.boardList02 dl dd {padding:15px 35px 15px 35px; border-top:1px solid #e4e4e4; background-color:#f8f8f8;}

/*tab*/
.tabWrap {position:relative; margin-bottom:30px;}
.tabWrap a:hover {text-decoration:underline}
.tabWrap a {font-weight:600;}
.tabWrap ul {border-left:1px solid #c1c1c1;}
.tabWrap ul li {display: inline-block; overflow:hidden; position:relative; margin-top:-1px; vertical-align:middle;}
.tabWrap ul li a {padding:8px 0; display:block; letter-spacing:-0.05em; margin-left:-1px; border-left-width:0; border:1px solid #c1c1c1; box-sizing:border-box; background-color:#fff;}
.tabTwo li {width:50%;}
.tabThree li {width:33.33%;}
.tabFour li {width:25%;}
.tabFive li {width:20%;}
.tabSix li {width:16.65%;}
.tabSeven li {width:14.28%;}

/*step*/
.stepWrap {text-align:left; padding:0;}
.stepFour {overflow:hidden; margin-left:-25px;}
.stepFour li {position:relative; display:table; float:left; width:calc(25% - 25px); margin-left:25px; margin-bottom:10px; background-color:#e5e5e5; box-sizing:border-box; padding:10px 5px; background-image:url("/img/sub/bg_line_01.gif"); background-repeat: repeat-x;}
.stepFour li p {display:table-cell; vertical-align:middle; width:100%; text-align:center;}
.stepFour li:before {content:''; display:block; position:absolute; right:-22px; top:40%; width:15px; height:23px; background:url(/img/sub/step_arrow_02.gif) no-repeat 0 0;}
.stepFour li:last-child:before {content:''; display:none;} 
.stepFive {overflow:hidden; margin-left:-25px;}
.stepFive li {position:relative; display:table; float:left; width:calc(20% - 25px); margin-left:25px; margin-bottom:10px; background-color:#e5e5e5; box-sizing:border-box; padding:10px 5px; background-image:url("/img/sub/bg_line_01.gif"); background-repeat: repeat-x;}
.stepFive li p {display:table-cell; vertical-align:middle; width:100%; text-align:center;}
.stepFive li:before {content:''; display:block; position:absolute; right:-22px; top:40%; width:15px; height:23px; background:url(/img/sub/step_arrow_02.gif) no-repeat 0 0;}
.stepFive li:last-child:before {content:''; display:none;} 
.stepFacitily li {min-height:100px;}
.stepDev01 li {min-height:80px;}

/*width line up*/
.widthLine {overflow:hidden; border-left:1px solid #c1c1c1;}
.widthLine li {position:relative; display:table; float:left; overflow:hidden; min-height:60px; margin-left:-1px; border-left-width:0; border:1px solid #c1c1c1; box-sizing:border-box;background-color:#f9f9f9; margin-bottom:2px;}
.widthLine li p {display:table-cell; vertical-align:middle; text-align:center; width:100%;}
.choiceDevWork {border-color:#009999; background-color:#009999 !important; color:#fff; font-weight:600;}
.wLEight li {width:108px;}
.wLTen li {width:87px;}


/*width line up 2*/
.widthLine2 {text-align:center;}
.widthLine2 li {overflow:hidden; float:left; margin:2px 0 2px 15px; text-align:center; box-sizing:border-box;}
.widthLine2 img {display:block; margin:0 auto;}
.wL2Two li {width:calc(25% - 16px);}

/*gallery*/
.galleryList {overflow:hidden; margin-left:-20px;}
.galleryList li {float:left; width:33.333%; margin-bottom:20px;}
.galleryList li .galleryBox {margin-left:20px; border:1px solid #e4e4e4;}
.galleryList li .galleryBox .gBox {display:block; position:relative; padding-top:68%; text-align:center; border-bottom:1px solid #e4e4e4;}
.galleryList li .galleryBox .gBox img {display:block; position:absolute; top:0; left:0; width:100%; height:100%;}
.galleryList li .gSummary {position:relative; margin:10px;}
.galleryList li .gTit {overflow:hidden; height:50px; line-height:25px; word-break:keep-all; display:-webkit-box; -webkit-box-orient:vertical; -webkit-line-clamp:2;}
.galleryList li .gTit a {font-weight:600;}
.galleryList li .gInfo {margin-top:10px; padding-top:10px; padding-left:0; line-height:1.2; border-top:1px solid #e4e4e4;}
.galleryList li .gDate {margin-top:5px;}
.galleryList li .gHits {position:relative; padding:0 10px 8px; text-align:right; font-size:13px;}
.galleryList li.emptyData {width:calc(100% - 20px) !important;  float:none; clear:left; margin-left:20px; }

/*noticeBox*/
.noticeBox {margin-bottom:30px; padding:15px 20px; border:1px solid #e4e4e4; background-color:#f9f9f9;}
.noticeBox ul li {margin:2px 0; padding-left:13px; background:url(/img/common/icon_mark_03.gif) no-repeat left 10px;}
.noticeBox01 {padding:15px 20px; border:1px solid #e4e4e4; background-color:#f9f9f9;}

/*background-color*/
.tit01 {background-color:#f8f8f8;}
.itit02 {background-color:#f9f9f9 !important;}

/*border*/
.ibdrBtm01 {border-bottom:2px solid #e4e4e4 !important;}

/*font-color*/
.blue01 {color:#2585c5;}
.blue02 {color:#3d96d3;}
.blue03 {color:#2d59aa;}
.green01 {color:#097d25;}
.purple01 {color:#4964ad;}
.purple02 {color:#7b538e;}
.purple03 {color:#5b548f;}
.red01 {color:#b34a57;}

/*font-weight*/
.weight600 {font-weight:600;}

/*width-size*/
.width100 {width:100%;}

/*keyword*/
.kwd01 {font-weight:600; font-size:16px; color:#2585c5;}

/*button*/
.btnArea {overflow:hidden; position:relative; margin:30px auto; text-align:right;}
.btnArea .btn a, .btnArea .btn input {padding-top:5px; padding-bottom:5px; color:#fff; background:#00a0ea;}
.btn a, .btn input {display:inline-block; margin-top:1px; margin-bottom:1px; padding:6px 15px; line-height:20px; text-align:center; vertical-align:middle; white-space:nowrap; word-break:break-all; box-sizing:border-box;}
.btn01 {display: inline-block; padding:5px 24px; color:#fff; background:#00a0ea; font-weight:600;}
.btn02 {display: inline-block; padding:1px 10px; color:#fff; background:#00a0ea; font-weight:600;}
.btnArea01 {overflow:hidden; position:absolute; right:150px; top:97px; margin:0 auto; text-align:right;}
.btnArea01 .btn a {padding-top:5px; padding-bottom:5px; color:#fff; background:#00a0ea;}

/*location*/
.center {text-align:center;}
.centerImp {text-align:center !important;}
.left {text-align:left;}
.ileft {text-align:left !important;}
.fleft {float:left;}
.right {text-align:right;}

/*empty-data*/
.emptyData {padding:10px 0; text-align:center; border-top:2px solid #3c97c3; border-bottom:1px solid #e4e4e4;}

/*tab*/
.tab01 {background:#fff; color:#3c97d3; padding:7px 2px; border:1px solid #3c97d3; border-top:3px solid #3c97d3;}

/*marker*/
.mark01 {padding-left:15px; font-size:16px; font-weight:600; background:url(/img/common/icon_mark_03.gif) no-repeat left 12px;}
.mark02 {padding-left:15px; font-size:16px; background:url(/img/common/icon_mark_04.gif) no-repeat left 12px;}
.mark03 {padding-left:12px; background:url(/img/common/icon_mark_05.gif) no-repeat left 13px;}
.mark04 {padding-left:12px; background:url(/img/common/icon_mark_06.gif) no-repeat left 13px;}
.mark05 {padding-left:15px; font-size:16px; background:url(/img/common/icon_mark_03.gif) no-repeat left 12px;}

/*선언문*/
.declare01 {border:1px solid #555; border-top:3px solid #555; padding:40px 30px;}
.declareTit {font-weight:600; margin-bottom:30px; font-size:17px; overflow:hidden; line-height:26px; text-align:justify;}
.declarePlain {margin-bottom:10px; text-align:justify; font-size:16px;}
.declarePlainMarker {padding-left:39px; margin-bottom:10px; text-align:justify; font-size:16px; background:url(/img/common/declare_mark_01.gif) no-repeat left 7px;}

/*페이징*/
.pageWrap {overflow:hidden; position:relative; text-align:center; margin-top:40px;}
.pageWrap p a {display:inline-block; width:27px; height:27px; margin:0 2px; text-align:center; vertical-align:top; border:1px solid #e4e4e4;}
.pageWrap .pagingFirst {background:url(/img/sub/paging_01.gif) no-repeat center center;}
.pageWrap .pagingPrev {background:url(/img/sub/paging_02.gif) no-repeat center center;}
.pageWrap .pagingNext {background:url(/img/sub/paging_03.gif) no-repeat center center;}
.pageWrap .pagingLast {background:url(/img/sub/paging_04.gif) no-repeat center center;}
.pageWrap span.pageCur {display:inline-block; width:29px; height:29px; font-weight:600; color:#fff; border:none; line-height:29px;}

/*검색*/
.pageSearch {margin-top:40px; background:#f1f1f1;}
.pageSearch .pageSchFrm {overflow:hidden; margin:0 0 30px; padding:10px; text-align:center; border:1px solid #cccccc;}
.pageSearch .pageSchFrm .schFrm {display:inline-block;}
.pageSearch input[type="text"] {padding:0 5px; border:1px solid #cccccc; width:250px;}
.schBtn {vertical-align:middle;}
.schBtn a {display:inline-block; min-width:40px; padding:0 10px; line-height:30px; text-align:center; border:1px solid #cccccc; box-sizing:border-box;}
.schBtn input[type="submit"] {display:inline-block; min-width:40px; padding:0 10px; line-height:36px; text-align:center; border:1px solid #cccccc; background:#f1f1f1; box-sizing:border-box;}
.schBtn01 {vertical-align:middle; min-width:40px; padding:0 10px; line-height:30px; border:1px solid #cccccc; box-sizing:border-box; background:#f1f1f1; font-weight:600;}
.searchArea {margin-bottom:30px; padding:15px; text-align:right; background:#f1f1f1; border:1px solid #cccccc;}
.tabSelectGov {background-color:#2e6ebb;}
.tabSelectCustomer {background-color:#666699;}
.tabSelectDevWork {background-color:#009999;}
.tabSelectOpenBusiness {background-color:#556fb5;}

/*평가*/
.appraisal {position:relative; margin-top:60px; border:1px solid #e4e4e4;}
.appraisal .appCont {padding:8px 20px; border-bottom:1px solid #e4e4e4; background-color:#f9f9f9;}
.appraisal .appCheck {padding:8px 20px;}
.appCheck p, .appCheck ul, .appCheck ul li {float:left; margin-right:20px;}

/*동의서*/
.formAgree {overflow:auto; max-height:450px; margin:20px 0 8px; padding:15px; line-height:1.7; border:1px solid #e4e4e4; background-color:#f9f9f9;}
.formAgree>ul {margin-top:10px;}
.checkAgree {text-align:right;}

/*가로정렬 링크*/
.linkArea {position:relative; width:100%; min-height:200px; padding-bottom:30px; margin:0 auto; line-height:20px; overflow:hidden;}
.linkArea>div {position:relative; min-height:140px; width:49%; padding-bottom:15px; border-top:2px solid #3c97d3; border-left:1px solid #e4e4e4; border-right:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;  text-align:justify;}
.linkArea>div p {padding:20px 25px 0;}
.linkArea>div .linkTit{font-weight:600; padding-top:15px; padding-bottom:15px; text-align:center; background-color:#f1f1f1;}
.linkArea .linkLeft {float:left;}
.linkArea .linkRight {float:right;}
.linkArea .certPhone .certBtn {margin-top:40px;}
.linkBtn {overflow:hidden; text-align:center; margin:0 auto;}
.linkBtn a {color:#fff; background-color:#00a0ea; padding-top:5px; padding-bottom:5px;}
.linkArea>div.link {position:relative; min-height:140px; width:99%; padding-bottom:15px; border-top:2px solid #3c97d3; border-left:1px solid #e4e4e4; border-right:1px solid #e4e4e4; border-bottom:1px solid #e4e4e4;  text-align:justify;}

@media screen and (max-width:1000px) {
    #tabSelect {display:block;}
    #tabSelect a {display:inline-block; width:100%; font-size:17px; line-height:43px; color:#fff; text-align:center;}
    #tabView {display:none;}
    #tabView li {width:100%;}
    .tabSelectGov a {background:url(/img/main/lmenu_v_06.gif) no-repeat 97% 50%;}
    .tabSelectCustomer a {background:url(/img/main/lmenu_v_01.gif) no-repeat 97% 50%;}
    .tabSelectDevWork a {background:url(/img/main/lmenu_v_03.gif) no-repeat 97% 50%;}
    .tabCustomer li.choiceTab a, .tabGov li.choiceTab a, .tabDevWork li.choiceTab a, .tabOpenBusiness li.choiceTab a {border-color:#c1c1c1; background-color:#dddddd; color:#555;}
    .btnArea01 {position:static; width:99%; text-align:center; padding:10px 5px;}
    .btnArea01 .btn a {width:50%;}
.btnArea01 button,input[type="button"],input[type="reset"],input[type="submit"] {
    overflow:visible;-webkit-appearance:button;cursor:pointer}
.btnArea01 button {border:0;background-color:transparent}
    .officeInfo p {float:none;}
    .officeInfo .tel {padding-left:0;}
}

@media screen and (max-width:840px) { 
    .stepDev01 li {min-height:120px;}
}

@media screen and (max-width:640px) { 
    .pageSearch input[type="text"] {width:160px;}
    .declareTit {font-size:15px;}
    .declarePlain {font-size:13px;}
    .declarePlainMarker {padding-left:34px; font-size:13px; background:url(/img/common/declare_mark_02.gif) no-repeat left 6px;}
    .mark01 {font-size:15px;}
    .mark02 {font-size:13px;}
    .btn01 {padding:3px 17px;}
    .stepFour {margin-left:0;}
    .stepFour li {width:100%; margin:0 0 10px 0; min-height:50px;}
    .stepFive {margin-left:0;}
    .stepFive li {width:100%; margin:0 0 10px 0; min-height:50px;}
    .linkArea>div {min-height:auto; width:99%; margin-bottom:20px;}
    .search_box_se {
       /* position: absolute;*/
        text-align: center;
        padding: 30px 0px 0px 0px;
        height: 10px;
        /* background: blue; */
    }
.coun_list ul li a .txt_box {
    padding-top: 25px;
}
.coun_list ul li a .date_box {
    display: block;
    position: static;
    margin-top:10px;
    left: 15px;
    bottom: 15px;
    top: inherit;
    right: inherit;
    width: auto;
}
.coun_list ul li a .mark_box {
    left: 15px;
    top: 23px;
    width: 50px;
    height: 50px;
}
.coun_list ul li a {
    height: 90px;
    min-height: auto;
    padding: 0px 15px 0 75px;
}
.coun_list ul li a .date_box span em {
    width: 47px;
    height: 18px;
    font-size: 11px;
    line-height: 18px;
    background-size: 47px 18px;
    margin-right: 3px;
    color: white;
}

.list_btn_area2 .btn_gray {
    width: 100px;
    height: 35px;
    font-size: 16px;
    line-height: 35px;
    background-color: #556fb5; 
}


}


@media screen and (max-width:500px) { 
    .pageSearch input[type="text"] {width:160px;}
}

@media screen and (min-width:1000px) { 
    .pageSearch input[type="text"] {width:360px;}
    .btnArea .btn a, .btnArea .btn input {min-width:100px;}
    #tabSelect {display:none;}
    #tabView {display:block !important;}
    .tabWrap ul li {float:left;}
    .tabCustomer li.choiceTab a {border-color:#666699; background-color:#666699; color:#fff;}
    .tabGov li.choiceTab a {border-color:#2e6ebb; background-color:#2e6ebb; color:#fff;}
    .tabDevWork li.choiceTab a {border-color:#009999; background-color:#009999; color:#fff;}
    .tabOpenBusiness li.choiceTab a {border-color:#556fb5; background-color:#556fb5; color:#fff;}
    .officeInfo .tel {background:url(/img/common/icon_mark_07.gif) no-repeat left 4px;}
}

.themeBG, .tB02, #gnb > li.fbC > ul, #lnb .submenu, .summaryDesc, .calHead {
    background-color: #3c97d3 !important;
}
.summaryDesc {
    margin-bottom: 30px;
    padding: 7px;
    background: url(/assets/img/bg_pattern_desc.png) repeat;
}
.summaryDesc .innerBox {
    overflow: hidden;
    position: relative;
    padding: 20px;
    background-color: #fff;
}
.summaryDesc .innerBox ul {
    display: block;
    list-style-type: disc;
    margin-block-start: 1em;
    margin-block-end: 1em;
    margin-inline-start: 0px;
    margin-inline-end: 0px;
   /* padding-inline-start: 10px;*/
}
.dot-list > li, .dot {
    padding-left: 13px;
    font-size: 15px;
    background: url(/assets/img/bul_dot.png) 0 8px no-repeat;
    margin: 2px 0;
}
.dot-list > li, .dot {
    background-image: url(/assets/img/bul_dot.png);
}
.dash-list > li, .dash {
    padding-left: 13px;
    background: url(/assets/img/bul_dash.png) 0 12px no-repeat;
}
.themeFC a {
    vertical-align: inherit;
}
.qna_wrap{
    padding-top:0px;
}
.write_table_wrap1{
    border-top: solid 3px #33a5fc;
    border-bottom: solid 3px #33a5fc;
}



}

</style>

    <div id="container">
    <div id="contents" class="qna_wrap">

<div class="summaryDesc">
    <div class="innerBox"><ul class="dot-list">
    <li>공사와 관련된 궁금한 사항을 남겨주시면 즉시 답변해 드리겠습니다.</li>
    <li>단순 문의에 관한 글만 작성해 주시기 바라며 민원 관련 글은 공사 홈페이지 <strong class="themeFC"><a href="http://www.isdc.co.kr/board/qna/boardQnaQuarter.asp?HiddenBbsNo=52" style="color: #3c97d3">[고객의 소리]</a></strong>를 이용하시기 바랍니다.</li>
    <li><strong class="themeFC">개인정보</strong>가 불법적으로 이용되는 것을 막기 위해 이용자께서는 e-메일, 주소, 주민번호, 전화번호 등 개인정보에 관한 사항을 게시하는 것을 주의하시기 바랍니다.</li>
    <li>고객 소통 프로그램의 건전한 운영을 위하여 아래 내용의 게시물은 별도 통지 없이 관리자에 의해 <strong class="themeFC">삭제</strong>되고 있음을 알려드립니다.
    <ul class="dash-list">
            <li>국가 안전이나 보안에 위배되는 경우</li>
    <li>정치적 목적이나 성향이 있는 경우</li>
    <li>특정기관, 단체, 부서를 근거없이 비난하는 경우</li>
    <li>특정인을 비방하거나 명예훼손의 우려가 있는 경우</li>
    <li>영리목적의 상업성 광고, 저작권을 침해할 수 있는 내용</li>
    <li>욕설, 음란물 등 불건전한 내용</li>
    <li>동일인이라고 인정되는 사람이 동일 또는 유사내용을 반복하여 게재하는 도배성 글</li>
    <li>연습성, 오류, 장난성의 내용</li>
    <li>기타 해당 게시판의 취지와 부합하지 않을 경우 등</li>
        </ul>
    </li>
</ul></div>
                        </div>

            <div class="list_btn_area2">
                <a href="/home/qna/write" id="writeBtn" class="btn_gray">글쓰기</a>
            </div>

            <div class="coun_list">
                <ul id="moveLink">

                    <?php foreach ($list as $key => $value): ?>
                        <?php if($value['ps_code'] == 'waiting'):?>
                            <li class=""> <!--class complietion은 답변완료 -->
                        <?php elseif($value['ps_code'] == 'handling'):?>
                            <li class="processing"> <!--class complietion은 답변완료 -->    
                        <?php else:?>
                            <li class="completion"> <!--class complietion은 답변완료 -->    
                        <?php endif;?>
                            <a href="/home/participatory/detail?bid=<?=$value['bid']?>">
                                <span class="mark_box">
                                    <span class="cover"></span>
                                    <span class="mark"><?=$value['pstitle']?></span>
                                </span>
                                     <strong class="txt_box">
                                    <!--     <span class="<?=$value['optitle_color']?>"> <?=$value['optitle']?> </span>  --><span></span>
                                        <?php if($value['new_icon'] == true):?>
                                        <span class="w3-tag w3-medium w3-red">NEW!</span>
                                        <?php endif;?>

                                    <span class="name"><?=$value['title']?></span>
                                </strong>
                                <span class="date_box">
                                    <span><em>등록일</em> <?=$value['create_date']?></span>
                                     <span><em>작성자</em>오준택</span>
                                </span>
                            </a>
                        </li>
                        

                    <?php endforeach?>    
                </ul>
            </div>

                    <div class="search_box_se">
                        <form id="searchFrm" method="GET" action="/home/participatory/share">
                        <input type="hidden" id="type" name="type" value="" />
                        <select id="sf" name="sf" title="검색">
                            <option value="all" >전체</option>
                            <option value="title" >제목</option>
                            <option value="content">내용</option>
                        </select>
                        <input type="text" id="keyword" name="keyword" value="1" required />
                        <input type="submit" value="검색" />
                        </form>
                    </div>       

            <div class="pagination_wrap">
                <div class="pagination">
                    <?=$pagination?>
                </div>
            </div>
        </div> <!-- end : online wrap -->
    </div> <!-- end : container -->
</div>