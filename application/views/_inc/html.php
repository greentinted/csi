<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta HTTP-EQUIV="Pragma" CONTENT="no-cache">
        <meta HTTP-EQUIV="Expires" CONTENT="-1">
        <title><?php echo $title;?></title>
        <link rel="canonical" href="<?php echo base_url();?>" />
        <?php echo $head;?>
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" sizes="16x16 24x24 32x32 64x64">

        <title>성남도시개발공사 고객소통</title>
        
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />

        <link rel="shortcut icon" href="/assets/images/favicon.ico">
        <link rel="stylesheet" type="text/css" href="/assets/css/main1.css?=v122" />
        <link rel="stylesheet" type="text/css" href="/assets/css/w3-theme-black.css" />
        <link rel="stylesheet" type="text/css" href="/assets/css/w3.css?=v1223" />
        <link href="/vendors/bootstrap/dist/css/bootstrap.css?=v12" rel="stylesheet">
        <link rel="stylesheet" href="/assets/summernote/summernote-bs4.css" type="text/css" />

            <!-- Custom Theme Style -->

        <script src="/assets/js/jquery-1.12.4.min.js"></script>
        <script src="/assets/js/jquery-ui.min.js"></script>
        <script src="/assets/js/jquery.easing.min.js"></script>
        <script src="/assets/js/jquery.cycle2.min.js?v=1"></script>
        <script src="/assets/js/jquery.cycle2.swipe.min.js?v=1"></script>
        <script src="/assets/js/jquery.backgroundpos.min.js"></script>
        <script src="/assets/js/media.match.min.js"></script>
        <script src="/assets/js/cookie.js"></script>
        <script src="/assets/js/ui.js"></script>
        <script src="/assets/js/common.js"></script>
        <script src="/assets/js/fontawesome.js"></script>
        <script src="/assets/summernote/summernote-bs4.js"></script>
        <script src="/assets/summernote/lang/summernote-ko-KR.js"></script>

        <script src="/vendors/bootstrap/dist/js/bootstrap.min.js?ver=1" rel="stylesheet"></script>        

        <script src="/assets/js/blockui.js?ver=1"></script>
        <script src="/assets/js/csi.js?ver=12"></script>        
         <!--  <script src="/assets/js/obfuscated_csi.js?ver=1"></script>  -->       
    </head>

    <body id="<?php echo str_replace('/', '-', $this->uri->ruri_string());?>">
        <?php echo $header;?>
        <section><?php echo $section;?></section>
    </body>
</html>

