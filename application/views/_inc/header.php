   <div id="wrap">
        <header id="header"> 
            <div class="inner_header">
                <h1 class="logo" style="z-index:10;"><a href="/home/map"><img src="/assets/img/logo.png" alt="성남도시개발공사" /></a></h1>
                <a href="#self" class="all_menu_opener"><img src="/assets/img/btn_allmenu.png" alt="전체메뉴열기" /></a>
            </div><!-- end : inner_header -->
                                
            <div class="gnb_section">            
                <ul class="global_menu">
                    <?php if($this->session->userdata('is_login')):?>                 
                        <li><a href="/home/login"><?=$this->session->userdata('real_name')?>님</a></li>
                        <li><a href="/base/logout">로그아웃</a></li>
                        <li><a href="/home/myinfo" id="b_favorite" class="add_Favorite">마이페이지</a></li>                
<!--                          <li><a href="/home/login">로그인</a></li>
                        <li><a href="join1.html">회원가입</a></li>
 -->    
                    <?php else:?>
                        <li><a href="/home/login">로그인</a></li>
                        <li><a href="/home/join">회원가입</a></li>
                    <?php endif?>
                </ul>

                <div class="gnb_wrap">
                    <ul id="gnb">

                        <li class="cate2">
                            <a href="/home/qna" class="btn_depth1">묻고답하기</a>
<!--                             <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">자주하는질문</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="#">지도서비스란?</a></li>
                                        <li><a href="#">시민참여방법</a></li>
                                        <li><a href="#">고객소통 참여방법</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </li>

                        <li class="cate3">
                            <a href="#" class="btn_depth1">공개자료실</a>
                            <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">공개자료실</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="/home/information/covid19">코로나현황</a></li>
                                        <li><a href="/home/information">공사소식</a></li>
                                        <li><a href="/home/participatory/share">성과공유</a></li>
                                    </ul>
                                </div>
                            </div>
                        </li>


   
                        <li class="cate4">
                            <a href="/home/participatory/index" class="btn_depth1">주민참여위원회</a>
<!--                             <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">주민참여위원회</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="/home/participatory/index">주민참여위원회</a></li>
                                        <li><a href="/home/participatory/index">고객소통</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </li>
                        <li class="cate5">
                            <a href="/home/evaluation" class="btn_depth1">시민서비스평가단</a>
<!--                             <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">시민서비스평가단</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="/home/evaluation">평가참여하기</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </li>

<!--                         <li class="cate4 ">
                            <a href="/home/participatory/share" class="btn_depth1">성과공유</a> -->
<!--                             <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">성과공유</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="/home/participatory/share">성과공유게시판</a></li>
                                    </ul>
                                </div>
                            </div> -->
                        </li>

                        <li class="cate1">
                            <a href="/home/map" class="btn_depth1">지도서비스</a>
                            <div class="dns_wrap">
                                <div class="dns">
                                    <h3 class="dns_tit">지도서비스</h3>
                                    <ul class="depth2_wrap">
                                        <li><a href="/home/map">시설물정보</a></li>
                                        <li><a href="/home/map">미세먼지정보</a></li>                       
                                    </ul>
                                </div>
                            </div>
                        </li>

                        <li class="cate8">
                            <a href="/admin" class="btn_depth1">자주하는질문</a>
                        </li>        
                        <li class="cate7">
                            <a href="/admin" class="btn_depth1">관리자페이지</a>
                        </li>                  
                        <?php /*

                       <?php if($this->session->userdata('is_login')):?>  
                        <?php if($this->session->userdata('admin_type') != 'user'):?>     

                        <li class="cate6">
                            <a href="/admin" class="btn_depth1">관리자페이지</a>
                        </li>                        
                        <?php endif?>
                        <?php endif?>
                        */ ?>

                    </ul>                   
                </div> 
               <a href="#self" class="mobile_gnb_close"><img src="/assets/img/img_gnb_close.gif" alt="주메뉴 닫기" /></a>
            </div><!-- end : gnb_section -->
        </header><!-- end : header -->




<?php /*
<div class="nav_box">
    <nav>
        <div class="ul_box">
            <ul class="user">
                <li><a class="brand" href="/"><img src="" alt="logo" /></a></li>
                <li>
                    <div class="search_box">
                        <div>
                            <div class="tbl_border_box">
                                <input type="text" class="search_bar" placeholder="" />
                                <img src="" class="search_icon" />
                            </div>
                        </div>
                    </div>
                </li>
                <li>
                    <a href="/home/login/index" class="login"><span>로그인</span></a>
                    <img src="" class="phone_icon" />
                </li>
            </ul>
        </div>
        <div class="ul_box">
            <ul class="tab_menu">
                <li <?php echo ($this->uri->uri_string() == '' ) ? 'class="active"': null;?>><a href="/">지도서비스</a></li>
                <li <?php echo ($this->uri->uri_string() == 'real') ? 'class="active"': null;?>><a href="/real">시민참여</a></li>
                <li <?php echo ($this->uri->uri_string() == 'admin') ? 'class="active"': null;?>><a href="/admin">어드민</a></li>
            </ul>
        </div>
    </nav>
    
</div>

*/ ?>