        <div id="container">
            <div class="sub_visual_wrap myinfo">
                <div class="inner_box">
                    <div class="location">
                        <a href="/"><i class="fa fa-home"></i></a> &nbsp;&gt;&nbsp;
                        <span>마이페이지</span> &nbsp;&gt;&nbsp;<strong>내 정보 수정</strong>
                    </div>
                    <h3 class="tit">내 정보 수정</h3>
                    <p>회원정보 수정이 가능합니다.</p>
                </div>
            </div>

           <div id="contents" class="mypage_wrap">
                <div class="sep_tab_box2">
                    <div class="tab_style1 three">
                        <ul>
                            <li class="on"><a href="#self">내정보수정</a></li>
                            <li><a href="/home/participatory"> 
                            나의참여</a></li>
                        </ul> 
                    </div>
                </div>

<style type="text/css">
    
    input:focus::-webkit-input-placeholder, textarea:focus::-webkit-input-placeholder { /* WebKit browsers */ color:transparent; } input:focus:-moz-placeholder, textarea:focus:-moz-placeholder { /* Mozilla Firefox 4 to 18 */ color:transparent; } input:focus::-moz-placeholder, textarea:focus::-moz-placeholder { /* Mozilla Firefox 19+ */ color:transparent; } input:focus:-ms-input-placeholder, textarea:focus:-ms-input-placeholder { /* Internet Explorer 10+ */ color:transparent; }

    @media screen and (max-width: 650px) {

    *           {margin:0; padding:0; box-sizing: border-box !important; }
    }

</style>

                <form id="postFrm" method="POST" enctype="multipart/form-data" action="/home/myinfo/save" onsubmit="return checkValue(this)">
                
                <div class="write_table_wrap1">
                    <table class="write_table" cellpadding="0" cellspacing="0">
                        <tr>
                            <th><label for="email">* 이메일</label></th>
                            <td>
                                <div class="mail_box">
                                    <div class="box1"><input type="text" id="amail_0" name="amail_0" value="<?=$email[0]?>" />
                                    </div>
                                    <div class="box2"><input type="text" id="amail_1"  name="amail_1" value="<?=$email[1]?>"/></div>
                  <!--                   <select id="mMail3" name="mMail3" value="naver.com">
                                        <option value="isdc.co.kr">isdc.co.kr</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="gmail.com">gmail.com</option>
                                    </select> -->
                                </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPw">* 비밀번호</label></th>
                            <td>
                                <input type="password" id="passwd_0" name="passwd_0" class="w326" placeholder="영문, 숫자 포함 6자리 이상을 입력해주세요." value="" />
                                <span id="pwMsg" class="red"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPw2">* 비밀번호 확인</label></th>
                            <td>
                                <input type="password" id="passwd_1" name="passwd_1" class="w326" value="" />
                                <span id="pwMsg2" class="red"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mName">* 이름</label></th>
                            <td>
                                <input type="text" id="aname" name="aname" class="w229" value="<?=$name?>" readonly />
                                <span id="pwMsg3" class="red">이름은 변경할 수 없습니다.</span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPhone">* 연락처</label></th>
                            <td>
                                <div class="phone_box">
                                    <select name="mPhone1" id="mPhone1" value="<?=$phone[0]?>">
                                        <option value="010">010</option>
                                        <option value="011">011</option>
                                        <option value="016">016</option>
                                        <option value="017">017</option>
                                        <option value="018">018</option>   
                                        <option value="019">019</option>
                                    </select>
                                    <div class="box1"><input type="text" id="mPhone2" name="mPhone2" class="numberonly" maxlength="4" value="<?=$phone[1]?>" /></div>
                                    <div class="box2"><input type="text" id="mPhone3" name="mPhone3" class="numberonly" maxlength="4" value="<?=$phone[2]?>" /></div>
                                </div>
                            </td>
                        </tr>
       <!--                  <tr>
                            <th><label for="mMailService1">메일링 서비스</label></th>
                            <td>
                                <label for="mMailService1"><input type="radio" id="mMailService1" name="mMailService" value="Y"  /> 수신</label>
                                <label for="mMailService2"><input type="radio" id="mMailService2" name="mMailService" value="N" checked="checked" /> 수신거부</label>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mSmsService1">SMS 서비스</label></th>
                            <td>
                                <label for="mSmsService1"><input type="radio" id="mSmsService1" name="mSmsService" value="Y"  /> 수신</label>
                                <label for="mSmsService2"><input type="radio" id="mSmsService2" name="mSmsService" value="N" checked="checked" /> 수신거부</label>
                            </td>
                        </tr> -->
                    </table>
                </div>

                <div class="write_btn_area">
                    <div class="fl_c">
                        <!-- <a href="#del" id="delbtn" class="btn_gray">탈퇴</a> -->
                        <input type="submit" class="btn_blue" value="수정" />
                        <a href="#self" onclick="history.back(-1);" id="cancelbtn" class="btn_dark">취소</a>
                    </div>
                </div>
                </form>
            </div><!-- id : contents -->

<script type="text/javascript">
    // 비밀번호 미입력시 경고창

    function checkValue(f){

        if(!$("#passwd_0").val()){
            alert("비밀번호를 입력하세요.");
            $("#passwd_0").focus();
            return false;
        }

        if(!/^[A-Za-z0-9]{6,}$/.test($("#passwd_0").val())) {
            alert('숫자와 영문자 포함 6자 이상 입력하세요.');
            $("#passwd_0").focus();
            return false;
        }
        var chk_num = $("#passwd_0").val().search(/[0-9]/g);
        var chk_eng = $("#passwd_0").val().search(/[a-z]/ig);
        if(chk_num < 0 || chk_eng < 0 )
        {
            alert('비밀번호는 숫자와 영문자를 혼용하여야 합니다.')
            $("#passwd_0").focus();
            return false;
        }


        if($("#passwd_0").val() != $("#passwd_1").val() ){
            document.getElementById("pwMsg2").innerHTML ="비밀번호를 동일하게 입력하세요."
            //alert("비밀번호를 동일하게 입력하세요.");
            $("#passwd_1").focus();
            return false;
        }

        if(!$("#aname").val()){
            alert("이름을 입력하세요.");
            $("#aname").focus();
            return false;
        }
    }

 
</script>
