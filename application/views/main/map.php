<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>다양한 이미지 마커 표시하기</title>
    <style>
.wrap {position: absolute;left: 0;bottom: 40px;width: 288px;height: 132px;margin-left: -144px;text-align: left;overflow: hidden;font-size: 12px;font-family: 'Malgun Gothic', dotum, '돋움', sans-serif;line-height: 1.5;}
    .wrap * {padding: 0;margin: 0;}
    .wrap .info {width: 286px;height: 120px;border-radius: 5px;border-bottom: 2px solid #ccc;border-right: 1px solid #ccc;overflow: hidden;background: #fff;}
    .wrap .info:nth-child(1) {border: 0;box-shadow: 0px 1px 2px #888;}
    .info .title {padding: 5px 0 0 10px;height: 30px;background: #eee;border-bottom: 1px solid #ddd;font-size: 18px;font-weight: bold;}
    .info .close {position: absolute;top: 10px;right: 10px;color: #888;width: 17px;height: 17px;background: url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/overlay_close.png');}
    .info .close:hover {cursor: pointer;}
    .info .body {position: relative;overflow: hidden;}
    .info .desc {position: relative;margin: 13px 10px 0 90px;height: 75px;}
    .desc .ellipsis {overflow: hidden;text-overflow: ellipsis;white-space: nowrap;}
    .desc .jibun {font-size: 11px;color: #888;margin-top: -2px;}
    .info .img {position: absolute;top: 6px;left: 5px;width: 73px;height: 71px;border: 1px solid #ddd;color: #888;overflow: hidden;}
    .info:after {content: '';position: absolute;margin-left: -12px;left: 50%;bottom: 0;width: 22px;height: 12px;background: url('')}
    .info .link {color: #5085BB;}        
#mapwrap{position:relative;overflow:hidden;}
.category, .category *{margin:0;padding:0;color:#000;}   
.category {position:absolute;overflow:hidden;top:10px;left:10px;width:150px;height:50px;z-index:10;border:1px solid black;font-family:'Malgun Gothic','맑은 고딕',sans-serif;font-size:12px;text-align:center;background-color:#fff;}
.category .menu_selected {background:#FF5F4A;color:#fff;border-left:1px solid #915B2F;border-right:1px solid #915B2F;margin:0 -1px;} 
.category li{list-style:none;float:left;width:50px;height:45px;padding-top:5px;cursor:pointer;} 
.category .ico_comm {display:block;margin:0 auto 2px;width:22px;height:26px;background:url('/assets/img/category_0.png') no-repeat;} 
.category .ico_coffee {background-position:-10px 0;}  
.category .ico_store {background-position:-10px -36px;}   
.category .ico_carpark {background-position:-10px -72px;} 

.overlaybox {position:relative;width:350px; border:1px solid black; background-color:white; background-size:500px 500px;padding:15px 10px;}
.overlaybox div, ul {overflow:hidden;margin:0;padding:0;}
.overlaybox li {list-style: none;}
.overlaybox .boxtitle {color:#000;font-size:20px;;width:250px;font-weight:bold;background: url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/arrow_white.png')  no-repeat right 120px center;margin-bottom:8px;}
.overlaybox .first {position:relative;width:448px; background-size:348px; margin-bottom:8px;}
.first .text {color:#fff;font-weight:bold;}
.first .triangle {position:absolute;width:48px;height:48px;top:0;left:0;background: url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/triangle.png') no-repeat; padding:6px;font-size:18px;}
.first .movietitle {position:absolute;width:50%;bottom:0;background:rgba(0,0,0,0.4);padding:7px 15px;font-size:20px; text-align: center;}
/*---------------------------- 오버레이 CSS 영역 -----------------------------*/
.overlaybox ul {width:448px;}
.overlaybox li {position:relative;margin-bottom:2px;background:white;padding:5px 10px;color:#aaabaf;line-height: 1;text-align:left;}
.overlaybox li span {display:inline-block;}
.overlaybox li .number {font-size:16px;font-weight:bold;}
.overlaybox li .title {font-size:16px;}
.overlaybox ul .arrow {position:absolute;margin-top:8px;right:25px;width:5px;height:3px;background:url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/updown.png') no-repeat;} 
.overlaybox li .up {background-position:448px -40px;}
.overlaybox li .summary {height:80px; float: left;border:1;}
.overlaybox li .down {background-position:0 -60px;}
.overlaybox li .count {position:absolute;margin-top:5px;right:15px;font-size:10px;}
.overlaybox li:hover {color:#fff;background:#d24545;}
.overlaybox li:hover .up {background-position:0 0px; text-align: left;}
.overlaybox li:hover .down {background-position:0 -20px;} 
.overlaybox .close {position: absolute;top: 10px;right: 10px;color: #888;width: 17px;height: 17px;background: url('http://t1.daumcdn.net/localimg/localimages/07/mapapidoc/overlay_close.png');}
.overlaybox .close:hover {cursor: pointer;}  


</style>
</head>
<body>

<!-- Sidebar -->


<nav class="w3-sidebar w3-bar-block w3-collapse w3-large w3-theme-d1 w3-animate-left" id="mySidebar">

        <div class="search_box_">
          
          <div class="sch_box_in">

            <form id="" role="form" method="GET" action="/home/map/index">
                <select id="gu" name="did" title="검색">
                    <option value="0" <?=return_select($did ,0)?>>지역구</option>
                    <option value="1" <?=return_select($did ,1)?>>수정구</option>
                    <option value="3" <?=return_select($did ,3)?>>중원구</option>
                    <option value="2" <?=return_select($did ,2)?>>분당구</option>
                </select>

                <select id="fac" name="ftype" title="검색">
                    <option value="" <?=return_select($ftype ,'')?>>시설종류</option>
                    <option value="SC" <?=return_select($ftype ,'SC')?>>운동장</option>
                    <option value="LI" <?=return_select($ftype ,'LI')?>>도서관</option>
                    <option value="PL" <?=return_select($ftype ,'PL')?>>주차장</option>
                </select>
              <div class="sch_box_bottom">
                <input type="text" id="keyword" name="keyword" value="<?=$keyword?>" />
                <button type="submit" class="w3-button w3-blue w3-hover-grey w3-padding-large" value="<?=$keyword?>">검색</button>
              </div>
            </form>


            <?php /*
            <select name="did" id="gu">
              <option value="" selected="selected">지역구</option>
              <option value="1">수정구</option>
              <option value="3">중원구</option>
              <option value="2">분당구</option>
            </select>
            <select name="ftype" id="fac">
              <option value="" selected="selected">시설종류</option>
              <option value="SC">운동장</option>
              <option value="LI">도서관</option>
              <option value="PL">주차장</option>
            </select>
            <div class="sch_box_in">
              <input type="text" name="" placeholder="Search..">
              
            </div>
            */?>


          </div>
        </div>

    <div class="side_info">
    
        
    <?php foreach ($list as $key => $value): ?>
    <div class="w3-cell-row">

        <div class="w3-container w3-white w3-cell">
         <table>
            <tr><td><a href="#" onclick="panTo_(<?=$value['lat']?>, <?=$value['lng']?>); return false;">
          <b class="w3-hover-text-blue"><?=$value['title']?></b></a>
          </td></tr>
          <tr><td>
          체육시설
          </td></tr>
        <tr><td>
          <i class="fa fa-map-marker-alt"> 주소: <?=$value['address']?> </i>
          </td></tr>
        <tr><td>
          <i class="fa fa-phone-square"> 전화:  <?=$value['contact']?></i>
          </td></tr>
         </table>
        </div>
        <div class="w3-container w3-white w3-cell">
            <?php if(isset($value['imgs'][0])):?>
            <img src="/<?=$value['imgs'][0]['path']?>/<?=$value['imgs'][0]['filename']?>" class="w3-round">
            <?php else:?>
            <img src="" class="w3-round">

            <?php endif;?>

        </div>
        
    </div>
    <?php endforeach ?>

<!--         <div class="w3-bar-item w3-button w3-white w3-hover-blue" onclick="myAccFunc()" style="width: 350px; font-size: 15px;">
          상세보기 <i class="fa fa-caret-down"></i></div>
        <div id="demoAcc" class="w3-hide w3-white w3-card-4" style="width: 350px">
            <a href="#" class="w3-bar-item w3-button" style="font-size: 15px; ">시민참여 바로가기</a>
            <a href="#" class="w3-bar-item w3-button" style="font-size: 15px; ">시설물 정보 확인하기</a>
        </div> -->


</nav> <!-- 사이드바 끝 -->


<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<div id="container">
    <div class="w3-main" style="margin-left:400px">
  
    <!-- 모바일 검색영역 -->
        <div class="search_box_body">
            <form id="" role="form" method="GET" action="/home/map/index">
                <select id="did" name="did" title="검색">
                    <option value="0" selected="selected">지역구</option>
                    <option value="1" >수정구</option>
                    <option value="3" >중원구</option>
                    <option value="2" >분당구</option>
                </select>

                <select id="siseol" name="ftype" title="검색">
                    <option value="" selected="selected">시설종류</option>
                    <option value="SC" >운동장</option>
                    <option value="LI" >도서관</option>
                    <option value="PL" >주차장</option>
                </select>
                <input type="text" id="keyword" name="keyword" value="<?=$keyword?>" />
                <input type="submit"  value="검색" />
            </form>
        </div>
    <!-- 모바일 검색영역 끝 -->

      <div id="mapwrap"> 
          <!-- 지도가 표시될 div -->
          <div id="map" style="width:100%;height:900px;"></div>
          <!-- 지도 위에 표시될 마커 카테고리 -->
          <div class="category">
              <ul>
                  <li id="facilityMenu" onclick="changeMarker('facility')">
                      <span class="ico_comm ico_coffee"></span>
                      시설물
                  </li>
                  <li id="dustMenu" onclick="changeMarker('dust')">
                      <span class="ico_comm ico_store"></span>
                      미세먼지
                  </li>
                  <li id="maskMenu" onclick="changeMarker('mask')">
                      <span class="ico_comm ico_carpark"></span>
                      마스크
                  </li>

              </ul>
          </div>
      </div> <!-- end:: mapwrap -->
    </div> <!-- end:: w3-main -->
</div> <!-- end:: container -->


<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=eab7bf8f8e99c2f64f538dd3596492be"></script>
<script>


var mapContainer = document.getElementById('map'), // 지도를 표시할 div  
    mapOption = { 
        center: new kakao.maps.LatLng(37.41011800, 127.12040890), // 지도의 중심좌표 
        level: 5 // 지도의 확대 레벨 
    }; 

var map = new kakao.maps.Map(mapContainer, mapOption); // 지도를 생성합니다

// 커피숍 마커가 표시될 좌표 배열입니다
var facilityList = [ 
    //new kakao.maps.LatLng(37.49754540521486, 127.02546694890695)                
];

// 편의점 마커가 표시될 좌표 배열입니다
var dustList = [
    
];


var maskList = [
    
];



// var   iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

// // 인포윈도우를 생성합니다
// var infowindow = new kakao.maps.InfoWindow({
//     content : iwContent,
//     removable : iwRemoveable
// });


 var infowindow = new kakao.maps.InfoWindow({
     content : 'This is a Sample',
     removable : true ,
 });   

//console.log(infowindow);

var markerImageSrc = '/assets/img/category_4.png';  // 마커이미지의 주소입니다. 스프라이트 이미지 입니다
    scMarkers = [], // 커피숍 마커 객체를 가지고 있을 배열입니다
    maskMarkers = [],
    dustMarkers = []; // 주차장 마커 객체를 가지고 있을 배열입니다
var markerDustImageSrc = '/assets/images/dust_m.png';  // 마커이미지의 주소입니다. 스프라이트     
var markerMaskImageSrc = '/assets/images/mask.png';  // 마커이미지의 주소입니다. 스프라이트     


    var jbAry = new Array();
    jbAry[0] = '경기도 성남시 분당구';
    jbAry[1] = '경기도 성남시 수정구';
    jbAry[2] = '경기도 성남시 중원구';



    $.ajax({  
            url:"/home/map/ajax_getdata",  
            method:"post",  
            data:{title:''},  
            dataType: 'json',  
            success:function(resData) {  

                //console.log(resData);


           for(var i in jbAry) {

                $.ajax({  
                url:"https://8oi9s0nnth.apigw.ntruss.com/corona19-masks/v1/storesByAddr/json",  
                method:"GET",  
                data:{address: jbAry[i]},  
                dataType: 'json',  
                success:function(result) {  

                    

                    for ( var i  = 0; i < result.stores.length; i++){
                        var data = result.stores[i];
                        //console.log('data' , data);
                        var status = '없음';
                        if(data.remain_stat == 'empty'){
                            status = '없음';
                        } else if(data.remain_stat == 'few'){
                            status = '소량';
                        }
                        else if(data.remain_stat == 'some'){
                            status = '부족';
                        }                        
                        else if(data.remain_stat == 'plenty'){
                            status = '충분';
                        }                        

                        var content = '<div class="wrap_">' + 
                        '    <div class="info">' + 
                        '        <div class="title">' + 
                        '            '+data.name+'' + 
                        
                        '        </div>' + 
                        '        <div class="body">' + 
                        '            <div class="img">' +
                        '                <img src="http://cfile181.uf.daum.net/image/250649365602043421936D" width="73" height="70">' +
                        '           </div>' + 
                        '            <div class="desc">' + 
                        '                <div class="ellipsis">'+ status + 
                        '                <div class="jibun ellipsis">'+ data.addr +  
                        '                <div>입고시간 '+data.stock_at+'</div>' + 
                        '            </div>' + 
                        '        </div>' + 
                        '    </div>' +    
                        '</div>'; 

                        var arr = {title : data.name, 
                                  address : data.address,
                                  lat : data.lat,
                                  lng : data.lng ,
                                  remain_stat : data.remain_stat , 
                                  stock_at:data.stock_at ,
                                  code:data.code ,
                                  stock_at:data.stock_at ,
                                  type:'mask' ,
                                  o_type:data.type,
                                  content:content,

                                  pos : new kakao.maps.LatLng(data.lat, data.lng)
                        };

                        if(arr.type == 'mask'){
                            maskList.push(arr);
                        }  
                    }
                    createMaskMarkers();
                }
             });
            }


                for(var i = 0; i < resData.list.length; i++){

                    var data = resData.list[i];
                    //console.log('data' , data);
                    var content = '<div class="wrap_">' + 
                    '    <div class="info">' + 
                    '        <div class="title">' + 
                    '            '+data.title+'' + 
                    
                    '        </div>' + 
                    '        <div class="body">' + 
                    '            <div class="img">' +
                    '                <img src="'+data.filename+'" width="73" height="70">' +
                    '           </div>' + 
                    '            <div class="desc">' + 
                    '                <div class="ellipsis">'+ data.address + 
                    '                <div class="jibun ellipsis">' + data.contact + 
                    '                <div><a href="/home/participatory/" target="_blank" class="link">[시설물 의견 남기기]</a></div>' + 
                    '            </div>' + 
                    '        </div>' + 
                    '    </div>' +    
                    '</div>';                    

                    var arr = {title : data.title, 
                              address : data.address,
                              lat : data.lat,
                              lng : data.lng ,
                              fut_type : data.fut_code , 
                              type:data.type ,
                              type:data.type ,
                              content:content,
                              pos : new kakao.maps.LatLng(data.lat, data.lng)
                    };


                    if(data.type == 'facility'){
                        facilityList.push(arr);
                    }  
                }
                for(var i = 0; i < resData.mlist.length; i++){

                    var data = resData.mlist[i];
                    //console.log('data' , data);
                  var content = '<div class="overlaybox">';
                  content +=    '    <div class="boxtitle">'+data.title+'<div class=""  title="닫기"></div>';
                  content += '        </div>';
                  content +=    '    <ul>';
                  
                  if( data.pm25 < 15){
                    content +=    '        <li class="up" style="background-color:blue;width: 332px;">';
                    content +=        '            <span style="font-size:25px;bold;"><i class="far fa-laugh fa-2x"></i>  ('+Math.floor(data.pm25)+' ) - 좋음</span>';

                  } else if(data.pm25 > 15 && data.pm25 < 35){  
                    content +=    '        <li class="up" style="background-color:green;width: 332px;">';
                    content +=        '            <span style="font-size:25px;bold;"><i class="far fa-smile fa-2x"></i>  ('+Math.floor(data.pm25)+' ) - 보통</span>';

                  } else if(data.pm25 > 35 && data.pm25 < 75){
                    content +=    '  <li class="up" style="background-color:yellow;width: 332px;" >';
                    content +=    ' <i class="far fa-meh fa-2x" sytle="text-align:left;"></i><span style="font-size:25px;bold;margin-left:10px;"> ( '+Math.floor(data.pm25)+' ) - 나쁨(건강 주의)</span>';

                  } else if(data.pm25 > 75){
                    content +=    '        <li class="up" style="background-color:red;width: 332px;"> ';
                    content +=        '            <span style="font-size:25px;bold;"><i class="far fa-frown fa-2x"></i>  ('+Math.floor(data.pm25)+' ) - 매우나쁨(건강에 매우 안좋음)</span>';

                  } else {
                    content +=    '        <li class="up">';
                    content +=        '            <span style="font-size:25px;bold;width: 332px;"><i class="far fa-surprise fa-2x"></i>  ( '+Math.floor(data.pm25)+' ) - 측정결과 없음</span>';

                  }
                  
                  content +=        '        </li>';

                  content +=    '        <li class="summary" style="height:65px;width:330px;margin-top:5px;text-align:center;border: 1px solid #ada8a8;">';
                  content +=        '            <span style=""><i class="fas fa-thermometer-half fa-2x"></i><div>(온도)</div><div>('+Math.floor(data.temp)+'C)</div></span>';                  


                  content +=        '            <span style="margin-left:25px"><i class="fas fa-tint fa-2x"></i><div>(습도)</div><div>('+Math.floor(data.humi)+'%)</div></span>';
                  content +=        '            <span style="margin-left:25px"><i class="fas fa-volume-up fa-2x"></i><div>(진동)</div><div>(30db)</div></span>';
                  content +=        '            <span style="margin-left:25px"><i class="fas fa-fire-alt fa-2x"></i><div>(화재)</div><div>(off)</div></span>';




                  content +=    '        </li>';


                  content +=    '    </ul>';

                  content +=    '    <div class="first">';
                  content +=    '        <div class="text" style="color:black;text-align:center; width:330px;font-size:22px;"><a href="/home/participatory">[시설물의견 남기기]</a></div>';
                  content +=    '    </div>';


                  

                  content +=    '              </ul>';
                    var arr = {title : data.title, 
                              address : data.address,
                              lat : data.lat,
                              lng : data.lng ,
                              fut_type : data.fut_code , 
                              type:data.type ,
                              content:content,
                              pm25:data.pm25,
                              pm10:data.pm10, 

                              pos : new kakao.maps.LatLng(data.lat, data.lng)
                    };


                    if(data.type == 'dust'){
                        dustList.push(arr);
                    }
                }



                createFacilityMarkers(); // 커피숍 마커를 생성하고 커피숍 마커 배열에 추가합니다
                createDustMarkers(); // 편의점 마커를 생성하고 편의점 마커 배열에 추가합니다
                
                
                changeMarker('facility'); // 지도에 커피숍 마커가 보이도록 설정합니다    




            }
    });



    


// 마커이미지의 주소와, 크기, 옵션으로 마커 이미지를 생성하여 리턴하는 함수입니다
function createMarkerImage(src, size, options) {
    var markerImage = new kakao.maps.MarkerImage(src, size, options);
    return markerImage;            
}

// 좌표와 마커이미지를 받아 마커를 생성하여 리턴하는 함수입니다
function createMarker(position, image) {
    var marker = new kakao.maps.Marker({
        position: position,
        image: image,

        clickable: true
    });

                // 마커에 클릭이벤트를 등록합니다

    
    return marker;  
}   

   
// 커피숍 마커를 생성하고 커피숍 마커 배열에 추가하는 함수입니다
function createMaskMarkers() {

    //console.log('maskList'  , maskList);
    
    for (var i = 0; i < maskList.length; i++) {  

        var data = maskList[i];


        var remain_stat = data.remain_stat;
        var text = '';
        //var fc_type = string.substring(2,4);

        //console.log('remain_stat : ' , remain_stat);
        var imageSize , imageOptions;

        if(remain_stat == 'empty'){
            text = '없음';
            imageSize = new kakao.maps.Size(20, 20),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(0, 0),    
                    spriteSize: new kakao.maps.Size(20, 80)  
                };     
            
            // 마커이미지와 마커를 생성합니다


        } else if(remain_stat == 'few'){
            text = '소량';
            imageSize = new kakao.maps.Size(20, 20),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(0, 20),    
                    spriteSize: new kakao.maps.Size(20, 80)  
                };     
            


        } else if(remain_stat == 'some'){
            text = '부족';
            imageSize = new kakao.maps.Size(20, 20),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(0, 40),    
                    spriteSize: new kakao.maps.Size(20, 80)  
                };     
            

        } else if(remain_stat == 'plenty'){
            text = '충분';
            imageSize = new kakao.maps.Size(20, 20),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(0, 60),    
                    spriteSize: new kakao.maps.Size(20, 80)  
                };     
        }


        var markerImage = createMarkerImage(markerMaskImageSrc, imageSize, imageOptions),    
            marker = createMarker(maskList[i].pos, markerImage);  



            
            // 생성된 마커를 커피숍 마커 배열에 추가합니다
        maskMarkers.push(marker);


        




     // 마커에 click 이벤트를 등록합니다
        //kakao.maps.event.addListener(marker, 'click', clickListner(map, marker, data , infoWindow));
        //kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
        //kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));        


        kakao.maps.event.addListener(marker, 'click', makeOverListener(map, marker, infowindow ,data.content));
        //kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
        //makeOutListener(infowindow);

        // kakao.maps.event.addListener(marker, 'click', function() {
        //     // 마커 위에 인포윈도우를 표시합니다
        //     //console.log('data : ' , data);
        //     //infoWindow.setPosition(data.pos);          
        //     infowindow.open(map, marker);          
        // });
    }     
}


function closeOverlay(){
    overlay.close();

}

// 인포윈도우를 표시하는 클로저를 만드는 함수입니다 
function makeOverListener(map, marker, infowindow , content) {
    return function() {
        // console.log(content);
        infowindow.setContent(content);
        infowindow.open(map, marker);
    };
}

function pantoInfowindow(map, marker, moveLatLon, iwContent) {
 // console.log(iwContent);        
        infowindow.setContent(iwContent);
        infowindow.open(map, marker);
}

// 인포윈도우를 닫는 클로저를 만드는 함수입니다 
function makeOutListener(infowindow) {
    return function() {
        infowindow.close();
    };
}
function markerClick(map, marker, data , window){
    window.open(map, marker);    
}

function clickListner(map, marker, data , window){
     //infowindow.open(map, marker);  
     //   iwRemoveable = true; // removeable 속성을 ture 로 설정하면 인포윈도우를 닫을 수 있는 x버튼이 표시됩니다

     //console.log('clickListner' , data);

    //InfoWindow.setPosition();

        // 인포윈도우를 생성합니다

        window.open(map, marker);
}

// 커피숍 마커들의 지도 표시 여부를 설정하는 함수입니다
function setFacilityMarkers(map) {        
    for (var i = 0; i < scMarkers.length; i++) {  
        scMarkers[i].setMap(map);
    }        

}

function createFacilityMarkers(){
    for (var i = 0; i < facilityList.length; i++) {  

        var data = facilityList[i];


        var string = data.fut_type;
        var fc_type = string.substring(2,4);

        //console.log('fc_type : ' , fc_type);
        var imageSize , imageOptions;

        if(fc_type == 'SC'){
            imageSize = new kakao.maps.Size(22, 26),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(10, 0),    
                    spriteSize: new kakao.maps.Size(36, 98)  
                };     
            
            // 마커이미지와 마커를 생성합니다


        } else if(fc_type == 'LI'){
            imageSize = new kakao.maps.Size(22, 26),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(10, 36),    
                    spriteSize: new kakao.maps.Size(36, 98)  
                };     
            


        } else if(fc_type == 'PL'){
            imageSize = new kakao.maps.Size(22, 26),
                imageOptions = {  
                    spriteOrigin: new kakao.maps.Point(10, 72),    
                    spriteSize: new kakao.maps.Size(36, 98)  
                };     
            

        }

        var markerImage = createMarkerImage(markerImageSrc, imageSize, imageOptions),    
            marker = createMarker(facilityList[i].pos, markerImage);  



            
            // 생성된 마커를 커피숍 마커 배열에 추가합니다
        scMarkers.push(marker);


        




     // 마커에 click 이벤트를 등록합니다
        //kakao.maps.event.addListener(marker, 'click', clickListner(map, marker, data , infoWindow));
        //kakao.maps.event.addListener(marker, 'mouseover', makeOverListener(map, marker, infowindow));
        //kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));        


        kakao.maps.event.addListener(marker, 'click', makeOverListener(map, marker, infowindow ,data.content));
        //kakao.maps.event.addListener(marker, 'mouseout', makeOutListener(infowindow));
        //makeOutListener(infowindow);

        // kakao.maps.event.addListener(marker, 'click', function() {
        //     // 마커 위에 인포윈도우를 표시합니다
        //     //console.log('data : ' , data);
        //     //infoWindow.setPosition(data.pos);          
        //     infowindow.open(map, marker);          
        // });
    }     

}

// 편의점 마커를 생성하고 편의점 마커 배열에 추가하는 함수입니다
function createDustMarkers() {
    for (var i = 0; i < dustList.length; i++) {

        var data = dustList[i];
        

        var pm25 = data.pm25;

        if(pm25 == 0){
            pm25 = 1;
        }

        if(pm25 > 100){
            pm25 = 100;
        }


        var x = parseInt(pm25 / 10);
        var y  = parseInt(pm25 % 10);

        y  -= 1;


        // console.log('data' , data);
        // console.log('x' , x);
        // console.log('y' , y);

        var imageSize = new kakao.maps.Size(30, 40),
            imageOptions = {   
                spriteOrigin: new kakao.maps.Point(30*y, 40*x),    
                spriteSize: new kakao.maps.Size(300, 400)  
            };       
     
        // 마커이미지와 마커를 생성합니다
        var markerImage = createMarkerImage(markerDustImageSrc, imageSize, imageOptions),    
            marker = createMarker(dustList[i].pos, markerImage);  

        // 생성된 마커를 편의점 마커 배열에 추가합니다
        dustMarkers.push(marker);    

    kakao.maps.event.addListener(marker, 'click', makeOverListener(map, marker, infowindow ,data.content));


        //return;
    }   

}

// 편의점 마커들의 지도 표시 여부를 설정하는 함수입니다
function setDustMarkers(map) {        
    for (var i = 0; i < dustMarkers.length; i++) {  
        dustMarkers[i].setMap(map);
    }        
}


// 편의점 마커들의 지도 표시 여부를 설정하는 함수입니다
function setMaskMarkers(map) {   
    //console.log('mask' , maskMarkers);     
    for (var i = 0; i < maskMarkers.length; i++) {  
        maskMarkers[i].setMap(map);
    }        
}



// 주차장 마커들의 지도 표시 여부를 설정하는 함수입니다
function setCarparkMarkers(map) {        
    for (var i = 0; i < carparkMarkers.length; i++) {  
        carparkMarkers[i].setMap(map);
    }        
}


// 사이드바 이름 클릭 시 지도 이동시키는 함수입니다.
function panTo_(lat, lng) {

    // 이동할 위도 경도 위치를 생성합니다 
    var marker = '';

    var moveLatLon = new kakao.maps.LatLng(lat, lng);
     
    for (i = 0; i < facilityList.length; i++){
      if(lat==facilityList[i].lat && lng==facilityList[i].lng){
      var iwContent = facilityList[i].content;
      marker = scMarkers[i];
     // marker = '';
      //console.log(marker);
      } //end if
    } //end for
    if(marker === ''){
      map.panTo(moveLatLon);
       return;
   }
    pantoInfowindow(map, marker, moveLatLon, iwContent);
    //kakao.maps.event.addListener(marker, 'click', pantoInfowindow(map, marker, infowindow ,iwContent));  
    map.panTo(moveLatLon);
    // 지도 중심을 부드럽게 이동시킵니다
    //map.setCenter(moveLatLon);  
} 

// 카테고리를 클릭했을 때 type에 따라 카테고리의 스타일과 지도에 표시되는 마커를 변경합니다
function changeMarker(type){
    
    var facilityMenu = document.getElementById('facilityMenu');
    var dustMenu = document.getElementById('dustMenu');
    var maskMenu = document.getElementById('maskMenu');
    
    // 커피숍 카테고리가 클릭됐을 때
    if (type === 'facility') {
    
        // 커피숍 카테고리를 선택된 스타일로 변경하고
        facilityMenu.className = 'menu_selected';
        
        // 편의점과 주차장 카테고리는 선택되지 않은 스타일로 바꿉니다
        dustMenu.className = '';

        maskMenu.className = '';
        
        // 커피숍 마커들만 지도에 표시하도록 설정합니다
        setFacilityMarkers(map);
        setDustMarkers(null);
        setMaskMarkers(null);
        infowindow.close();
        
    } else if (type === 'dust') { // 편의점 카테고리가 클릭됐을 때
    
        // 편의점 카테고리를 선택된 스타일로 변경하고
        facilityMenu.className = '';
        dustMenu.className = 'menu_selected';

        maskMenu.className = '';
        
        // 편의점 마커들만 지도에 표시하도록 설정합니다
        setFacilityMarkers(null);
        setDustMarkers(map);
        setMaskMarkers(null);
        infowindow.close();

    } else if(type === 'mask')   {
        // 편의점 카테고리를 선택된 스타일로 변경하고
        facilityMenu.className = '';
        dustMenu.className = '';
        maskMenu.className = 'menu_selected';

        //console.log('type : ' , type);
        
        // 편의점 마커들만 지도에 표시하도록 설정합니다
        setFacilityMarkers(null);
        setDustMarkers(null);
        setMaskMarkers(map);
        infowindow.close();
    }
} 

</script>
</body>
</html>