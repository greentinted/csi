      <div id="container">
            <div class="sub_visual_wrap member">
                <div class="inner_box">
                    <div class="location">
                        <a href="/"><i class="fa fa-home"></i></a>&nbsp;&gt;&nbsp;
                        <span>로그인</span> &nbsp;&gt;&nbsp;<strong>회원가입</strong>                    
                    </div>
                    <h3 class="tit">회원가입</h3>
                    <p>
                        <span>시민참여는 회원가입을 하셔야</span>
                        <span>열람 및 참여 가능합니다</span>
                    </p>
                </div>
            </div>

            <div id="contents" class="member_wrap">
                <div class="sep_tab_box1">
                    <div class="tab_style1 three">
                        <ul>
                            <li><a href="#self">약관동의</a></li>
                            <li class="on"><a href="#self">정보입력</a></li>
                            <li><a href="#self">가입완료</a></li>
                        </ul>
                    </div>
                </div>
                
                <form id="postFrm" method="POST" action="join_proc.php">
                <input type="hidden" name="terms" value="Y">
                <input type="hidden" name="privacy" value="Y">
                <div class="write_table_wrap1">
                    <table class="write_table" cellpadding="0" cellspacing="0">
                        <tbody><tr>
                            <th><label for="mId">* 아이디</label></th>
                            <td>
                                <input type="text" id="mid" name="mid" class="w326">
                                <input type="hidden" id="hmId" name="hmId" value="N">
                                <button type="button" id="singup_id_confirm" class="btn_blue">아이디 중복 확인</button>
                                <span id="idmsg" class="red"></span>

                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPw">* 비밀번호</label></th>
                            <td>
                                <input type="password" id="mPw" name="mPw" class="w326" placeholder="영문, 숫자 포함 6자리 이상을 입력해주세요.">
                                <input type="hidden" id="hmPw" name="hmPw" value="N">
                                <span id="pwMsg" class="red"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPw2">* 비밀번호 확인</label></th>
                            <td>
                                <input type="password" id="mPw2" name="mPw2" class="w326">
                                <input type="hidden" id="hmPw2" name="hmPw2" value="N">
                                <span id="pwMsg2" class="red"></span>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mName">* 이름</label></th>
                            <td>
                                <input type="text" id="mName" name="mName" class="w229">
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mPhone">* 연락처</label></th>
                            <td>
                                <div class="phone_box">
                                    <select id="mPhone1" name="mPhone1"><option value="">선택하세요</option>
                                        <option value="010">010</option>
                                        <option value="011">011</option>
                                        <option value="016">016</option>
                                        <option value="017">017</option>
                                        <option value="018">018</option>
                                        <option value="019">019</option>
                                    </select>
                                    <div class="box1"><input type="text" id="mPhone2" name="mPhone2" class="numberonly" maxlength="4"></div>
                                    <div class="box2"><input type="text" id="mPhone3" name="mPhone3" class="numberonly" maxlength="4"></div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mMail">* 이메일</label></th>
                            <td>
                                <div class="mail_box">
                                    <div class="box1"><input type="text" id="mMail" name="mMail"></div>
                                    <div class="box2"><input type="text" id="mMail2" name="mMail2"></div>
                                    <select id="mMail3" name="mMail3">
                                        <option value="">직접입력</option>
                                        <option value="naver.com">naver.com</option>
                                        <option value="nate.com">nate.com</option>
                                        <option value="daum.com">daum.com</option>
                                        <option value="gmail.com">gmail.com</option>
                                    </select>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mMailService1">메일링 서비스</label></th>
                            <td>
                                <label for="mMailService1"><input type="radio" id="mMailService1" name="mMailService" value="Y" checked="checked"> 수신</label>
                                <label for="mMailService2"><input type="radio" id="mMailService2" name="mMailService" value="N"> 수신거부</label>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mSmsService1">SMS 서비스</label></th>
                            <td>
                                <label for="mSmsService1"><input type="radio" id="mSmsService1" name="mSmsService" value="Y" checked="checked"> 수신</label>
                                <label for="mSmsService2"><input type="radio" id="mSmsService2" name="mSmsService" value="N"> 수신거부</label>
                            </td>
                        </tr>
                    </tbody></table>
                </div>

                <div class="write_btn_area">
                    <div class="fl_c">
                        <input type="submit" onclick="" class="btn_blue" value="다음">
                        <a href="#self" id="cancelBtn" class="btn_dark">취소</a>
                    </div>
                </div>
                </form>
            </div><!-- id : contents -->
        </div>

<!--         <script>
var dataLayer=dataLayer||[];
$(document).ready(function(){
    // Enable.Id();
    // Enable.Pw();

    $("#postFrm").submit(function(){
        if(!$("#mId").val()){
            alert("아이디를 입력하세요.");
            $("#mId").focus();
            return false;
        }

        if($("#hmId").val() === "N"){
            alert("아이디를 확인하세요");
            $("#mId").focus();
            return false;
        }

        if(!$("#mPw").val()){
            alert("비밀번호를 입력하세요.");
            $("#mPw").focus();
            return false;
        }

        if($("#hmPw").val() === "N"){
            alert("비밀번호를 확인하세요.");
            $("#mPw").focus();
            return false;
        }

        if(!$("#mPw2").val()){
            alert("비밀번호 확인을 입력하세요.");
            $("#mPw2").focus();
            return false;
        }

        if($("#hmPw2").val() === "N"){
            alert("비밀번호 확인을 확인하세요.");
            $("#mPw2").focus();
            return false;
        }

        if($("#hmPw").val() == "N" || $("#hmPw2").val() == "N"){
            alert("비밀번호를 동일하게 입력하세요.");
            $("#mPw2").focus();
            return false;
        }

        if(!$("#mName").val()){
            alert("이름을 입력하세요.");
            $("#mName").focus();
            return false;
        }

        if(!$("#mPhone1").val() || !$("#mPhone2").val() || !$("#mPhone3").val()){
            alert("연락처를 입력하세요.");
            if(!$("#mPhone1").val()){
                $("#mPhone1").focus();
            } else if(!$("#mPhone2").val()){
                $("#mPhone2").focus();
            } else {
                $("#mPhone3").focus();
            }
            return false;
        }

        if($("#mPhone1").val() || $("#mPhone2").val() || $("#mPhone3").val()){
            var user_tel = $("#mPhone1").val() + "-" + $("#mPhone2").val() + "-" + $("#mPhone3").val();
            var regex = /^[0-9]{2,3}-[0-9]{3,4}-[0-9]{4}$/;

            if(!regex.test(user_tel)){
                alert("유효한 연락처 형식이 아닙니다.");
                $("#mPhone1").focus();
                return false;
            }
        }

        if(!$("#mMail").val()){
            alert("이메일을 입력하세요.");
            $("#mMail").focus();
            return false;
        }

        if($("#mMail").val()){
            var user_email = $("#mMail").val() + "@" + $("#mMail2").val();
                regex = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;

            if(!regex.test(user_email)){
                alert("유효한 이메일 형식이 아닙니다.");
                $("#mMail").focus();
                return false;
            }
        }
        //ga('send','event','member_join','member_join_button');
                        dataLayer.push({'event':'ga_lead','eventCategory':'memberJoin','eventAction':document.getElementById("mId").value,'eventLabel':document.getElementById("mName").value});
        return true;
    });
});
</script>

<!-- 중복아이디 체크 -->

<!-- 
<script type="text/javascript">
$(document).ready(function(){
    $('#id').keyup(function(){
        if ( $('#aid').val() == 0){
        var id = $(this).val();
        //console.log("아이디:",id);
            // ajax 실행
            $.ajax({
                type : "POST",
                url : "/admin/account/check_duplicated_id",
                data:
                { id: id },
                success : function(result) {
                    //console.log("return:",result);
                    if (result == 1) {
                        $("#result_id_msg").html("사용 가능한 아이디(이름)입니다.");
                    } else {
                        $("#result_id_msg").html("아이디(이름)가 존재합니다.");
                    }
                }
            }); // end ajax
        } //end if
        else{
            // 계정 수정할 경우 
            // console.log($('#id').val());
        }
    }); // end keyup
});
</script> -->