							
<style>
#header .global_menu{
    letter-spacing:0px;
    padding:0px 10px;
}
.event-list {
    overflow: hidden;
    margin-left: -20px;
}

.event-list li {
    float: left;
    width: 25%;
    margin-bottom: 20px;
}

.event-list li .cate.typeG, .event-list .typeG .thumb {
    background-color: #995955;
}

.event-list li .thumbBox .thumb img, .event-list li .thumbBox .thumb span {
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
}

.event-list li .thumbBox {
    margin-left: 20px;
    border: 1px solid #e4e4e4;
}
.event-list li .thumbBox .thumb {
    display: block;
    position: relative;
    padding-top: 68%;
    text-align: center;
    border-bottom: 1px solid #e4e4e4;
}
.event-list li .eventSummary {
    position: relative;
    margin: 12px;
    padding-top: 30px;
}

.event-list li .tit {
    overflow: hidden;
    height: 50px;
    line-height: 25px;
    word-break: keep-all;
    display: -webkit-box;
    -webkit-box-orient: vertical;
    -webkit-line-clamp: 2;
}
.event-list li .cate {
    display: inline-block;
    position: absolute;
    top: 0;
    left: 0;
    padding: 0 5px;
    color: #fff;
    background-color: #3f5f9c;
}

.event-list li .hits {
    position: absolute;
    top: 3px;
    right: 0;
    padding-left: 22px;
    line-height: 19px;
    color: #777;
    background: url(/assets/img/ico_so.png) left no-repeat; background-size: 20px auto;;
}
.event-list li .date {
    margin-top: 10px;
    padding-top: 10px;
    padding-left: 0;
    line-height: 1.2;
    color: #777;
    border-top: 1px solid #e4e4e4;
    /* background: url(/include/cmm_new/image/board/ico_sp.png) 0 9px no-repeat; */
}
.blind {
    overflow: hidden;
    position: absolute;
    top: -9999em;
    left: -9999em;
    width: 1px;
    height: 1px;
    line-height: 1px;
    text-indent: -9999em;
}
.event-list li .eventDate {
    position: relative;
    padding: 0 10px 8px;
    text-align: right;
    color: #aaa;
    font-size: 13px;
}

@media screen and (max-width:1620px ){
    #header .global_menu{
        bottom:95px;
    }
.event-list li {
    float: left;
    width: 32%;
    margin-bottom: 20px;
}

}
@media screen and (max-width: 640px) {
    #header .global_menu{
        bottom:inherit;
    }
.event-list {
    overflow: hidden;
    margin-left: -20px;
    padding: 0px 0px 0px 3px;
}    

.event-list li {
    float: left;
    width: 50%;
    margin-bottom: 20px;
}
}
</style>

       <div id="container">
<!--             <div class="sub_visual_wrap member">
                <div class="inner_box">
                    <div class="location">
                        <a href="/"><i class="fa fa-home"></i></a>&nbsp;&gt;&nbsp;
                        <span>로그인</span> &nbsp;&gt;&nbsp;<strong>회원가입</strong>                    </div>
                    <h3 class="tit">회원가입</h3>
                    <p>
                        <span>시민참여는 회원가입을 하셔야</span>
                        <span>열람 및 참여 가능합니다</span>
                    </p>
                </div>
            </div> -->
         <div id="contents" class="member_wrap">




<div class="event-list">
	<li class="typeG">
		<div class="thumbBox">
			<a href="#192848" onclick="dataView('192848');" class="thumb">
				<img src="/assets/img/Thum_20200221163544317.jpg" alt="신해철 음악작업실 잠정 휴관" ><!-- onerror="this.src='/include/cmm_new/image/board/img_none.png'" -->
			</a>
			<div class="eventSummary">
				<p class="tit">
						<span class="cate typeF">기타</span>
					<a href="#192848" onclick="dataView('192848');" title="신해철 음악작업실 잠정 휴관">신해철 음악작업실 잠정 휴관</a>
				</p>
				<p class="hits"><span class="blind">조회수</span>1,006</p>
				<p class="date">	
					<span class="blind">기간</span>2020.02.22 ~ 2020.12.31
				</p>
				<!--<p class="time">
					<span class="blind">시간</span>
					</p>-->
			</div>
			<p class="eventDate">2020-02-21</p>
		</div>
	</li>

<li class="typeG">
		<div class="thumbBox">
			<a href="#192848" onclick="dataView('192848');" class="thumb">
				<img src="/assets/img/Thum_20200221163544317.jpg" alt="신해철 음악작업실 잠정 휴관" ><!-- onerror="this.src='/include/cmm_new/image/board/img_none.png'" -->
			</a>
			<div class="eventSummary">
				<p class="tit">
						<span class="cate typeF">기타</span>
					<a href="#192848" onclick="dataView('192848');" title="신해철 음악작업실 잠정 휴관">신해철 음악작업실 잠정 휴관</a>
				</p>
				<p class="hits"><span class="blind">조회수</span>1,006</p>
				<p class="date">	
					<span class="blind">기간</span>2020.02.22 ~ 2020.12.31
				</p>
				<!--<p class="time">
					<span class="blind">시간</span>
					</p>-->
			</div>
			<p class="eventDate">2020-02-21</p>
		</div>
	</li>

<li class="typeG">
		<div class="thumbBox">
			<a href="#192848" onclick="dataView('192848');" class="thumb">
				<img src="/assets/img/Thum_20200221163544317.jpg" alt="신해철 음악작업실 잠정 휴관" ><!-- onerror="this.src='/include/cmm_new/image/board/img_none.png'" -->
			</a>
			<div class="eventSummary">
				<p class="tit">
						<span class="cate typeF">기타</span>
					<a href="#192848" onclick="dataView('192848');" title="신해철 음악작업실 잠정 휴관">신해철 음악작업실 잠정 휴관</a>
				</p>
				<p class="hits"><span class="blind">조회수</span>1,006</p>
				<p class="date">	
					<span class="blind">기간</span>2020.02.22 ~ 2020.12.31
				</p>
				<!--<p class="time">
					<span class="blind">시간</span>
					</p>-->
			</div>
			<p class="eventDate">2020-02-21</p>
		</div>
	</li>

<li class="typeG">
		<div class="thumbBox">
			<a href="#192848" onclick="dataView('192848');" class="thumb">
				<img src="/assets/img/Thum_20200221163544317.jpg" alt="신해철 음악작업실 잠정 휴관" ><!-- onerror="this.src='/include/cmm_new/image/board/img_none.png'" -->
			</a>
			<div class="eventSummary">
				<p class="tit">
						<span class="cate typeF">기타</span>
					<a href="#192848" onclick="dataView('192848');" title="신해철 음악작업실 잠정 휴관">신해철 음악작업실 잠정 휴관</a>
				</p>
				<p class="hits"><span class="blind">조회수</span>1,006</p>
				<p class="date">	
					<span class="blind">기간</span>2020.02.22 ~ 2020.12.31
				</p>
				<!--<p class="time">
					<span class="blind">시간</span>
					</p>-->
			</div>
			<p class="eventDate">2020-02-21</p>
		</div>
	</li>

	<li class="typeG">
		<div class="thumbBox">
			<a href="#192848" onclick="dataView('192848');" class="thumb">
				<img src="/assets/img/Thum_20200221163544317.jpg" alt="신해철 음악작업실 잠정 휴관" ><!-- onerror="this.src='/include/cmm_new/image/board/img_none.png'" -->
			</a>
			<div class="eventSummary">
				<p class="tit">
						<span class="cate typeF">기타</span>
					<a href="#192848" onclick="dataView('192848');" title="신해철 음악작업실 잠정 휴관">신해철 음악작업실 잠정 휴관</a>
				</p>
				<p class="hits"><span class="blind">조회수</span>1,006</p>
				<p class="date">	
					<span class="blind">기간</span>2020.02.22 ~ 2020.12.31
				</p>
				<!--<p class="time">
					<span class="blind">시간</span>
					</p>-->
			</div>
			<p class="eventDate">2020-02-21</p>
		</div>
	</li>

</div>

</div>
</div>