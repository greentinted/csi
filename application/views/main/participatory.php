    <div id="container">
        <div class="sub_visual_wrap com">
            <div class="inner_box">
                <div class="location">
                    <a href="/"><i class="fa fa-home"></i></a> &nbsp;&gt;&nbsp;
                    <span>시민참여</span> &nbsp;&gt;&nbsp;<strong>고객소통</strong>
                </div>
                <h2 class="tit">고객소통</h2>
                <p>
                    <span>작성해주신 모든 문의사항에</span>
                    <span>친절하고 신속히 답변해드리겠습니다.</span>
                </p>
            </div>
        </div>
        <div id="contents" class="online_wrap">
            <div class="list_btn_area2">
                <a href="/home/participatory/write" id="writeBtn" class="btn_gray">글쓰기</a>
            </div>

            <div class="coun_list">
                <ul id="moveLink">

                    <?php foreach ($list as $key => $value): ?>
                        <?php if($value['ps_code'] == 'waiting'):?>
                            <li class=""> <!--class complietion은 답변완료 -->
                        <?php elseif($value['ps_code'] == 'handling'):?>
                            <li class="processing"> <!--class complietion은 답변완료 -->    
                        <?php else:?>
                            <li class="completion"> <!--class complietion은 답변완료 -->    
                        <?php endif;?>
                            <a href="/home/participatory/detail?bid=<?=$value['bid']?>">
                                <span class="mark_box">
                                    <span class="cover"></span>
                                    <span class="mark"><?=$value['pstitle']?></span>
                                </span>
                                     <strong class="txt_box">
                                        <span class="<?=$value['optitle_color']?>"> <?=$value['optitle']?> </span> 
                                        <?php if($value['new_icon'] == true):?>
                                        <span class="w3-tag w3-medium w3-red">NEW!</span>
                                        <?php endif;?>

                                    <span class="name"><?=$value['title']?></span>
                                </strong>
                                <span class="date_box">
                                    <span><em>등록일</em> <?=$value['create_date']?></span>
                                </span>
                            </a>
                        </li>
                        

                    <?php endforeach?>    
                </ul>
            </div>



            

            <div class="pagination_wrap">
                <div class="pagination">
                    <?=$pagination?>
                </div>
            </div>
        </div> <!-- end : online wrap -->
    </div> <!-- end : container -->
</div>