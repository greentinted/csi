        <div id="container">

            <div class="sub_visual_wrap com">
                <div class="inner_box">
                    <div class="location">
                        <a href="/"><i class="fa fa-home"></i></a> &nbsp;&gt;&nbsp;
                        <span>주민참여위원회</span> &nbsp;&gt;&nbsp;
                        <strong>주민참여 게시판</strong>
                    </div>
                    <h3 class="tit">주민참여</h3>
                    <p>
                        <span>소중한 여러분의 한마디!</span>
                        <span>보다 빠른 개선을 위해 노력하겠습니다.</span>
                    </p>
                </div>
            </div>

            <div id="contents" class="online_wrap">
                <div class="online_step1">
                    <ul>
                        <li class="s1"><div>온 &middot; 오프라인<br />주민참여 <br />접수</div></li>
                        <li class="s2"><div>해당시설<br />운영진에게<br />전달</div></li>
                        <li class="s3"><div>문제 원인파악<br />및 담당자 배정</div></li>
                        <li class="s4"><div>문제 해결</div></li>
                        <li class="s5"><div>고객님 메일로<br />결과 전달</div></li>
                    </ul>
                </div>


                
                <form id="postFrm" method="POST" enctype="multipart/form-data" action="/home/participatory/save">
                <input type="hidden" name="boardType" value="customer" />
                <input type="hidden" id="bid" name="bid" value="<?=$bid?>">
                <input type="hidden" id="aid" name="aid" value="<?=$aid?>">                
                <div class="write_table_wrap1">
                    

                    <table class="write_table" cellpadding="0" cellspacing="0">
                        <tr>
                            <th><label for="mCategory1">구분</label></th>
                            <td>
                                <label for="mCategory1"><input type="radio" id="mCategory1" name="type" value="program" <?=return_checked($board['optype_code'], 'program')?>  /> 프로그램</label>
                                <label for="mCategory2"><input type="radio" id="mCategory2" name="type" value="facility" <?=return_checked($board['optype_code'], 'facility')?>  /> 시설</label>
                                <label for="mCategory3"><input type="radio" id="mCategory3" name="type" value="service" <?=return_checked($board['optype_code'], 'service')?>  /> 서비스</label>
                                <label for="mCategory4"><input type="radio" id="mCategory4" name="type" value="etc" <?=return_checked($board['optype_code'], 'etc')?>  /> 기타 </label>
                            </td>
                        </tr>
                        <tr>
                            <th><label for="mTitle">제목</label></th>
                            <td>
                                <input type="text" id="title" name="title" class="wFull" value="<?=$board['title']?>" required  />
                            </td>
                        </tr>

                        <tr>
                            <th><label for="mTitle">소속</label></th>
                            <td>
                                <input type="text" id="sort" name="sort" class="wFull"  value="<?=$account['title']?>" readonly />
                            </td>
                        </tr>


                        <tr>
                            <th><label for="descripti">글작성</label></th>
                            <td>
                               <textarea id="summernote" name="description"><?=$board['description']?></textarea>           
                            </td>

                        </tr>

                    </table>



                </div>
                <div class="write_btn_area">
                    <div class="fl_c">
                        <input type="submit" class="btn_blue confirm_btn" value="확인" />
                        <a href="/home/participatory" id="cancelBtn" class="btn_dark">취소</a>
                    </div>
                </div>
                </form>

            </div>
        </div><!-- end : container -->


<script type="text/javascript">

    $(".confirm_btn").on("click", function() {
    var title = $("#title").val();
    var summernote = $("#summernote").val();
    if(title.trim()==""){
        alert("제목을 입력해주세요");
        $("#title").focus();
    return false;    
    } else if(summernote.trim()==""){
        alert("내용을 입력해주세요");
        $("#summernote").focus();
    return false;
    }  
    document.getElementById("postForm").submit();  
});

</script>