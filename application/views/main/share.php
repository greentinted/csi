        <div id="container">
            <div class="sub_visual_wrap share">
                <div class="inner_box">
                    <div class="location">
                        <a href="/"><i class="fa fa-home"></i></a> &nbsp;&gt;&nbsp;
                        <span>성과공유</span> &nbsp;&gt;&nbsp;<strong>성과공유게시판</strong>                    
                    </div>
                    <h3 class="tit">성과공유</h3>
                    <p>시민이 직접 제안한 의견을<span>반영한 결과 입니다.</span></p>
                </div>
            </div>
            <div id="contents" class="sub_epilogue">
                <div class="sep_tab_box1">
                    <div class="tab_style1 two">
                        <ul id="moveList">
 <!--                            <li class="on" Style="float:left;width:15%;min-width:95px;">
                                <a href="#sc=#sd=#st=#sf=#sk=#sb="  Style="">전체보기</a>
                            </li>
                            <li  Style="float:left;width:15%;min-width:95px;"><a href="self_list.php?sc=N">체육시설</a></li>
                            <li  Style="float:left;width:15%;min-width:95px;"><a href="self_list.php?sc=M">도서관시설</a></li>
                            <li ><a href="#sb=Y" style="display:none;"></a></li> -->
                        </ul>
                    </div>
                    <div class="search_box_se">
                        <form id="searchFrm" method="GET" action="/home/participatory/share">
                        <input type="hidden" id="type" name="type" value="<?=$type?>" />
                        <select id="sf" name="sf" title="검색">
                            <option value="all" <?=return_select($sf, 'all')?>>전체</option>
                            <option value="title" <?=return_select($sf, 'title')?>>제목</option>
                            <option value="content" <?=return_select($sf, 'content')?>>내용</option>
                        </select>
                        <input type="text" id="keyword" name="keyword" value="<?=$keyword?>" required />
                        <input type="submit" value="검색" />
                        </form>
                    </div>
                </div>

                <div class="sep_tab_box1">
                    <div class="sep_tab">
                        <h3 class="tit"><a href="#self">전체보기</a></h3>
                        <ul id="moveList">
                            <li class="<?php if($type === '') { echo 'on'; } ?>"><a href="/home/participatory/share">전체보기</a></li>
                            <li class="<?php if($type === 'program') { echo 'on'; } ?>"><a href="/home/participatory/share?type=program">프로그램</a></li>
                            <li class="<?php if($type === 'facility') { echo 'on'; } ?>"><a href="/home/participatory/share?type=facility">시설</a></li>
                            <li class="<?php if($type === 'service') { echo 'on'; } ?>"><a href="/home/participatory/share?type=service">서비스</a></li>
                            <li class="<?php if($type === 'etc') { echo 'on'; } ?>"><a href="/home/participatory/share?type=etc">기타</a></li>
                        </ul>
                    </div>
                </div>
                <div class="sep_list">
                    <ul id="moveView">
                            <?php foreach ($list as $key => $value): ?>
                            <li class="best">    
                                <a href="/home/participatory/detail?bid=<?=$value['bid']?>&type=share">
                                    <span class="img_box">
                                        <span class="<?=$value['fut_code_']?>"></span>
                                        <span class="best"><?=$value['ptitle']?></span>
                                    </span>
                                    <strong class="txt_box">
                                        <span class="w3-tag w3-round <?=$value['optitle_color']?>"><?=$value['optitle']?></span>
                                        <span class="name"><?=$value['title']?></span>
                                    </strong>
                                    <span class="date_box">
                                        <span><em>작성자</em><?=$value['name']?></span>
                                        <span><em>등록일</em> <?=$value['create_date']?></span>
                                        <span><em>조회</em> <?=$value['view_count']?></span>
                                    </span>
                                </a>

                            
                        </li>
                        <?php endforeach?>    

                    </ul>
                </div>

                <div class="pagination_wrap">
                    <div class="pagination">
                          <?=$pagination?>
                    </div>
                </div>
            </div>
        </div>
</div>                
                                
