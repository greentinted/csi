
<style type="text/css">
.reply_area {margin-bottom: 20px;}
.reply_content{-webkit-tap-highlight-color: rgba(0, 0, 0, 0);font-weight: 400;font-size: 17px;box-sizing: border-box !important;color: #555;margin: 0;padding: 0;font-family: 'Nanum Gothic', dotum, Arial, sans-serif;line-height: 130%;position: relative;height: 93px;border: 1px solid #ddd;background: #f6f7f8;margin-top: 20px;}
.reply_content .txt_ar{position: relative;padding-right: 99px;padding-bottom: 0;}
.reply_content textarea {display: inline-block;outline: none;box-sizing: border-box !important;font: inherit;overflow: auto;line-height: 25px;font-family: 'Noto Sans KR', sans-serif;font-size: 16px;width: 100%;border: 0;resize: none;overflow-y: auto;padding: 10px 100px 10px 20px;height: 91px;background: #fff;color: #767676;}
.reply_content .file_att{color: #555;overflow: hidden;position: absolute;top: -1px;right: -1px;height: 93px;padding: 0;border: 0;}
.reply_content .at_wrap {color: #555;font-family: 'Nanum Gothic', dotum, Arial, sans-serif;overflow: hidden;position: relative;padding-left: 100px;}
.reply_content .total_n{float: none;position: absolute;bottom: 20px;left: 0;font-size: 15px;color: #808080;}
.reply_content button{box-sizing: border-box !important;font: inherit;overflow: visible;text-transform: none;-webkit-appearance: button;font-family: 'Noto Sans KR', sans-serif;font-size: 16px;cursor: pointer;border-width: 0;color: #fff;height: 93px;line-height: 93px;width: 100px;position: relative;top: auto;right: auto;background: #488bf8;}
</style>

<div id="container">
    <input type="hidden" id="bid" name="bid" value="<?=$row['bid']?>">
    <input type="hidden" id="aid" name="aid" value="<?=$row['writer_aid']?>">                

    <div id="contents" class="sub_epilogue">
        <div class="sub_location">
            <a href="/"><i class="fa fa-home"></i></a> &nbsp;&gt;&nbsp;
            <span>시민참여</span> &nbsp;&gt;&nbsp;<strong>고객소통</strong>                
        </div>
    <div class="sep_view">
        <div class="sep_tab_box2">
            <div class="title_box">
                <div class="outer_box cover">
                    <div class="inner_box">
                        <strong class="txt_box promo">    
                            <span class="name"><?=$row['title']?>                            <?php if($row['new_icon'] == true):?>
                            <span class="badge w3-red">NEW!!</span>
                            <?php endif;?> </span>

                        </strong>
                        <span class="date_box">
                            <span><em>등록일</em> <?=$row['create_date']?></span>
                            <span><em>조회</em> <?=$row['view_count']?></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="cont_box">
                <h3>
                    <?=$row['description']?>
                </h3>
             </div>
        <?php if($type != 'share'):?>    
        <?php if($row['writer_aid'] == $this->session->userdata('aid') || $account['user_type'] == 'admin'):?>    
             <div class="text_modify_btn">
             <a href="/home/participatory/write?bid=<?=$row['bid']?>" class="btn_modi w3-hover-red">수정하기</a>
             </div>
        <?php endif?>
        <?php endif?>
        </div>
        <?php if($type != 'share'):?>    
        <?php if($account['user_type'] != 'guest'):?>    

        <div class="reply_area">
            <p><h4>댓글 쓰기 </h4></p>
    <div class="reply_content" id="replyContentNoDiv">
        <div class="txt_ar text_area">
            <textarea class="doc_text" rows="2" placeholder="여러분의 의견을 입력해주세요. 주제와 무관한 댓글 악플은 삭제될 수 있습니다." title="내용입력" id="description"></textarea>
        </div>
        <div class="file_att">
            <div class="at_wrap">
                <span class="total_n" id="counter" name="counter"><!-- <b>0</b>/1000 --></span>
                <button class="sub_mit" type="button" id="submit_ment">등록</button>
            </div>
        </div> 
    </div>
<!--             <div class="reply_body" >
                <span class="text_area">
                    <textarea id="description" name="description" rows="5" cols="40"></textarea>

                </span>
                <span class="loc">
                    <button class="w3-btn w3-blue w3-hover-grey" id="submit_ment" style="width: 60px;">등록</button>
                </span>
            </div> --> <!-- end:: reply_body -->
        </div> <!-- end:: reply_area -->
        <?php endif?>
        <?php endif?>

             <!--리플 글 영역-->
            
            <?php foreach ($ment_list as $key => $value): ?>
            <div class="reply_wrap">
                <div class="re_profile" name="re_profile">
                    <span class="reply_txt"></span>
                </div><!-- end:: profile -->
                <div class="re_area" name="re_area">        
                    <div class="re_area up">
                        <span class= "w3-tag w3-round-large w3-medium <?php if($value['is_admin'] != 1):?>w3-red<?php endif?><?php if($value['is_admin'] != 0):?>w3-blue<?php endif?>"><?=$value['name']?> </span>
                        <span id="datetime" class="reply_txt"><?=$value['create_time']?></span>
                    </div>
                    <div class="re_area down">
                        <span class="reply_txt" id="replytxt_<?=$value['mid']?>">
                            <?=nl2br($value['body'])?>                        
                        </span>
                    </div>
                    <div class="re_button">


                        <span class="loc">
                            <?php if($type != 'share'):?>    
                            <?php if($account['user_type'] != 'guest'):?>    
                                <button class="w3-button w3-border w3-border-green ment_rereply" id="ment_<?=$value['mid']?>_<?=$value['aid']?>">댓글달기</button>
                            <?php endif?>
                            <?php if($value['match_aid'] == 1 || $account['user_type'] == 'admin'):?>    
                                <button class="w3-button w3-border w3-border-blue-grey modify_ment" id="modify_<?=$value['mid']?>_<?=$value['aid']?>">수정</button>
                                <button class="w3-button w3-border w3-border-red ment_delete" id="mentdelete_<?=$value['mid']?>_<?=$value['aid']?>">삭제</button>
                            <?php endif?>
                            <?php endif?>
                        </span>
             
                    </div> <!-- end:: re_button -->
                </div> <!-- end:: re_area -->
                 <div id="ment_reply_<?=$value['mid']?>_<?=$value['aid']?>" style="display:none">
                    <span style="width: 100%;">
                        <textarea id="body_<?=$value['mid']?>_<?=$value['aid']?>" name="description" style="width: 98%" rows="5" cols="40"></textarea>
                    </span>
                    <span class="loc">

                        <button class="w3-btn w3-green submit_reply_ment" id="rement_<?=$value['mid']?>_<?=$value['aid']?>">등록</button>

                        <button class="w3-btn w3-green submit_modify_ment" id="modment_<?=$value['mid']?>_<?=$value['aid']?>" style="display:none">수정 </button>

                    </span>                    
                 </div>

                    <?php foreach ($value['re_list'] as $key1 => $value1): ?>
                        <div class="reply" id="rereply"> <!--리리플 -->
                        <div class="re_profile" name="re_profile">
                            <span class="reply_txt"></span>
                        </div><!-- end:: profile -->
                        <div class="re_area" name="re_area">        
                            <div class="re_area up">
                                <span class="w3-tag w3-round-large w3-medium <?php if($value1['is_admin'] != 1):?>w3-red<?php endif?><?php if($value1['is_admin'] != 0):?>w3-blue<?php endif?>"> <?=$value1['name']?> </span>
                                <span id="datetime" class="reply_txt"><?=$value1['create_time']?></span>
                            </div>
                            <div class="re_area down">
                                <span class="reply_txt" id="replytxt_<?=$value1['mid']?>">
                                    <?=nl2br($value1['body'])?>        
                                </span>
                            </div>
                            <div class="re_button">
                                <span class="loc">
                                    <?php if($type != 'share'):?>    
                                    <?php if($account['user_type'] != 'guest'):?>    
                                        <button class="w3-button w3-green ment_rereply" id="ment_<?=$value['mid']?>_<?=$value['aid']?>">댓글달기</button>
                                    <?php endif?>
                                    <?php if($value1['match_aid'] == 1 || $account['user_type'] == 'admin'):?>    

                                        <button class="w3-button w3-blue-grey modify_rement" id="modify_<?=$value1['mid']?>_<?=$value1['aid']?>">수정</button>
                                        <button class="w3-button w3-red ment_delete" id="mentdelete_<?=$value1['mid']?>_<?=$value1['aid']?>">삭제</button>
                                    <?php endif?>
                                    <?php endif?>
                                </span>
                            </div> <!-- end:: re_button -->
                        </div> <!-- end:: re_area -->
                         <div class="reply_ment" id="ment_rereply_<?=$value1['mid']?>_<?=$value1['aid']?>" style="display:none">
                            <span style="width: 100%;">
                                <textarea id="body_<?=$value1['mid']?>_<?=$value1['aid']?>" name="description" style="width: 98%" rows="5" cols="40"></textarea>
                            </span>
                            <span class="loc">
                                <button class="w3-btn w3-green submit_reply_ment" id="rement_<?=$value1['mid']?>_<?=$value1['aid']?>">등록</button>
                                <button class="w3-btn w3-green submit_modify_ment" id="modment_<?=$value1['mid']?>_<?=$value1['aid']?>" style="display:none">수정 </button>

                            </span>                    
                         </div>

                        </div>       
         
                        
                    <?php endforeach ?>
            </div> <!-- end:: reply_wrap -->
            <?php endforeach?>    



<!-- 리플 글 영역 끝 -->

 <!-- 리플 작성 영역 끝 -->

            <div id="moveView" class="prev_next_wrap">
                    <span class="prev" ><strong>이전 글</strong><a href="<?=$prev_url?>"><?=$prev_title?></a></span>


                    <span class="next"><strong>다음 글</strong><a href="<?=$next_url?>"><?=$next_title?></a></span>
            </div>
        </div>

        <div id="moveList" class="btn_area">
            <a href="/home/participatory" class="btn1">돌아가기</a>
        </div>        
      </div>
  </div>


<script type="text/javascript">
    //textarea 글자수 체크
$('.doc_text').keyup(function (e){
    var content = $(this).val();
    $('#counter').html("("+content.length+"/200)");    //글자수 실시간 카운팅

    if (content.length > 200){
        alert("최대 200자까지 입력 가능합니다.");
        $(this).val(content.substring(0, 200));
        $('#counter').html("(200/200)");
    }
});
</script>