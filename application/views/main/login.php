<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title> 성남도시개발공사 </title>

    <!-- Bootstrap -->
    
    <!-- Font Awesome -->
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->`
    
    <!-- Animate.css -->
    <link href="/vendors/animate.css/animate.min.css" rel="stylesheet">
 
    <!-- Custom Theme Style -->
    <!-- <link href="/assets/css/custom.css" rel="stylesheet"> -->
    <link href="/assets/css/login_style.css?ver=7" rel="stylesheet">
  </head>

  <body class="">

   <div class="container">

      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>


      <div id="login">

        <h1><i class="fa fa-arrows" aria-hidden="true"></i> 고객 서비스 혁신 </h1>

        <form name="form" method="post" action="/base/request_login">
          <fieldset class="clearfix">
            <input type="hidden" name="return_url" value="/home/notice">
            <p><span class="fa fa-user"></span>
              <input type="text" name="login_id" value="" onBlur="if(this.value == '') this.value = ''" onFocus="if(this.value == 'Username') this.value = ''" required placeholder="ex)김종훈">
            </p> <!-- JS because of IE support; better: placeholder="Username" -->
            <p><span class="fa fa-lock"></span>
              <input type="password" name="passwd" value="" onBlur="if(this.value == '') this.value = ''" onFocus="if(this.value == 'Password') this.value = ''" placeholder="ex)패스워드" required>
            </p> <!-- JS because of IE support; better: placeholder="Password" -->
            <p><input type="submit" value="로그인"></p>

          </fieldset>
        </form>
        
        <p>[13495] 경기도 성남시 분당구 탄천로 215, 5층(야탑동)  전화: 031-725-9300 </p> 


      </div> <!-- end login -->

    </div>


<!--         <div class="animate form login_form" style="">
          <section class="login_content" style="width: 480px;">
            <form name="form" method="post" action="/base/request_login">
              <h1>환영합니다</h1>
              <input type="hidden" name="return_url" value="/home/notice">
              <div>
                <input type="text" name="login_id" class="form-control" placeholder="ex)김종훈" required="" />
              </div>
              <div>
                <input type="password" name="passwd" class="form-control" placeholder="패스워드" required="" />
              </div>
              <div>
                <button type="submit" class="btn btn-default">로그인</button>                
              </div>

              <div class="clearfix"></div>



              <div class="separator">
                
                <div class="clearfix"></div>
                <br />

                <div>
                  <h1><i class="fa fa-car"></i> 주차업무 포털</h1>
                  <p>[13495] 경기도 성남시 분당구 탄천로 215, 5층(야탑동)  전화: 031-725-9300 </p> 
                    
                </div>
              </div>
            </form>
          </section>
        </div>
 -->      
     
  </body>
</html>
