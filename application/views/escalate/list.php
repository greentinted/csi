<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 신규</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장명</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="contractor_"class="select2_single form-control" tabindex="-1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>

							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">차량번호 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_no" name="car_no_check" class="form-control col-md-7 col-xs-12" placeholder="26가1111 or 서울12치1233">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> 결제일
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="pay_date" name="pay_date" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01">
							<span class="fas fa-envelope form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_type"> 감면 유형 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="discount_type" class="select2_single form-control" tabindex="-1">
							<?php foreach ($discount_type as $key => $value): ?>
								<option name="discount" value="<?=$value['dis_public_id']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자"> 소유자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_owner" name="car_owner" class="form-control col-md-7 col-xs-12" placeholder="실명">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 차종
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_type" name="car_type" class="form-control col-md-7 col-xs-12" placeholder="티볼리">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="tel_number" name="tel_number" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="button" class="btn btn-success checkContractForm">저장</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


