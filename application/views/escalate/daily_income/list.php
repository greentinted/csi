<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>일일 수입 일계표 / 미납 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="/escalate/daily_income/detail" class="">신규 입력으로 이동</a>
							</li>
						</ul>


				</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<label class="col-sm-4 control-label">조회 월</label>

				<div class="col-sm-4">
					<div class="input-group">
						<span class="input-group-btn">
							<button id="prev_income_btn" type="button" class="btn btn-primary">◀</button>
						</span>
						<input id="income_viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
						<span class="input-group-btn">
							<button id="next_income_btn" type="button" class="btn btn-primary">▶</button>
						</span>

					</div>
				</div>

				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>수입일</th>							
							<th>주차장</th>
							<th>작업자</th>
							<th>일차건수(소계)</th>
							<th>일차금액(합계)</th>
							<th>월차건수(소계)</th>
							<th>월차금액(합계)</th>							
							<th>등록일</th>
							<th>수정</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$value['ic_id']?></td>
							<td><?=$value['income_date']?></td> 
							<td><?=$value['wpid_title']?></td>
							<td><?=$value['user']['id']?></td>
							<td><?=$value['daily_total']?></td>
							<td><?=$value['daily_total_amount']?></td>
							<td><?=$value['monthly_total']?></td>
							<td><?=$value['monthly_total_amount']?></td>							
							<td><?=$value['rdate']?></td>

							<td><a class="btn btn-info" href="/escalate/daily_income/detail?ic_id=<?=$value['ic_id']?>">수정</a></td>	

							<?php /*

							<td><input type="button"  name="view" class="btn btn-danger init_password" id="<?=$value['aid']?>" value="비번초기화"></td>						

							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>




