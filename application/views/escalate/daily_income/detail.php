<style type="text/css">
 fieldset 
	{
		border: 1px solid #ddd !important;
		margin: 0;
		xmin-width: 0;
		padding: 10px;       
		position: relative;
		border-radius:4px;
		background-color:#f5f5f5;
		padding-left:10px!important;
	}	
	
		legend
		{
			font-size:22px;
			font-weight:bold;
			margin-bottom: 0px; 
			width: 35%; 
			border: 1px solid #ddd;
			border-radius: 4px; 
			padding: 5px 5px 5px 10px; 
			background-color: #ffffff;
			text-align: center;
		}	
</style>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>일일 수입 일계표 / 미납</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
                            <li><a href="/escalate/daily_income/list" class="">입력 내역 보기</a>
                            </li>
                        </ul>

					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="/escalate/daily_income/save">

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="ic_id" name="ic_id" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$nc_row['ic_id']?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="access_type" name="access_type" required="required" class="form-control col-md-7 col-xs-12" placeholder="" value="<?=$access_type?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="did" class="select2_single form-control district_change" id="new_contract_did" tabindex="1">
						<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $nc_row['did'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name="wpid" id="new_contractor_wpid"class="select2_single form-control" tabindex="1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $nc_row['wpid'])?>><?=$value['title']?></option>

							<?php endforeach ?>

							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">정산 날짜
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="income_date" name="income_date" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01" value="<?=$nc_row['income_date']?>">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email">일계표 작성일
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="today" name="today" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01" value="<?=$nc_row['income_date']?>" readonly>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						</label>
						<label class="control-label" ><font color="red"> ※ 정산일을 기입해주세요.</font></label>
					</div>

					<fieldset class="control-label">
					<legend>일차</legend>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 현금
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_cash" name="daily_cash" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_cash']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_cash_amount" name="daily_cash_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_cash_amount']?>">

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 카드
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_card" name="daily_card" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_card']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_card_amount" name="daily_card_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_card_amount']?>">

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 계좌 이체 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_v_transfer" name="daily_transfer" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_transfer']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_v_transfer_amount" name="daily_transfer_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_transfer_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> T-머니(교통)
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_tmoney" name="daily_tmoney" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_tmoney']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_tmoney_amount" name="daily_tmoney_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_tmoney_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 카카오T
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_kakaot" name="daily_kakaot" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_kakaot']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_kakaot_amount" name="daily_kakaot_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_kakaot_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 기타
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_etc" name="daily_etc" class="daily_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['daily_etc']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_etc_amount" name="daily_etc_amount" class="daily_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['daily_etc_amount']?>">

						</div>
					</div>

					</fieldset>

					<fieldset class="control-label">
					<legend>월차</legend>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 현금
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_cash" name="monthly_cash" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_cash']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_cash_amount" name="monthly_cash_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_cash_amount']?>">

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 카드
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
						<!-- 	<input type="number" id="monthly_card" name="monthly_card" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_card']?>"> -->

							<input type="number" id="monthly_card" name="monthly_card" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_card']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_card_amount" name="monthly_card_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_card_amount']?>">

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 계좌 이체 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_transfer" name="monthly_transfer" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_transfer']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_transfer_amount" name="monthly_transfer_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_transfer_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 웹 연장
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_web" name="monthly_web" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_web']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_web_amount" name="monthly_web_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_web_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 가상 계좌
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_v_transfer" name="monthly_v_transfer" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_v_transfer']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_v_transfer_amount" name="monthly_v_transfer_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_v_transfer_amount']?>">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 기타
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_etc" name="monthly_etc" class="monthly_input form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['monthly_etc']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_etc_amount" name="monthly_etc_amount" class="monthly_input_amount form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['monthly_etc_amount']?>">

						</div>
					</div>

					</fieldset>

					<fieldset class="control-label">
					<legend>합계</legend>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 일차 합계
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_total" name="daily_total" class="form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="daily_total_amount" name="daily_total_amount" class="form-control col-md-7 col-xs-12" placeholder="금액(원)" value="">

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 월차 합계
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_total" name="monthly_total" class="form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="monthly_total_amount" name="monthly_total_amount" class="form-control col-md-7 col-xs-12" placeholder="금액(원)" value="">

						</div>
					</div>


					<fieldset></fieldset>


					<fieldset class="control-label">
					<legend>미납</legend>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 미납
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="unpaid" name="unpaid" class="form-control col-md-7 col-xs-12" placeholder="건수(매)" min="0" max="3000" value="<?=$nc_row['unpaid']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="unpaid_amount" name="unpaid_amount" class="form-control col-md-7 col-xs-12" placeholder="금액(원)" value="<?=$nc_row['unpaid_amount']?>">

						</div>
					</div>


					<fieldset></fieldset>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 특이사항
						</label>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<textarea id="summernote" name="description" style="text-align: left"><?=$nc_row['description']?></textarea>

						</div>
					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">초기화</button>
							<button type="submit" class="btn btn-success">저장</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


