<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2 style="padding-top: 10px">쿠폰 신청 내역 <small></small></h2>
        <a href="/escalate/coupon/detail"><button type="button" class="btn btn-success  navbar-right">입력화면으로 돌아가기</button></a>
        <div class="clearfix"></div>
      </div>
      <form id="period_form" action="/escalate/coupon/history" metho="POST">  

      <div class="col-sm-12">
     <div class="form-group">
            <label class="col-sm-4 control-label"><small>조회기간</small></label>

            <div class="col-sm-3">
              <div class="input-group">
                              <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                              <input type="text" class="query_period" name="query_period" id="approve_vac_query_period" class="form-control" value="<?=$query_period?>" />
              
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" id="approve_vac_query_btn">조회하기</button>
                </span>
              </div>
            </div>
          </div>
          </form>


      <div class="x_content">

        <table id="coupon_datatable" class="table table-striped table-bordered bulk_action" style="font-size: 17px">
          <thead>
            <tr>
              <th>#</th>
              <th>처리자</th>              
              <th>지역</th>
              <th>주차장</th>
              <th>신청인</th>
              <th>상가명</th>
              <th>연락처</th>
              <th>입금자</th>
              <th>수량(30분)</th>
              <th>금액(30분)</th>

              <th>수량(1시간)</th>
              <th>금액(1시간)</th>
              <th>수량(1시간30분)</th>
              <th>금액(1시간30분)</th>
              <th>수량(2시간)</th>
              <th>금액(2시간)</th>
              <th>작성일자</th>
              <th>수정</th>
              <th>삭제</th>
                            
           <!--    <?php if($user_type != "operator") { ?>
                <th>Action</th>
              <?php } ?> -->
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_list as $key => $value): ?>
              <tr>
                <td id="cp_id"><?=$value['cp_id']?></td>
                <td><?=$value['confirm_id']?></td>
                <td><?=$value['district_title']?></td>
                <td><?=$value['workplace_title']?></td>
                <td><?=$value['applicant']?></td>
                <td><?=$value['mall_name']?></td>
                <td><?=$value['cou_cellphone']?></td>
                <td><?=$value['depositor']?></td>
                <td><?=$value['half_coupon']?></td>
                <td><?=$value['half_coupon_amount']?></td>
                <td><?=$value['hour_coupon']?></td>
                <td><?=$value['hour_coupon_amount']?></td>
                <td><?=$value['onehalf_coupon']?></td>
                <td><?=$value['onehalf_coupon_amount']?></td>
                <td><?=$value['twohour_coupon']?></td>
                <td><?=$value['twohour_coupon_amount']?></td>
                <td><?=$value['create_date']?></td>
                <td><a class="btn btn-info" href="/escalate/coupon/detail?cp_id=<?=$value['cp_id']?>&wpid=<?=$value['wpid']?>">수정</a></td>
                <td><input type="button" id="<?=$value['cp_id']?>" class="btn btn-danger btn_delete_coupon" value="삭제"></td>
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

