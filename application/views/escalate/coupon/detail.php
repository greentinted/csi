<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>주차 쿠폰 신청서</h2>
								<a href="/escalate/coupon/history"><button type="button" class="btn btn-success  navbar-right">주차 쿠폰 신청 내역</button></a>
<!-- 				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul> -->
				<div class="clearfix"></div>
			</div>


			
			<div class="x_content">
				<br />
				<form id="form_coupon" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" action="/escalate/coupon/save" >

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>





					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <select class="select2_single form-control district_change" name="did" id="new_contract_did" tabindex="1">
              <?php foreach ($districts as $key => $value): ?>
                <option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $cp_row['did'])?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>


						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장명</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							 <select id="new_contractor_wpid" name="wpid" class="select2_single form-control" tabindex="1">
              <?php foreach ($parkinglot as $key => $value): ?>
                <option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $cp_row['wpid'])?>><?=$value['title']?></option>

              <?php endforeach ?>

              </select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required"> 결제 수단</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control" tabindex="1" name="pm_id">
								<option value="0">선택없음</option>
							<?php foreach ($method_data as $key => $value): ?>
								<option name="pm_id" value="<?=$key?>" <?=return_select($key, $cp_row['pm_id'])?>><?=$value?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="신청인"> 신청인 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="applicant" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="홍길동" value="<?=$cp_row['applicant']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 상가명
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="mall_name" name="mall_name" class="form-control col-md-7 col-xs-12" placeholder="빽다방"  value="<?=$cp_row['mall_name']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="cou_cellphone" name="cou_cellphone" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885"  value="<?=$cp_row['cou_cellphone']?>">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="입금자"> 입금자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="depositor" name="depositor" class="form-control col-md-7 col-xs-12" placeholder="홍길동" value="<?=$cp_row['depositor']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 30분권
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="half_coupon" name="half_coupon" class="coupon_input form-control col-md-7 col-xs-12" placeholder="0" min="0" max="3000" value="<?=$cp_row['half_coupon']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="half_coupon_amount" name="half_coupon_amount" class="coupon_input_amount form-control col-md-7 col-xs-12" placeholder="0"  value="<?=$cp_row['half_coupon_amount']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 1시간권 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="hour_coupon" name="hour_coupon" class="coupon_input form-control col-md-7 col-xs-12" placeholder="0" min="0" max="3000" value="<?=$cp_row['hour_coupon']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="hour_coupon_amount" name="hour_coupon_amount" class="coupon_input_amount form-control col-md-7 col-xs-12" placeholder="0"  value="<?=$cp_row['hour_coupon_amount']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 1.5시간권 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="onehalf_coupon" name="onehalf_coupon" class="coupon_input form-control col-md-7 col-xs-12" placeholder="0" value="<?=$cp_row['onehalf_coupon']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="onehalf_coupon_amount" name="onehalf_coupon_amount" class="coupon_input_amount form-control col-md-7 col-xs-12" placeholder="0"  value="<?=$cp_row['onehalf_coupon_amount']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
							
						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 2시간권 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="twohour_coupon" name="twohour_coupon" class="coupon_input form-control col-md-7 col-xs-12" placeholder="0" value="<?=$cp_row['twohour_coupon']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="twohour_coupon_amount" name="twohour_coupon_amount" class="coupon_input_amount form-control col-md-7 col-xs-12" placeholder="0"  value="<?=$cp_row['twohour_coupon_amount']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 소계 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="total_coupon" name="total_coupon" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$cp_row['total_coupon']?>">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="total_coupon_amount" name="total_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="0" value="<?=$cp_row['total_coupon_amount']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
						</div>

					</div>



						<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="비고"> 비고 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="remarks" name="remarks" class="form-control col-md-7 col-xs-12" placeholder="기타사항은 여기에 입력해주세요." value="<?=$cp_row['remarks']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">초기화</button>
							<button type="submit" class="btn btn-success">저장</button>
						</div>
					</div>
					<input type="hidden" value="" name="coupon_price">
					<input type="hidden" value="<?=$cp_type?>" name="edit_type">
					<input type="hidden" value="<?=$cp_row['cp_id']?>" name="cp_id">
				</form>
			</div>
		</div>
	</div>
</div>


