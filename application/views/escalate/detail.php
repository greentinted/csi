<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>주차 쿠폰 신청서</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="demo-form2" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장명</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="contractor_"class="select2_single form-control" tabindex="-1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>

							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required"> 결제 수단</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control" tabindex="1">
							<?php foreach ($method_data as $key => $value): ?>
								<option name="pm_id" value="<?=$value['pm_id']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="신청인"> 신청인 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="applicant" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="홍길동">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 상가명
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="mall_name" name="mall_name" class="form-control col-md-7 col-xs-12" placeholder="빽다방">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="cou_cellphone" name="cou_cellphone" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="입금자"> 입금자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="applicant" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="홍길동">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 1시간권 (900원) 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="hour_coupon" name="hour_coupon" class="form-control col-md-7 col-xs-12" placeholder="10" min="0" max="3000">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="hour_coupon_amount" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="홍길동" readonly="">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 1.5시간권 (1,440원) 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="onehalf_coupon" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="100(매)">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="onehalf_coupon_amount" name="applicant" class="form-control col-md-7 col-xs-12" placeholder="1440" readonly="">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
							
						</div>

					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> 2시간권 (1,980원) 
						</label>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="number" id="twohour_coupon" name="twohour_coupon" class="form-control col-md-7 col-xs-12" placeholder="100">
						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">
							<input type="text" id="twohour_coupon_amount" name="twohour_coupon_amount" class="form-control col-md-7 col-xs-12" placeholder="0" readonly="">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
						</div>

					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">리셋</button>
							<button type="button" class="btn btn-success">저장</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>


