<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 입력 리스트<small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>

						<ul class="dropdown-menu" role="menu">
							<li><a href="/escalate/contractor/extend_" class="">신규 입력으로 이동</a>
							</li>
						</ul>


				</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">

				<label class="col-sm-4 control-label">조회 월</label>

				<div class="col-sm-4">
					<div class="input-group">
						<span class="input-group-btn">
							<button id="prev_ec_btn" type="button" class="btn btn-primary">◀</button>
						</span>
						<input id="ec_viewing_month" type="text" class="form-control" value="<?=$viewing_from?>" style="text-align:center;">
						<span class="input-group-btn">
							<button id="next_ec_btn" type="button" class="btn btn-primary">▶</button>
						</span>

					</div>
				</div>

				<table id="account_datatable" class="table table-striped table-bordered bulk_action">
					<thead>
						<tr>
							<th>#</th>
							<th>차량번호</th>
							<th>금액</th>
							<th>결제일</th>
							<th>등록일시간</th>
							<th>주차장</th>
							<th>수정</th>
							<th>삭제</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($list as $key => $value): ?>
						<tr>
							<td><?=$key+1?></td>
							<td><?=$value['car_no']?></td>
							<td><?=$value['amount']?></td>
							<td><?=$value['pay_date']?></td>
							<td><?=$value['create_date']?></td>
							<td><?=$value['wpid_title']?></td>
							<td><a class="btn btn-info" href="/escalate/contractor/extend_?ecid=<?=$value['ecid']?>&aid=<?=$value['aid']?>&rdate=<?=$value['rdate']?>&did=<?=$value['did']?>&wpid=<?=$value['wpid']?>">수정</a></td>	


							<td><a onclick="return confirm('정말로 삭제하시겠습니까? ')" href="/admin/contractor/extend_delete?ecid=<?=$value['ecid']?>" class="btn btn-danger">삭제</a></td>
							

							<?php /*
							<td><a class="btn btn-info" href="/escalate/contractor/extend_?rdate=<?=$value['rdate']?>&aid=<?=$value['aid']?>">수정</a></td>	
							*/?>
						</tr>
						<?php endforeach?>    
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>



