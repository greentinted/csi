<div class="row">	
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 <small><?=$parking_type_str?></small></h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
		<div class="x_content" style="text-align: center">

				<a href="/escalate/contractor/new_" class="btn btn-primary">월정기신규</a>
				<a href="/escalate/contractor/extend_" class="btn btn-success">월정기연장</a>
				<a href="/escalate/contractor/change_" class="btn btn-info">월정기변경</a>
				<a href="/escalate/contractor/cancel_" class="btn btn-danger">월정기해지</a>

<!-- 				<a href=""button type="button" class="btn btn-primary">월정기 신규</button>

				<button type="button" class="btn btn-primary">월정기 연장</button>


				<button type="button" class="btn btn-primary">월정기 변경</button>


				<button type="button" class="btn btn-primary">월정기 해지</button>
 -->
			</div>


		</div>
	</div>
</div>
