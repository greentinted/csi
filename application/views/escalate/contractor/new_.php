<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 신규</h2>
				<ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
						<ul class="dropdown-menu" role="menu">
                            <li><a href="/escalate/contractor/new_list" class="">입력 내역 보기</a>
                            </li>
                        </ul>

					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="new_contractor_form" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="n_id" name="n_id" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="<?=$nc_row['n_id']?>">
						</div>
					</div>

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="nc_type" name="nc_type" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="<?=$nc_type?>">
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" id="new_contract_did" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $nc_row['did'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="new_contractor_wpid"class="select2_single form-control" tabindex="1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $nc_row['wpid'])?>><?=$value['title']?></option>

							<?php endforeach ?>

							</select>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">차량번호 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_no" name="car_no_check" class="form-control col-md-7 col-xs-12 han" placeholder="26가1111 이거나 서울12치1233" value="<?=$nc_row['car_no']?>">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">월정기 금액 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="nc_origin_amount" name="nc_origin_amount" class="form-control col-md-7 col-xs-12" placeholder="20000" value="<?=$nc_row['origin_amount']?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">실제 결제 금액 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="nc_amount" name="nc_amount" class="form-control col-md-7 col-xs-12" placeholder="20000" value="<?=$nc_row['amount']?>">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> 월정기 시작일
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="pay_date" name="pay_date" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01" value="<?=$nc_row['pay_date']?>">
						</div>
					</div>

					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_type"> 감면 유형 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="discount_type" class="select2_single form-control" tabindex="0">
								<option value="0">일반 (100%)</option>
							<?php foreach ($discount_type as $key => $value): ?>

								<option name="discount" value="<?=$value['dis_public_id']?>" <?=return_select($value['dis_public_id'], $nc_row['dis_public_id'])?>><?=$value['title_desc']?>  </option>

							<?php endforeach ?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자"> 소유자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_owner" name="car_owner" class="form-control col-md-7 col-xs-12" placeholder="실명" value=
							"<?=$nc_row['car_owner']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 차종
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_type" name="car_type" class="form-control col-md-7 col-xs-12" placeholder="티볼리" value="<?=$nc_row['car_type']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12 aa">
							<input type="tel" id="tel_number" name="tel_number" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885" value="<?=$nc_row['tel_num']?>">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="비고"> 비고
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="message" name="description" class="form-control" rows="10"><?=$nc_row['description']?></textarea>
						</div>
					</div>


					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">초기화</button>
							<button type="button" class="btn btn-success checkContractForm">저장</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

