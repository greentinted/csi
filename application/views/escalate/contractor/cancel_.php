<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 해지 / 요금 환불</h2>
				<!-- <ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul> -->
				<a href="/escalate/contractor/cancel_history"><button type="button" class="btn btn-success navbar-right">해지 처리 내역</button></a>
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="form_cancel_contractor" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" onsubmit="return checkContractForm();" action="/escalate/contractor/cancel_save">

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" name="did" id="new_contract_did" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $cancel_row['did'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="new_contractor_wpid" name="wpid" class="select2_single form-control" tabindex="1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $cancel_row['wpid'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>
						</div>
					</div>





		<!-- 			<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name = "did" class="select2_single form-control district_change" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장명</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="wpid" name="wpid" class="select2_single form-control" tabindex="-1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['did']?>"><?=$value['title']?></option>
							<?php endforeach ?>

							</select>

						</div>
					</div> -->

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자"> 차량 소유자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_owner" name="car_owner" class="form-control col-md-7 col-xs-12 han" placeholder="실명" value="<?=$cancel_row['car_owner']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true" ></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="tel_number" name="tel_number" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885" value="<?=$cancel_row['tel_num']?>">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group" style="padding-bottom: 30px">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> 해지날짜
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="cancel_date" name="cancel_date" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01" value="<?=$cancel_row['cancel_date']?>">
							<span class="fas fa-envelope form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">차량번호 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_no" name="car_no" class="form-control car_no col-md-7 col-xs-12 han" placeholder="26가1111 이거나 서울12치1233" value="<?=$cancel_row['car_no']?>">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_type">감면증빙확인 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="dis_id" name="dis_id" class="select2_single form-control" tabindex="-1">
								<option value="0">일반 (100%)</option>
							<?php foreach ($discount_type as $key => $value): ?>
								<option name="discount" value="<?=$value['dis_public_id']?>" 
									<?=return_select($value['dis_public_id'], $cancel_row['dis_public_id'])?>><?=$value['title_desc']?>	
								</option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 은행명
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="bank_name" name="bank_name" class="form-control col-md-7 col-xs-12" placeholder="국민은행" value="<?=$cancel_row['bank_name']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 계좌번호
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="bank_account" name="bank_account" class="form-control col-md-7 col-xs-12" placeholder="계좌번호 형식 대로 기재" value="<?=$cancel_row['bank_account']?>">
							<span class="fa fa-address-card  form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group" style="padding-bottom: 30px">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 금액
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="fee" name="fee" class="form-control col-md-7 col-xs-12" placeholder="48000" value="<?=$cancel_row['fee']?>">
							<span class="fa fa-money form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						</label>
						<label class="control-label" ><font color="red"> ※ 공정거래 표준 약관에 따라 미 사용기간에 대한 주차 요금의 80%만 반환 ( 공사 전체에서 동일하게 적용됩니다. )
					
						 </font></label>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12">
						</label>
						<label class="control-label" ><font color="red"> ※ 통장 사본 및 할인 증빙 자료 필수 제출( 이곳이 요청글 작성후 웹 메일 또는 인편을 통해 사무실 전달 요망)
					
						 </font></label>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for=""> (필수입력) 해지 사유 및 전달 사항
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="message" name="description" class="form-control" rows="10"><?=$cancel_row['description']?></textarea>
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자">변경 확인자
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="confirm_user" name="confirm_user" class="form-control col-md-7 col-xs-12" placeholder="실명" value="<?=$confirm_user_id?>" readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>




					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">초기화</button>
							<button type="submit" class="btn btn-success">저장</button>
						</div>
					</div>
					<input type="hidden" id="edit_type" name="edit_type" value="$cancel_type">
					<input type="hidden" id="c_id" name="c_id" value="<?=$cancel_row['c_id']?>">
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
    function checkContractForm(){
        var v  = $('.car_no').val();

	    //var result = document.getElementById('result'); 

	    var pattern1 = /\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 12저1234 
	    var pattern2 = /[가-힣ㄱ-ㅎㅏ-ㅣ\x20]{2}\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 서울12치1233 

	    if (!pattern1.test(v)) { 
	        if (!pattern2.test(v)) { 
	            //result.innerHTML = "No"; 
	            alert('차량번호 형식이 잘못되었습니다. 12저1234 or 서울12치1233');
	            return false;
	        } 
	        else { 
	            //result.innerHTML = "Yes"; 
	            //return true;
	            //alert('차량번호 형식 맞음');
	            return true;
	        } 
	    } 
	    else { 
	        //result.innerHTML = "Yes"; 
	        //return true;
	        //alert('차량번호 형식이 맞음');
	        return true;
	    } 

	    return false;

    }

</script>