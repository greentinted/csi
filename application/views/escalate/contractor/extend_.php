<style>
input{border:0px solid #000; margin:0; background:transparent; width:100%; }
table tr td{border-right:1px solid #000; border-bottom:1px solid #000; }
table{background: #fff none repeat scroll 0 0;
    border-left: 1px solid #000;
    border-top: 1px solid #000;}
    table tr:nth-child(even){background:#ccc;}
    table tr:nth-child(odd){background:#eee;}

input[type="text"]
{
    font-size:40px;
}

input[type="tel"]
{
    font-size:40px;
}
input[type="date"]
{
    font-size:40px;
}

input[type="number"]
{
    font-size:40px;
}


</style>    

<div class="row">   
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                <h2>월정기 연장 <small><?=$parking_type_str?></small></h2>
                <ul class="nav navbar-right panel_toolbox">
                    <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                    </li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="/escalate/contractor/extend_list" class="">입력 내역 보기</a>
                            </li>
                        </ul>

                    </li>
                    <li><a class="close-link"><i class="fa fa-close"></i></a>
                    </li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="x_content" style="text-align: center">
                 <button type="button" class="btn btn-primary contractor_extend">월정기 연장 보고하기</button>

                <form id="" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" >
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="new_contract_did" class="select2_single form-control district_change" tabindex="1">
                            <?php foreach ($districts as $key => $value): ?>
                                <option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $did)?>><?=$value['title']?></option>
                            <?php endforeach ?>
                            </select>

                        </div>
                    </div>
                     
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <select id="new_contractor_wpid"class="select2_single form-control" tabindex="1">
                            <?php foreach ($parkinglot as $key => $value): ?>
                                <option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $wpid)?>><?=$value['title']?></option>                                

                            <?php endforeach ?>

                            </select>

                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" id="access_type" name="access_type" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="<?=$access_type?>">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <input type="hidden" id="ecid" name="ecid" required="required" class="form-control col-md-7 col-xs-12" placeholder="ecid" value="<?=$ecid?>">
                        </div>
                    </div>


                    </form>    



                  <div class="table-responsive">
                    <table id="contract_extend_tbl"class="table table-bordered table-condensed" cellpadding="0" cellspacing="0">
                    <thead>
                        <tr class="headings">
                            <th class="">번호</th>
                            <th class="">차량번호</th>
                            <th class="">금액</th>
                            <th class="">결제날짜</th>
                            <th class="">비고</th>
                        </tr>
                    </thead>
                      <tbody>
                        <?php foreach ($e_list as $key => $value) :?>
                        
                        <tr>
                          <td width="100px"><input type="text"  value="<?=$key+1?>" style="text-align: left; height:140px; " readonly/ ></td>
                          
                      <td><input id="<?=$key?>_car_no" name="<?=$key?>_car_no" type="text" class="form-control" placeholder="26가1111 or 서울12치1233" style="height:140px" value="<?=$value['car_no']?>" /></td>

                      <td><input id="<?=$key?>_amount" name="<?=$key?>_amount" type="number" class="form-control" min="1000" max="" placeholder="10000 ( 숫자만 가능 )" style="height:140px" value="<?=$value['amount']?>" /></td>

                      <td><input id="<?=$key?>_date" name="<?=$key?>_date" type="date" class="form-control" style="height:140px" value="<?=$value['pay_date']?>"/></td>

                      <td><textarea id="<?=$key?>_etc" name="<?=$key?>_etc" rows="4"><?=$value['description']?></textarea></td>
                                
<!--                       <td><input id="<?=$key?>_etc" name="<?=$key?>_etc" type="text" class="form-control" style="height:140px" value="<?=$value['description']?>"/></td>
 -->                        </tr>
                        <?php endforeach?>
                      </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>