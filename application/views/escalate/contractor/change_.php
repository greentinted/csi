<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
		<div class="x_panel">
			<div class="x_title">
				<h2>월정기 변경</h2>
				<a href="/escalate/contractor/change_history"><button type="button" class="btn btn-success  navbar-right">변경 처리 내역</button></a>
				<!-- <ul class="nav navbar-right panel_toolbox">
					<li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
					</li>
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
					</li>
					<li><a class="close-link"><i class="fa fa-close"></i></a>
					</li>
				</ul> -->
				<div class="clearfix"></div>
			</div>
			<div class="x_content">
				<br />
				<form id="change_contractor_form" method="post" data-parsley-validate class="form-horizontal form-label-left" enctype="multipart/form-data" onsubmit="return checkContractForm();" action="/escalate/contractor/change_save">

					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="hidden" id="aid" name="aid" required="required" class="form-control col-md-7 col-xs-12" placeholder="aid" value="">
						</div>
					</div>



					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select class="select2_single form-control district_change" name="did" id="new_contract_did" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $cc_row['did'])?>><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="new_contractor_wpid" name="wpid" class="select2_single form-control" tabindex="1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $cc_row['wpid'])?>><?=$value['title']?></option>

							<?php endforeach ?>

							</select>

						</div>
					</div>



					<!-- <div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="district"> <span class="required">지역</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select name = "did" class="select2_single form-control district_change" tabindex="1">
							<?php foreach ($districts as $key => $value): ?>
								<option name="opt_district" value="<?=$value['did']?>"><?=$value['title']?></option>									
							<?php endforeach ?>
							</select>

						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name"> <span class="required">주차장명</span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="contractor_" name="wpid" class="select2_single form-control" tabindex="-1">
							<?php foreach ($parkinglot as $key => $value): ?>
								<option name="workplace" value="<?=$value['wpid']?>"><?=$value['title']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>
 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자"> 차량 소유자 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="car_owner" name="car_owner" class="form-control col-md-7 col-xs-12 han" placeholder="실명" value="<?=$cc_row['car_owner']?>">
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="cellphone"> 연락처
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="tel" id="tel_number" name="tel_number" class="form-control col-md-7 col-xs-12" pattern="\d{2,3}-\d{3,4}-\d{4}" placeholder="010-4885-4885" value="<?=$cc_row['tel_num']?>">
							<span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group" style="padding-bottom: 30px">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> 변경날짜
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="date" id="change_date" name="change_date" class="form-control col-md-7 col-xs-12" placeholder="2019-07-01" value="<?=$cc_row['change_date']?>">
							<span class="fas fa-envelope form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">변경 전 차량번호 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="before_car_no" name="before_car_no" class="form-control car_no col-md-7 col-xs-12 han" placeholder="26가1111 이거나 서울12치1233" value="<?=$cc_row['before_car_no']?>">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_type">변경 전 감면유형 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="before_dis_id" name="before_dis_id" class="select2_single form-control" tabindex="-1">
								<option value="0">일반 (100%)</option>
							<?php foreach ($discount_type as $key => $value): ?>
								<option name="discount" value="<?=$value['dis_public_id']?>" <?=return_select($value['dis_public_id'], $cc_row['before_dis_id'])?>><?=$value['title_desc']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 변경 전 차종
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="before_car_type" name="before_car_type" class="form-control col-md-7 col-xs-12 han" placeholder="티볼리" value="<?=$cc_row['before_car_type']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group" style="padding-bottom: 30px">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 변경 전 요금
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="before_fee" name="before_fee" class="form-control col-md-7 col-xs-12" placeholder="48000"  value="<?=$cc_row['before_fee']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">변경 후 차량번호 <span class="required"></span>
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="after_car_no" name="after_car_no" class="form-control car_no col-md-7 col-xs-12 han" placeholder="26가1111 이거나 서울12치1233" value="<?=$cc_row['after_car_no']?>">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<!-- profile 이미지 -->
					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="discount_type">변경 후 감면유형 
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<select id="after_dis_id" name="after_dis_id" class="select2_single form-control" tabindex="-1">
								<option value="0">일반 (100%)</option>
							<?php foreach ($discount_type as $key => $value): ?>
								<option name="discount" value="<?=$value['dis_public_id']?>" <?=return_select($value['dis_public_id'], $cc_row['after_dis_id'])?>><?=$value['title_desc']?></option>
							<?php endforeach ?>
							</select>

						</div>
					</div>


					<div class="form-group" >
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 변경 후 차종
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="after_car_type" name="after_car_type" class="form-control col-md-7 col-xs-12 han" placeholder="티볼리"  value="<?=$cc_row['after_car_type']?>">
							<span class="fa fa-car form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<div class="form-group" style="padding-bottom: 30px">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="car_type"> 변경 후 요금
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="number" id="after_fee" name="after_fee" class="form-control col-md-7 col-xs-12" placeholder="48000" value="<?=$cc_row['after_fee']?>">
							<span class="fa fa-phone form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="비고"> 비고
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<textarea id="message" name="description" class="form-control" rows="10"><?=$cc_row['description']?></textarea>
						</div>
					</div>


					<div class="form-group">
						<label class="control-label col-md-3 col-sm-3 col-xs-12" for="소유자">변경 확인자
						</label>
						<div class="col-md-6 col-sm-6 col-xs-12">
							<input type="text" id="confirm_user" name="confirm_user" class="form-control col-md-7 col-xs-12" placeholder="실명" value="<?=$confirm_user_id?>" readonly>
							<span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
						</div>
					</div>

					<input type="hidden" name="confirm_user_aid" value="<?=$confirm_user_aid?>">

					<div class="ln_solid"></div>
					<div class="form-group">
						<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">							
							<button class="btn btn-primary" type="reset">초기화</button>
							<button type="submit" class="btn btn-success">저장</button>
						</div>
					</div>
					<input type="hidden" id="edit_type" name="edit_type" value="$cc_type">
					<input type="hidden" id="ccid" name="ccid" value="<?=$cc_row['ccid']?>">
				</form>
			</div>
		</div>
	</div>
</div>


<script type="text/javascript">
    function checkContractForm(){
     //    var v  = $('.car_no').val();

	    // //var result = document.getElementById('result'); 

	    // var pattern1 = /\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 12저1234 
	    // var pattern2 = /[가-힣ㄱ-ㅎㅏ-ㅣ\x20]{2}\d{2}[가-힣ㄱ-ㅎㅏ-ㅣ\x20]\d{4}/g; // 서울12치1233 

	    // if (!pattern1.test(v)) { 
	    //     if (!pattern2.test(v)) { 
	    //         //result.innerHTML = "No"; 
	    //         alert('차량번호 형식이 잘못되었습니다. 12저1234 or 서울12치1233');
	    //         return false;
	    //     } 
	    //     else { 
	    //         //result.innerHTML = "Yes"; 
	    //         //return true;
	    //         //alert('차량번호 형식 맞음');
	    //         return true;
	    //     } 
	    // } 
	    // else { 
	    //     //result.innerHTML = "Yes"; 
	    //     //return true;
	    //     //alert('차량번호 형식이 맞음');
	    //     return true;
	    // } 

	    // return false;

    }

</script>