<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2 style="padding-top: 10px">시간대별 이용차량 조사 내역 <small></small></h2>
        <a href="/escalate/parking_count/index"><button type="button" class="btn btn-success  navbar-right">입력화면으로 돌아가기</button></a>
        <div class="clearfix"></div>
      </div>
      <form id="period_form" action="/escalate/parking_count/history" metho="POST">  

      <div class="col-sm-12">
      <div class="form-group">
            <label class="col-sm-4 control-label"><small>조회기간</small></label>

            <div class="col-sm-3">
              <div class="input-group">
                              <span class="add-on input-group-addon"><i class="glyphicon glyphicon-calendar fa fa-calendar"></i></span>
                              <input type="text" class="query_period" name="query_period" id="approve_vac_query_period" class="form-control" value="<?=$query_period?>" />
              
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" id="approve_vac_query_btn">조회하기</button>
                </span>
              </div>
            </div>
          </div>
          </form>


      <div class="x_content">

        <table id="bugreport_datatable" class="table table-striped table-bordered bulk_action" style="font-size: 17px">
          <thead>
            <tr>
              <th>#</th>
              <th>작성일자</th>
              <th>처리자</th>              
              <th>지역</th>
              <th>주차장</th>
              <th>요일</th>
              <th>11시</th>
              <th>16시</th>
              <th>20시</th>
              <th>24시</th>
              <th>비고</th>
              <th>FROM</th>
              <th>TO</th>
              <th>ACTION</th>
           <!--    <?php if($user_type != "operator") { ?>
                <th>Action</th>
              <?php } ?> -->
            </tr>
          </thead>
          <tbody>
            <?php foreach ($history_list as $key => $value): ?>
              <tr>
                <td id="pcid"><?=$value['pcid']?></td>
                <td><?=$value['create_date']?></td>
                <td><?=$value['confirm_id']?></td>
                <td><?=$value['district_title']?></td>
                <td><?=$value['workplace_title']?></td>
                <td><?=$value['day_']?></td>
                <td><?=$value['cnt_11']?></td>
                <td><?=$value['cnt_16']?></td>
                <td><?=$value['cnt_20']?></td>
                <td><?=$value['cnt_24']?></td>
                <td><?=$value['remarks']?></td>
                <td><?=$value['from_']?></td>
                <td><?=$value['to_']?></td>
                <td><button type="button" id="<?=$value['pcid']?>" class="mod_parking_count btn btn-success btn_delete_change">수정</button></td>
<!--                 <?php if($user_type != "operator") { ?>
                  <td>
                    <button type="button" id="<?=$value['pcid']?>" class="btn btn-danger btn_delete_change">삭제</button>
                  </td>
                <?php } ?>  -->
              </tr>
            <?php endforeach ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
