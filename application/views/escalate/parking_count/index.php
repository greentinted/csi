<style>
input{border:0px solid #000; margin:0; background:transparent; width:100%}
table tr td{border-right:1px solid #000; border-bottom:1px solid #000;}
table{background: #fff none repeat scroll 0 0;
    border-left: 1px solid #000;
    border-top: 1px solid #000;}
    table tr:nth-child(even){background:#eee;}
    table tr:nth-child(odd){background:#eee;}
</style>    

<div class="x_panel">
			<h2>시간대별 이용차량 조사표</h2>
	<div class="x_title">
	</div>
</div>

<form id="form_parking_count" action="/escalate/parking_count/save_parking_count">
    <div class="x_content">
      <div class="form-group">
            <label class="control-label col-md-1 col-sm-1 col-xs-12" for="district"> <span class="required">지역</span>
            </label>
            <div class="col-md-3 col-sm-3 col-xs-12">
             <select class="select2_single form-control district_change" name="did" id="new_contract_did" tabindex="1">
              <?php foreach ($districts as $key => $value): ?>
                <option name="opt_district" value="<?=$value['did']?>" <?=return_select($value['did'], $did)?>><?=$value['title']?></option>
              <?php endforeach ?>
              </select>

            </div>
          </div>
        </br>

   <div class="form-group" style = "padding-top: 10px">
      <label class="control-label col-md-1 col-sm-1 col-xs-12" for="first-name"> <span class="required">주차장명</span>
      </label>
      <div class="col-md-3 col-sm-3 col-xs-12">
         <select id="new_contractor_wpid" name="wpid" class="select2_single form-control" tabindex="1">
              <?php foreach ($parkinglot as $key => $value): ?>
                <option name="workplace" value="<?=$value['wpid']?>" <?=return_select($value['wpid'], $did)?>><?=$value['title']?></option>

              <?php endforeach ?>

              </select>
      </div>
    </div>
        </br>
          <div class="form-group" style = "padding-top: 10px">
                <label for="from"  class="col-md-1 col-sm-1 col-xs-12  form-control-label">조사기간</label>    
                      <div class="col-md-3 col-sm-3 col-xs-12">
                        <div class='col-sm-12 col-md-12 col-xs-12 input-group date form_datetime'>
                          <input type="text" id="from_date" name="from_date" required="required" class="form-control" placeholder="2019-05-01" id="datetimepicker1" ui-jp="datetimepicker">
                          <span class="input-group-addon">
                              <span class="fa fa-calendar"></span>
                          </span>             
                        </div>
                      </div>                  
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class='col-sm-12 col-md-12 col-xs-12 input-group date form_datetime'>
                    <input type="text" id="to_date" name="to_date" required="required" class="form-control" placeholder="2019-05-01" id="datetimepicker1" ui-jp="datetimepicker">
                    <span class="input-group-addon">
                      <span class="fa fa-calendar"></span>
                    </span>      
                  </div>
              </div>                  
            </div> 
        </div>

  <div class="x_content">

  <div class="table-responsive">


    <table class="table table-bordered table-condensed" cellpadding="0" cellspacing="0">
    <thead>
        <tr class="headings">
            <th class="column-title" style="text-align: center">조사요일</th>
            <th class="column-title" style="text-align: center">11시</th>
            <th class="column-title" style="text-align: center">16시</th>
            <th class="column-title" style="text-align: center">20시</th>
            <th class="column-title" style="text-align: center">24시</th>
            <th class="column-title" style="text-align: center">비고</th>
        </tr>
    </thead>
      <tbody>
        <?php $han_day = ["월","화","수","목","금","토","일"]; ?>
        <?php foreach ($day as $key => $value) { ?>
        <tr>
          <td><input type="text" name="day_" class="form-control" value="<?=$han_day[$key]?>" readonly style="text-align: center"/></td>
          <td><input type="text" name="<?=$value?>_time_cnt[eleven]" class="form-control" /></td>
          <td><input type="text" name="<?=$value?>_time_cnt[sixteen]" class="form-control" /></td>
          <td><input type="text" name="<?=$value?>_time_cnt[twenty]" class="form-control" /></td>
          <td><input type="text" name="<?=$value?>_time_cnt[twenty_four]" class="form-control" /></td>
          <td><input type="text" name="<?=$value?>_remarks" class="form-control" /></td>
        </tr>       
        <?php }?>
      </tbody>
    </table>

  </div>
</div>

    <div class="form-group">
            <div class="col-md-12 col-sm-12 col-xs-12 col-md-offset-5">
              <button type="submit" class="btn btn-success" id="btn_save_parking_count">저장</button>             
              <button class="btn btn-primary" type="reset">초기화</button>
              <a href="/escalate/parking_count/history"><button type="button" class="btn btn-warning">내역보기</button></a>
          </div>
</form>



      