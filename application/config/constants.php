<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


mb_internal_encoding("UTF-8");
date_default_timezone_set('Asia/Seoul');

@define('REMOTE_ADDR', $_SERVER['HTTP_X_FORWARDED_FOR'] ? array_pop(preg_split('/\s*,\s*/', $_SERVER['HTTP_X_FORWARDED_FOR'])) : $_SERVER['REMOTE_ADDR']);
define('TIMEZONE_OFFSET_STR', 'Asia/Seoul');
define('TIMEZONE_OFFSET', '+09:00');

@define('MAINTENANCE', false); //true이면 점검 페이지 표시


/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code



define('COMMON_MCACHE_PORT', '11211');


if(PROD_MODE == 'DEV' || PROD_MODE == 'DIST'){
	define('MYSQL_HOST', 'localhost');
	define('MYSQL_USER', 'ppdbadminpp');
	define('MYSQL_PASS', 'Ekrvnf!!33');

	define('MYSQL_HOST1', '172.172.221.208');
	define('MYSQL_USER1', 'ppdbadminpp');
	define('MYSQL_PASS1', 'Ekrvnf!!33');

	
	// define('MYSQL_HOST', 'localhost');
	// define('MYSQL_USER', 'root');
	// define('MYSQL_PASS', '111111');
	define('BASE_URL', 'http://parking_portal');   // 클로즈 전 확인 ( greentinted )

	define('COMMON_MCACHE_HOST1', 'localhost');


	define('REDIS_HOST', '127.0.0.1');
	define('REDIS_PORT', '6379');



} else {
	define('MYSQL_HOST', '172.172.4.191');
	define('MYSQL_USER', 'ppdbadminpp');
	define('MYSQL_PASS', 'Ekrvnf!!33');


	define('MYSQL_HOST1', '172.172.221.208');
	define('MYSQL_USER1', 'ppdbadminpp');
	define('MYSQL_PASS1', 'Ekrvnf!!33');
	

	// define('MYSQL_HOST', 'localhost');
	// define('MYSQL_USER', 'root');
	// define('MYSQL_PASS', '111111');
	define('BASE_URL', 'http://parking_portal');   // 클로즈 전 확인 ( greentinted )

	define('COMMON_MCACHE_HOST1', 'localhost');

	define('REDIS_HOST', '127.0.0.1');
	define('REDIS_PORT', '6379');


}

// if (IS_REAL) {
// 	define('MYSQL_HOST', '172.172.221.200');
// 	define('MYSQL_USER', 'ppdbadminpp');
// 	define('MYSQL_PASS', 'Ekrvnf!!33');

// 	// define('MYSQL_HOST', 'localhost');
// 	// define('MYSQL_USER', 'root');
// 	// define('MYSQL_PASS', '111111');
// 	define('BASE_URL', 'http://parking_portal');   // 클로즈 전 확인 ( greentinted )

// 	define('COMMON_MCACHE_HOST1', 'localhost');

// } else {
// 	define('MYSQL_HOST', '172.172.1.249');
// 	define('MYSQL_USER', 'ppdbadminpp');
// 	define('MYSQL_PASS', 'Ekrvnf!!33');

// 	// define('MYSQL_HOST', '127.0.0.1');
// 	// define('MYSQL_USER', 'root');
// 	// define('MYSQL_PASS', '111111');


// 	define('BASE_URL', 'http://parking_portal');   // 클로즈 전 확인 ( 

// 	// define('MYSQL_HOST', 'localhost');
// 	// define('MYSQL_USER', 'root');
// 	// define('MYSQL_PASS', '111111');


// 	define('COMMON_MCACHE_HOST1', 'localhost');

// }



define('MCACHE_KEY', 'parking_portal');
define('ADD_CONTENTS', 'add_contents');   // 클로즈 
define('SELECT_TYPE', 'Select Type.');   // 클로즈 


/*
* 어떻게 사용하는지 보여주기 위함 ..
*/

define('CDN_URL', 'https://s3-ap-southeast-1.amazonaws.com/parking_portal');
//define('CDN_URL', 'https://d4zj32lekmwbn.cloudfront.net');

// 디폴트 이미지
// define('DEFAULT_IMAGE', CDN_URL . '/no_image_thumb.jpg');


class constants {
	static function get_client_ip() {
		return REMOTE_ADDR;
	}

	static function is_office_ip() {
		if(REMOTE_ADDR == '172.172.0.1') {
			return true;
		} 
		return false;
	}
}