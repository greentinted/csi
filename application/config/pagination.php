<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['num_links'] = 4;

$config['page_query_string'] = true;

$config['full_tag_open'] = '<div style="width: 100%;margin: 0 auto;
    text-align: center;"><ul class="pagination pagination-sm m-0" style="justify-content:center">';
// 페이지네이션 왼쪽에 위치할 여는태그입니다.

$config['full_tag_close'] = '</ul></div>';
// 페이지네이션 오른쪽에 위치할 닫는태그 입니다.

// "처음으로"링크 커스터마이징 Customizing the First Link

$config['first_link'] = '<i class="fa fa-angle-double-left"></i>';
// 페이지네이션 맨 왼쪽에 위치할 "처음으로" 링크 글을 설정합니다.First 대신 "처음","맨처음" 등을 쓰시는게 좋겠지요 :)
// 값을 FALSE로 설정하면, 이 링크는 렌더링 되지않습니다.

$config['first_tag_open'] = '<li>';
// "처음으로"링크의 여는태그 입니다.

$config['first_tag_close'] = '</li>';
// "처음으로"링크의 닫는태그 입니다.
// "끝으로"링크 커스터마이징 Customizing the Last Link

$config['last_link'] = '<i class="fa fa-angle-double-right"></i>';
// 페이지네이션 맨 오른쪽에 위치할 "끝으로" 링크 글을 설정합니다.
// 값을 FALSE로 설정하면, 이 링크는 렌더링 되지않습니다.

$config['last_tag_open'] = '<li>';
// "끝으로"링크의 여는태그 입니다.

$config['last_tag_close'] = '</li>';
// "끝으로"링크의 닫는태그 입니다.
// "다음" 링크 커스터마이징 Customizing the "Next" Link

$config['next_link'] = '<i class="fa fa-chevron-right"></i>';
// "다음" 링크 글을 설정합니다.
// 값을 FALSE로 설정하면, 이 링크는 렌더링 되지않습니다.

$config['next_tag_open'] = '<li>';
// "다음"링크의 여는태그 입니다.

$config['next_tag_close'] = '</li>';
// "다음"링크의 닫는태그 입니다.
// "이전"링크 커스터마이징 Customizing the "Previous" Link

$config['prev_link'] = '<i class="fa fa-chevron-left"></i>';
// "이전" 링크 글을 설정합니다.
// 값을 FALSE로 설정하면, 이 링크는 렌더링 되지않습니다.

$config['prev_tag_open'] = '<li>';
// "이전"링크의 여는태그 입니다.

$config['prev_tag_close'] = '</li>';
// "이전"링크의 닫는태그 입니다.
// "현재페이지"링크 커스터마이징 Customizing the "Current Page" Link

$config['cur_tag_open'] = '<li class="active"><a href>';
// "현재페이지"링크의 여는태그 입니다.

$config['cur_tag_close'] = '</a></li>';
// "현재페이지"링크의 닫는태그 입니다.
// 링크숫자 커스터마이징 Customizing the "Digit" Link

$config['num_tag_open'] = '<li>';
// 링크숫자 링크의 여는태그 입니다.

$config['num_tag_close'] = '</li>';
// 링크숫자 링크의 닫는태그 입니다.