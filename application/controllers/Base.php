<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* This is Example Controller
*/
class Base extends CI_Controller {

	private $account_row;

	private $version;

	private $api_key = '';
	private $mm = '';
	public $vendor_id = 0;


	const TABLE_LIMIT = 20;

	public function __construct() {

		parent::__construct();

		//$this->load->library(['session' , 'Account_library']);
		//$this->load->helper('url');

		// 로그인 하기 젼에 일단 일반 유저로 ...

		$this->version = "v.1.2.01";


		///$this->set_admin('operator');

		//$this->authenticate();
	}

	public function request_test($account_row = ''){
		debug_var('sdfsafsdfsafsfs');
		exit;
	}


	public function makePagination($base_url = '', $total_rows, $per_page = self::TABLE_LIMIT) {

		$this->load->library('pagination');
		$config = [];
		$config['base_url'] = $base_url; //'';


		$config['total_rows'] = $total_rows;
		$config['per_page'] = $per_page;
		$config['num_links'] = 9;

		// debug_var($total_rows);
		// debug_var($per_page);
		// exit;



		$this->pagination->initialize($config);

		$bind_data[] = $per_page;
		$bind_data[] = $config['per_page'];

		// debug_var($list);
		// exit;
		// $this->view_data['list'] = $list;
		return $this->pagination->create_links();
	}


	public function makeDataList($table = '', $where = '', $order = '', $bind_data = [], $search_keyword = '', $limit = self::TABLE_LIMIT , $select = '*') {
		$per_page = $this->input->get_post('per_page');
		if (!$per_page) {
			$per_page = 0;
		}

		//debug_var($where);
		// debug_var($order);
		// debug_var($bind_data);

		if (trim($where)) {
			$where = trim($where);
			//$where = substr($where, 3);
			// debug_var($where);
			// exit;
			$where = ' WHERE ' . $where;
		}
		//debug_var($where);
		$db_name = 'csi';
		$total_rows = SQL::DB($db_name)->result('SELECT COUNT(*) FROM ' . $table . $where, $bind_data);


		$bind_data[] = $per_page;
		$bind_data[] = $limit;

		$list = SQL::DB($db_name)->fetchAll('SELECT ' .$select. ' FROM ' . $table . $where . ' ORDER BY ' . $order . ' LIMIT ?,?', $bind_data);


		return ['list' => $list, 'total_rows' => $total_rows];
	}



	public function request_login($login_id = '' , $passwd = ''){
		$login_id = _trim($this->input->get_post('login_id'));	
		$passwd = _trim($this->input->get_post('passwd'));	





		$account_row = SQL::DB('csi')->fetchRow('SELECT * FROM tACCOUNT as a , tACCOUNT_ADD as b  , tFACILITY as c WHERE id = ? and a.aid = b.aid and b.fc_id = c.fc_id and a.status = ?', [$login_id , true]);

		
		// debug_var($account_row);
		// debug_var($login_id);
		// exit;


		if($account_row) 
		{ 

			$this->set_admin($account_row['admin_type']);
			if(!password_verify($passwd , $account_row['password'])){

				goto_caution('패스워드가 일치하지 않습니다' , '/home/login');
				// basic_caution('패스워드가 일치하지 않습니다');
				exit;
			} else {
				// debug_log($login_id);
				// debug_log($passwd);

				$ses = Account_library::setLogAccountLog($account_row);

				//$profile_img = Account_library::setProfileImg($account_row['profile_img']);

				$data = ['is_login' => true, 'aid' => $account_row['aid'] , 'ses'=>$ses , 'admin_type'=>$account_row['admin_type'] , 'profile_img'=>$profile_img , 'real_name'=>$account_row['name']];

				$this->session->set_userdata($data);
				if($account_row['admin_type'] != 'super_admin'){
					$this->set_cookie_custom('parking_lot_type' , $account_row['ptid']);	
				} else {
					$ptid = $this->get_parking_lot_type();
					if($ptid){
					$this->set_cookie_custom('parking_lot_type' , 1);	
					}
				}
				// debug_log($return_url);
				// $return_url = '';
				if (!$return_url) {
					// if($account_row['user_type']  != 'operator'){
					//  	$return_url = '/admin/dashboard';
					// } else {
						//$return_url = '/admin/workrecord?aid='.$account_row['aid'];
					$return_url = '/home/map';
					// }
				}

				$this->account_row = $account_row;
				ExternalUtil::redirect($return_url);

			}
		} else {
			back_caution('계정이 존재하지 않습니다.');	
		}


	}

	protected function error($msg) {
		$this->output(FALSE, $msg);
	}


	protected function success_data($data) {

		//debug_log($data);
		$this->output(TRUE, $data);
	}

	private function output($success, $arr = array(), $code = 0, $is_empty = false) {
		$ci = &get_instance();

		if (!is_array($arr)) {
			$ret = array();
		}

		//debug_var($ret);

		if ($is_empty == false) {

			$ret['err'] = '';
			$ret['status'] = $success;
			if ($success) {
				$ret = array_merge($arr, $ret);

			} else {
				$ret['msg'] = is_object($arr) ? $arr->getMessage() : (string) $arr;
				$ret['error'] = $code;
				$ret['err'] = is_object($arr) ? exceptionHandler($arr) : "";
			}
		} else {
			$ret = $arr;
		}

		if ($success == FALSE) {

			$ci->load->library('user_agent');

			$logMsg = array();
			$request = array(
				'get' => $ci->input->get(),
				'post' => $ci->input->post(),
			);
			$logMsg['request'] = $request;
			$logMsg['agent'] = $ci->agent->agent_string();
			$logMsg['ip'] = $_SERVER['REMOTE_ADDR'];
		}


		//debug_var($ret);

		$ci->output->set_content_type('application/json;charset=UTF-8')->set_output(json_encode($ret, JSON_UNESCAPED_UNICODE));
		$ci->output->_display();
		exit();
	}

	public function set_parking_lot(){

		$parking_lot_type = trim($this->input->get_post('id'));

		$this->set_cookie_custom('parking_lot_type' , $parking_lot_type);

 	 	$this->success_data(['parking_lot_type' => $parking_lot_type, 'stats' => true]);

	}

	public function set_cookie_custom($string , $parking_lot_type){
    	set_cookie($string, $parking_lot_type , 3600 * 24 * 30);
	}

	public function get_admin_type(){
		return get_cookie('admin_type');
	}


	public function get_parking_lot_type(){
		
		$parking_lot_type = get_cookie('parking_lot_type');

		// debug_var($parking_lot_type);
		// exit;

		return $parking_lot_type;
	}

	public function get_approve_vac_query_str(){
		return get_cookie('approve_vac_query_str');
	}

	public function set_approve_vac_query_str($str){
		$this->set_cookie_custom('approve_vac_query_str', $str );
	}


	public function get_history_work_query_str(){
		return get_cookie('history_work_query_str');
	}

	public function set_history_work_query_str($str){
		$this->set_cookie_custom('history_work_query_str', $str );
	}

	public function get_history_worktime_query_str(){
		return get_cookie('history_worktime_query_str');
	}

	public function set_history_worktime_query_str($str){
		$this->set_cookie_custom('history_worktime_query_str', $str );
	}

	public function get_history_leave_query_str(){
		return get_cookie('history_leave_query_str');
	}

	public function set_history_leave_query_str($str){
		$this->set_cookie_custom('history_leave_query_str', $str );
	}

	public function mobileCheck(){
		$HTTP_USER_AGENT = $_SERVER['HTTP_USER_AGENT']; 
	    $MobileArray  = array("iphone","lgtelecom","skt","mobile","samsung","nokia","blackberry","android","android","sony","phone");

	    $checkCount = 0; 
	        for($i=0; $i<sizeof($MobileArray); $i++){ 
	            if(preg_match("/$MobileArray[$i]/", strtolower($HTTP_USER_AGENT))){ $checkCount++; break; } 
	        } 
	   return ($checkCount >= 1) ? "Mobile" : "Computer"; 

	}


	public function authenticate(){




		if($this->mobileCheck() == "Mobile"){ 
		    // debug_var("모바일 접속."); 
		    close_caution("모바일에서는 접속할 수 없습니다.");
		}

		$is_login = $this->session->userdata('is_login');
		$ses = $this->session->userdata('ses');
		//$is_login = '';
		//$ses = '';
		//debug_var($ses);
		// 로그인이 필요한 경우이다. 

		if (!$is_login && !$ses) {
			$login_id = _trim($this->input->get_post('login_id'));
			$passwd = _trim($this->input->get_post('passwd'));

			if(!$login_id || !$passwd){
				// 로그인 페이지로 이동한다..  
				// ExternalUtil::redirect('/base/');

				$this->login();	
			}

		} else { 

			//if($this->check_dup_login($ses)){
				$return_url = _trim($this->input->get_post('return_url'));

				// debug_var('herer');
				// $nav_info = Admin_library::getNavigationInfos();
				// $view_data['nav_infos'] = $nav_info;
				$account = $this->getAccountInfo();

				// if(!$account['status']){
				// 	basic_caution('퇴사중인 인원은 접근할 수 없습니다.');
				// 	exit;
				// }

				// $ptid = $this->get_parking_lot_type();

				// $board_type = Board_library::getBoardTypes($ptid);
				// $board_list = Board_library::getBoardLists($ptid);

				// debug_var($ptid);

				// $board_info = Board_library::getBoardTypes(false, Const_library::BOARD_LIST_TYPE_NOTICE ,  $ptid);
				// debug_var($board_info);
				// exit;
				$view_data['account'] = $account;
				// $view_data['board_type'] = $board_type;
				// $view_data['board_list'] = $board_list;


				$this->template->write_view('sidenavs', 'template/default_sidenavs', $view_data , true);
				$this->template->write_view('navs', 'template/default_topnavs.php', $view_data  , true);

				// $login_id = _trim($this->input->get_post('login_id'));
				// $passwd = _trim($this->input->get_post('passwd'));

				$this->setLogin($ses);
			//}
				//ExternalUtil::redirect('/home/notice');
				// $this->request_login($login_id , $passwd);
		}
	}

	public function check_dup_login($ses){
		if ( $ses != $this->account_row['ses']) {

			$this->destroy_data();
			$err_msg = '<h5>already sign in other places.</h5>' . PHP_EOL . '<small>IP : ' . $this->account_row['login_ip'] . PHP_EOL . $this->account_row['login_user_agent'] . '</small>';
			$err_msg = nl2br($err_msg);
			if ($this->input->is_ajax_request()) {
				$this->output(FALSE, $err_msg);
				return false;
				// throw new Exception($err_msg . 'please sign in again.');
			} else {

				$view_data['alert'] = $err_msg;
				//$this->load->view('admin/login', $view_data);

				// debug_var($data);
				// exit;
				$this->output->_display();
				return false;
			}
		}

		return true;

	}

	public function setLogin($ses){
		$this->account_row = $this->getAccountInfo();
		if (!$this->account_row) {
		 	$this->destroy();
		 	//exit;
		}
	}

	public function logout(){

		//Account_library::setLogOutAccountLog($this->account_row);
		$this->destroy_data();

		if (!$this->account_row) {
			$this->destroy();
		}

		ExternalUtil::redirect('/');


		//$this->login(true);

		//$this->login();
		//ExternalUtil::redirect('/base/login_template');
	}


	public function getAccountInfo() {
		$aid = $this->session->userdata('aid');


		if(!$this->account_row){
			$this->account_row = Account_library::getAccountFullInfo($aid);	

		}


		//$account = [];
		//$account['real_name']

		return $this->account_row;

	}

	public function setAccountInfo(){
		$aid = $this->session->userdata('aid');
		$this->account_row = Account_library::getAccountFullInfo($aid);	

		return $this->account_row;

	}


	public function get_aid(){
		return $this->session->userdata('aid');
	}

	public function set_admin($user_type){
					
		if($user_type == 'operator'){
			define('IS_ADMIN', false);					

		} else {
			define('IS_ADMIN', true);		

		}
	}

	public function destroy() {

		$this->destroy_data();
		$this->load->helper('url');
		
		//ExternalUtil::redirect($redirect_url);
	}

	public function destroy_data() {
		$_SERVER['PHP_AUTH_USER'] = NULL;
		$_SERVER['PHP_AUTH_PW'] = NULL;
		$this->session->unset_userdata(['is_login', 'aid', 'ses' , 'user_type']);
	}


	public function index() {
		if (IS_ADMIN) {
			$return_url = '/home/participatory';
		} else {
			$return_url = '/home/participatory';
		}

		ExternalUtil::redirect($return_url);
	}

	public function login($logout=false) {

		$this->template->set_template('default');
		//if($logout){
			$this->template->render();			
		//}
		//$this->template->write_view('content', 'template/simple', '', true);
		//$this->template->render();
	}
	public function checkBaseParam(){

		// $mm =  ExternalUtil::cleanText($this->input->get_post('mm'));

		// debug_var($mm);
		// exit;

		$api_key =  ExternalUtil::cleanText($this->input->get_post('api_key'));

		$vendor_id =  ExternalUtil::cleanText($this->input->get_post('vendor_id'));

		//$ip =  ExternalUtil::cleanText($this->input->get_post('ip'));


		$ip =  $_SERVER['REMOTE_ADDR'];


		//$ip =  ExternalUtil::cleanText($this->input->get_post('ip'));

		//debug_log($api_key);
		//debug_log($vendor_id);		


		$this->vendor_id = $vendor_id;


		// debug_log($mm);
		// debug_var($api_key);
		// debug_var($vendor_id);
		// exit;


		//exit;
		// if (ExternalUtil::is_empty($mm)) {
		// 	throw new Exception("Error mm", 409);
		// }

		//api 키 체크


		// $success = mm_check($mm);
		// if ($success == false) {
		// 	throw new Exception("시간값이 올바르지 않습니다. .", -1000);
		// }


		//$row = Account_library::getAccountByKey($vendor_id, $api_key);

		// debug_var($vendor_id);
		// exit;

		if($vendor_id == 'ssch' || $vendor_id == 'ssch_dev'){
			$row = SQL::DB('csi')->fetchRow('SELECT * FROM tVENDOR_ACCOUNT WHERE vendor_id = ? AND api_key = ? AND ip = ?', [$vendor_id , $api_key , $ip]);

		} else {
			$row = SQL::DB('csi')->fetchRow('SELECT * FROM tVENDOR_ACCOUNT WHERE vendor_id = ? AND api_key = ?', [$vendor_id , $api_key]);

		}

		// debug_log('$row');
		// debug_log($vendor_id);
		// debug_log($api_key);
		// debug_log($row);

		$success = true;

		if(!$row){
			throw new Exception("필수파라미터 오류 .", 7003);
			$success = false;
		}


		try {
			return $success;
		} catch(Exception $e){
			$this->Error($e->getMessage() , $e->getCode());
		}


	}	
}