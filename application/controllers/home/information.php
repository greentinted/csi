<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Information extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}
	function index(){

		$result = 1;

        $this->param['section'] = $this->load->view('info/board', $result, true);
        $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}
        //동의확인 플래그를 받아서 가입허가 해줘야함.

		// debug_var($list);
		// exit;

		//$account = parent::get_account_info();

		//$view_data['parking_type_str'] = $parking_type_str;		

		//$this->load->view('/home/map' , $view_data);

		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}

	function view(){

		$result = 1;

	    $this->param['section'] = $this->load->view('info/view', $result, true);
	    $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}

	}	

	function covid19(){

		// 리스트
		$list = Survey_library::GetCovid19List();
		$view_data['list'] = $list;

		// debug_var($list);
		// exit;

		// 업데이트 날짜를 표시하기 위한 최신 mod_date 가져오기 
		$latest_date = Survey_library::Latest_Mod_Date();
		$view_data['update_date'] = date("m.d. H", strtotime($latest_date[0]['latest_date']));
		
		// 상단 집계
		// 작성된 부서를 불러옴
		$divi = Survey_library::CovidDivisionTotal();
		// debug_var($divi);
		
        foreach ($divi as $key => &$value) {
         if($value['dcode'] == 'SN'){
         	$total['SN'] = $value['total'];
         	$view_data['sn'] = $value['total'];
         }
         elseif ($value['dcode'] == 'TC'){
         	$total['TC'] = $value['total'];
         	$view_data['tc'] = $value['total'];
         }
         elseif ($value['dcode'] == 'SJ'){
         	$total['SJ'] = $value['total'];
         	$view_data['sj'] = $value['total'];
         }
         elseif ($value['dcode'] == 'JW'){
         	$total['JW'] = $value['total'];
         	$view_data['jw'] = $value['total'];
         }
        }
         
        $view_data['total'] = array_sum($total);
        $view_data['count'] = count($total);
         // 부서별 인원수


		// debug_var($total);
		// debug_var($view_data);
		 // exit;

	    $this->param['section'] = $this->load->view('info/covid19', $view_data, true);
	    $this->render_l->html($this->param);


	}	


}