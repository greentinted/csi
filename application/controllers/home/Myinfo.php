<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Myinfo extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}
	
	function index(){

		if(!$this->session->userdata('aid')){
			goto_caution('로그인이 필요한 페이지입니다' , '/home/login');
			exit;
		}
		
	$account = $this->getAccountInfo();

		  // debug_var($ses);
		  // exit;

		$email =explode('@' , $account['email']);
		$phone =explode('-' , $account['contact_number']);
	
		$result =[];
		$result['name'] = $account['name'];
		$result['passwd'] = $account['password'];
		$result['email'] = $email;
		$result['phone'] = $phone;
		$result['birth'] = $account['birth'];

		 // debug_var($result);
   		//     exit;

	        $this->param['section'] = $this->load->view('main/myinfo', $result, true);
	        $this->render_l->html($this->param);
	}


//--------------------------------------------------------

	public function save(){

		$aid = $this->session->userdata('aid');
		$name =	_trim($this->input->get_post('aname'));
		$passwd_0 =	_trim($this->input->get_post('passwd_0'));
		$passwd_1 =	_trim($this->input->get_post('passwd_1'));
		$phone = [];
		$phone[0] =	_trim($this->input->get_post('mPhone1'));
		$phone[1] =	_trim($this->input->get_post('mPhone2'));
		$phone[2] =	_trim($this->input->get_post('mPhone3'));
		$email= [];
		$email[0] =	_trim($this->input->get_post('amail_0'));
		$email[1] =	_trim($this->input->get_post('amail_1'));

		  // debug_var($email);
		  // exit;


		$data = [];
		//$data['name'] = $name;
		$data['password'] = password_hash( $passwd_0 , PASSWORD_DEFAULT );
		$data['contact_number'] = implode("-", $phone);
		$data['email'] = implode("@", $email);
		$data['mod_date'] = date("Y-m-d H:i:s");

		if ( password_verify( $passwd_1, $data['password'] ) ) {
			  // echo "<h1>Success</h1>";
			} else {
			  back_caution('비밀번호가 틀립니다.');
			}

		 // debug_var($data);
		 // exit;

		$rs = Account_library::updateAccount($aid, $data);


		 // debug_var($rs);
		 // exit;

			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/home/map');
			}
	}



}

	