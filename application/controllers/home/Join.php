<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Join extends Base
{
	function __construct(){
		parent::__construct();

		//$this->load->library("PHPExcel");
	}
	function index(){

		$result = 1;

        $this->param['section'] = $this->load->view('main/join1', $result, true);
        $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}
        //동의확인 플래그를 받아서 가입허가 해줘야함.

		// debug_var($list);
		// exit;

		//$account = parent::get_account_info();

		//$view_data['parking_type_str'] = $parking_type_str;		

		//$this->load->view('/home/map' , $view_data);

		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}
	function signup(){

		$result = 1;

	    $this->param['section'] = $this->load->view('main/join2', $result, true);
	    $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}

	}


	function check_duplicate_id() {

		$id = _trim($this->input->get_post('id'));


		$row = Account_library::getUserRowByID($id);

		if(!$row){
			$this->success_data(['msg' => '사용가능한 아이디입니다. ', 'is_reload' => false]);	


		} else {
			$this->error('이미 있는 아이디입니다.');
								
		}


	}

	function main(){

		$result = 1;

	    $this->param['section'] = $this->load->view('main/main', $result, true);
	    $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}

	}


	function board(){

		$result = 1;

	    $this->param['section'] = $this->load->view('main/board', $result, true);
	    $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}

	}

	function email(){

	$this->load->library('email');
        $config['useragent'] = '';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.naver.com';
        $config['smtp_user'] = '';
        $config['smtp_pass'] = '';
        $config['smtp_crypto'] = 'ssl';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = 10;
        $config['wordwrap'] = TRUE;
        $config['wrapchars'] = 76;
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['validate'] = FALSE;
        $config['priority'] = 3;
        $config['crlf'] = "\r\n";
        $config['newline'] = "\r\n";
        $config['bcc_batch_mode'] = FALSE;
        $config['bcc_batch_size'] = 200;
         
    $this->email->initialize($config);

    $this->email->from('lenny86@naver.com','juntaek');
    $this->email->to('juntoh86@gmail.com');

    $this->email->subject('SMTP 테스트');
    $data = "<div>SMTP 가냐</div>";
    $this->email->message($data);
    $this->email->send();
	}

	

	
}