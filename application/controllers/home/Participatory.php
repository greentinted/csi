<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Participatory extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}
	function index($ptid = 1){


		if(!$this->session->userdata('aid')){
			goto_caution('로그인이 필요한 페이지입니다' , '/home/login');
			exit;
		}


		$account = $this->getAccountInfo();

		
		if($account['user_type'] == 'admin' || $account['user_type'] == 'guest'){
			$aid = 0;	
		} else {
			
			$aid = $this->get_aid();

		}

	
 		$select = 'a.bid , a.title as title, b.title as pstitle, c.title as optitle, a.description, a.view_count, a.share , d.name  , a.create_date, a.admin_aid, c.color_code as optitle_color , a.ps_code as ps_code, IF(DATE_SUB(NOW(), INTERVAL 3 DAY) <= a.create_date, 1, 0) AS new_icon';
		$table = 'tBOARD as a';
		$table .= ' , tPROCESSING_STATUS  as b';
		$table .= ' , tOPINIONTYPE as c';
		$table .= ' , tACCOUNT  as d';
		$bind_data = [];
		$where = '';

		if($aid){
			$where .= 'a.writer_aid = ? and';
			$bind_data[] = $aid;
		}		
		
		// $where .= ' e.operator_aid = ? and ';
		// $bind_data[] = $aid;

		$where .= ' a.status = ? and ';
		$bind_data[] = true;


		$where .= ' a.optype_code = c.optype_code and a.ps_code = b.ps_code and a.writer_aid = d.aid';

		$order  = ' a.create_date desc';	


		$row = $this->makeDataList($table, $where, $order, $bind_data , '' ,  5 , $select);

		$list  = [];

		if(isset($row['list'])){
			$list = $row['list'];
		}


		//$list = Board_library::getList($aid);

		 // debug_var($this->get_aid());
		 //  debug_var($list);
		 //  exit;

		

		// debug_var($account);
		// exit;



		if($account['user_type'] == 'cjg' || $account['user_type'] == 'admin' || $account['user_type'] == 'super_admin' || $account['user_type'] == 'guest' ){


			//echo("<script>window.location.replace(".$loc.";</script>");

	        $data['type'] = "map"; // 최신순위 부터 가져오는 값
	        $result['list'] = $list;

			$result['pagination'] = $this->makePagination('/home/participatory' , $row['total_rows'] , 5);

   // foreach ($list as $key => $value) {
	        // 	# code...
	        // 	$dat = substr($value['pstitle']);
	        // 	debug_var($dat);
	        // 	exit;
	        // }

	        $this->param['section'] = $this->load->view('main/participatory', $result, true);
	        $this->render_l->html($this->param);



		} else {
			goto_caution('주민참여위원회만 참여할 수 있는 메뉴입니다' , '/home/map');
			exit;			
		}



		// 시민참여의원은 글 쓸수 없음..




		//$account = parent::get_account_info();
		//$view_data['parking_type_str'] = $parking_type_str;		
		//$this->load->view('/home/map' , $view_data);
		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}

	function write(){
		$bid =	_trim($this->input->get_post('bid'));
		if(!$bid){
			$bid = 0;
		}


		$account = $this->getAccountInfo();


		if($account['user_type'] == 'cjg' || $account['user_type'] == 'admin' || $account['user_type'] == 'super_admin' ){


        // $data['type'] = "map"; // 최신순위 부터 가져오는 값
        // $result['vod'] = '';

	        if($bid){
	        	$board = Board_library::getBoard($bid);

	        } else {
	        	$board = [];
	        	$board['title'] = '';
	        	$board['pstitle'] = '';
	        	$board['optitle'] = '';	
				$board['description'] = '';	
				$board['view_count'] = 0;	
				$board['share'] = 0;	
				$board['name'] = '';		
				$board['create_date'] = '';		
				$board['writer_aid'] = 0;	
				$board['ps_code'] = '';	
				$board['optype_code'] = 'program';	

				
	        }


	        // debug_var($board);
	        // exit;



	        $result = [];



	        $type_list = Board_library::getTypeList();


			$result['type_list'] = $type_list;        
			$result['aid'] = $this->get_aid();        
			$result['bid'] = $bid;
			$result['board'] = $board;
			$result['account'] = $account;




	        $this->param['section'] = $this->load->view('main/p_write', $result, true);
	        $this->render_l->html($this->param);
    	} else {
    		back_caution('권한이 없습니다');
    		exit;
    	}

	}

	function save(){
		$bid =	_trim($this->input->get_post('bid'));
		$aid =	_trim($this->input->get_post('aid'));
		$type =	_trim($this->input->get_post('type'));

		$title =	_trim($this->input->get_post('title'));


		$description =	$this->input->get_post('description');

		$data = [];

		$data['optype_code'] = $type;
		$data['writer_aid'] = $aid;
		$data['title'] = $title;
		if($description){
			$data['description'] = $description;
		}
		// debug_var($bid);
		// debug_var($aid);
		// exit;
		if(!$bid){
			$rs = Board_library::insertBoard($data);
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/home/qna');
			}
		} else {

			$rs = Board_library::updateBoard($bid ,$data);
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/home/qna');
			}



		}
	}

	function share(){
        $bid =	_trim($this->input->get_post('bid'));
        $type =	_trim($this->input->get_post('type'));
        $search_keyword =_trim($this->input->get_post('keyword'));
        $sf =	_trim($this->input->get_post('sf'));
        //$result['vod'] = '';

        //debug_var($type);

        //$list = Board_library::getShareList($type);

 		$select = 'a.bid , a.title as title, b.title as pstitle, c.title as optitle, a.description, a.view_count, a.share , d.name  , a.create_date  , a.admin_aid , c.color_code as optitle_color , a.ps_code as ps_code , a.writer_aid ';

		$table = 'tBOARD as a';
		$table .= ' , tPROCESSING_STATUS  as b';
		$table .= ' , tOPINIONTYPE as c';
		$table .= ' , tACCOUNT  as d';
		$bind_data = [];
		$where = 'a.share = ? and a.status = ? and ';
		$bind_data[] = 1;
		$bind_data[] = 1;


		if($type){
			$where .= 'a.optype_code = ? and ';
			$bind_data[] = $type;
		}

		if($search_keyword){
			if($sf == 'all'){
				$where  .= '(a.title LIKE ? OR  a.description LIKE ? ) AND ';	
				$bind_data[] = '%' .$search_keyword. '%';			
				$bind_data[] = '%' .$search_keyword. '%';			

			} else if($sf == 'title'){
				$where  .= 'a.title LIKE ? AND ';	
				$bind_data[] = '%' .$search_keyword. '%';			

			} else if($sf == 'content'){
				$where  .= 'a.description LIKE ? AND ';	
				$bind_data[] = '%' .$search_keyword. '%';			

			}
		}



		$where .= 'a.optype_code = c.optype_code and a.ps_code = b.ps_code and a.writer_aid = d.aid';


		$order  = ' a.create_date desc';	


		$row = $this->makeDataList($table, $where, $order, $bind_data , '' ,  5 , $select);

		$list  = [];

		$result = [];

		if(isset($row['list'])){
			$list = $row['list'];
		}


        foreach ($list as $key => &$value) {
        	# code...
        	$info = Account_library::getUserRow($value['writer_aid']);

        	// debug_var($value['writer_aid']);
        	// debug_var($info);
        	// exit;
        	

        	$value['ptitle'] = substr($info['title'] , 0 , 6 );
        		;        


        	$value['fut_code_'] = strtolower($info['fut_code'])
        		;        

        }

        $result['list'] = $list;




		$result['pagination'] = $this->makePagination('/home/participatory/share?type=' .$type. '&keyword=' .$search_keyword. '&sf=' .$sf  , $row['total_rows'] , 5);

		$result['type'] = $type;
		$result['keyword'] = $search_keyword;
		$result['sf'] = $sf;

        $this->param['section'] = $this->load->view('main/share', $result, true);
        $this->render_l->html($this->param);

	}

	function detail(){
        $bid =	_trim($this->input->get_post('bid'));
        $type =	_trim($this->input->get_post('type'));

        if(!$type){
        	$type ='';
        }

        if(!$bid){
        	back_caution('유효하지 않은 접근입니다');
        	exit;
        }

        $result['row'] = Board_library::getBoard($bid);

        $account = Account_library::getAccountFullInfo($this->get_aid());	

        $result['account'] = $account;
        $result['type'] = $type;
        $rs = array_key_exists("aid", $account);
    // debug_var($rs);
    // exit;
        $rl =  Board_library::getBeforeAfterList($bid);

        $prev_url = '#';
        $prev_title = '이전글이 없습니다.';

        $next_url = '#';
        $next_title = '다음글이 없습니다.';


        if($rl['prev']){
	        $prev_url = '/home/participatory/detail?bid='.$rl['prev']['bid'];
	        $prev_title =  $rl['prev']['title'];
        }


        if($rl['next']){
	        $next_url = '/home/participatory/detail?bid='.$rl['next']['bid'];
	        $next_title = $rl['next']['title'];
        }

        $result['prev_url'] = $prev_url;
        $result['prev_title'] = $prev_title;
        $result['next_url'] = $next_url;
        $result['next_title'] = $next_title;	



		Board_library::updateViewCount($bid);

        $ment_list = Ment_library::getMents($bid , 0);
        // debug_var($ment_list);
        // exit;
        if($rs){ // aid가 있을경우
	        foreach ($ment_list as $key => &$value) {
        		# code...
        		$value['re_list'] = Ment_library::getMents($bid , $value['mid']);

	        	if($account['aid'] == $value['aid']){
	        		//$value['re_list'][$key]['match_aid'] = 1;
	        		$value['match_aid'] = 1;
	        	}else {
	        		$value['match_aid'] = 0;
	        	}

	    	    foreach ($value['re_list'] as $key1 => &$val) {

		        	if($account['aid'] == $val['aid']){
		        		//$value['re_list'][$key]['match_aid'] = 1;
		        		$val['match_aid'] = 1;
		        	}else {
		        		$val['match_aid'] = 0;
		        	}
		        }
		    }

	    }else { // aid가 없을경우
			foreach ($ment_list as $key => &$value) {
    		# code...
    		$value['re_list'] = Ment_library::getMents($bid , $value['mid']);
	    	 }
	   	}

        $result['ment_list'] = $ment_list;

        $this->param['section'] = $this->load->view('main/detail', $result, true);
        $this->render_l->html($this->param);

	}


	public function ment_delete(){
		$mid = _trim($this->input->get_post('mid'));

		$ment_info = Ment_library::getMentInfo($mid);
		$child_ment_count = $ment_info['child_ment_count'];
		$parent_ment_id = $ment_info['parent_ment_id'];
		if($child_ment_count == 0)
		{
			if($parent_ment_id != 0) {
				$parent_ment_info = Ment_library::getMentInfo($parent_ment_id);
				$parent_ment_child_count = $parent_ment_info['child_ment_count'];
				Ment_library::updateChildCount($parent_ment_id,$parent_ment_child_count-1);
			}
			Ment_library::deleteMent($mid);
		} else {
			Ment_library::fakedeleteMent($mid);
		}		
	}

	public function ment_modify(){
		$mid = _trim($this->input->get_post('mid'));
		$bid = _trim($this->input->get_post('bid'));
		//$ntid = _trim($this->input->get_post('ntid'));
		$aid = _trim($this->input->get_post('aid'));
		$body = $this->input->get_post('body');

		$data=[];
		if($mid){
			$data['body'] = $body;
			$rs = Ment_library::updateMent($mid , $data);

			debug_log($rs);

			echo '';
		}

	}



	public function ment_save(){
		$parent_ment_id = _trim($this->input->get_post('parent_ment_id'));
		$bid = _trim($this->input->get_post('bid'));
		//$ntid = _trim($this->input->get_post('ntid'));
		$account = Account_library::getAccountFullInfo($this->get_aid());
		// $aid = SQL::DB('csi')->lastID();
		$body = $this->input->get_post('body');

		// debug_var($parent_ment_id);
		// debug_var($bid);
		  // debug_var($account);
		  // exit;

		$data=[];
		if($parent_ment_id) {
			
			$data['aid'] = $account['aid'];
			$data['bid'] = $bid;				
			$data['body'] = $body;
			$data['status'] = 1;
			$data['parent_ment_id'] = $parent_ment_id;
			$data['child_ment_count'] = 0;
		if($account['admin_type'] == 'user'){
			$data['is_admin'] = 0;
		}else{
			$data['is_admin'] = 1;
		}


			 // debug_var($data);
			 // exit;
			
			Ment_library::insertMent($data);

			$parent_ment_info = Ment_library::getMentInfo($parent_ment_id);
			$count = $parent_ment_info['child_ment_count'] + 1;
			Ment_library::updateChildCount($parent_ment_id, $count);

			$mid = SQL::DB('csi')->lastID();

			//$row = Account_library::getUserRow($aid);

			$html_text = '';

			// $html_text = "<p class='pull-right'><small>new</small></p>";
			// $html_text .= "<img src='".$row['profile_img']."' class='avatar' alt='Avatar'></a>";
			// $html_text .= "<div class='media-body'>";
			// $html_text .= "<h4 class='media-heading user_name'>".$row['id']."</h4>";
			// $html_text .= "<br>".$body;			
			// $echo_data['html_text'] = $html_text;
			// $echo_data['parent_ment_id'] = $parent_ment_id;
			// echo $echo_data;
			echo $html_text;

		} else {			

			$data['bid'] = $bid;				
			$data['aid'] = $account['aid'];
			$data['body'] = $body;
			$data['child_ment_count'] = 0;
			$data['parent_ment_id'] = 0;
			$data['status'] = 1;
		if($account['admin_type'] == 'user'){
			$data['is_admin'] = 0;
		}else{
			$data['is_admin'] = 1;
		}
				// 	 debug_var($data);
			 // exit;

			// $row = Account_library::getUserRow($aid);

			// debug_log($row);

			$rs = Ment_library::insertMent($data);


			$mid = SQL::DB('csi')->lastID();


			// $html_text = "<p>".$body."</p>";
			$html_text = '';

			echo $html_text;

		}
	}


}