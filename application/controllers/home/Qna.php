<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Qna extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}

	function index(){

		if(!$this->session->userdata('aid')){
			goto_caution('로그인이 필요한 페이지입니다' , '/home/login');
			exit;
		}


		$account = $this->getAccountInfo();
		
		if($account['user_type'] == 'admin' || $account['user_type'] == 'guest'){
			$aid = 0;	
		} else {
			
			$aid = $this->get_aid();

		}
		 // debug_var($account);
		 // exit;
	
 		$select = 'a.bid , a.title as title, b.title as pstitle, c.title as optitle, a.description, a.view_count, a.share , d.name  , a.create_date, a.admin_aid, c.color_code as optitle_color , a.ps_code as ps_code, IF(DATE_SUB(NOW(), INTERVAL 3 DAY) <= a.create_date, 1, 0) AS new_icon';
		$table = 'tBOARD as a';
		$table .= ' , tPROCESSING_STATUS  as b';
		$table .= ' , tOPINIONTYPE as c';
		$table .= ' , tACCOUNT  as d';
		$bind_data = [];
		$where = '';

		if($aid){
			$where .= 'a.writer_aid = ? and';
			$bind_data[] = $aid;
		}		
		
		// $where .= ' e.operator_aid = ? and ';
		// $bind_data[] = $aid;

		$where .= ' a.status = ? and ';
		$bind_data[] = true;

		$where .= ' a.optype_code = c.optype_code and a.ps_code = b.ps_code and a.writer_aid = d.aid';

		$order  = ' a.create_date desc';	


		$row = $this->makeDataList($table, $where, $order, $bind_data , '' ,  5 , $select);

		// debug_var($row);
		// exit;



		$list  = [];

		if(isset($row['list'])){
			$list = $row['list'];
		}

		// debug_var($list);
		// exit;

		if($account['user_type'] == 'cjg' || $account['user_type'] == 'admin' || $account['user_type'] == 'super_admin' || $account['user_type'] == 'guest' ){


			//echo("<script>window.location.replace(".$loc.";</script>");

	        $data['type'] = "map"; // 최신순위 부터 가져오는 값
	        $result['list'] = $list;

			$result['pagination'] = $this->makePagination('qna' , $row['total_rows'] , 5);

   // foreach ($list as $key => $value) {
	        // 	# code...
	        // 	$dat = substr($value['pstitle']);
	         	 // debug_var($result);
	         	 // exit;
	        // }

	        $this->param['section'] = $this->load->view('qna/list', $result, true);
	        $this->render_l->html($this->param);


		} else {
			goto_caution('주민참여위원회만 참여할 수 있는 메뉴입니다' , '/home/map');
			exit;			
		}

	}


	function detail(){

		$result = 1;

        $this->param['section'] = $this->load->view('qna/view', $result, true);
        $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}
        //동의확인 플래그를 받아서 가입허가 해줘야함.

		// debug_var($list);
		// exit;

		//$account = parent::get_account_info();

		//$view_data['parking_type_str'] = $parking_type_str;		

		//$this->load->view('/home/map' , $view_data);

		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}

	function write(){

		$bid =	_trim($this->input->get_post('bid'));
		if(!$bid){
			$bid = 0;
		}


		$account = $this->getAccountInfo();


		if($account['user_type'] == 'cjg' || $account['user_type'] == 'admin' || $account['user_type'] == 'super_admin' ){


        // $data['type'] = "map"; // 최신순위 부터 가져오는 값
        // $result['vod'] = '';

	        if($bid){
	        	$board = Board_library::getBoard($bid);

	        } else {
	        	$board = [];
	        	$board['title'] = '';
	        	$board['pstitle'] = '';
	        	$board['optitle'] = '';	
				$board['description'] = '';	
				$board['view_count'] = 0;	
				$board['share'] = 0;	
				$board['name'] = '';		
				$board['create_date'] = '';		
				$board['writer_aid'] = 0;	
				$board['ps_code'] = '';	
				$board['optype_code'] = 'program';				
	        }

	         // debug_var($board);
	         // exit;

	        $result = [];

	        $type_list = Board_library::getTypeList();


			$result['type_list'] = $type_list;        
			$result['aid'] = $this->get_aid();        
			$result['bid'] = $bid;
			$result['board'] = $board;
			$result['account'] = $account;



        $this->param['section'] = $this->load->view('qna/write', $result, true);
        $this->render_l->html($this->param);
    	} else {
    		back_caution('권한이 없습니다');
    		exit;
    	}

	

		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}
        //동의확인 플래그를 받아서 가입허가 해줘야함.

		// debug_var($list);
		// exit;

		//$account = parent::get_account_info();

		//$view_data['parking_type_str'] = $parking_type_str;		

		//$this->load->view('/home/map' , $view_data);

		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}


	function covid19(){

		$result = 1;

        $this->param['section'] = $this->load->view('main/covid-19', $result, true);
        $this->render_l->html($this->param);
		// $this->template->set_template('default');
		//if($logout){
		// $this->template->render();			
		//}
        //동의확인 플래그를 받아서 가입허가 해줘야함.

		// debug_var($list);
		// exit;

		//$account = parent::get_account_info();

		//$view_data['parking_type_str'] = $parking_type_str;		

		//$this->load->view('/home/map' , $view_data);

		//$this->template->write_view('content', 'home/noticelist', $view_data , true);
		//$this->template->render();

	}

	function save(){

		$bid =	_trim($this->input->get_post('bid'));
		$aid =	_trim($this->input->get_post('aid'));
		$title =	_trim($this->input->get_post('title'));
		$description =	$this->input->get_post('description');

		$data = [];

		// $data['optype_code'] = $type;
		$data['writer_aid'] = $aid;
		$data['title'] = $title;
		$data['optype_code'] ='etc';

			if($description){
				$data['description'] = $description;
			}
			// debug_var($bid);
		  // debug_var($data);
		  // exit;
	
		if(!$bid){
			$rs = Board_library::insertBoard($data);
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/home/qna');
			}
		} else {

			$rs = Board_library::updateBoard($bid ,$data);
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/home/qna');
			}

		}
	}

	function view(){

        $bid =	_trim($this->input->get_post('bid'));
        $type =	_trim($this->input->get_post('type'));

        if(!$type){
        	$type ='';
        }

        if(!$bid){
        	back_caution('유효하지 않은 접근입니다');
        	exit;
        }

        $result['row'] = Board_library::getBoard($bid);

        $account = Account_library::getAccountFullInfo($this->get_aid());	

        $result['account'] = $account;
        $result['type'] = $type;
        $rs = array_key_exists("aid", $account);
    // debug_var($rs);
    // exit;
        $rl =  Board_library::getBeforeAfterList($bid);

        $prev_url = '#';
        $prev_title = '이전글이 없습니다.';

        $next_url = '#';
        $next_title = '다음글이 없습니다.';


        if($rl['prev']){
	        $prev_url = '/home/qna/view?bid='.$rl['prev']['bid'];
	        $prev_title =  $rl['prev']['title'];
        }


        if($rl['next']){
	        $next_url = '/home/qna/view?bid='.$rl['next']['bid'];
	        $next_title = $rl['next']['title'];
        }

        $result['prev_url'] = $prev_url;
        $result['prev_title'] = $prev_title;
        $result['next_url'] = $next_url;
        $result['next_title'] = $next_title;	



		Board_library::updateViewCount($bid);

        $ment_list = Ment_library::getMents($bid , 0);
        // debug_var($ment_list);
        // exit;
        if($rs){ // aid가 있을경우
	        foreach ($ment_list as $key => &$value) {
        		# code...
        		$value['re_list'] = Ment_library::getMents($bid , $value['mid']);

	        	if($account['aid'] == $value['aid']){
	        		//$value['re_list'][$key]['match_aid'] = 1;
	        		$value['match_aid'] = 1;
	        	}else {
	        		$value['match_aid'] = 0;
	        	}

	    	    foreach ($value['re_list'] as $key1 => &$val) {

		        	if($account['aid'] == $val['aid']){
		        		//$value['re_list'][$key]['match_aid'] = 1;
		        		$val['match_aid'] = 1;
		        	}else {
		        		$val['match_aid'] = 0;
		        	}
		        }
		    }

	    }else { // aid가 없을경우
			foreach ($ment_list as $key => &$value) {
    		# code...
    		$value['re_list'] = Ment_library::getMents($bid , $value['mid']);
	    	 }
	   	}

        $result['ment_list'] = $ment_list;

        $this->param['section'] = $this->load->view('qna/view', $result, true);
        $this->render_l->html($this->param);

	}


	public function ment_delete(){
		$mid = _trim($this->input->get_post('mid'));

		$ment_info = Ment_library::getMentInfo($mid);
		$child_ment_count = $ment_info['child_ment_count'];
		$parent_ment_id = $ment_info['parent_ment_id'];
		if($child_ment_count == 0)
		{
			if($parent_ment_id != 0) {
				$parent_ment_info = Ment_library::getMentInfo($parent_ment_id);
				$parent_ment_child_count = $parent_ment_info['child_ment_count'];
				Ment_library::updateChildCount($parent_ment_id,$parent_ment_child_count-1);
			}
			Ment_library::deleteMent($mid);
		} else {
			Ment_library::fakedeleteMent($mid);
		}		
	}

	public function ment_modify(){
		$mid = _trim($this->input->get_post('mid'));
		$bid = _trim($this->input->get_post('bid'));
		//$ntid = _trim($this->input->get_post('ntid'));
		$aid = _trim($this->input->get_post('aid'));
		$body = $this->input->get_post('body');

		$data=[];
		if($mid){
			$data['body'] = $body;
			$rs = Ment_library::updateMent($mid , $data);

			debug_log($rs);

			echo '';
		}

	}


	public function ment_save(){
		$parent_ment_id = _trim($this->input->get_post('parent_ment_id'));
		$bid = _trim($this->input->get_post('bid'));
		//$ntid = _trim($this->input->get_post('ntid'));
		$account = Account_library::getAccountFullInfo($this->get_aid());
		// $aid = SQL::DB('csi')->lastID();
		$body = $this->input->get_post('body');

		// debug_var($parent_ment_id);
		// debug_var($bid);
		  // debug_var($account);
		  // exit;

		$data=[];
		if($parent_ment_id) {
			
			$data['aid'] = $account['aid'];
			$data['bid'] = $bid;				
			$data['body'] = $body;
			$data['status'] = 1;
			$data['parent_ment_id'] = $parent_ment_id;
			$data['child_ment_count'] = 0;
		if($account['admin_type'] == 'user'){
			$data['is_admin'] = 0;
		}else{
			$data['is_admin'] = 1;
		}


			 // debug_var($data);
			 // exit;
			
			Ment_library::insertMent($data);

			$parent_ment_info = Ment_library::getMentInfo($parent_ment_id);
			$count = $parent_ment_info['child_ment_count'] + 1;
			Ment_library::updateChildCount($parent_ment_id, $count);

			$mid = SQL::DB('csi')->lastID();

			//$row = Account_library::getUserRow($aid);

			$html_text = '';

			// $html_text = "<p class='pull-right'><small>new</small></p>";
			// $html_text .= "<img src='".$row['profile_img']."' class='avatar' alt='Avatar'></a>";
			// $html_text .= "<div class='media-body'>";
			// $html_text .= "<h4 class='media-heading user_name'>".$row['id']."</h4>";
			// $html_text .= "<br>".$body;			
			// $echo_data['html_text'] = $html_text;
			// $echo_data['parent_ment_id'] = $parent_ment_id;
			// echo $echo_data;
			echo $html_text;

		} else {			

			$data['bid'] = $bid;				
			$data['aid'] = $account['aid'];
			$data['body'] = $body;
			$data['child_ment_count'] = 0;
			$data['parent_ment_id'] = 0;
			$data['status'] = 1;
		if($account['admin_type'] == 'user'){
			$data['is_admin'] = 0;
		}else{
			$data['is_admin'] = 1;
		}
				// 	 debug_var($data);
			 // exit;

			// $row = Account_library::getUserRow($aid);

			// debug_log($row);

			$rs = Ment_library::insertMent($data);


			$mid = SQL::DB('csi')->lastID();


			// $html_text = "<p>".$body."</p>";
			$html_text = '';

			echo $html_text;

		}
	}

}