<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Evaluation extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}
	function index($ptid = 1){

        $data['type'] = "map"; // 최신순위 부터 가져오는 값
        $result['vod'] = '';

        //$list = Facility_library::getFaciltiyList();


        if(!$this->session->userdata('aid')){
			goto_caution('로그인이 필요한 페이지입니다' , '/home/login');
			exit;
		}

		$account = $this->getAccountInfo();

		// debug_var($account);
		// exit;

		if($account['user_type'] == 'ceg' || $account['user_type'] == 'admin' || $account['user_type'] == 'guest'){

			$gettimelist = Survey_library::GetLastEndTime();
			
				// $rs = in_array($start_date, $gettimelist);
				// debug_var($gettimelist);
				
				$start_target = date("Ymd", strtotime($gettimelist[0]['start_date']));
				$end_target = date("Ymd", strtotime($gettimelist[0]['end_date']));
				$timenow = date("Ymd");
				// $timetarget = "2020-05-21 00:00:00";

				if($start_target <= $timenow && $timenow <= $end_target) {
				$lastsurvey = Survey_library::GetLastOrderNum();


					$lastorder = $lastsurvey[0]['order'];
					$pstitle = trim($account['facility']);
					$name = $account['name'];
					$loc = "https://docs.google.com/forms/d/e/1FAIpQLSetI8-vIEoWbNg4HMbSyGJXayhSQ6m3wBHlD2XVBc0ZYug-pQ/viewform?entry.1778528117=".$lastorder."&entry.1547985899=".urlencode($pstitle)."&entry.1394607130=".urlencode($name);

					ExternalUtil::replace_url($loc);

					//echo "<script>window.location.replace(".$loc.";</script>)";

				}else {
					goto_caution('서비스 품질평가 기간이 아닙니다.' , '/home/map');
					exit;
			}

		} else {
			goto_caution('시민서비스평가단만 참여할 수 있는 메뉴입니다' , '/home/map');
			exit;			
		}





        // $this->param['section'] = $this->load->view('main/map', $result, true);
        // $this->render_l->html($this->param);


	}

}
