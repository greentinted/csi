<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Map extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}
	function index($ptid = 1){

        $data['type'] = "map"; // 최신순위 부터 가져오는 값
        $result['vod'] = '';

        //$list = Facility_library::getFaciltiyList();

        $did       = _trim($this->input->get_post('did'));
        $ftype       = _trim($this->input->get_post('ftype'));
        $search_keyword       = _trim($this->input->get_post('keyword'));

        if(!$did)
        	$did = 0;

        if(!$ftype)
        	$ftype = '';

        if(!$search_keyword)
        	$search_keyword = '';



 		$select = '*';
		$table = 'tFACILITY';
		$bind_data = [];
		$where = '';

		if($did || $ftype ||  $search_keyword){
			if($did){
				$where  .= 'did = ? AND ';	
				$bind_data[] = $did;				
			}


			if($ftype){
				$where  .= 'fut_code LIKE ? AND ';	
				$bind_data[] = '__' .$ftype;				
			}

			if($search_keyword){
				$where  .= 'title LIKE ? AND ';	
				$bind_data[] = '%' .$search_keyword. '%';			
			}

		}
		$where  .= 'p_view = ?';	
		$order  = ' fc_id asc';	
		$bind_data[] = 1;		
		//$table = 'tRECONCILE_SETTLMNT';

		$row = $this->makeDataList($table, $where, $order, $bind_data , '' ,  100);

		$list  = [];

		if(isset($row['list'])){
			$list = $row['list'];
			foreach ($list as $key => &$value) {
				# code...
				$imgs = Facility_library::getImgs($value['fc_id']);

				$value['imgs'] = $imgs;
			}
		}



		$result['list'] = $list;
		$result['keyword'] = $search_keyword;
		$result['did'] = $did;
		$result['ftype'] = $ftype;

		//debug_var($did);



        $this->param['section'] = $this->load->view('main/map', $result, true);
        $this->render_l->html($this->param);


	}

	function ajax_getdata(){
		$list = Facility_library::getFaciltiyList();

		$mlist = Facility_library::getMDustNowList();

		foreach ($mlist as $key => &$value) {
			# code...
			$value['type'] = 'dust';

			$info = Facility_library::getFaciltyInfo($value['sd_code']);

			if(!$info) continue;
			$value['title'] = $info['title'];
			$value['fut_code'] = $info['fut_code'];
			$value['description'] = $info['description'];
			$value['sl_id'] = $info['sl_id'];
			$value['fc_id'] = $info['fc_id'];
			$value['lat'] = $info['lat'];
			$value['lng'] = $info['lng'];
			$value['address'] = $info['address'];
		}


		foreach ($list as $key => &$value) {
			# code...
			$value['type'] = 'facility';

			$imgs = Facility_library::getImgs($value['fc_id']);
			if($imgs) {
				$value['filename']	= '/'.$imgs[0]['path'].'/'.$imgs[0]['filename'];
				$value['imgs'] = $imgs;
			} else {
				$value['imgs'] = [];
				$value['filename']	= '/assets/images/no-image.jpg';
			}


			

		}

		// debug_var($list);
		// exit;

		$this->success_data(['msg'=>'success' , 'list'=>$list , 'mlist'=>$mlist]);
	}

	function detail(){
		try{
			$ntid = _trim($this->input->get_post('ntid'));

			Notice_library::updateViewCount($ntid);

			$this->template->write('title', '성남도시개발공사', TRUE);

			$notice_detail = Notice_library::getNotice($ntid);
			$view_data  = [];
			$view_data['notice_detail'] = $notice_detail ;

			$mtitle = '공지사항';

			$view_data['ntid'] = $ntid;
			$view_data['title'] = $notice_detail['title'];
			$view_data['description'] = $notice_detail['description'];
			$view_data['status'] = $notice_detail['status'];
			$view_data['view_count'] = $notice_detail['view_count'];


			if($notice_detail['attch_file_dir']){
				$notice_detail['attch_file_dir'] = '/assets/files/'.$notice_detail['attch_file_dir'];
				$notice_detail['is_attch_file'] = '첨부 파일 다운로드';


			} else {
				$notice_detail['is_attch_file'] = '';			
			}


			$ptid = $this->get_parking_lot_type();		
			
					// public static function getMents($bid , $parent_ment_id , $ptid  ,  $is_admin = true  ,$status = 1 , $is_notice = false) {

	  		$aid = $this->get_aid();
	  		$view_data['aid'] = $aid;
	  		$user_info = Account_library::getUserRow($aid);
	  		$user_id = $user_info['id'];
	  		$view_data['user_id'] = $user_id;

	  		$view_data['notice_detail'] = $notice_detail;



			$ment_list = Ment_library::getMents($ntid, 0 , $ptid , false , 1, true);



	  	 	foreach ($ment_list as $key => &$value) {

				$row = Account_library::getUserRow($value['aid']);
				$value['writer'] = $row['id'];
				$value['profile_img']  = Account_library::setProfileImg($row['profile_img']);
			


		  	 	if($value['child_ment_count']){
		  	 		$c_list = Ment_library::getMents($ntid, $value['mid'] , $ptid , false , 1 , true);


		  	 		foreach ($c_list as $key1 => &$value1) {
				 		$row1= Account_library::getUserRow($value1['aid']);
				 		$value1['writer'] = $row1['id'];
				 		$value1['profile_img']  = Account_library::setProfileImg($row1['profile_img']);
					}
					
						$value['c_list'] = $c_list;
		  		}	
	  		}
	  		


	  		$view_data['ment_list'] = $ment_list;

			// view count 추가 
			

			$this->template->write_view('content', 'home/noticedetail', $view_data , true);
			$this->template->render();

		}
		catch(Exception $e){
			back_caution($e->getMessage());
		}
	}


	function write($ptid = 1){
		// debug_var('write');
		// exit;
	}

	function save($status = 'start'){
		
	}}
