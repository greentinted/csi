<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Board extends AdminBase
{
	function __construct(){
		parent::__construct();

	}

	function operator_list(){
		$this->template->write('title', '민원처리 리스트', TRUE);

		$view_data = [];
		$list = Board_library::getList();

		$account_list = Account_library::getAccountList();

		$account = $this->getAccountInfo();

		// debug_var($list);
		// exit;

		foreach ($list as $key => &$value) {
			# code...
			//$admins = Board_library::getBoardRefAdminList($value['bid']);

			$operators = Board_library::getBoardRefOperatorList($value['bid']);

			//$value['admins'] = $admins;
			$value['operators'] = $operators;
			$value['admin_name'] = '';

			foreach ($account_list as $key1 => $value1) {
				# code...
				if($value1['aid'] == $value['admin_aid']) {
					$value['admin_name'] = $value1['name'];
				}				
			}

			if($account['admin_type'] == 'operator'){
				$remove = true;
				foreach($operators as $key2 => $value2) {
					if($value2['operator_aid'] == $account['aid']){
						$remove =false;
					}
				}

				if($remove){
					unset($list[$key]);
				}
			}
		}



		$view_data['list'] = $list;
		$view_data['account'] = $account; 	


		$this->template->write_view('content', 'admin/board/operator_list', $view_data , true);
		$this->template->render();

	}
	

	function index($ptid=1){

		$account = $this->getAccountInfo();

		if($account['admin_type'] == 'user'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

		 // debug_var($account['admin_type']);
		 // exit;

		elseif($account['admin_type'] == 'operator'){
			just_go('operator_list');
			exit;
		}
			

		
		$this->template->write('title', '성남도시개발공사', TRUE);
		


		// 1000개 이상이 예상이 된다면 ...
		// 1 .  먼저 크기를 읽어온후 
		// 2 .  offset 값을 기준으로 읽어온후 리스트 뒤에 넣어주는 식으로 하면 될듯..

		//$view_data['list'] = $account_list;
		// $list = [];

		// for($i = 0; $i < 2000; $i++){
		// 	$rand_num = rand(0,2);

		// 	$list[$i] = $account_list[$rand_num];

		// }

		//$is_admin = true;

		$view_data = [];
		$list = Board_library::getList();
		$account_list = Account_library::getAccountList();


		// debug_var($list);
		//  exit;

		foreach ($list as $key => &$value) {
			# code...
			//$admins = Board_library::getBoardRefAdminList($value['bid']);

			$operators = Board_library::getBoardRefOperatorList($value['bid']);

			//$value['admins'] = $admins;
			$value['operators'] = $operators;
			$value['admin_name'] = '';

			foreach ($account_list as $key1 => $value1) {
				# code...
				if($value1['aid'] == $value['admin_aid']){
					$value['admin_name'] = $value1['name'];
				}		
			}
		}

		$view_data['list'] = $list; 	

		// debug_var($view_data);
		// exit;


		$this->template->write_view('content', 'admin/board/list', $view_data , true);
		$this->template->render();
	}

	function write(){
		// debug_var('detail');
		// exit;

		$this->template->write('title', '민원처리', TRUE);

		if($this->get_admin_type() == 'user' || $this->get_admin_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

		$bid = _trim($this->input->get_post('bid'));
		$operator = _trim($this->input->get_post('operator'));
		if(!$bid){
			back_caution('유효한 접근이 아닙니다.');
			exit;			
		}

		$is_operator = false;

		if($operator)
			$is_operator = true;			

		$row = Board_library::getBoard($bid);


		// debug_var($row);
		// exit;

		$account_list = Account_library::getAccountListOperator();


		$operators = Board_library::getBoardRefOperatorList($row['bid']);

		// debug_var($operators);
		// exit;

		$board_status = Board_library::getBoardStatus();

		// debug_var($board_status);
		// exit;


		$row['operators'] = $operators;

		
		$view_data = [];
		$view_data['row'] = $row;
		$view_data['account_list'] = $account_list;
		$view_data['board_status'] = $board_status;
		$view_data['my_aid'] = $this->get_aid();
		$view_data['is_operator'] = $is_operator;


		$this->template->write_view('content', 'admin/board/write', $view_data , true);
		$this->template->render();			


	}

	function operator_write(){
		try{
			$bid = _trim($this->input->get_post('bid'));
			//$row = Board_library::getBoard($bid);
			$row = Board_library::getBoard($bid);
			$operators = Board_library::getBoardRefOperatorList($bid);


			$form[] = ['type' => 'text_hidden', 'name' => 'bid', 'name_desc' => '게시글아이디' , 'placeholder' => 'id', 'value' => isset_return_str($bid)];


			$form[] = ['type' => 'text_hidden', 'name' => 'writer_aid', 'name_desc' => '작성자ID' , 'placeholder' => 'id', 'value' => isset_return_str($row['writer_aid'])];

			$output = make_html_input_text($form);
			$account_list = Account_library::getAccountListData();
			$division_list = array('' => '전체','NOPL' => '노외주차처','NSPL' => '노상주차처','JWLI' => '중원도서관','SJLI' => '수정도서관','SGMA' => '상가관리처','TCSC' => '탄천종합운동장','SNSC' => '성남종합운동장');

			//  debug_var($account_list);
			// debug_var($division_list);
			// exit;

			$taskOption = _trim($this->input->get_post('selelct_opt'));
			 if ($taskOption == 'NOPL') {
			 
			// 	# code...
			 debug_var($taskOption);
			 exit;
			 }
			// $dcode = _trim($this->input->get_post('dcode'));

			// $facilitylist = Facility_library::getFaciltyInfoBydcode($dcode);
			// $this->success_data(['facilitylist' => $facilitylist, 'stats' => true]);

			$division = make_html_input_select2('division', '부서', $division_list , '');
			$admin = make_html_input_select('aid', '담당자', $account_list , '');
			$output .= $division;
			$output .= $admin;

			echo $output;

		}
		catch(Exception $e){
			back_caution($e->getMessage());
		}
	}

	function operator_division(){
	$fct_code = _trim($this->input->get_post('fct_code'));
	$account_list = Account_library::getAccountListData($fct_code);
	$this->success_data(['account_list' => $account_list, 'stats' => true]);

}

	function operator_delete(){
		try{
			$brid = _trim($this->input->get_post('brid'));
			$bid = _trim($this->input->get_post('bid'));

			$rs =  SQL::DB('csi')->query('DELETE FROM tBOARD_REPLY WHERE brid = ?',[$brid]);
			if(!$rs){
				throw new Exception("민원처리 담당자 저장에 실패하였습니다", 1);
			}

			just_go('/admin/board/write?bid='.$bid);

		} 
		catch(Exception $e){
			back_caution($e->getMessage());
		}
	}

	function operator_save(){
		try{
			$bid = _trim($this->input->get_post('bid'));
			$admin_aid = _trim($this->input->get_post('writer_aid'));
			$operator_aid = _trim($this->input->get_post('aid'));
			$brid = _trim($this->input->get_post('brid'));

			// 수정방법 고려해야 함.
			$data = [];
			$data['bid'] = $bid;
			$data['admin_aid'] = $admin_aid;
			$data['operator_aid'] = $operator_aid;			

			if(!$brid){
				$rs = SQL::DB('csi')->query('INSERT INTO tBOARD_REPLY (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));
			} else {
				$rs = 1;
			}

			if(!$rs){
				throw new Exception("담당자 저장에 실패하였습니다", 1);
			}


			just_go('/admin/board/write?bid='.$bid);

		} 
		catch(Exception $e){
			back_caution($e->getMessage());
		}

	}

	function status(){
		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

			$ntid = _trim($this->input->get_post('id'));
			$status = _trim($this->input->get_post('status'));

			// debug_log($pid);
			// debug_log($status);

			if(!$status)
				$status = 0;

			if(!$ntid){
				throw new Exception(' ntid 없습니다.');
			}


			$data = [];
			$data['status'] = $status;


			$rs = Notice_library::updateNotice($ntid, $data);

			if(!$rs){
				throw new Exception("공지사항  수정에 실패하였습니다", 1);
			}

			$msg = "수정 성공";


			$this->success_data(['msg' => $msg , 'is_reload' => false , 'url'=>'']);

	}

	function save() {
		//debug_var('account_action');
		//exit;
		if($this->get_admin_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

		try{
			$bid = _trim($this->input->get_post('bid'));
			$admin_aid = _trim($this->input->get_post('admin_aid'));
			$board_status = _trim($this->input->get_post('board_select_status'));
			$description = $this->input->get_post('content');
			$share = $this->input->get_post('share_select_status');




			// $status = _trim($this->input->get_post('status'));
			// $description = $this->input->get_post('body');
			// $aid = $this->input->get_post('aid');
			// $is_new = $this->input->get_post('is_new');



			
			$data = [];
			if($description){
				$data['description'] = $description;
			}				

			$data['admin_aid'] = $admin_aid;

			$data['ps_code'] = $board_status;
			$data['share'] = $share;

			// update	
			if($bid){

				$cond = ['bid' => $bid];
				$rs = SQL::DB('csi')->query('UPDATE tBOARD SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));
				if(!$rs){
					throw new Exception("수정에 실패하였습니다", 1);
				}

				just_go('/admin/board/index');
				//exit;
			} 
		}
		catch(Exception $e){
			back_caution($e->getMessage());
		}

	}

}
