<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Account extends AdminBase
{
	function __construct(){
		parent::__construct();
	}

	function write_warning(){
		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

		try{
			$aid = _trim($this->input->get_post('aid'));
			if(!$aid){
				throw new Exception('잘못된 접근입니다');
			}
			$form[] = ['type' => 'text_hidden', 'name' => 'aid', 'name_desc' => 'aid' , 'placeholder' => 'aid', 'value' => isset_return_str($aid)];

			$output = make_html_input_text($form);
			echo $output;


		} catch(Exception $e){
			back_caution($e->getMessage());
		}		
	}
	
	function write() {
		try{

			$aid = _trim($this->input->get_post('aid'));

			$user_type = Account_library::getUserType();
			$division = Account_library::getDivisionList();
			// debug_var($user_type);
			// exit;

		if($aid){
			
			$mtitle = '계정수정';

			$view_data['mtitle'] = $mtitle;
			$account = Account_library::getAccountFullInfo($aid);

			$view_data['aid'] = $account['aid'];
			$view_data['email'] = $account['email'];
			$view_data['name'] = $account['name'];
			$view_data['gender'] = $account['gender'];
			$gender_list = array("남자" => "m","여자" => "f","기타" => "e");
			$view_data['gender_list'] = $gender_list;
			 // debug_var($view_data['gender_list']);
			 // exit;
			$view_data['contact_number'] = $account['contact_number'];
			$view_data['address'] = $account['address'];
			$view_data['birth'] = $account['birth'];
			$view_data['account_type'] = $account['admin_type'];
			$view_data['fut_code'] = $account['fut_code'];
			$view_data['belong'] = $account['title'];
			$view_data['facility'] = $account['facility'];
			// $view_data['profile_img'] = $account['profile_img'];
			if($view_data['account_type']=='super_admin'){
				$admin_type = Account_library::getAdminTypeList('0');
			}
			else{
				$admin_type = Account_library::getAdminTypeList("super_admin");	
			}	
			// debug_var($admin_type);
			// exit;
			// $admin_type = Account_library::getAdminTypeList("super_admin");	
			// $user_type = Account_library::getUserType();
			// $division = Account_library::getDivisionList();

				     // debug_var($division);
				     // exit;

			// $bb = Account_library::getDivisionFacility('sn');
			// $tc = Facility_library::getFaciltyInfoBydcode('SN');
		    $info_add = Facility_library::getFaciltyFromAid($aid);
				          // debug_var($info_add);
				          // exit;
			$view_data['admin_type'] = $admin_type;
			$view_data['user_type'] = $user_type;
				// $view_data['fut_list'] = $list;
			$view_data['division'] = $division;
					// $view_data['tc'] = $tc;
			$view_data['fc_id'] = $info_add['fc_id'];			
		        
		          // debug_var($view_data);
		          // exit;

			//   debug_var($view_data['profile_img']);
			//   exit;
		} else {

			$mtitle = '계정등록';

			$admin_type = Account_library::getAdminTypeList("super_admin");

			$view_data['aid'] = '';
			$view_data['email'] = '';
			$view_data['name'] = '';
			$view_data['gender'] = '';
			$gender_list = array("남자" => "m","여자" => "f","기타" => "e");
			$view_data['gender_list'] = $gender_list;
			 // debug_var($view_data['gender_list']);
			 // exit;
			$view_data['contact_number'] = '';
			$view_data['address'] = '';
			$view_data['birth'] = '';
			$view_data['account_type'] = $admin_type;
			$view_data['admin_type'] =  $admin_type;
			$view_data['fut_code'] = '' ;
			$view_data['belong'] = '';
			$view_data['facility'] = '';
			// debug_var($view_data['facility']);
			// exit;

			$view_data['user_type'] = $user_type;
			$view_data['division'] = $division;


		}
		     // debug_var($view_data);
		     // exit;

			$this->template->write('title', '성남도시개발공사', TRUE);
			$this->template->write_view('content', 'admin/account/write', $view_data , true);
			$this->template->render();			

		} catch(Exception $e){
			back_caution($e->getMessage());
		}
	}

function getfacilities(){
	$dcode = _trim($this->input->get_post('dcode'));

	$facilitylist = Facility_library::getFaciltyInfoBydcode($dcode);
	$this->success_data(['facilitylist' => $facilitylist, 'stats' => true]);

	 debug_var($facilitylist);
	 exit;

}


	function working_history(){

		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}


		// debug_var('working_history');
		try{
			$this->template->write('title', '성남도시개발공사', TRUE);
			$aid = _trim($this->input->get_post('aid'));

			$from = _trim($this->input->get_post('from'));
			//$to = _trim($this->input->get_post('to'));			

			if(!$from){
				$from = date("Y-m");
				//$viewing_from = $from;
				$from .= '-01';
			}
			$viewing_from = $from;

			$time = strtotime($from);
			$prev_month = strtotime("+1 months", $time);
			$to = date("Y-m-d" , $prev_month);

			$ptid = $this->get_parking_lot_type();
			

			$parking_type = parent::get_parking_lot_type($aid, $ptid);

			// if($to){
			// 	$to = '2019-06-01';	
			// }			

			$list = Work_library::getWorkingRecordById($aid, $from , $to);


			// $normal_work = Work_library::getNormalWorkTime($aid, $from , $to, Const_library::WORKTYPE_WORK);
			// $offday_work = Work_library::getOffDayWorkTime($aid, $from , $to, Const_library::WORKTYPE_OFFDAY_WORK);
			// $holiday_work = Work_library::getHolidayDayWorkTime($aid, $from , $to, Const_library::WORKTYPE_HOLIDAY_WORK);


			foreach ($list as $key => &$value) {

				# code...

				// debug_var($value);
				// exit;
				$workplace = Work_library::getWorkPlaceRow($value['wpid']);
				$worktype = Work_library::getWorkTypeRow($value['wtid']);
				$worktemplate = Work_library::getWorkTemplateRow($value['wtlt_id']);
				// exit;
				if($worktemplate){
					$workstyle = Work_library::getWorkStyleRow($worktemplate['wsid']);
					$workwhen = Work_library::getWorkWhenRow($worktemplate['wwid']);					
				} else{					
					$workstyle = '';
					$workwhen = '';
				}

				$commute_row = Work_library::getCommuteRecordRowByDateAndAid($value['rdate'] , $value['aid']);

				if($commute_row){
					$value['work_start_time'] = $commute_row['work_start_time'];
					$value['work_end_time'] = $commute_row['work_end_time'];
				} else {
					$value['work_start_time'] = '';
					$value['work_end_time'] = '';

				}

				$normal_work = Work_library::getNormalWorkTimeByDate($aid, $value['rdate'] , Const_library::WORKTYPE_WORK);


				$offday_work = Work_library::getOffDayWorkTimeByDate($aid, $value['rdate'], Const_library::WORKTYPE_OFFDAY_WORK);
				$holiday_work = Work_library::getHolidayDayWorkTimeByDate($aid, $value['rdate'], Const_library::WORKTYPE_HOLIDAY_WORK);


				$urgent_overtime = Work_Library::getUrgentOverTimeByDate($aid, $value['rdate']);

				$urgent_nighttime = Work_Library::getUrgentOverTimeByDate($aid, $value['rdate']);

				$urgent_holidaytime = Work_Library::getUrgentHolidayTimeByDate($aid, $value['rdate']);


				// $value['normal_work_flag']	 = 1;
				// debug_var($value);
				// debug_var($normal_work);
				// debug_var($offday_work);
				// debug_var($holiday_work);

				// debug_var($urgent_overtime);
				// debug_var($urgent_nighttime);
				// debug_var($urgent_holidaytime);

				//exit;
				// debug_var($overtime_);
				// exit;


				// $rank = Work_library::getRankRow($value['rid']);

				$value['wp_info'] = $workplace;
				$value['wt_info'] = $worktype;
				$value['wtp_info'] = $worktemplate;
				$value['ws_info'] = $workstyle;
				$value['ww_info'] = $workwhen;
				//$value['rank_info'] = $rank;
			}

			$account = Account_library::getUserRow($aid);

			
			$view_data = [];
			
			$view_data['list'] = $list;
			$view_data['aid'] = $aid;

			// debug_var($list);
			// exit;
			$view_data['account'] = $account;
			$view_data['viewing_from'] = $viewing_from;
			// debug_var($view_data);
			// exit;
			

			$this->template->write_view('content', 'admin/account/history', $view_data , true);
			
			$this->template->render();


		} catch(Exception $e){
			//back_caution($e->getMessage());
		}

	}


	function write_working_history(){
		try{

			$aid = _trim($this->input->get_post('aid'));

		} catch(Exception $e){

		}	
	}

	function get_info(){
		try{
			$aid = _trim($this->input->get_post('aid'));

			$account = Account_library::getAccountFullInfo($aid);	


			$this->success_data(['msg'=>'success' , 'account'=>$account]);


		} catch(Exception $e){

		}
	}



	function get_history_info(){

		// debug_var('working_history');
		try{
			$this->template->write('title', '성남도시개발공사', TRUE);
			$wrid = _trim($this->input->get_post('wrid'));
			$aid = _trim($this->input->get_post('aid'));

			$ptid = $this->get_parking_lot_type();


			$working_record = Work_library::getWorkingRecord($wrid);
			$commute_row = Work_library::getCommuteRecordRowByDateAndAid($working_record['rdate'] , $working_record['aid']);


			if($commute_row){
				$crid = $commute_row['crid'];
			} else {
				$crid = 0;
			}
			

			$aid = $working_record['aid'];
			$account = Account_library::getUserRow($aid);

			$workplace = Work_library::getWorkPlaceRow($working_record['wpid']);
			$worktype = Work_library::getWorkTypeRow($working_record['wtid']);
			$worktemplate = Work_library::getWorkTemplateRow($working_record['wtlt_id']);
				// exit;
			if($worktemplate){
				$workstyle = Work_library::getWorkStyleRow($worktemplate['wsid']);
				$workwhen = Work_library::getWorkWhenRow($worktemplate['wwid']);					
			} else{					
				$workstyle = '';
				$workwhen = '';
			}

			// $rank = Work_library::getRankRow($value['rid']);
			$working_record['wp_info'] = $workplace;
			$working_record['wt_info'] = $worktype;
			$working_record['wtp_info'] = $worktemplate;
			$working_record['ws_info'] = $workstyle;
			$working_record['ww_info'] = $workwhen;
			$working_record['commute_record'] = $commute_row;
			$working_record['crid'] = $crid;
			$working_record['ptid'] = $ptid;
			$working_record['aid'] = $aid;
			$working_record['rdate'] = $working_record['rdate'];
			//$value['rank_info'] = $rank;
			$working_record['account'] = $account;

			$worktemplate_list = Work_library::getWorkTemplateList(1, $ptid);


			//debug_log($working_record);
				$this->success_data(['msg'=>'success' , 'working_record'=>$working_record , 'worktemplate_list'=>$worktemplate_list]);
			// $this->template->write_view('content', 'admin/account/history', $view_data , true);
			// $this->template->render();
		} catch(Exception $e){
			//back_caution($e->getMessage());
		}
	}

	function save_working_history() {
		$crid =	_trim($this->input->get_post('crid'));
		$work_start_time = _trim($this->input->get_post('work_start_time'));
		$work_end_time = _trim($this->input->get_post('work_end_time'));
		$aid = _trim($this->input->get_post('aid'));
		$rdate = _trim($this->input->get_post('rdate'));
		$ptid = $this->get_parking_lot_type();

		// $work_start_time = getRemoveApmTime($work_start_time);
		// $work_end_time = getRemoveApmTime($work_end_time);



		if(!$crid){
			$origin = '';
			$data = [];
			if($work_start_time){
				$data['work_start_time'] = $work_start_time;	
			}
			if($work_end_time){
				$data['work_end_time'] = $work_end_time;	
			}
			$data['aid'] = $aid;
			$data['rdate'] = $rdate;
			$data['ptid'] = $ptid;
			$rs = Work_library::insertWorkTime($data);
			if(!$rs){
				debug_log('error ');
			}

			// 근무 기록도 만들어 넣어줌.

			$account_row = Account_library::getAccountFullInfo($aid);


			$data1 = [];
			$data1['start']  =  $rdate .' 00:00:00';
			$str_date  = strtotime($rdate.'+1 days');
			$next_date  = date("Y-m-d H:i:s" , $str_date);

			$data1['end']  =  $next_date;
			$data1['rdate']    =  $rdate;
			$data1['aid']      =  $account_row['aid'];
			$data1['ptid']     =  $ptid;
			$data1['wpid']     =  $account_row['wpid'];
			$data1['wtid']     =  1;


			$data1['wtlt_id']  =  $account_row['wtlt_id'];

			$rs = Work_library::insertWorkRecord($data1);	
			if(!$rs){
				throw new Exception("근무  기록이 잘못되었습니다", 1);
			}



			$temp_crid = SQL::DB('db')->lastID();
			$dest = Work_library::getCommuteRecord($temp_crid);

		} else {
			$origin = Work_library::getCommuteRecord($crid);

			// debug_log('$origin');
			// debug_log($origin);
			// exit;

			$data = [];
			if($work_start_time){
				$data['work_start_time'] = $work_start_time;	
			}
			if($work_end_time){
				$data['work_end_time'] = $work_end_time;	
			}
			
			

			$rs = Work_library::updateWorkTime($crid, $data);
			if(!$rs){
				debug_log('error updateWorkTime');
			}
			$dest = Work_library::getCommuteRecord($crid);

		}

		//debug_log($dest);

		$worker_aid = $dest['aid'];
		$admin = $this->getAccountInfo();
		$admin_aid = $admin['aid'];
		$type = 'modify';
		$origin_wrid = $wrid;
		$origin = json_encode( $origin )."<br>";
		$dest = json_encode($dest)."<br>";
		$ptid = $this->get_parking_lot_type();

		$h_data['admin_aid'] = $admin_aid;
		$h_data['worker_aid'] = $worker_aid;
		$h_data['type'] = $type;
		$h_data['origin_wrid'] = $origin_wrid;
		$h_data['origin'] = $origin;
		$h_data['dest'] = $dest;
		$h_data['ptid'] = $ptid;

		// debug_log($h_data);

		Admin_library::insertWorkTimeHistory($h_data);
		if(!$rs){
			throw new Exception("히스토리 정보 수정에 실패하였습니다", 1);
		}
		            
	}

	function init_passwd(){
		try{
			$aid = _trim($this->input->get_post('aid'));

			// debug_var($aid);
			// exit;
			if($aid){
				$data['password'] = password_hash('1111', PASSWORD_DEFAULT);

				// $temp = password_hash('1111', PASSWORD_DEFAULT);
				// debug_var($temp);
				// exit;
				$cond = ['aid' => $aid];

				$rs = SQL::DB('csi')->query('UPDATE tACCOUNT SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

				if(!$rs){
					throw new Exception("패스워드 수정에 실패하였습니다", 1);
				}

				back_caution('패스워드를 초기화 하였습니다.');

			} else {
				back_caution('잘못된 접근입니다.');
			}

		} catch(Exception $e){
			back_caution($e->getMessage());
		}
	}

	function delete_account(){
		try{
			$aid = _trim($this->input->get_post('aid'));

			// debug_var($aid);
			// exit;
			if($aid){
				$data['status'] = 0;

				// $temp = password_hash('1111', PASSWORD_DEFAULT);
				// debug_var($temp);
				// exit;
				$cond = ['aid' => $aid];

				$rs = SQL::DB('csi')->query('UPDATE tACCOUNT SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));
				if(!$rs){
					throw new Exception("계정 수정에 실패하였습니다", 1);
				}
				back_caution('계정을 삭제 하였습니다.');

			} else {
				back_caution('잘못된 접근입니다.');
			}

		} catch(Exception $e){
			back_caution($e->getMessage());
		}
	}


	function list($ptid=1){

		// debug_var('오삼');
		// exit;


		$this->template->write('title', '성남도시개발공사', TRUE);


		$account = $this->getAccountInfo();


		if($account['admin_type'] == 'user'){
			back_caution('권한이 없습니다.');
			exit;
		}
		elseif($account['admin_type'] == 'operator'){
			back_caution('권한이 없습니다.');
			exit;
		}




		// 1000개 이상이 예상이 된다면 ...
		// 1 .  먼저 크기를 읽어온후 
		// 2 .  offset 값을 기준으로 읽어온후 리스트 뒤에 넣어주는 식으로 하면 될듯..

		$start = get_time();

		$account_list = Account_library::getAccountList_();


		$end = get_time();
		$time = $end - $start;
		// debug_var($account_list);
		// //debug_var($time);
		// exit;


		// debug_var($account_list);
		// exit;


		// $view_data['list'] = $list;
		$view_data['list'] = $account_list;

		// debug_var($account_list);
		// exit;

		$this->template->write_view('content', 'admin/account/list', $view_data , true);
		$this->template->render();

	}

	function save() {
		//debug_var('account_action');
		//exit;

		$account = $this->getAccountInfo();

		if($account['admin_type'] == 'user'){
			back_caution('권한이 없습니다.');
			exit;
		}


		try{


				$aid = _trim($this->input->get_post('aid'));
				$email = _trim($this->input->get_post('email'));
				$name = _trim($this->input->get_post('name'));
				$address = _trim($this->input->get_post('address'));
				$gender = _trim($this->input->get_post('gender'));
				$contact_number = _trim($this->input->get_post('contact_number'));
				$birthday = _trim($this->input->get_post('birthday'));
				$admin_type = _trim($this->input->get_post('admin_type'));
				$user_type = _trim($this->input->get_post('user_type'));
				$facility = _trim($this->input->get_post('facility'));
				// $division = _trim($this->input->get_post('division'));
					
				$division = Facility_library::getFaciltyFromFcid($facility);
				

				$account_data = [];	
				// $account_data['aid'] = $aid;
				$account_data['email'] = $email;
				$account_data['id'] = $name;
				$account_data['name'] = $name;
				$account_data['address'] = $address;
				$account_data['gender'] = $gender;
				$account_data['contact_number'] = $contact_number;
				$account_data['birth'] = $birthday;
				$account_data['admin_type'] = $admin_type;
				$account_data['status'] = 1;
				$account_data['password'] = password_hash('1111', PASSWORD_DEFAULT);
				$account_data['create_date'] = date("Y-m-d H:i:s");
				$account_add['user_type'] = $user_type;
				$account_add['fct_code'] = $division['fut_code'];
				$account_add['fc_id'] = $facility;

				 // debug_var($account_data);
				 //  debug_var($account_add);
				 //  exit;

				if($aid){

					$rs = Account_library::updateAccount($aid, $account_data);
					$rs1 = Account_library::updateAccountAdd($aid, $account_add);
						// debug_var($rs1);
						// exit;
					if(!$rs){

						back_caution('저장에 실패하였습니다.');

					} else{
						goto_caution('저장하였습니다' , '/admin/account/list');

					}
				}
				// 계정 생성
				else{

					$rs = Account_library::insertAccount($account_data);
					if(!$rs){
						back_caution('계정생성에 실패하였습니다.');
					} else{
						$aid = SQL::DB('csi')->lastID();
						$account_add['aid'] = $aid;
						$rs1 = Account_library::insertAccountAdd($account_add);
						if(!$rs){
							back_caution('계정생성에 실패하였습니다.');
						}else{
							goto_caution('계정을 생성하였습니다.' , '/admin/account/list');
						}				
					}


				}


			// // debug_var($file_name);
			// // exit;
			// if(!$select_rank){
			// 	$select_rank = 0;
			// }

			// $account_data = [];
			// $account_data['id'] = $name;
			// $account_data['real_name'] = $real_name;

			// // password check 
			// //$account_data['password'] = $passwd;
			// if($join_date){
			// 	$account_data['join_date'] = $join_date;	
			// }
			
			// //debug_var($join_date);
			// //exit;

			// //$account_data['vacation_days'] = $vacation_days;
			// if($select_status){
			// 	$account_data['status'] = $select_status;	
			// }
			
			// if($email){
			// 	$account_data['email'] = $email;	
			// }
			// if($cellphone_number){
			// 	$account_data['cellphone'] = $cellphone_number;	
			// }	
			// if($contact_number){
			// 	$account_data['contact_number'] = $contact_number;	
			// }
			// if($birthday){
			// 	$account_data['birthday'] = $birthday;	
			// }
			
			// //$account_data['ptid'] = $ptid;
			// if($select_rank){
			// 	$account_data['rid'] = $select_rank;	
			// }
			
			// if($select_workplace){
			// 	$account_data['wpid'] = $select_workplace;	
			// }
			
			// if($select_workassign){
			// 	$account_data['waid'] = $select_workassign;	
			// }
			
			// if($select_worktemplate){
			// 	$account_data['wtlt_id'] = $select_worktemplate;	
			// }

			// if($select_parktype){
			// 	$account_data['ptid'] = $select_parktype;	
			// }

			// // $account_data['wsid'] = $select_workstyle;
			// // $account_data['wwid'] = $select_workwhen;
			// if($file_name){
			// 	$account_data['profile_img'] = $file_name;	
			// }
			// if($user_type){
			// 	$account_data['user_type'] = $user_type;	
			// }

			// $account_data['status'] = $select_status;	
			
			// if($aid){

			// 	$cond = ['aid' => $aid];


			// 	$rs = SQL::DB('db')->query('UPDATE tACCOUNT SET ' . db_array_to_set($account_data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($account_data, $cond)));


			// 	if(!$rs){
			// 		throw new Exception("계정 수정에 실패하였습니다", 1);
			// 	}


			// 	if($leave_days){
			// 		$data = [];
			// 		$data['leave_days'] = $leave_days;
			// 		$data['use_leave_days'] = $use_leave_days;

			// 		$rs = SQL::DB('db')->query('UPDATE tACCOUNT_LEAVE SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

			// 		if(!$rs){
			// 			throw new Exception("휴가 기록은 실패하였습니다.", 1);
			// 		}


			// 	}



			// } else {
			// 	$account_data['password'] = password_hash('1111', PASSWORD_DEFAULT);

			// 	$rs = SQL::DB('db')->query('INSERT INTO tACCOUNT (' . implode(',', array_keys($account_data)) . ') VALUES(' . db_array_to_values($account_data) . ')', array_values($account_data));

			// 	$temp_aid = SQL::DB('db')->lastID();

			// 	$etc_data = [];
			// 	$etc_data['aid'] = $temp_aid;
			// 	$etc_data['draw_flag'] = 0 ;
			// 	$etc_data['ptid'] = $account_data["ptid"];


			// 	$rs_etc = SQL::DB('db')->query('INSERT INTO tACCOUNT_ETC (' . implode(',', array_keys($etc_data)) . ') VALUES(' . db_array_to_values($etc_data) . ')', array_values($etc_data));

			// 	if(!$rs || !$rs_etc){
			// 		throw new Exception("계정등록에 실패하였습니다", 1);
			// 	}

			// 	if($leave_days){
			// 		$data = [];
			// 		//$board_no = SQL::DB('db')->lastID();
			// 		$data['aid'] = $temp_aid;
			// 		$data['leave_days'] = $leave_days;

			// 		$rs = SQL::DB('db')->query('INSERT INTO tACCOUNT_LEAVE (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));
			// 	}

			// }
			// //$this->setAccountInfo();
			// goto_caution('저장이 완료되었습니다' , '/admin/account/list');

			



		}catch(Exception $e){
			back_caution($e->getMessage());
		}

	}

	function check_duplicated_id() {
		$id = $this->input->post('id');
		$query = $this->db->query('SELECT id FROM tACCOUNT WHERE id = ?', [$id]);
		$count = $query->num_rows();
		
		if($count == 0){
			echo 1;	
		}else{
			echo 0;
		}	
	}


}
