<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Surveyconf extends AdminBase
{
	function __construct(){
		parent::__construct();

	}

	function index(){
		$this->template->write('title', '서비스 평가단 스케쥴', TRUE);

		$account = $this->getAccountInfo();

		$list = Survey_library::getList();


		$view_data['list'] = $list;
		$view_data['account'] = $account; 	

		$this->template->write_view('content', 'admin/survey/list', $view_data , true);
		$this->template->render();

	}

	function detail(){


		$this->template->write('title', '서비스 평가단 스케쥴', TRUE);
		$ssid = _trim($this->input->get_post('ssid'));

		// debug_var($ssid);
		// exit;

		if($ssid){ //내용이 있으면
			$mtitle = '평가일정 수정';
			$list = Survey_library::getList1($ssid);

			 // debug_var($ssid);
			 //  exit;
			$view_data['order'] = $list[0]['order'];
			$view_data['list'] = $list;
			$start_date = date("m/d/Y", strtotime($list[0]['from']));
			$end_date = date("m/d/Y", strtotime($list[0]['to']));
			$description = $list[0]['description'];	
			  // debug_var($description);
			 // debug_var($end_date);
			  // exit;

		}else{
			$mtitle = '평가일정 생성';
			$list = Survey_library::GetLastOrderNum();
			$lastorder = $list[0]['order'];
			$ssid = $lastorder+1;
			$start_date = date("m/d/Y", time());
			$end_date =  date("m/d/Y" , strtotime($start_date."+3 days"));
			$description = '';
			$view_data['order'] = $ssid;
			 // debug_var($ssid);
			 //  exit;
		}

		$account = $this->getAccountInfo();
		 // debug_var($account);
		 // exit;

		$view_data['mtitle'] = $mtitle;
		$view_data['account'] = $account; 
		$view_data['start_date'] = $start_date;
		$view_data['end_date'] = $end_date;
		$view_data['description'] = $description;	

		// debug_var($list[]);
		// exit;
	
		$this->template->write_view('content', 'admin/survey/detail', $view_data , true);
		$this->template->render();

	}

	function save(){
		$this->template->write('title', '서비스 평가단 스케쥴', TRUE);

		$order = _trim($this->input->get_post('order'));
		$start_date = _trim($this->input->get_post('start_date'));
		$end_date = _trim($this->input->get_post('end_date'));
		$description = _trim($this->input->get_post('message'));
		$status = _trim($this->input->get_post('status'));
		$from = date("Y-m-d", strtotime($start_date));
		$to = date("Y-m-d", strtotime($end_date));
		
			if($status == 'on'){
				$status = 1;
			}else{
				$status = 0;
			}

		$data['from'] = date("Y-m-d", strtotime($start_date));
		$data['to'] = date("Y-m-d", strtotime($end_date));
		$data['status'] = $status;
		$data['description'] = $description;

		//DB에 회차 존재 유무 확인
		$list =  Survey_library::ExistsOrderNum($order);
		$checkorder = $list['success'];

		if($checkorder == 1){ // 1이면 모디파이드
		$getorder['order'] = $order;	

		$rs = $this->db->update('tSURVEYCONF', $data ,$getorder);

		debug_var($data);
		debug_var($getorder);
		debug_var($rs);
		exit;

			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/admin/surveyconf');
			}
		}
		
		else{ //  0이면 신규등록

		// debug_var('신규등록');
		$data['order'] = $order;
		$sql = "INSERT INTO tSURVEYCONF";
		$sql .= " (`";
		$sql .= implode("`,`", array_keys($data));
		$sql .= "`) VALUES ('";
		$sql .= implode("','", array_values($data));
		$sql.="')";

		$rs = Survey_library::IntoServeyConf($sql);
	
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/admin/surveyconf');
			}
		}
	}


	function timecheck(){

		$gettimelist = Survey_library::GetLastEndTime();
		
		// $rs = in_array($start_date, $gettimelist);
		// debug_var($gettimelist);
		
		$start_target = date("Ymd", strtotime($gettimelist[0]['start_date']));
		$end_target = date("Ymd", strtotime($gettimelist[0]['end_date']));
		$timenow = date("Ymd");
		// $timetarget = "2020-05-21 00:00:00";

		if($start_target <= $timenow && $timenow <= $end_target) {
			echo "설문가능함";
		}else {
			echo "설문불가함";
		}

	} //end timecheck()



}
