<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Covid19 extends AdminBase
{
	function __construct(){
		parent::__construct();

	}

	function index(){
		$this->template->write('title', '코로나현황', TRUE);

		$account = $this->getAccountInfo();

		$list = Survey_library::GetCovid19List();


		$view_data['list'] = $list;
		$view_data['account'] = $account; 	

		// debug_var($view_data);
		// exit;

		$this->template->write_view('content', 'admin/covid19/list', $view_data , true);
		$this->template->render();

	}

	function detail(){


		$this->template->write('title', '서비스 평가단 스케쥴', TRUE);
		$ssid = _trim($this->input->get_post('ssid'));
		$division = Account_library::getDivisionList();
		// debug_var($ssid);
		// exit;

		if($ssid){ //내용이 있으면
			$mtitle = '코로나현황 수정';
			$list = Survey_library::GetCovid19List1($ssid);

			  // debug_var($list);			  
			// $view_data['order'] = $list[0]['order'];
			$view_data['list'] = $list;
			$start_date = $list[0]['date_from'];
			$end_date = $list[0]['date_to'];
			$cid = $list[0]['cid'];
			$dcode = $list[0]['dcode'];
			$name = $list[0]['name'];
			$contact = $list[0]['contact'];
			$action = $list[0]['action'];
			$etc = $list[0]['etc'];
			$description = $list[0]['description'];
			// debug_var($view_data);
			 //  exit;

			  // debug_var($description);
			 // debug_var($end_date);
			  // exit;

		}else{
			$mtitle = '코로나현황 생성';

  			
  			 // debug_var($division);
  			 // exit;
			$start_date = date("Y-m-d", time());
			$end_date =  date("Y-m-d" , strtotime($start_date."+3 days"));
			$description = '';
			$cid = '';
			$dcode = '';
			$name = '';
			$contact = '';
			$action = '';
			$etc = '';


			 // debug_var($ssid);
			 //  exit;
		}

		$account = $this->getAccountInfo();
		 // debug_var($account);
		 // exit;
		$view_data['division_list'] = $division;
		$view_data['mtitle'] = $mtitle;
		$view_data['dcode'] = $dcode;
		$view_data['cid'] = $cid;
		$view_data['account'] = $account;
		$view_data['name'] = $name;
		$view_data['contact'] = $contact;
		$view_data['start_date'] = $start_date;
		$view_data['end_date'] = $end_date;
		$view_data['description'] = $description;
		$view_data['action'] = $action;	
		$view_data['etc'] = $etc;	
		// debug_var($list[]);
		// exit;
	
		$this->template->write_view('content', 'admin/covid19/detail', $view_data , true);
		$this->template->render();

	}

	function save(){
		$this->template->write('title', '서비스 평가단 스케쥴', TRUE);

		$cid = _trim($this->input->get_post('cid'));
		$dcode = _trim($this->input->get_post('dcode'));
		$name = _trim($this->input->get_post('name'));
		$start_date = _trim($this->input->get_post('start_date'));
		$end_date = _trim($this->input->get_post('end_date'));
		$contact = _trim($this->input->get_post('contact'));
		$description = _trim($this->input->get_post('description'));
		$action = _trim($this->input->get_post('action'));
		$etc = _trim($this->input->get_post('etc'));
		$status = _trim($this->input->get_post('status'));

			if($status == 'on'){
				$status = 1;
			}else{
				$status = 0;
			}
		// $data['cid'] = $cid;
		$data['dcode'] = $dcode;
		$data['name'] = $name;
		$data['date_from'] =$start_date;
		$data['date_to'] = $end_date;
		$data['contact'] = $contact;
		$data['description'] = $description;	
		$data['action'] = $action;	
		$data['etc'] = $etc;		
		$data['status'] = $status;


		//DB에 회차 존재 유무 확인
		// $list =  Survey_library::ExistsOrderNum($order);
		// $checkorder = $list['success'];

		if($cid == true){ // 존재하면

		$ssid['cid'] = $cid;
		$rs = $this->db->update('tCOVID19', $data, $ssid);
		// debug_var($data);	
		// debug_var($ssid);
		// debug_var($rs);
		// exit;

			if(!$rs){
				back_caution('수정에 실패하였습니다.');
			} else{
				goto_caution('수정하였습니다' , '/admin/covid19');
			
			}
		}
		
		else{ //  0이면 신규등록

		//수정시각을 최초에 한번 넣고 비교해서 가장 빠른 시간을 list의 현행화 시간에 뿌려줌
		$data['mod_date'] = date("Y-m-d H:i:s", time());

		$rs = $this->db->insert('tCOVID19', $data);
	
			if(!$rs){
				back_caution('저장에 실패하였습니다.');
			} else{
				goto_caution('저장하였습니다' , '/admin/covid19');
			}
		}
	}






}
