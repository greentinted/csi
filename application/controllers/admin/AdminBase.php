<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* This is Example Controller
*/

require_once APPPATH . 'controllers/Base.php';

class AdminBase extends Base {

	public function __construct() {

		parent::__construct();


		//$this->load->library(['session' , 'Account_library']);
		//$this->load->helper('url');

		// 로그인 하기 젼에 일단 일반 유저로 ..

        if(!$this->session->userdata('aid')){
			goto_caution('로그인이 필요한 페이지입니다' , '/home/login');
			exit;
		}
		

		$account = $this->getAccountInfo();

		if($account['admin_type'] == 'user'){
			back_caution('권한이 없습니다.');
			exit;
		}


		$view_data['account'] = $account;


		$this->template->write_view('sidenavs', 'template/default_sidenavs', $view_data , true);
		$this->template->write_view('navs', 'template/default_topnavs.php', $view_data , true);

		///$this->set_admin('operator');

	}

}