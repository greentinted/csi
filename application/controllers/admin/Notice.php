<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Notice extends AdminBase
{
	function __construct(){
		parent::__construct();

	}
	

	function index($ptid=1){

		
		$this->template->write('title', '성남도시개발공사', TRUE);
		

		// 1000개 이상이 예상이 된다면 ...
		// 1 .  먼저 크기를 읽어온후 
		// 2 .  offset 값을 기준으로 읽어온후 리스트 뒤에 넣어주는 식으로 하면 될듯..

		//$view_data['list'] = $account_list;
		// $list = [];

		// for($i = 0; $i < 2000; $i++){
		// 	$rand_num = rand(0,2);

		// 	$list[$i] = $account_list[$rand_num];

		// }

		$is_admin = true;

		$view_data = [];
		$list = [];
		$view_data['list'] = $list;

		$this->template->write_view('content', 'admin/notice/list', $view_data , true);
		$this->template->render();

	}

	function status(){
		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

			$ntid = _trim($this->input->get_post('id'));
			$status = _trim($this->input->get_post('status'));

			// debug_log($pid);
			// debug_log($status);


			if(!$status)
				$status = 0;


			if(!$ntid){
				throw new Exception(' ntid 없습니다.');
			}


			$data = [];
			$data['status'] = $status;


			$rs = Notice_library::updateNotice($ntid, $data);

			if(!$rs){
				throw new Exception("공지사항  수정에 실패하였습니다", 1);
			}

			$msg = "수정 성공";


			$this->success_data(['msg' => $msg , 'is_reload' => false , 'url'=>'']);

	}

	function save($is_update = 0 , $ptid= 1) {
		//debug_var('account_action');
		//exit;
		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}

		try{
			$ntid = _trim($this->input->get_post('ntid'));
			$title = _trim($this->input->get_post('title'));
			$status = _trim($this->input->get_post('status'));
			$description = $this->input->get_post('body');
			$aid = $this->input->get_post('aid');
			$is_new = $this->input->get_post('is_new');

			
			$data = [];
			if($title){
				$data['title'] = $title;
			}				

			$data['status'] = $status;

			$data['aid'] = $aid;
			$data['is_new'] = $is_new;

			if(isset($_FILES['upload_file'])){

				// debug_var($_FILES['file']);
				// exit;
				$file_name = File_library::uploadAttachedFile('upload_file'  , 'upload' , false);

				$data['attch_file_dir'] = $file_name;			


			}


			$data['description'] = $description;

			//debug_log($description);


			// update	
			if($ntid){

				$cond = ['ntid' => $ntid];
				$rs = SQL::DB('db')->query('UPDATE tNOTICE SET ' . db_array_to_set($data) . ' WHERE ' . db_array_to_set($cond, '=', 'AND'), array_values(array_merge($data, $cond)));

				if(!$rs){
					throw new Exception("공지사항 수정에 실패하였습니다", 1);
				}

				just_go('/admin/notice/index');

				//exit;

			} else { // insert

				$data['ptid'] = 1;
				$rs = SQL::DB('db')->query('INSERT INTO tNOTICE (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

				if(!$rs){
					throw new Exception("공지사항 등록에 실패하였습니다", 1);
				}

				just_go('/admin/notice/index');
			}
		}
		catch(Exception $e){
			back_caution($e->getMessage());
		}

	}

	function write() {
		if($this->get_user_type() == 'operator'){
			back_caution('페이지를 볼수 있는 권한이 없습니다');
			exit;
		}
		
		try{
			$this->template->write('title', '성남도시개발공사', TRUE);
			$ntid = _trim($this->input->get_post('ntid'));
			$aid = $this->get_aid();

			// debug_var($aid);
			// exit;

			//debug_log($did);
			//exit;
			$view_data = [];
			$mtitle = '공지사항 추가';

			if($ntid) {

				$mtitle = '공지사항 수정';

				$notice_row = Notice_library::getNotice($ntid);



				if($notice_row['attch_file_dir']){
					$notice_row['attch_file_dir'] = '/assets/files/'.$notice_row['attch_file_dir'];
					$notice_row['is_attch_file'] = '첨부 파일 다운로드';

					$view_data['is_attch_file'] = true;			

				} else {
					$notice_row['is_attch_file'] = '';			
					$view_data['is_attch_file'] = false;			
				}

				$view_data['ntid'] = $ntid;
				$view_data['title'] = $notice_row['title'];
				$view_data['description'] = $notice_row['description'];
				$view_data['status'] = $notice_row['status'];
				$view_data['aid'] = $aid;
				$view_data['is_new'] = $notice_row['is_new'];
				$view_data['attch_file_dir'] = $notice_row['attch_file_dir'];

				$view_data['is_attch_file'] = '';			


				$view_data['notice_row'] = $notice_row;



			} else {

				$view_data['ntid'] = 0;
				$view_data['title'] = '';
				$view_data['description'] = '';
				$view_data['status'] = 1;
				$view_data['aid'] = $aid;
				$view_data['is_new'] = 1;

				$view_data['attch_file_dir'] = '';
				$notice_row['is_attch_file'] = false;			

			}
			$view_data['mtitle'] = $mtitle;

			$this->template->write_view('content', 'admin/notice/write', $view_data , true);
			$this->template->render();			



		}
		catch(Exception $e){
			back_caution($e->getMessage());
		}


	}


}
