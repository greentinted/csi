<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/admin/AdminBase.php';

class Handy extends Base
{
	function __construct(){
		parent::__construct();
	}
	


	public function upload_file(){

		try{
			if(isset($_FILES['upload'])){	 

				debug_log($_FILES['upload']);
				if($_FILES['upload']['name']){
					$upload = File_library::uploadImgFile('upload', '', false);

					debug_log($upload);

					$data = [];
					$data['path'] = $upload['path'];
					$data['filename'] = $upload['filename'];

					$rs = File_library::insertFileDB($data);
					if(!$rs){
						throw new Exception('file insert error');
					}

					$url1 = $data['path'].'/' .$data['filename'];

					$move_url = File_library::getUrl($url1);


					debug_log($move_url);



					$this->success_data(['msg' => '', 'is_reload' => false, 'url' => $move_url]);

				}
			}

		} catch(Exception $e){
			$this->error($e->getMessage());
		}




	}






}
