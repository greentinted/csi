<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Workingplace extends Base
{
	function __construct(){
		parent::__construct();
	}

	function set(){

		$json_data = file_get_contents('php://input');
		$r_data = json_decode(file_get_contents('php://input'), true);
		$data = [];

		$wpid = $r_data['wpid'];
		$data['mac_address'] = $r_data['mac_address'];
		$data['windows'] = $r_data['windows'];
		$data['memsize'] = $r_data['memSize'];
		$data['cpu_speed'] = $r_data['cpu_speed'];
		$data['bit'] = $r_data['bit'];
		$data['computer_spec'] = $json_data;

		$rs = Work_library::updateWorkPlace($wpid , $data);

		if($rs){
			$this->success_data(['msg' => 'success', 'is_reload' => true, 'url' => '']);

		} else {
			$this->error('error');

		}
	}
		// echo "aid:".$aid." + bid:".$bid." + body:".$body;
		// echo $data;
}