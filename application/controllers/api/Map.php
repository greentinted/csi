<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Map extends Base
{
	function __construct(){
		parent::__construct();
	}

	function do(){

		$this->template->set_template('map');
		$list = Arduino_library::getSensingPlacesRawData(true);
		//$row = Arduino_library::getAvgTempAndHumi(1, 1);

		//$title = _trim($this->input->get_post('title'));

		//debug_log($title);

		// $data = [];
		// $data['status'] = $status;
		//$list1 = Arduino_library::getSensingDataRow(true);

		$this->template->render();			

	}

	function list(){
		$this->template->set_template('list');
		$list = Arduino_library::getSensingPlacesRawData(true);
		//$row = Arduino_library::getAvgTempAndHumi(1, 1);

		//$title = _trim($this->input->get_post('title'));

		//debug_log($title);

		// $data = [];
		// $data['status'] = $status;
		//$list1 = Arduino_library::getSensingDataRow(true);
		$this->template->render();		

		}


	function collect(){
		$lid       = _trim($this->input->get_post('lid'));
		$did       = _trim($this->input->get_post('did'));
		$temp      = _trim($this->input->get_post('temp'));
		$humi      = _trim($this->input->get_post('humi'));
		$vibration = _trim($this->input->get_post('vibration'));
		$noise     = _trim($this->input->get_post('noise'));
		$fire = _trim($this->input->get_post('fire'));
		$pm10 = _trim($this->input->get_post('pm10'));
		$pm25 = _trim($this->input->get_post('pm25'));
		
		$co = _trim($this->input->get_post('co'));
		
		$co2 = _trim($this->input->get_post('co2'));
		$formaldehyde = _trim($this->input->get_post('formaldehyde'));

		$nitrogen = _trim($this->input->get_post('nitrogen'));
		$rec_date = _trim($this->input->get_post('rec_date'));

		try{

			$data = [];
			//$data['sl_id'] = $lid;
			$data['sd_code'] = $did;
			
			if($temp)
				$data['temp'] = $temp;
			if($humi)
				$data['humi'] = $humi;
			if($vibration)
				$data['vibration'] = $vibration;
			if($noise)
				$data['noise'] = $noise;
			if($fire)
				$data['fire'] = $fire;
			if($pm10)
				$data['pm10'] = $pm10;
			if($pm25)
				$data['pm25'] = $pm25;
			if($co)
				$data['co'] = $co;
			if($co2)
				$data['co2'] = $co2;
			if($formaldehyde)
				$data['formaldehyde'] = $formaldehyde;

			if($nitrogen)
				$data['nitrogen'] = $nitrogen;
			if($rec_date)
				$data['rec_date'] = $rec_date;


			$data['rdate'] = Date('Y-m-d');

			// debug_var($data);
			// exit;

			$rs = SQL::DB('csi')->query('INSERT INTO tSENSOR_RECORD (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

			if($rs){
				//$this->success_data(['msg'=>'success']);			
				$this->output->set_content_type('text/plain', 'UTF-8');
				echo 'OK';				
			} else {
				echo 'ERROR';
			}
		} catch(Exception $e){
			$this->output->set_content_type('text/plain', 'UTF-8');
			echo 'ERROR';				
				//echo $this->output->get_header('ERROR');				
		}

	}

	function get_sensors(){

		$list = Arduino_library::getAllRecordlist(true);

		$msg = "성공";

		if($list){
			// exit;
			 //debug_log('$list');
			$this->success_data(['msg' => $msg , 'wp_list' => $list , 'url'=>'']);			
		} else {
			//debug_log('error');
			$this->error('There is no workingplace');

		//$this->success_data([]);
		}

	}

	public function ajax_getdata(){

		// step 1: device,  lid 리스트 
		//$row= Arduino_library::getAvgTempAndHumi(1, 1);

		$list = Arduino_library::getDeviceAll();

		$dust_list = Arduino_library::getDeviceAll();

		foreach ($list as $key => &$value) {
			# code...

			$row = Arduino_library::getAvgTempAndHumi($value['lid'], $value['did']);
			  // debug_log($row);
			  // exit;


			$record = Arduino_library::getRecordByLid($value['lid']);

			$value['humi'] = $row['humiditiy'];
			$value['temp'] = $row['tempature'];
			$value['pm25'] = $row['pm25'];
			$value['record'] = $record;

			//if($value['did'] == $row['did'])

			//$value['temp_avg']  = $row[]
		}

		// debug_var($list);
		// exit;

		$msg = "성공";

		//debug_var($list);
		//exit;

		if($list){
			 debug_log($list);
			// exit;
			 //debug_log('$list');
			$this->success_data(['msg' => $msg , 'wp_list' => $list , 'url'=>'']);			
		} else {
			//debug_log('error');
			$this->error('There is no workingplace');

		//$this->success_data([]);
		}
	}

		public function get_sp(){

		$title = _trim($this->input->get_post('title'));

		//debug_log($title);

		// $data = [];
		// $data['status'] = $status;


		$list = Arduino_library::getSensingPlacesRawData(true);
		//$list1 = Arduino_library::getSensingDataRow(true);

		// debug_var($list);
		// exit;
		// if(!$rs){
		// 	throw new Exception("근무편성   수정에 실패하였습니다", 1);
		// }

		$msg = "수정 성공";

		if($list){
			// debug_log($list);
			// debug_log('sdfasdfsadfasd list');
			$this->success_data(['msg' => $msg , 'wp_list' => $list , 'url'=>'']);			
		} else {
			//debug_log('error');
			$this->error('There is no workingplace');
		}
	}

	function ajax_getloc(){
		$html=make_html_overlay_helper('수정');


		$this->success_data(['msg' => $msg , 'html' => $html , 'url'=>'']);

	}
}