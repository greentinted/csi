<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Collect extends Base
{
	function __construct(){
		parent::__construct();
	}

	function test(){

		// $list = ["Count" => 26, "TOTAL" => 65048];
		// $Response=["MID"=>"SMTPAY001m", "COUNT"=> 26, "TOTAL_AMT"=> "65048" ];
		// $list[] = $Response;
		//header('Content-Type: application/json');

		$groupData = array();
		$groupData["Count"] = 26;
		$groupData["TOTAL_AMT"] = 65048;
		$groupData["TOTAL_DEPOSIT_AMT"] = 59992;
		$groupData["TOTAL_GENERAL_FEE"] = 4596;
		$groupData["TOTAL_NO_INT_FEE"] = 0;
		$groupData["TOTAL_GENERAL_VAT"] = 460;
		$groupData["TOTAL_NO_INT_VAT"] = 0;


		$Response = array();
		$Response['MID'] = "SMTPAY001m";
		$Response['COUNT'] = "26";
		$Response['TOTAL_AMT'] = "65048";
		$Response['TOTAL_DEPOSIT_AMT'] = 59992;
		$Response['TOTAL_GENERAL_FEE'] = 4596;
		$Response['TOTAL_NO_INT_FEE'] = 0;
		$Response['TOTAL_GENERAL_VAT'] = 460;
		$Response['TOTAL_NO_INT_VAT'] = 0;

		$groupData["Response"] = $Response;

		$DATA = array();
		$DATA['TID'] = "SMTPAY001m01011906241253434054";
		$DATA['OTID'] = "SMTPAY001m01011906241253434054";
		
		$DATA['TR_DT'] = "20190624";
		$DATA['TR_TM'] = "125412";

		$DATA['MID'] = "SMTPAY001m";
		$DATA['SVC_CD'] = "01";
		$DATA['AMT'] = "1000";
		$DATA['STATE_CD'] = "0";
		$DATA['GENERAL_FEE'] = "29";
		$DATA['NO_INT_FEE'] = "0";
		$DATA['GENERAL_VAT'] = "3";
		$DATA['NO_INT_VAT'] = "0";

		$DATA['FRNCS_FEE'] = "29";
		$DATA['PNT_FEE'] = "0";
		$DATA['VAT'] = "3";
		$DATA['DEPOSIT_AMT'] = "968";

		$DATA['SETTLMNT_DT'] = "20190627";
		$DATA['MOID'] = "1291";
		$DATA['APP_CO'] = "06";
		$DATA['ACQU_CO'] = "06";
		$DATA['APP_NO'] = "17801937";
		$DATA['CURRENCY_CODE'] = "410";


		$groupData["DATA"][] = $DATA;

		//$output =  json_encode($groupData);
 
    	// 출력
    	//echo  urldecode($output);

		$DATA1 = array();
		$DATA1['TID'] = "SMTPAY001m01011906241253434055";
		$DATA1['OTID'] = "SMTPAY001m01011906241253434055";
		
		$DATA1['TR_DT'] = "20190624";
		$DATA1['TR_TM'] = "125412";

		$DATA1['MID'] = "SMTPAY001m";
		$DATA1['SVC_CD'] = "01";
		$DATA1['AMT'] = "1000";
		$DATA1['STATE_CD'] = "0";
		$DATA1['GENERAL_FEE'] = "29";
		$DATA1['NO_INT_FEE'] = "0";
		$DATA1['GENERAL_VAT'] = "3";
		$DATA1['NO_INT_VAT'] = "0";

		$DATA1['FRNCS_FEE'] = "29";
		$DATA1['PNT_FEE'] = "0";
		$DATA1['VAT'] = "3";
		$DATA1['DEPOSIT_AMT'] = "968";

		$DATA1['SETTLMNT_DT'] = "20190627";
		$DATA1['MOID'] = "1291";
		$DATA1['APP_CO'] = "06";
		$DATA1['ACQU_CO'] = "06";
		$DATA1['APP_NO'] = "17801937";
		$DATA1['CURRENCY_CODE'] = "411";


		$groupData["DATA"][] = $DATA1;


		$this->success_data(['list'=>$groupData]);


	}

	function do(){

		$com_name = _trim($this->input->get_post('com_name'));
		$ip = _trim($this->input->get_post('ip'));
		$os = _trim($this->input->get_post('os'));

		$data = [];
		$data['com_name'] = $com_name;
		$data['ip'] = $ip;
		$data['os'] = $os;


		$rs = SQL::DB('parkinglot')->query('INSERT INTO tCOLLECT_INFO (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

		$view_data = [];

		$this->template->write_view('content', '/etc/detail', $view_data , true);
		$this->template->render();


		// $json_data = file_get_contents('php://input');

		// $r_data = json_decode(file_get_contents('php://input'), true);


		// $data = [];

		// $wpid = $r_data['wpid'];
		// $data['mac_address'] = $r_data['mac_address'];
		// $data['windows'] = $r_data['windows'];
		// $data['memsize'] = $r_data['memSize'];
		// $data['cpu_speed'] = $r_data['cpu_speed'];
		// $data['bit'] = $r_data['bit'];
		// $data['computer_spec'] = $json_data;

		// $rs = Work_library::updateWorkPlace($wpid , $data);

		// if($rs){
		// 	$this->success_data(['msg' => 'success', 'is_reload' => true, 'url' => '']);

		// } else {
		// 	$this->error('error');

		// }



	}


		// echo "aid:".$aid." + bid:".$bid." + body:".$body;
		// echo $data;

}