<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Account extends Base
{
	function __construct(){
		parent::__construct();

		$this->load->library("PHPExcel");
	}

	function gets(){
		$objPHPExcel = new PHPExcel();
		$filename = '/home/files/account.xls';


		try {
			$objReader = PHPExcel_IOFactory::createReaderForFile($filename);

			$objReader->setReadDataOnly(true);
			$objExcel = $objReader->load($filename);
			// 첫번째 시트를 선택

			$objExcel->setActiveSheetIndex(0);
			$objWorksheet = $objExcel->getActiveSheet();

			$rowIterator = $objWorksheet->getRowIterator();


			// foreach ($rowIterator as $row) { // 모든 행에 대해서

			// 	$cellIterator = $row->getCellIterator();

			// 	$cellIterator->setIterateOnlyExistingCells(false); 

			// }





			$maxRow = $objWorksheet->getHighestRow();


			for ($i = 2 ; $i <= $maxRow ; $i++) {

				$data = [];

				$data1 = [];						

				$data['email'] = $objWorksheet->getCell('B' . $i)->getValue(); // A열


				$data['birth'] = $objWorksheet->getCell('C' . $i)->getValue(); // B열
	 
				$data['id'] = $objWorksheet->getCell('D' . $i)->getValue(); // C열

				$pass = $objWorksheet->getCell('C' . $i)->getValue(); // 


				if(!$pass) continue;


				$data['name'] = $objWorksheet->getCell('E' . $i)->getValue(); // C열				

				$data['password'] = password_hash($pass ,  PASSWORD_DEFAULT);

				$data['contact_number'] = $objWorksheet->getCell('F' . $i)->getValue(); //

				$data['admin_type'] = $objWorksheet->getCell('G' . $i)->getValue(); // E열

				$data1['user_type'] = $objWorksheet->getCell('H' . $i)->getValue(); // F열

				$data1['fct_code'] = $objWorksheet->getCell('I' . $i)->getValue(); // G열


				$data['address'] = $objWorksheet->getCell('J' . $i)->getValue(); // H열

				$data['gender'] = $objWorksheet->getCell('K' . $i)->getValue(); // H열

				$data1['fc_id'] = $objWorksheet->getCell('L' . $i)->getValue(); // 

				//$dataK = $objWorksheet->getCell('K' . $i)->getValue(); // H열

				// debug_var($data);
				// debug_var($data1);


				$rs = SQL::DB('csi')->query('INSERT INTO tACCOUNT (' . implode(',', array_keys($data)) . ') VALUES(' . db_array_to_values($data) . ')', array_values($data));

				usleep(20000);

				if($rs){
					$temp_aid = SQL::DB('csi')->lastID();


					$data1['aid'] = $temp_aid;
					$rs1 = SQL::DB('csi')->query('INSERT INTO tACCOUNT_ADD (' . implode(',', array_keys($data1)) . ') VALUES(' . db_array_to_values($data1) . ')', array_values($data1));

					if($rs1){

					} else {
						debug_var('error_admin');
						debug_var($data1);
					}

				} else {
					debug_var('error_admin');
					debug_var($data);

				}

			  }

			  debug_var('end');


		} catch(exception $e){
			echo '엑셀파일을 읽는도중 오류가 발생하였습니다.<br/>';		

		}
	}
}
