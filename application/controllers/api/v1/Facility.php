<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Facility extends Base
{
	function __construct(){
		parent::__construct();
	}

	function types(){
		try{
			parent::checkBaseParam();			
			$list = Facility_library::types();

			$result = [];
			$result['status'] = true;

			$result['list'] = $list;
			$this->success_data($result);

		} catch(Exception $e){
			$this->Error($e->getMessage() , $e->getCode() , 'ERR_BASE_PARAMS');	
		}

	}

	function get(){
		try{
			parent::checkBaseParam();

			$f_type =  ExternalUtil::cleanText($this->input->get_post('f_type'));

			if(!$f_type){
				throw new Exception("필수파라미터가 없습니다.  ", 9001);
			}
			


			$list1 = SQL::DB('parkinglot')->fetchAll('SELECT title ,  discount_rate , description FROM tDISCOUNT WHERE p_status = ?' , [true]);


			if($f_type == 'NOPL'){
				$list = Facility_library::getFaciltyInfoByFutCode($f_type);


				foreach ($list as $key => &$value) {
					# code...

					$imgs = Facility_library::getImgs($value['fc_id']);
					
					foreach ($imgs as $key1 => &$value1) {						
						# code...
						$value1['url'] = $_SERVER['HTTP_HOST']. '/' .$value1['path']. '/' . $value1['filename'];
						unset($value1['path']);
						unset($value1['filename']);
						unset($value1['fc_id']);
						unset($value1['ff_id']);

						
					}
					$value['p_type'] = '노외주차장';
					$value['img_list'] = $imgs;
					$value['discount_list'] = $list1;

				}

				$result = [];

				//$data = [];
				$result['list'] = $list;
				// $data['cah_id'] = $cah_id;
				// $data['rt_id'] = $rt_id;
				// $data['rth_id'] = $rth_id;

				// $result['msg'] = '월정기 신규 저장 성공';
				


				//debug_var($list);
				//debug_var($list1);


				$this->success_data($result);


			} else if($f_type == 'NSPL'){
				$list = Facility_library::getFaciltyInfoByFutCode($f_type);

				foreach ($list as $key => &$value) {
					$value['discount_list'] = $list1;
					$value['p_type'] = '노상주차장';
				}	

				$result = [];
				//$data = [];
				$result['list'] = $list;


				$this->success_data($result);

				//debug_var($list);
				//e//xit;
			} else{
				throw new Exception("현재 지원하지 않는 타입입니다 ", 9001);				
			}


		} catch(Exception $e){
			$this->Error($e->getMessage() , $e->getCode() , 'ERR_BASE_PARAMS');	
		}	
	}
}
