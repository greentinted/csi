<?php defined('BASEPATH') OR exit('No direct script access allowed');

require_once APPPATH . 'controllers/Base.php';

class Table extends Base
{
	function __construct(){
		parent::__construct();
	}

	function del(){
      $path = '/db_backup/table delete';
      $php_bin = '/usr/local/php/bin/php';
      $run = $php_bin . " " . getcwd() . "/index.php " . $path . " > " . getcwd() . "/application/logs/rundelLOG.txt 2>" . getcwd() . "/application/logs/runedelERR.txt & ";

		$output = array();
       exec($run , $output , $worked);
	}


	// 한달 전 데이터 삭제함 ..
	function delete(){
		$dir = ASSETSPATH. '/db_backup';
		 
		// 핸들 획득
		$handle  = opendir($dir);
		 
		$files = []	;

		while(($file=readdir($handle)) !== false){
			//debug_var($file);

    		if(is_file($dir . "/" . $file)){
        		$files[] = $file;
			}
		}

		//debug_var($files);

		$now_timestamp = strtotime("Now");

		foreach ($files as $key => $value) {
			# code...

			$str =explode('.' , $value);


			//debug_var($str);
			//exit;
			$month_time =  time();
			$prev_month = strtotime("-1 month", $month_time);

			$file_time = strtotime($str[0]);
			//$file_ = strtotime("1 months ago", $time);
			//$month_before = date("Y-m-d", $str[0]);

			$r = $month_time - $file_time;

			$d_r = ceil($r / (86400 ));


			// 30일 전에 생성된 파일이면 삭제
			// 30일 전에 생성된 파일이면 삭제
			// 30일 전에 생성된 파일이면 삭제
			if($d_r > 30) {
				$file_name = $dir . "/" . $value ;
				if(!unlink($file_name )){
					echo $file_name .'failed' ;
				} else{
					echo $file_name .'success' ;
				}

			}
		}

		closedir($handle);

			

	}

	function do(){

		$db1 ='parking_portal';
		$db2 ='parkinglot';
		$db3 ='parking_standard';
		
		$mysqlUserName = MYSQL_USER;
		$mysqlPassword = MYSQL_PASS;
		$mysqlHostName = MYSQL_HOST;

		$file_name = '';

		// $mysqlExportPath ='Your-desired-filename.sql';
		$_log_path = ASSETSPATH. '/db_backup/';

		$_path = $_log_path;
		if (!is_dir($_path)) {
			@mkdir($_path, 0777, TRUE);
			@chmod($_path, 0777);
		}

		//$_path = $_log_path . date('Y') . "/";
		//if (!is_dir($_path)) {
		//	@mkdir($_path, 0777, TRUE);
		//	@chmod($_path, 0777);
		//}

		//$_path = $_log_path . date('Y') . "/" . date('m') . "/";
		//if (!is_dir($_path)) {
		//	@mkdir($_path, 0777, TRUE);
		//	@chmod($_path, 0777);
		//}

		//$_path = $_log_path . date('Y') . "/" . date('m') . "/" . $file_name ;
		//if (!is_dir($_path)) {
		//	@mkdir($_path, 0777, TRUE);
		//	@chmod($_path, 0777);
		//}


		$_path = $_path . date('Y-m-d') . '.sql';		




		// $command = 'mysqldump --single-transaction  --databases ' . $db1 . ' ' . $db2 . ' ' .$db3. ' -h '  .$mysqlHostName .' -u ' .$mysqlUserName .' -p ' .$mysqlPassword .' > ' .$_path;

		// $command = 'mysqldump --single-transaction –-routines --all-databases -h '  .$mysqlHostName .' -u ' .$mysqlUserName .' -p ' .$mysqlPassword .' > ' .$_path;

		// $command = 'mysqldump --single-transaction -h '  .$mysqlHostName .' -u ' .$mysqlUserName .' -p'  .$mysqlPassword . ' '  . $db1 . ' ' . $db2 . ' ' .$db3. ' > ' .$_path;


		$command = 'mysqldump --column-statistics=0 --single-transaction -h '  .$mysqlHostName .' -u ' .$mysqlUserName .' -p'  .$mysqlPassword . ' ' . ' --databases '. $db1 . ' ' .$db2. ' ' .$db3. ' > ' .$_path;


		// debug_var($_path);
		// debug_var($command);
		// exit;

		$output = array();
		exec($command,$output,$worked);

		// debug_var($worked);
		// debug_var($output);
		switch($worked){
			case 0:
			echo 'The database <b>' . $db1 . ' ' . $db2 . ' ' . $db3 .'</b> was successfully stored in the following path '.getcwd().'/' .$_path .'</b>';
			break;
			case 1:
			echo 'An error occurred when exporting <b>' . $db1 . ' ' . $db2 . ' ' . $db3 .'</b> zu '.getcwd().'/' .$_path .'</b>';
			break;
			case 2:
			echo 'An export error has occurred, please check the following information: <br/><br/><table><tr><td>MySQL Database Name:</td><td><b>' . $db1 . ' ' . $db2 . ' ' . $db3 .'</b></td></tr><tr><td>MySQL User Name:</td><td><b>' .$mysqlUserName .'</b></td></tr><tr><td>MySQL Password:</td><td><b>NOTSHOWN</b></td></tr><tr><td>MySQL Host Name:</td><td><b>' .$mysqlHostName .'</b></td></tr></table>';
			break;
		}


	}

	function backup_table(){
		$tables = array();

		//$result = mysql_query('SHOW TABLES');

		$result = SQL::DB('db')->fetchAll('SHOW TABLES');


		// while($row = mysql_fetch_row($result)){
	 //  		$tables[] = $row[0];

		// }

		$tables = [];
		$return = '';

		foreach ($result as $key => $value) {
			# code...
			
			$tables[] = $value['Tables_in_PARKING_PORTAL'];
			//debug_var($tables);
		}



		foreach ($tables as $key => $table) {
			# code...
			$result = SQL::DB('db')->fetchAll('SELECT * FROM ' .$table);
			$num_fields = count($result);

			$return.= 'DROP TABLE '.$table.';';

			$row2 = SQL::DB('db')->fetchRow('SHOW CREATE TABLE '.$table);
			$return.= "\n\n".$row2[1].";\n\n";


			foreach ($result as $key => &$value) {
				# code...
				 $return.= 'INSERT INTO '.$table.' VALUES(';

				 for($j=0; $j<$num_fields; $j++) {
				 	$value[$j] = addslashes($value[$j]);
				 	$value[$j] = ereg_replace("\n","\\n",$value[$j]);
				 	if (isset($value[$j])) { 
				 		$return.= '"'.$row[$j].'"' ; 
				 	} else { 
				 		$return.= '""'; 
				 	}

				 	if ($j<($num_fields-1)) { $return.= ','; }

				}

				$return.= ");\n";

			}
	 		$return.="\n\n\n";

		}
		$handle = fopen('db-backup-'.time().'-'.(md5(implode(',',$tables))).'.sql','w+');
	  	fwrite($handle,$return);
	  	fclose($handle);
			
	}


}
